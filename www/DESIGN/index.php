<!DOCTYPE html>
<html>
    <? include( BLOCKS_DIR . 'new/common/head.php' );?>
<body>
    <div class="rootWrap">
        <? include( BLOCKS_DIR . 'new/common/header.php' );?>

        <main>

            <? include( BLOCKS_DIR . 'new/main/slider.php' );?>

            <? include( BLOCKS_DIR . 'new/main/catalog.php' );?>

            <? include( BLOCKS_DIR . 'new/main/news.php' );?>

            <div class="centerWrap centerFix">
                <div class="seoText">

                    <h1><?= $_TITLEH1 ?></h1>

                    <div class="js-wrapper hidden-text">

                        <?= getTextMod(13) ?>
                    </div>

                    <a class="moar js-more" href="#">Читать подробнее</a>
                </div>
            </div>
        </main>

        <? include( BLOCKS_DIR . 'new/common/footer.php' );?>
    </div>
    <?= showJs() ?>
</body>
</html>




