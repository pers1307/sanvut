<?php
UseModule("ajax");
$JsHttpRequest = new JsHttpRequest("utf-8");
ob_clean();

// ###############################################################################################
// Сюда ВАШ КОД обработки аякс запросов
// ###############################################################################################

/* $out['id объекта'] = array('контент','режим записи')
режимы записи:
    rewrite (по умолчанию) - замена содержимого объекта
    append - добавление в конец
    insert - добавление в начало
*/

$out = array();
$action = getParam(0);
$cont = '';
$host = $_SERVER['HTTP_HOST'];

switch( $action )
{
    case 'order':
	{
		$error = false;
		if( isset( $_POST['name'] ) && !$_POST['name'] )
		{
			if( !is_valid_string( $_POST['fio'] ) )
			{
				$error = true;
				$cont .= '$( \'.window input[name=fio]\' ).addClass( \'f-error\' );
					$( \'.window input[name=fio]\' ).parent().append( \'<div class="error">Неверно заполнено поле</div>\' );';
			}

			if( !$_POST['contact'] )
			{
				$error = true;
				$cont .= '$( \'.window input[name=contact]\' ).addClass( \'f-error\' );
					$( \'.window input[name=contact]\' ).parent().append( \'<div class="error">Неверно заполнено поле</div>\' );';
			}



			if( !$error )
			{
				$mailers = mailer_forsendByCat( 1 );
				if( $mailers )
				{
					$text = str_replace( '\r', '' , safe( $_POST['text'] ) );
					$text = str_replace( '\n', '<br />' , $text );

					$mes = '<p>'.$text.'</p>
						ФИО: <b>'.safe( $_POST['fio'] ).'</b><br/>
						Контакты: <b>'.safe( $_POST['contact'] ).'</b>
						<p>'.$text.'</p>';

					$SendMail = new SendMail();
					$SendMail->init();
					$SendMail->setTo( $mailers );
					$SendMail->setFrom( "noreply@".$host, "сообщение с сайта ".$host );
					$SendMail->setSubject( "Сообщение с сайта ".$host." / обратная связь" );
					$SendMail->setMessage( $mes );
					$SendMail->send();
				}

				$cont = '$( location ).attr( "href", "/thanks/" );';
			}

			$out['.overlay'] = array( '<script type="text/javascript">'.$cont.'</script>', 'append' );
		}

		break;
	}




	case 'series':
	{
		$_SESSION['cat']['view'] = $s = (int)str_replace( 's%20s', '', getParam(1) );
		auth_StoreSession();

		$onpage = 20;

		$art = $DB->getRow( 'SELECT id, parent, code FROM '.PRFX.'catalog_articles WHERE id="'.(int)$_SESSION['cat']['id'].'" LIMIT 1' );

		$sql = $s ? ' AND type="'.$s.'" ' : '';
		$items = array();
		$count = $DB->getOne( 'SELECT COUNT(*) FROM '.PRFX.'catalog_items WHERE parent="'.(int)$art['id'].'" '.$sql.' AND active' );
		if( $count )
		{
			$items = $DB->getAll( 'SELECT id, code, title, type, img, shema1, shema2, photo, width, height, deep, text, mark FROM '.PRFX.'catalog_items WHERE parent="'.(int)$art['id'].'" '.$sql.' AND active ORDER BY `order`, id LIMIT 0, '.$onpage );
		}
		else
		{
			$sql = $s ? ' AND i.type="'.(int)$s.'" ' : '';
			$count = $DB->getOne( 'SELECT COUNT(*) FROM '.PRFX.'catalog_items i, '.PRFX.'catalog_articles a WHERE a.parent="'.(int)$art['id'].'" '.$sql.' AND i.active AND i.parent=a.id' );
			$items = $DB->getAll( 'SELECT i.code, i.title, i.img, a.code pcode, i.shema1, i.shema2, i.type, i.photo, i.width, i.height, i.deep, i.text, i.mark FROM '.PRFX.'catalog_items i, '.PRFX.'catalog_articles a WHERE a.parent="'.(int)$art['id'].'" '.$sql.' AND i.active AND i.parent=a.id ORDER BY i.parent, i.`order`, i.id LIMIT 0, '.$onpage );
		}


		$out['.catalog'] = array( echoCatItems( $items, $art, $count, 0, $onpage ) );
		break;
	}
}

// ###############################################################################################
// Сюда ВАШ КОД обработки аякс запросов
// ###############################################################################################

$_RESULT = array('action' => $action, 'content' => $out);

die();
?>