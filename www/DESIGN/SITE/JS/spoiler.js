$(document).ready(function()
{
    $(document).on({
        click:function(event){

            if ($('.js-wrapper').hasClass('hidden-text')) {

                $('.js-wrapper').removeClass('hidden-text');
                $(this).text('Скрыть');
            } else {

                $('.js-wrapper').addClass('hidden-text');
                $(this).text('Читать подробнее');
            }

            return false;
        }
    },'.js-more');

    $('.modal-window-close').click(function(){
        $('.modal-window-wrap').addClass('hidden_text');
    });

    $('#callform').on('submit', function(){

        getJson($(this).attr('action'), $(this).serialize());
        return false;
    });
   
   
    $('.spoller_handler2').click(function(e){
        e.preventDefault();
        var $this = $(this),
            $parent = $this.closest('.spoller_container2'),
            $spoller_container = $('.spoller_container2');

        if(!$parent.hasClass('active'))
        {
            $spoller_container.removeClass('active').find('.spoller2').slideUp();
            $parent.addClass('active').find('.spoller2').slideDown();
        }
    });
  
    $('.price_form form button').submit(function(){
        if ($('.labelBlock input').val()==""){
           $('.labelBlock input').addClass('error')
           $('.errorText').removeClass('hidden_text');  
        }
    });
  
   $('.gl_slide').bxSlider({
        mode: 'horizontal',
        minSlides: 1,
        maxSlides: 1,
        moveSlides: 1,
        pagerCustom: '#bx-pager',
        auto: true,
        pause: 6000,
        autoStart: true
    });
  
    $('.spoller_handler').click(function(e){
        e.preventDefault();
        var $this = $(this),
            $parent = $this.closest('.spoller_container'),
            $spoller_container = $('.spoller_container');

        if(!$parent.hasClass('active'))
        {
            $spoller_container.removeClass('active').find('.spoller').slideUp();
            $parent.addClass('active').find('.spoller').slideDown();
        }
    });

    $('.moar').click(function(event){
        event.preventDefault();

        if($('.js-moreText').hasClass('hidden_text'))
        {
            $('.js-moreText').removeClass('hidden_text').slideDown();
            $('.moar').text('Скрыть');
        }
        else
        {
            $('.js-moreText').addClass('hidden_text').slideUp();
            $('.moar').text('Читать подробнее');
        }


    });

    var maxHeight = 0;

    $('.wrap > div').each(function() {

        var thisHeightString = $(this).css('height');

        var thisHeight = Number(thisHeightString.replace('px', ''));

        if (thisHeight > maxHeight) {

            maxHeight = thisHeight;
        }
    });

    $('.wrap > div').each(function() {

        $(this).css('height', maxHeight + 'px');
    });

    if( $( '.series .s' ).length )
    {
        $( '.series .s' ).click( function()
        {
            $( '.series .s' ).removeClass( 'act' );

            doLoad( '', '/ajax/series/'+ $( this ).attr( 'class' ) +'/' );

            $( this ).addClass( 'act' );
        });
    }
});