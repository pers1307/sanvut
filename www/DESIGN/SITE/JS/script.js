$(document).ready(function(){

	TMenuRound('.tmenu .act', '8px');
	TMenuRound('div.index div.cat2main p', '12px');
	TMenuRound('.news2main', '10px');
	
	$('.form textarea').css({ 'resize': 'none' }); 
	$('.search form input').css({ 'box-shadow': '0px 1px 1px #cccccc inset' }); 
	$('.search form input.submit').css({ 'box-shadow': 'none' }); 
	
	
	
		
	function TMenuRound(obj,pix){	
		
		if (navigator.userAgent.indexOf("Opera") != -1 && $(obj).attr('src')){ 
			if (parseInt(pix)>0)
				$(obj).each(function(){
					if (!$(this).hasClass('np'))
						$(this).imgr({size:"0px",color:"",radius:pix});
				});
		}else{
			$(obj).css({
				'-webkit-border-radius': pix,
				'-moz-border-radius': pix,
				'border-radius': pix
			});
		}
		if (navigator.userAgent.indexOf("MSIE") != -1) { 
			$(obj).css({ 'behavior':'url(/DESIGN/SITE/JS/PIE.htc)'});
			if ($(obj).css('position')=='static')
				$(obj).css({ 'position': 'relative', 'z-index': 1 });
			if (obj=='.teaser .sales')
				$(obj).css({ 'position': 'absolute'});
		}
	}
	
	$('div.top .tmenu li').hover(
        function() {
        	if ($(this).children('div').text()!='')
        		TMenuRound($(this).children('div').toggleClass('act'), '8px 8px 0px 0px');
        		TMenuRound($(this).children('div').children('div'), '0px 8px 8px 8px');
        },
        function() {
        	if ($(this).children('div').text()!='')
        		$(this).children('div').toggleClass('act');
        }
	);


	
	$('div.top .contact p u a').click(function(){
		$('#contactmap').css({ 'box-shadow': '0px 2px 3px #c2c1c1' })/*.toggle()*/; 
		if ($('#contactmap').css('visibility')!='visible')
			$('#contactmap').css('visibility','visible'); 
		else 
			$('#contactmap').css('visibility','hidden'); 
	});
	
	$('#contactmap .close').click(function(){
		$('#contactmap').css({'visibility':'hidden' }); 
//		$('#contactmap').toggle(); 
	});
	
	$('div.cat2main p u').click(function(){
		$(this).parent().toggleClass('act');
		$(this).parent().parent().children('div').toggleClass('act');
		TMenuRound('div.index div.cat2main p', '12px');
		TMenuRound('div.index div.cat2main p.act', '12px 12px 0px 0px');
		TMenuRound('div.index div.cat2main div.act', '0px 0px 12px 12px');
	});
	
	$('select').each(function(){
		var newselect = jQuery('<div>').addClass('select');
		var newoptiongroup = jQuery('<div>');
		var newselected = '';
		var newvalue = '';
		var name = $(this).attr('name');
		$(this).children('option').each(function(){
			var newoption = jQuery('<p>').text($(this).text().replace('*','')).attr('id',name+$(this).attr('value'));
			$(newoptiongroup).append($(newoption));
			if ($(this).attr('selected')){
				newselected = $(this).text();
				newvalue = $(this).attr('value');
			}
		});
		var newinput = jQuery('<input>').attr('type','hidden').attr('name',name).attr('value',newvalue);
		var img = jQuery('<img>').attr('src','img/select_fon.png').attr('alt','');
		var newtext = jQuery('<span>').addClass($(this).attr('class')).html(newselected.replace('*','<i>*</i>'));
		$(this).after($(newselect).append($(newinput)).append($(newtext).append($(img))).append($(newoptiongroup)));
		if (navigator.userAgent.indexOf("MSIE")==-1) $(newoptiongroup).width($(newselect).width()+10);
		TMenuRound(newtext, '10px');
		$(this).remove();
	});

//	var selectfon = jQuery('<div>');
//	$('body').prepend($(selectfon).attr('id','nsb'));

	$('#nsb').live('click', function(){
		$(this).css({ display: 'none'});
		$('div.select div').removeClass('sel');
	});

	$('div.select p').live('hover',function(ev){
        if (ev.type == 'mouseenter') {
	       	$(this).addClass('hover');
        }
        if (ev.type == 'mouseleave') {
            $(this).removeClass('hover');
        }
	});

	$('div.select span').live('click', function(){
		InputSelect(this);
	});
	
	function InputSelect(obj){
		var SelBlock = $(obj).parent().children('div');
		if ($(SelBlock).hasClass('sel')){
			$(SelBlock).removeClass('sel');
			$('#nsb').css({ display: 'none'});
		} else {
			$('#nsb').css({ display: 'block'});
			$('div.select div').removeClass('sel');
			$(SelBlock).addClass('sel');
		}
	}

	$('div.select p').live('click', function(){		
		var thisInput = $(this).parent().parent().children('input');
		var value = $(this).attr('id').replace($(thisInput).attr('name'),'');
		$(thisInput).val(value);
		var img = jQuery('<img>').attr('src','img/select_fon.png').attr('alt','');
		$(this).parent().parent().children('span').text($(this).text()).removeClass('error').append($(img));
		$(this).parent().removeClass('sel');
		$('#nsb').css({ display: 'none'});
	});

});

function objHide(id)
{
	$('#'+id).css({display:'none'});
}

