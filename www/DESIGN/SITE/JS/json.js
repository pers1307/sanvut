function getJson( url, send, showElement, hideElement ){
	
	/**
	showElement : Элемент для отображения во время обмена
	hideElement : Элемент для скрытия во время обмена
	**/

	$.ajax({
		
		url: url,
		data: send,
		timeout: 5000,
		type: 'POST',
		
		beforeSend: function(){
            if( hideElement !== undefined )
				$( hideElement ).hide();
            
			if( showElement !== undefined )
				$( showElement ).show();
		},
		
		success: function( data, textStatus, jqXHR ){
            if(typeof(data.callback) != 'undefined')
			{
				var callback = data.callback; delete data.callback;
			}
            else 
                var callback = [];
			
			$.each(data, function( elementId, item ){

				var selector = '#' + elementId;

				switch( item[1] == undefined ? 'rewrite' : item[1] ){
					
					case 'rewrite':
						$( selector ).html( item[0] );
					break;
					
					case 'before':
						$( selector ).prepend( item[0] );
					break;

					case 'after':
						$( selector ).append( item[0] );
					break;
				
				}
			
			});

			$.each(callback, function(key, item) {
				eval(item);
			});
		},
		
		complete: function(){
            if( showElement !== undefined )
				$( showElement ).hide();
            
			if( hideElement !== undefined )
				$( hideElement ).show();
		},

		error: function( request, error ){

			var errorElement = '#ajaxError';
			var errorStyle = 'position:fixed;top:0;padding:10px;background:red;opacity:0.4;font-size:12px;color:#fff';

			$('body').after('<div style="' + errorStyle + '"><a href="#" onclick="$(this).parent().remove();return false" style="float:right;margin:0 0 0 10px">[X]</a>AJAX [' + url + ']: ' + error + '</div>');

		}
		
	});
	
}