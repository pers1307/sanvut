$().ready( function()
{
    /*map*/
    $('.map .point').click(hidepointtext);

	var index = 0;
	var images = $( "#imgs img" );
	for( i=0; i<images.length; i++ )
	{
		$( images[i] ).addClass( "image-" +i );
	}
	
	show( index );
	setInterval( sift, 5000 );
	
	function sift()
	{
		if( index < ( images.length-1 ) )
		{
			index += 1;
		}
		else
		{
			index = 0
		}
		
		show( index );
	}
	
	function show( num )
	{
		$( images ).fadeOut();
		$( ".image-"+num ).stop().fadeIn();
	}
	
	
	
	
	if( $( '.overlay' ).length )
	{
		$( '.overlay' ).click( function()
		{
			$( this ).fadeOut();
			$( '.window' ).fadeOut();
			
			if( $( '.window' ).length )
			{
				$( '.window .field' ).val( '' );
			}
		});
		
		$( '.window .close' ).click( function()
		{
			$( '.overlay' ).click();
		});
		
		
		$( '.btn_order' ).click( function()
		{
			$( '.overlay' ).css({ opacity:0.3 });
			$( '.overlay' ).fadeIn();
			$( '.window' ).fadeIn();
		});
		
		
		$( window ).keyup( function( y )
		{
			if( y.keyCode == 13 )
			{
				$( '.window btn-send' ).click();
			}
		});
		
		
		$( document ).keyup( function( d )
		{
			if( d.keyCode == 27 )
			{
				$( '.overlay' ).click();
			}
		});
		
		
		
		
		$( '.window .btn-send' ).click( function()
		{
			$( '.window .error' ).remove();
			$( '.window .f-error' ).removeClass( 'f-error' );
			
			if( !$( '.window input[name=contact]' ).val() )
			{
				$( '.window input[name=contact]' ).addClass( 'f-error' );
				$( '.window input[name=contact]' ).parent().append( '<div class="error">Неверно заполнено поле</div>' );
			}
			else
			{
				doLoad( $( '.window form' )[0], '/ajax/order/' );
			}
		});
		
		
		
		
		
		if( $( '.series .s' ).length )
		{
			$( '.series .s' ).click( function()
			{
				$( '.series .s' ).removeClass( 'act' );
				
				doLoad( '', '/ajax/series/'+ $( this ).attr( 'class' ) +'/' );
				
				$( this ).addClass( 'act' );
			});
		}
	}
	
	
	
	if( $( '.left-col .title' ).length )
	{
		$( '.left-col .title .item' ).click( function()
		{
			if( $( this ).attr( 'class' ) != 'item act' )
			{
				$( '.left-col .sub' ).slideUp();
				$( '.left-col .title .act' ).removeClass( 'act' );
				
				$( this ).addClass( 'act' );
				$( this ).parent().parent().children( '.sub' ).slideDown();
				
				return false;
			}
		});
	}
	
	
	
	if( $( '.btn_order' ).length )
	{
		$( window ).scroll( function()
		{
			$( '.btn_order' ).css({ top:( $( window ).scrollTop() + ( $( window ).height() / 2 ) - 50 )+'px' });
		});
	}
});




 
/*map*/    
function hidepointtext()
    {
    var timer=300;
    var textid=this.id;
    var visible=$('.map .maptext.visible');
    if (!visible.hasClass(textid))
    //var foo=function(x){$('.map .maptext.'+x).addClass('visible').slideDown(300);};{return function() { $('.map .maptext.'+x).addClass('visible').slideDown(300);} }(textid)
    {visible.slideUp(timer);
    setTimeout(function(){return function(x){showpointtext(x)}(textid)},timer);
    }
    }
    
function showpointtext(x)
    {
    $('.map .maptext.visible').hide().removeClass('visible');
    $('.map .maptext.'+x).addClass('visible').slideDown(300);
    }

/*//map*/