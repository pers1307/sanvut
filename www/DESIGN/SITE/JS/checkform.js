/*
//$(window).load(function() {

	$('.form textarea').live('keyup', function(){
		ErrorCheck($(this));
	});
	$('.form input').live('keyup', function(){
		if ($(this).attr('type')=='text'){
			ErrorCheck($(this));
		} else if ($(this).attr('type')=='password'){
			var passw0 = $('.form input[type=password]:eq(0)');//var passw0 = $('.form input.passw')[0];
			var passw1 = $('.form input[type=password]:eq(1)');//var passw1 = $('.form input.passw')[1];
			if ($(passw0).val() && $(passw1).val() && $(passw1).val().length >= $(passw0).val().length && $(passw0).val() != $(passw1).val()){
				ErrorDisplay(passw1,'Пароли не совпадают');
			}else{
				if ($(passw0).val()) ErrorDisplay(passw0,'');
				if ($(passw1).val()) ErrorDisplay(passw1,'');
			}
		}
	});
	
	$('.form input[type=text]').live('blur', function(){
		ErrorCheck($(this));
	});

	$('.form textarea').live('blur', function(){
		ErrorCheck($(this));
	});

	$('.form input[name=email]').live('blur', function(){
		ErrorCheckEmail($(this));
	});

	$('.form form').live('submit',function(){
		
		if ($(this).hasClass('fb')){
	    	if (CheckSubmit($(this)))
				$.post('/ajax/sendform/feedback/', $(this.elements) , function(data) {
					$('#cboxLoadedContent').html(data);
					$('form.fb h1').css({ 'margin-top': '-5px' });
				});
			return false;
		}else if ($(this).hasClass('cb')){
	    	if (CheckSubmit($(this)))
				$.post('/ajax/sendform/callback/', $(this.elements) , function(data) {
					$('.phone .form').html(data).css({display:'block'});
				});
			return false;
		}else 
			return CheckSubmit($(this));
	});

	function ErrorDisplay(obj,data){
		var em = ($(obj).parent('td').length ? ($(obj).parent().children('div').text() ? $(obj).parent().children('div') : jQuery('<div>').append(jQuery('<p>'))) : ($(obj).parent().next().hasClass('error')?$(obj).parent().next():''));
		if ($(em).length){
			if (data){
				if ($(em).children('p').length){
					$(em).children('p').text(data).css({'-webkit-border-radius': '5px','-moz-border-radius': '5px','border-radius': '7px'});
					$(em).css('margin-left',($(obj).width() + ($(obj).parent().parent('tr').hasClass('textarea')?11:3)));
					$(obj).parent().prepend($(em));
					$('.form tr').removeClass('error');
					$(obj).addClass('error');
					$(obj).parent().parent('tr').addClass('error');
				}else{
					$(obj).addClass('error').attr('autocomplete','off');
					$(em).children('em').parent().css({'-webkit-border-radius': '5px','-moz-border-radius': '5px','border-radius': '7px'});
					$(em).children('em').text(data);
					$(em).children('em').parent().addClass('act');
					$(em).css('margin-left',($(obj).parent().children('span').width() + 0));
					$('#cboxWrapper').height('auto');
					$('#cboxLoadedContent').height('auto');
					$('#cboxContent').height('auto');
					$('#colorbox').height($('#cboxContent').height()).css({top: (($('#cboxOverlay').height() - $('#cboxContent').height())/2 - 28) +'px' });

				}
				return ActButton($(obj));
//				return false;
			}else{
				if ($(em).children('p').length){
					$(obj).parent().parent('tr').removeClass('error');
					$(obj).removeClass('error');
					$(em).remove();
				}else{
					$(obj).removeClass('error').attr('autocomplete','auto');
					$(em).children('em').text('');
					$(em).removeClass('act');
				}
				return ActButton($(obj));
//				return true;
			}
		}
	}
	
	function ErrorCheck(obj){
		if ($(obj).hasClass('preg')){
			$.post('/ajax/check/',$(obj), function(data) {
				return ErrorDisplay($(obj),data);
			});
		}else
			return true;
	}

	function ErrorCheckEmail(obj){
		if ($(obj).hasClass('preg')){
			$.post('/ajax/checkemail/',$(obj), function(data) {
				return ErrorDisplay($(obj),data);
			});
		}else
			return true;
	}

	function CheckSubmit(myform){
		
		var res = 0;

		$(myform).find('textarea').each(function(){
			if ($(this).hasClass('preg') && !$(this)[0].value){
				var ans = ErrorDisplay(this,'Поле не заполнено');
				if (ans==false) res = 1;
			}
	    });
		$(myform).find('input[type=text]').each(function(){
			if ($(this).hasClass('preg') && !$(this).val()){
				var ans = ErrorDisplay(this,'Поле не заполнено');
				if (ans==false) res = 1;
			} else {
				if ($(this).attr('name')=='email'){
    				var ans = ErrorCheckEmail($(this));
    				if (ans==false) res = 1;
				} else {
    				var ans = ErrorCheck($(this));
    				if (ans==false) res = 1;
				}
			}
	    });

	    
		$(myform).find('input[type=password]').each(function(){
			var passw0 = $(myform).find('input[type=password]:eq(0)');
			var passw1 = $(myform).find('input[type=password]:eq(1)');
			if (!$(this).val()){
				var ans = ErrorDisplay(this,'Поле не заполнено');
				if (!ans) res = 1;
			} else if ($(passw0).val() && $(passw1).val() && $(passw0).val() != $(passw1).val()){
				var ans = ErrorDisplay(passw1,'Пароли не совпадают');
				if (!ans) res = 1;
			}else{
				var ans = ErrorDisplay(this,'');
				if (!ans) res = 1;
			}
	    });
//	    $(mform).delay(1000);
	    if (res>0) return false;
	    else return true;
//	    if ($(mform).find('input.error').length>0) return false;
	}
	
	$('.form label').live('click',function(){
		$(this).parent().children('input').focus();
		$(this).parent().children('textarea').focus();
		$(this).remove();
	});
	
	$('.form input').live('focus',function(){
		$(this).parent().children('label').remove();
	});

	$('.form textarea').live('focus',function(){
		$(this).parent().children('label').remove();
	});

	function ActButton(obj){	
		var err = 0;
		$(obj).closest(".form").find('input.preg').each(function(){
			if ($(this).hasClass('error') || !$(this).val())
				err = 1;
		});
		$(obj).closest(".form").find('textarea.preg').each(function(){
			if ($(this).hasClass('error') || !$(this).val())
				err = 1;
		});
		
		if (err==0){
			$(obj).closest(".form").find('input.submit').addClass('act');
			return true;
//			$(obj).closest(".form").find('input.submit').addClass('act').live('click',function(){ alert(111); return true; });
		} else {
			$(obj).closest('.form').find('input.submit').removeClass('act');
			return false;
//			$(obj).closest('.form').find('input.submit').removeClass('act').live('click',function(){ alert(222); return false; });
		}
	}
	
	$('.form input.submit').live('click',function(){ return ActButton(this); });	
	
    function toggleAll(frty,mode) {
        var allCheckboxes = $(frty+'input:checkbox:enabled');
        var notChecked = allCheckboxes.not(':checked');
        allCheckboxes.removeAttr('checked');
        notChecked.attr('checked', 'checked');
    }

//	ActButton(".form input.preg");
//});
*/