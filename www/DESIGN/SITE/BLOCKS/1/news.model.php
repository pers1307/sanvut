<?

/**
 * Модель для вывода ленточного модуля
 *
 * Этот ПРИМЕР написан для демонстрации внедрения "Символьного кода" в ленточном модуле.
 * @author Philipp Nehaev <phil@mediasite.ru>
 * @version 20110813
 */
class Lenta
	{
	
	public
		$table = 'mp_news',
		$pathId = 0,
		$path = '/', /** Путь до страницы с лентой */
		$onPage = 20, /** Сколько объектов на страницу */
		$page = 0, /** Номер текущей страницы */
		$order = '', /** SQL код для сортировки */
		$keyPrimary = 'id',
		$keyUnique = 'code';

	private
		$key,
		$itemKey,
		$db,
		$cachePagination = false;
	
	/**
	 * Передаем в конструктор объект DB_Engine
	 */
	public function __construct ( DB_Engine $DB )
		{

		$this->db = $DB;

		}

	/**
	 * Вернет имя поля по которому сделать выборку
	 * @return string
	 */
	private function getKey ()
		{

		if ( preg_match('/^[0-9]+$/', $this->itemKey) )
			{
			
			return $this->keyPrimary;
			
			}
		
		if ( preg_match('/^[a-z0-9_]+$/i', $this->itemKey) )
			{
			
			return $this->keyUnique;
			
			}
		
		return $this->keyPrimary;
		
		}
	
	/**
	 * Метод для записи приватного свойства с ключом для выборки.
	 * Сделано так, что-бы при указании ключа сразу определялось имя поля для выборки.
	 * @retrun string
	 */
	public function setItemKey ( $value )
		{
		
		$this->itemKey = $value;
		$this->key = $this->getKey();
		
		return $this->key;
		
		}
		
	/**
	 * @return string
	 */
	public function getFieldKey ()
		{
		
		return $this->key;
		
		}
	
	/**
	 * @return array
	 * @return boolean
	 */
	public function getItems ( $count = false, $notIdIn = array() )
		{

		$order = $count ? '' : $this->order;
		$limit = $count ? '' : ' LIMIT ' . $this->page * $this->onPage . ', ' . $this->onPage;
		$notIdIn = count($notIdIn) ? ' AND `id` NOT IN (' . implode( ',', $notIdIn ) . ') ' : '';

		$query = 'SELECT * FROM `' . $this->table . '` WHERE `path_id` = ' . $this->pathId . $notIdIn . $order . $limit;

		$items = $this->db->getAll( $query );

		if ( $count )
			{

			return count($items);

			}

		if ( count($items) )
			{
				
			return $items;

			}

		return false;
		
		}
		
	/**
	 * @return array
	 * @return boolean
	 */
	public function getItem ()
		{

		$item = $this->db->getRow( $query = 'SELECT * FROM `' . $this->table . '` WHERE `path_id` = ' . $this->pathId . ' AND `' . $this->getFieldKey() . '` = "' . $this->db->pre($this->itemKey) . '"' );
		
		if ( count($item) )
			{
		
			$this->redirect($this->getFieldKey(), $item);
			
			return $item;
		
			}
		
		return false;
		
		}
	
	/**
	 * Возвращает номера доступных страничек в виде массива
	 * @return string
	 */
	public function getPagination ()
		{
		
		if ( $this->cachePagination === false )
			{
				
			$this->cachePagination = array();

			$pagesNum = ceil( $this->getItems( true ) / $this->onPage );
			
			for ( $i=0; $i < $pagesNum; $i++ )
				{

				$this->cachePagination[$i]['title'] = $i+1;
				$this->cachePagination[$i]['active'] = $this->page == $i;

				}

			}

		return $this->cachePagination;

		}

	/**
	 * Если клиент делает выборку по первичному ключу (id), проверяем наличие символьного кода
	 * и если он есть редиректим по правильному урлу :)
	 * @return boolean
	 */
	private function redirect ($originKey, $item)
		{
		
		if ( $originKey == $this->keyPrimary && $this->setItemKey( $item[$this->keyUnique] ) !== $this->keyPrimary )
			{
			
			go('/' . $this->path . '/' . $item[$this->keyUnique] . '/');

			return true;
			
			}
		
		return false;
		
		}
	
	}

?>