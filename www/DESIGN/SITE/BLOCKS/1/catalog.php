<?

require_once('catalog.model.php');

$Catalog = new Catalog();
$Catalog->pathId = $page['path_id'];
$Catalog->path = $page['path'];
$tree = $Catalog->getTree();
$Catalog->checkRedirect($_VARS);
$Catalog->checkTree($tree, $_VARS);

?>

<table width="100%" border="1"><tr valign="top">

<td width="20%">

	<?	
	
	if ( count($tree) )
		{
		
		treeHtml($tree);
		
		}
	else
		{
		
		?>Разделов нет<?
		
		}
	
	?>
	
</td>
<td width="80%">

	<?
	
	if ( count($_VARS) )
		{
		
		$art = $Catalog->getArticle($_VARS);

		if ( $Catalog->isItem($_VARS[count($_VARS)-1]) )
			{
			
			if ( $item = $Catalog->getItem($art, $_VARS) )
				{
				
				$breadCrumbs->addLevel('', $item['title']);
				
				?><h1><?=$item['title']?></h1><?
				echo $item['comment'];
			
				}
			else
				{
				
				page404();
				
				}
			
			}
		else
			{
				
			?><h1><?=$art['name']?></h1><?
			echo $art['text'];

			if ( $items = $Catalog->getItems($art['id']) )
				{
				
				?><ul><?
				
				foreach ( $items as $item )
					{

					$Catalog->setObjKey( $item[$Catalog->keyUnique] );
					
					?><li><a href="<?=$item[ $Catalog->getFieldKey() ]?><?=$Catalog->itemExt?>/"><?=$item['title']?></a></li><?
					
					}
				
				?></ul><?
				
				}

			}
		
		}
	
	?>
	
</td>

</tr></table>

<?

/**
 * Функция для построения HTML-дерева разделов каталога.
 * В переменную tree передается дерево в виде массива, все дочернии элементы должны находится в ветке childs
 * @return string
 */
function treeHtml( $tree, $level = 0, $url = array(), $bc = array() )
	{
	
	global $Catalog, $_VARS, $breadCrumbs;
	
	$level++;
	
	?><ul><?
	
	foreach ( $tree as $t )
		{
		
		$url[$level] = $t[ $Catalog->setObjKey( $t[$Catalog->keyUnique] ) ];
		$bc[$level]['url'] = '/' . $Catalog->path . '/' . implode('/', $url) . '/';
		$bc[$level]['name'] = $t['name'];
		$__VARS = $_VARS;
		
		if ( count($__VARS) && $Catalog->isItem($__VARS[count($__VARS)-1]) )
			{
			
			unset($__VARS[count($__VARS)-1]);
			
			}
		
		if ( implode('/', $__VARS) == implode('/', $url) )
			{
			
			/** Строим хлебную крошку */
			foreach ( $bc as $crumb )
				{

				$breadCrumbs->addLevel($crumb['url'], $crumb['name']);
				
				}
			
			?>
			<li><?=$t['name']?></li>
			<?
			
			}
		else
			{
			
			?>
			<li><a href="/<?=$Catalog->path?>/<?=implode('/', $url)?>/"><?=$t['name']?></a></li>
			<?
			
			}
		
		if ( count($t['childs']) )
			{
			
			treeHtml( $t['childs'], $level, $url, $bc );
			
			}
		
		}
	
	?></ul><?
	
	}
	
?>
