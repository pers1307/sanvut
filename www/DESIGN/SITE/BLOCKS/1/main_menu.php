<?php
function getParent($id){
	global $DB;
	return($DB->GetOne("SELECT `parent` FROM `mp_www` WHERE `path_id`=".$id));
}

function submenuactive($id){
	global $page;
	
	$cid=$page['path_id'];
	
	if($cid==$id || getParent($cid)==$id || getParent(getParent($cid))==$id)return true;

	return false;
}

function echo_sub_menu2($par,$only_count=false){
	global $DB,$page;
	$menu2='';
	$links = $DB->getAll('SELECT * FROM `mp_www` WHERE `parent`='.$par.' AND `visible`=1 AND `path_id`!=1436 AND `www_type`=2 ORDER BY `order`');

	if($only_count)return (sizeof($links));

	if(sizeof($links)){
	
		foreach ($links as $num => $data){

			if($page['path_id']==$data['path_id']) $menu2.= '<p><a href="/'.$data['path'].'/">'.$data['title_menu'].'</a></p>';
			else $menu2.='<p><a href="/'.$data['path'].'/">'.$data['title_menu'].'</a></p>';

		}
		
	}
	return $menu2;
}


function echo_sub_menu($par,$only_count=false){
    global $DB,$page;
    
    $links = $DB->getAll('SELECT * FROM `mp_www` WHERE `parent`='.$par.' AND `visible`=1 AND `path_id`!=1436 AND `www_type`=2 ORDER BY `order`');
    
    if($only_count)return (sizeof($links));
    $i=0;
    $li_colors=array('#D66CA0','#0970D9','#0970D9','#098E9F','#098E9F','#71CBD5','#71CBD5');
    if(sizeof($links)){
    	$menu='<ul class="tmenu">';
    	
		foreach ($links as $num => $data){
            
			if($page['path_id']==$data['path_id']) $menu.= '<li class="act">'.$data['title_menu'].'';
			else $menu.='<li><a href="/'.$data['path'].'/" style="color:'.$li_colors[$i].';">'.$data['title_menu'].'</a>';
            
			$links2 = $DB->getAll('SELECT * FROM `mp_www` WHERE `parent`='.$data['path_id'].' AND `visible`=1 AND `path_id`!=1436 AND `www_type`=2 ORDER BY `order`');
			

			
			if(sizeof($links2)){
            //if(echo_sub_menu($data['path_id'],true)>0 && submenuactive($data['path_id'])){
            	$menu.='<div><a href="/'.$data['path'].'/">'.$data['title_menu'].'</a><div>';
            	$menu.=echo_sub_menu2($data['path_id']);
            	$menu.='</div>';
            	$menu.='</div>';
			}
            //}
            $menu.='</li>';
            $i++;
            
        }
        $menu.='</ul>';
    }
    echo $menu;
}

//----------------------------------------------------------------------------------------
echo_sub_menu(1);
?>