<?

$limit=isset($outerSettings['limit']) ? $outerSettings['limit']:0;
//print_r($outerSettings);
$Lenta = new ModelLenta( $DB );
$Lenta->pathId = isset($outerSettings['path_id']) ? $outerSettings['path_id'] :$page['path_id'];
$Lenta->path = isset($outerSettings['path']) ? $outerSettings['path'] : path($Lenta->pathId);
$Lenta->order = ' ORDER BY `date` DESC ';
$Lenta->onPage = isset($outerSettings['onpage']) ? $outerSettings['onpage'] : 5;
$Lenta->page = preg_match( '/^page([0-9]+)$/', getParam(0), $match ) ? $match[1] : 0;
$Lenta->paginationType = isset($outerSettings['paginator']) ? $outerSettings['paginator'] : 'paginator'; //simple, paginator

if ( gettype(getParam(0)) == 'string' && ! $Lenta->page )
	{
	
	$Lenta->setItemKey( getParam(0) );
	
	if ( $item = $Lenta->getItem() )
		{
		
        $_CONTENT_='';
        $_TITLEH1 = $item['title'];
		$breadCrumbs->addLevel('', $item['title']);
		
		?>
		
		<?=$item['text']?>
		
		<div><a href="../">&laquo; Назад</a></div>

		<?

		// Другие новости
		$Lenta->onPage = 5;

		}
	else
		{
		
		page404();
		
		}
	
	}
else
	{
	
	if ( $items = $Lenta->getItems() )
		{
			
		?><div class="grid"><?
		
		foreach ( $items as $item )
			{
            $Lenta->setItemKey( $item[$Lenta->keyUnique] );
			$link="/{$Lenta->path}".$item[$Lenta->getFieldKey()]."/";
			
			if (getImageURL($item['image'],0)) {$img = getImageURL($item['image'],0);}
            else{ $img='';} 
            
			?>
            <div class="x1 t1">
                <?if($img){?>
                <a class="img" href="<?=$link?>">
                    <img src="<?=$img?>"/>
                </a>
                <?}?>
                
                <div>
                    <span class="date"><?=sql2date( $item['date'], 5 )?></span>
                    <a class="title" href="<?=$link?>"><?=$item['title']?></a>
                </div>

                <div class="announce">
                    <?=$item['announce']?>
                </div>

                <div class="clear"></div>
			</div>

                <br>
            <?
			}
		
		?></div><?

		if ( count($Lenta->getPagination()) > 1 )
			{

			?><div>
				<div style="float:left;margin-top:5px;"><?

			foreach ( $Lenta->getPagination() as $k=>$p )
				{

				if ( $p['active'] )
					{

					?><div style="width:20px;float:left;"><span><?=$p['title']?></span></div><?

					}
				elseif ( $Lenta->paginationType == 'paginator' && $p['skip'] )
					{
					
					?><div style="width:20px;float:left;"><i>...</i></div><?

					}
				else
					{

					?><div style="width:20px;float:left;"><a href="/<?=$Lenta->path?><?=$k ? 'page' . $k . '/' : ''?>"><?=$p['title']?></a></div><?

					}

				}

			?></div></div>
                <?

			}

		}
	elseif ( $Lenta->page )
		{
		
		page404();
		
		}
	else
		{
			
		?>Пусто<?

		}
	
	}

?>

