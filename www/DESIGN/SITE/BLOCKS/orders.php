<?
if ($_USER_INFO)
{
    $_TITLEH1 .= ' <a href="/'.path(19).'">Личные данные</a>';

    $orders = $DB->getAll("SELECT * FROM `mp_orders` WHERE `user_id` = ".$_USER_INFO['id']." ORDER by `date` DESC");
    
    ?>
    <div id="orders" class="orders-list">
        <?
        foreach ($orders as $order)
        {
            ?>
            <div class="order" id="order-<?=$order['id']?>">
                <div class="order-title"><a href="#">Заказ от <?=sql2date($order['date'], 5)?> г.</a></div>
                <div class="order-data">
                    <form action="/json/add_order/" method="post" class="ajax">
                        <input type="hidden" name="order_id" value="<?=$order['id']?>" />
                        <?
                        $items = stringToOrder($order['items']);
                        
                        if (empty($items)) continue;
                        
                        
                        list($total_count, $total_price) = infoOrder($items);
                        ?>
                        <div class="items">
                            <?
                            foreach ($items as $item)
                            {
                                $article = $DB->getRow("SELECT * FROM `mp_catalog_articles` WHERE `id`=".$item['parent']);
                                $brand = $DB->getRow("SELECT * FROM `mp_brands` WHERE `id`=".$item['brand']);
                                $image = getGroupImageHTML($item['images'], 3, 0, false, $item['title']);
                                ?>
                                <div class="item clearfix">
                                    <input type="hidden" name="item_id" value="<?=$item['id']?>" />
                                    <?
                                    if ($image)
                                    {
                                        ?>
                                        <div class="image"><a href="/<?=path($article['path_id'], getCode($article).'/'.getCode($brand))?>/<?=getCode($item)?>/"><?=$image?></a></div>
                                        <?
                                    }
                                    ?>
                                    <div class="data">
                                        <div class="title"><a href="/<?=path($article['path_id'], getCode($article).'/'.getCode($brand))?>/<?=getCode($item)?>/"><?=$item['title']?></a></div>
                                        <div class="text"><?=$item['smalltext']?></div>
                                        <div class="info clearfix">
                                            <div class="order-box"><input type="text" name="order[<?=$item['id']?>]" class="count string" value="<?=$item['order_count']?>" /> шт.</div>
                                            <div class="price"><span class="num"><?=formatPrice($item['price'])?></span> за шт.</div>
                                            <div class="price"><span class="num" id="item-price-<?=$order['id']?>-<?=$item['id']?>"><?=formatPrice($item['price'] * $item['order_count'])?></span> сумма</div>
                                        </div>
                                    </div>
                                    <div class="delete"><button type="button">Х</button></div>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                        <div class="result"><span id="result-price-<?=$order['id']?>" class="num"><?=$total_price?></span> сумма заказа</div>
                        <div class="resend"><button>Повтороить заказ</button></div>
                    </form>
                </div>
            </div>
            <?
        }
        ?>
    </div>
    <?
} 
else 
{
    echo 'Для доступа к странице пройдите авторизацию';
}
?>