<?
$items = $DB->getAll("SELECT * FROM `mp_catalog_articles` WHERE `parent`=0 AND `visible`='1' ORDER BY `order`");

$_art = (getPathId() == 16) ? (int)$DB->getOne("SELECT `id` FROM `mp_catalog_articles` WHERE ".checkCode(getParam(0))) : false;

$fast = $_art ? fast_get_catalog('catalog', $_art) : array();


if (!empty($items))
{
    ?>
    <ul id="catalog-menu">
        <?
        foreach ($items as $n=>$item)
        {
            $n++;
            $sel = ($item['path_id']==getPathId() && isset($fast[$item['id']]));

            $subs = printSubItems($item['id']);
            ?>
            <li class="e0<?=$n?>">
                <div class="title clearfix">
                    <a href="/<?=path($item['path_id'], getCode($item))?>/"<?=($sel) ? ' class="active"' : ''?>><?=$item['name']?></a>
                    <span class="image"><img src="/DESIGN/SITE/images/icons/0<?=$n?>.png" alt="" /></span>
                </div>
                <?
                if (!empty($subs))
                {
                    ?>
                    <div class="items" <?=($sel?' style="display:block"':'')?>><?=$subs?></div>
                    <div class="open<?=($sel?' show':'')?>"><a href="#" class="full">Показать полный список</a><a href="#" class="min">Скрыть полный список</a></div>
                    <?
                }
                ?>
            </li>
            <?
        }
        ?>
    </ul>
    <?
}


function printSubItems($parent)
{
    global $DB, $_art, $fast;

    $sbus = $DB->getAll("SELECT * FROM `mp_catalog_articles` WHERE `parent`=".$parent." AND `visible`='1' ORDER BY `name`");

    ob_start();
    if (!empty($sbus))
    {
        ?>
        <ul>
            <?
            foreach ($sbus as $sub)
            {
                $sel = (isset($fast[$sub['id']]));
                ?>
                <li>
                    <a href="/<?=path($sub['path_id'], getCode($sub))?>/"<?=($sel) ? ' class="active'.(getParam(0)==$sub['id']?' cur':'').'"' : ''?>><?=$sub['name']?></a>
                    <?=($sel ? printSubItems($sub['id']) : '')?>
                </li>
                <?
            }
            ?>
        </ul>
        <?
    }

    return ob_get_clean();
}
?>