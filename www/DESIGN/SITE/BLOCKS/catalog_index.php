<?
$items = $DB->getAll("SELECT * FROM `mp_catalog_articles` WHERE `parent`=0  ORDER BY `order`");

$_art = (getPathId() == 28) ? (int)$DB->getOne("SELECT `id` FROM `mp_catalog_articles` WHERE ".checkCode(getParam(0))) : false;

$fast = $_art ? fast_get_catalog('catalog', $_art) : array();

if (!empty($items))
{
    ?>
    <div id="catalog-blocks" class="clearfix">
        <ul>
            <?
            $i=0;
            foreach ($items as $n=>$item)
            {
                $n++;
                $i++;

                $subs = $DB->getAll("SELECT * FROM `mp_catalog_articles` WHERE `parent`=".$item['id']." ORDER BY `order` LIMIT 6");


                if ($i>3)
                {
                    ?>
                    </ul><ul>
                    <?
                    $i=1;
                }

                ?>
                <li class="e0<?=$n?>">
                    <div class="title">
                        <a href="/<?=path($item['path_id'], getCode($item))?>/"><?=$item['name']?></a>
                    </div>
                    <div class="data clearfix">
                        <span class="image"><img src="/DESIGN/SITE/images/icons/0<?=$n?>.png" alt="" /></span>
                        <?
                        if (!empty($subs))
                        {
                            ?>
                            <div class="items">
                                <?
                                foreach ($subs as $sub)
                                {
                                    ?>
                                    <div><a href="/<?=path($sub['path_id'], getCode($sub))?>/"><?=$sub['name']?></a></div>
                                    <?
                                }
                                ?>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <div class="all"><noindex><a href="/<?=path($item['path_id'], getCode($item))?>/">Показать полный список</a></noindex></div>
                </li>
                <?
            }
            ?>
        </ul>
    </div>
    <?
}
?>===========