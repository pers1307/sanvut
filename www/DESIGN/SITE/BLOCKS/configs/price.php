<?
$formconfig = array(



    array(
        'name' => 'name',
        'caption' => 'Ваше имя',
        'type' => 'string',
        'empty' => 'поле не заполнено',
        'req' => array(
            'reg' => '/^[ёа-яА-Я -]+$/u',
        ),
        'regexp' =>'Можно использовать только буквы русского алфавита',
        //'text' => '123',

    ),

    array(
        'name' => 'phone',
        'caption' => 'Телефон',
        'type' => 'string',
        'req' => array(
            'reg' => '/^[0-9+ \-\(\)]{5,17}$/',
        ),
        'empty' => 'поле не заполнено',
        'regexp' => 'Можно использовать только цифры, скобки, +, -',
        //'text' => '123',
    ),

    array(
        'name' => 'comp_name',
        'caption' => 'Организация',
        'type' => 'string',
        //'empty' => 'поле не заполнено',
        'req' => array(
            'reg' => '/^[0-9a-zA-Zёа-яА-Я -]+$/u',
        ),
        'regexp' =>'Можно использовать только буквы русского алфавита',
        //'text' => '123',

    ),

    array(
        'name' => 'email',
        'caption' => 'E-mail',
        'type' => 'string',
        'empty' => 'поле не заполнено',
        'req' => array(
            'reg' => '/[.+a-zA-Z0-9_-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/',
        ),
        'regexp' => 'Не верный адрес'
    ),

    array(
        'name' => 'text',
        'caption' => 'Комментарий',
        'type' => 'memo',
        //'empty' => 'поле не заполнено',
        //'regexp' => 'поле заполнено не правильно',
        'class' => 'wide',
        //'text' => '123',
    ),
);
?>