<?
$disable = (int)$DB->getOne("SELECT `data` FROM `mp_modules_config` WHERE `module`='slider' AND `name`='disable'");


if (!$disable)
{
    $items = $DB->getAll("SELECT * FROM `mp_slider` WHERE `active`='1' ORDEr by `order`");

    if (!empty($items))
    {
        ?>
        <div id="slider">
            <div class="image clearfix">
                <ul class="items">
                    <?
                    foreach ($items as $item)
                    {
                        $image = getImageHTML($item['image'], 1, $item['title']);

                        if (!$image) continue;
                        ?>
                        <li><?=$image?></li>
                        <?
                    }
                    ?>
                </ul>
                <div class="controls">
                    <?
                    foreach ($items as $n=>$item)
                    {
                        ?>
                        <a href="#"<?=(!$n?' class="active"':'')?>></a>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
        <?
    }
}
?>
