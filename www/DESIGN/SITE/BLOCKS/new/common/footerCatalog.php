<? $items = getFooterCatalog(); ?>

<? if(!empty($items)): ?>

    <div class="theEnd_block">
        <div class="spoilers">

            <? foreach($items as $key => $item): ?>

                <? $class = ''; ?>
                <? $style = ''; ?>

                <? if ($key == 0): ?>

                    <? $class = 'active'; ?>
                <? else: ?>

                    <? $style = 'style="display: none;"'; ?>
                <? endif; ?>

                <div class="spoller_container2 <?= $class ?>">

                    <a href="<?= $item['path'] ?>" class="spoller_handler2"><?= $item['name'] ?></a>

                    <? if(!empty($item['child'])): ?>

                        <? $ItemsInColon = arrayHorizontal($item['child'], 3); ?>

                        <div class="spoller2" <?= $style ?>>
                            <div class="dop_footMenu">

                                <? foreach($ItemsInColon as $colon): ?>

                                    <div>
                                        <? foreach($colon as $item): ?>

                                            <a href="<?= $item['path'] ?>"><?= $item['name'] ?></a>
                                        <? endforeach; ?>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>