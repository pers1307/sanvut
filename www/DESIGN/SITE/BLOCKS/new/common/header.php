<header>
    <div class="header-middle">
        <div class="centerWrap">

            <div class="logo">
                <span>
                    <? if ($_SERVER['REQUEST_URI'] == '/'): ?>

                        <span>
                            <img src="/DESIGN/SITE/images/logo.png" alt="Логотип">
                        </span>
                    <? else: ?>

                        <a href="/">
                            <img src="/DESIGN/SITE/images/logo.png" alt="Логотип">
                        </a>
                    <? endif; ?>
                </span>
            </div>

            <div class="slogan">
                Комплексные решения<br>
                <span>во благо жизни!</span>
            </div>

            <div class="header-phone">

                <?= getTextMod(7) ?>
            </div>

            <div class="callback">
                <a href="#" onclick="getJson('/json/GetCall/'); return false;">Заказать обратный звонок</a>
            </div>

            <div class="addr">

                <?= getTextMod(8) ?>
            </div>
        </div>
    </div>

    <div class="header-bottom">
        <div class="centerWrap">

            <? include( BLOCKS_DIR . 'new/common/menu.php' );?>
        </div>
    </div>
</header>