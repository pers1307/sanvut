<head>
    <meta charset=utf-8>

    <title><?= $_TITLEPAGE ?></title>

    <?= showCss() ?>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <script src="/DESIGN/SITE/JS/jquery-1.11.3.min.js"></script>
    <script src="/DESIGN/SITE/JS/jquery.bxslider.js"></script>
    <script src="/DESIGN/ADMIN/js/JsHttpRequest.js"></script>
    <script src="/DESIGN/SITE/JS/json.js"></script>
    <script src="/DESIGN/SITE/JS/doload.js"></script>

    <script src="/DESIGN/SITE/JS/spoiler.js"></script>

</head>