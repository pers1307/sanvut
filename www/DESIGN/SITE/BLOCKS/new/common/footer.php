<footer>
    <div class="pre_footer">
        <div class="centerWrap">

            <div class="first_leftMenu">

                <?= getTextMod(9) ?>
            </div>

            <div class="second_infoBlock">

                <div class="number">

                    <?= getTextMod(10) ?>
                </div>

                <div class="city">

                    <?= getTextMod(11) ?>
                </div>

                <div class="mail_block">

                    <?= getTextMod(12) ?>
                </div>

                <div class="callback">
                    <a href="#" onclick="getJson('/json/GetCall/'); return false;">Заказать обратный звонок</a>
                </div>
            </div>

            <? include( BLOCKS_DIR . 'new/common/footerCatalog.php' );?>
        </div>
    </div>
    <div class="footer">
        <div class="centerWrap">
            <div class="copy">

                <?= getTextMod(1) ?>
            </div>
            <div class="company">
                Проектирование, изготовление, доставка, монтаж медицинского оборудования по России и
                странам СНГ
            </div>
        </div>
    </div>
</footer>

<div class="modal-window-wrap hidden_text">
    <div class="modal-window">
        <div class="modal-window-body">
            <div class="modal-window-close"></div>
            <div class="modal">
                <div class="modalTitle">
                    Обратный звонок
                </div>
                <div class="modalContent">
                    <form
                            id="callform"
                            class="call"
                            method="post"
                            action="/json/call/"
                    >

                        <input type="hidden" name="feedbacks" value="" />

                        <label class="labelBlock">
                            <span>Ваше имя </span>
                            <input type="text" class="f-name" name="cname" placeholder="Иванов Иван Иванович">
                            <div class="errormsg" id="err-name"></div>
                        </label>
                        <label class="labelBlock">
                            <span>Телефон <i>*</i></span>
                            <input type="text" class="f-phone" name="cphone" placeholder="+7(ххх)ххх-хх-хх">
                            <div class="errormsg" id="err-phone"></div>
                        </label>
                        <label class="labelBlock">
                            <span>Комментарий: </span>
                            <textarea type="text" class="f-where" name="cwhere"></textarea>

                            <div class="err-war" id="err-where"></div>
                            <div class="errormsg" id="err-q"></div>
                        </label>
                        <button>Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>