<?
global $DB;

$path_id = getPathId();

$page = $DB->getRow('
        SELECT *
        FROM ' . PRFX . 'www
        WHERE path_id = ' . $path_id . '
        '
);

if ($page['parent'] != 1) {

    $rootPage = $DB->getRow('
        SELECT *
        FROM ' . PRFX . 'www
        WHERE path_id = ' . $page['parent'] . '
        '
    );

    $page = $rootPage;
}

$subPages = $DB->getAll('
        SELECT *
        FROM ' . PRFX . 'www
        WHERE parent = ' . $page['path_id'] . '
        AND visible = 1
        ORDER BY `order` ASC
        '
);

?>

<? if(!empty($subPages)): ?>

    <nav class="leftmenu">
        <div class="menuTitle"><?= $page['title_menu'] ?></div>
        <div>
            <div>

                <? foreach($subPages as $subPage): ?>

                    <? if ('/' . $subPage['path'] . '/' == $_SERVER['REQUEST_URI']): ?>

                        <span><?= $subPage['title_menu'] ?></span>
                    <? else: ?>

                        <a href="/<?= $subPage['path'] ?>/"><?= $subPage['title_menu'] ?></a>
                    <? endif; ?>

                <? endforeach; ?>
            </div>
        </div>
    </nav>
<? endif; ?>