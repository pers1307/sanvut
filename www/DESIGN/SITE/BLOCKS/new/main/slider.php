<? $items = getSliders(); ?>

<? if(!empty($items)): ?>

    <div class="mainSlider">
        <div class="gl_slide">

            <? foreach($items as $item): ?>

                <? $imageUrl = getImageURL($item['image'], 0); ?>

                <div style="background: url(<?= $imageUrl ?>) no-repeat 100%">
                    <div class="centerWrap">
                        <div>
                            <div class="okh1"><?= $item['title'] ?></div>
                            <p><?= $item['title_two'] ?></p>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>

        <div id="bx-pager" class="centerWrap">

            <? foreach($items as $key => $item): ?>

                <? $imageUrl = getImageURL($item['image'], 1); ?>

                <a data-slide-index="<?= $key ?>" href=""><img src="<?= $imageUrl ?>"/></a>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>