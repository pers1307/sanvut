<div class="centerWrap">
    <div class="seoText">
        <div class="spoiler_block">
            <div class="spoilers">

                <? $items = getMainCatalog() ?>

                <? if(!empty($items)): ?>

                    <? foreach($items as $key => $item): ?>

                        <? $class = ''; ?>
                        <? $style = ''; ?>

                        <? if ($key == 0): ?>

                            <? $class = 'active'; ?>
                        <? else: ?>

                            <? $style = 'style="display: none;"'; ?>
                        <? endif; ?>

                        <div class="spoller_container <?= $class ?>">

                            <a href="<?= $item['path'] ?>" class="spoller_handler"><?= $item['name'] ?></a>

                            <? if(!empty($item['child'])): ?>

                                <div class="spoller" <?= $style ?>>
                                    <div class="wrap2">

                                        <? $ItemsInColon = arrayHorizontal($item['child'], 3); ?>

                                        <? foreach($ItemsInColon as $colon): ?>

                                            <div>

                                                <? foreach($colon as $colonItem): ?>

                                                    <div>
                                                        <a href="<?= $colonItem['path'] ?>"><?= $colonItem['name'] ?></a>

                                                        <? if(!empty($colonItem['child'])): ?>

                                                            <div>

                                                                <? foreach($colonItem['child'] as $child): ?>

                                                                    <a href="<?= $child['path'] ?>"><?= $child['name'] ?></a>
                                                                <? endforeach; ?>
                                                            </div>
                                                        <? endif; ?>
                                                    </div>
                                                <? endforeach; ?>

                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    <? endforeach; ?>
                <? endif; ?>

                <? $projects = getProjectCatalog(); ?>

                <? if(!empty($projects)): ?>

                    <div class="complite_project">
                        <div class="complite_title">
                            Реализованные проекты
                        </div>
                        <div class="compite_cards">

                            <? foreach($projects as $project): ?>

                                <? $imageUrl = getImageURL($project['img'], 1); ?>

                                <div>
                                    <a href="<?= $project['path'] ?>">
                                        <div class="card_img">
                                            <div><img src="<?= $imageUrl ?>" alt="<?= $project['name'] ?>"/></div>
                                        </div>
                                    </a>

                                    <p>
                                        <a class="no-link" href="<?= $project['path'] ?>"><?= $project['name'] ?></a>
                                    </p>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>

            </div>
        </div>
    </div>
</div>