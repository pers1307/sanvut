<? $newsItems = getNews(); ?>

<? if(!empty($newsItems)): ?>

    <div class="news_company">
        <div class="centerWrap w1160">

            <div class="news_title">
                <div class="okh1">Новости компании</div>
                <a class="allnews" href="<?= path(101) ?>">Все новости</a>
            </div>

            <div class="wrap3">

                <? foreach($newsItems as $newsItem): ?>

                    <?

                    $imageUrl = getImageURL($newsItem['image'], 1);

                    if (empty($imageUrl)) {

                        $imageUrl = '/DESIGN/SITE/images/noImage/no-image_160_120.jpg';
                    }

                    ?>

                    <div>

                        <div class="wrap3Img">
                            <div>
                                <img src="<?= $imageUrl ?>" alt="<?= $newsItem['title'] ?>"/>
                            </div>
                        </div>

                        <div class="wrap_info">
                            <a href="<?= $newsItem['path'] ?>">
                                <?= $newsItem['title'] ?>
                            </a>

                            <p><?= $newsItem['announce'] ?></p>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
<? endif; ?>