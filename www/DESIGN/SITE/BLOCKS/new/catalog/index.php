<? $items = getArts( 'catalog_articles', 0, 'AND ind' ); ?>

<? if(!empty($items)): ?>

    <div class="wrap">

        <? foreach($items as $item): ?>

        <?

        $imageUrl = getImageURL($item['img'], 0);

        if (empty($imageUrl)) {

            $imageUrl = '/DESIGN/SITE/images/noImage/no-image_150_150.jpg';
        }

        ?>

            <div>
                <a href="/<?= path(65) ?><?= $item['code'] ?>/"><?= $item['name'] ?></a>

                <div class="wrapImg">
                    <div>
                        <img src="<?= $imageUrl ?>" alt="<?= $item['name'] ?>"/>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>

<?= getContent(65) ?>