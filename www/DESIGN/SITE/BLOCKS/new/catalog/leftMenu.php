<?
$table = getPathId() == 65 ? 'catalog_articles' : 'catalog2_articles';
$items = getArts( $table );
?>

<? if(!empty($items)): ?>

    <nav class="leftmenu">

        <div class="menuTitle">Каталог</div>
        <div>

            <? foreach($items as $item): ?>

                <? if ('/' . path((int)getPathId()) . $item['code'] . '/' == $_SERVER['REQUEST_URI']): ?>

                    <span><?= $item['name'] ?></span>
                <? else: ?>

                    <a href="/<?= path((int)getPathId()) ?><?= $item['code'] ?>/"><?= $item['name'] ?></a>
                <? endif; ?>

                <? if ($_VARS[0] == $item['code']): ?>

                    <? $subItems = getArts($table, $item['id']); ?>

                    <div>

                        <? foreach($subItems as $subItem): ?>

                            <? if ('/' . path((int)getPathId()) . $_VARS[0] . '/' . $subItem['code'] . '/' == $_SERVER['REQUEST_URI']): ?>

                                <span><?= $subItem['name'] ?></span>
                            <? else: ?>

                                <a href="/<?= path((int)getPathId()) ?><?= $_VARS[0] ?>/<?= $subItem['code'] ?>/"><?= $subItem['name'] ?></a>
                            <? endif; ?>
                        <? endforeach; ?>

                    </div>
                <? endif; ?>

            <? endforeach; ?>
        </div>
    </nav>
<? endif; ?>