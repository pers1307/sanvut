<?

if (empty($_TITLEH1)) {

    $_TITLEH1 = $art['name'];
}

$s = isset( $_SESSION['cat']['view'] ) ? $_SESSION['cat']['view'] : 0;

if( !isset( $_SESSION['cat']['id'] ) || $_SESSION['cat']['id'] != $art['id'] )
{
    $s = $_SESSION['cat']['view'] = 0;
}

$_SESSION['cat']['id'] = $art['id'];
auth_StoreSession();

$sql = $s ? ' AND type="'.$s.'" ' : '';
$types = $items = array();
$count = $DB->getOne( 'SELECT COUNT(*) FROM '.PRFX.'catalog_items WHERE parent="'.(int)$art['id'].'" '.$sql.' AND active' );

if( $count )
{
    //Папка с файлами.
    $items = $DB->getAll( 'SELECT id, code, title, type, img, shema1, shema2, photo, width, height, deep, text, mark FROM '.PRFX.'catalog_items WHERE parent="'.(int)$art['id'].'" '.$sql.' AND active ORDER BY `order`, id LIMIT '.(int)( $onPage * ( $sel_page ? $sel_page-1 : 0 ) ).','.(int)$onPage );
    $types = $DB->getAll( 'SELECT DISTINCT `type` FROM '.PRFX.'catalog_items WHERE parent="'.(int)$art['id'].'" AND active ORDER BY `type`' );
}
else
{
    if ($sel_page) {
        page404();
    }

    $count = $DB->getOne( 'SELECT COUNT(*) FROM '.PRFX.'catalog_articles a WHERE a.parent="'.(int)$art['id'].'" AND a.visible="1" '.$sql );
    $folders = $DB->getAll( 'SELECT a.* FROM '.PRFX.'catalog_articles a WHERE a.parent="'.(int)$art['id'].'" AND a.visible="1" ORDER BY a.order');
}

?>

<? if (!empty( $items ) && empty($folders)): ?>

    <? if (count( $types ) > 1): ?>

        <?

        $tmp = array();

        foreach($types as $t) {
            $tmp[] = $t['type'];
        }
        ?>

        <div class="series">
            показывать <span class="s s0<?=$_SESSION['cat']['view'] == 0 ? ' act' : ''?>"><span>все</span></span>
            или только <?=( in_array( 1, $tmp ) ? '<span class="s s1'.( $_SESSION['cat']['view'] == 1 ? ' act' : '' ).'"><span>эконом</span></span>, ' : '' )?>
            <?=( in_array( 2, $tmp ) ? '<span class="s s2'.( $_SESSION['cat']['view'] == 2 ? ' act' : '' ).'"><span>элит</span></span> ' : '' )?>
            <?=( in_array( 3, $tmp ) ? 'или может быть <span class="s s3'.( $_SESSION['cat']['view'] == 3 ? ' act' : '' ).'"><span>премиум</span></span>' : '' )?>?
        </div>
    <? endif; ?>

    <div class="catalog">
        <?= echoCatItems( $items, $art, $count, $sel_page, $onPage, $art['text'] ) ?>
    </div>
<? elseif (!empty($folders) && empty($items)): ?>

    <div class="catalog">
        <?= echoCatFolders($folders, $art, $count, $art['text']) ?>
    </div>

    <?= $art['text'] ?>
<? elseif (empty($folders) && empty($items)): ?>

    <?= $art['text'] ?>
<? endif; ?>