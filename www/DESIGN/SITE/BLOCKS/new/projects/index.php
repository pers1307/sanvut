<?php

$articles = $DB->getall('Select * from `mp_catalog2_articles` where parent=0 AND visible ORDER BY `order`, id');

?>

<div class="wrap">

    <? foreach($articles as $article): ?>

        <div>
            <a href="/<?= $page['path'] ?>/<?= $article['code'] ?>/"><?= $article['name'] ?></a>

            <? $imageUrl = getImageURL($article['img'], 0); ?>

            <div class="wrapImg">
                <div>
                    <img src="<?= $imageUrl ?>" alt="<?= $article['name'] ?>"/>
                </div>
            </div>
        </div>
    <? endforeach; ?>

</div>

