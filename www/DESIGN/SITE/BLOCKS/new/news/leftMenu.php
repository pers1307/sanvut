<?
global $DB;

$items = $DB->getAll('
        SELECT 
        title_menu,
        path 
        FROM ' . PRFX . 'www
        WHERE visible = 1
        AND parent = 62
        ORDER BY `order` ASC
        '
);
?>

<? if(!empty($items)): ?>

    <nav class="leftmenu">
        <div class="menuTitle">О компании</div>
        <div>
            <div>

                <? foreach($items as $item): ?>

                    <? if ('/' . $item['path'] . '/' == $_SERVER['REQUEST_URI']): ?>

                        <span><?= $item['title_menu'] ?></span>
                    <? else: ?>

                        <a href="/<?= $item['path'] ?>/"><?= $item['title_menu'] ?></a>
                    <? endif; ?>

                <? endforeach; ?>
            </div>
        </div>
    </nav>
<? endif; ?>