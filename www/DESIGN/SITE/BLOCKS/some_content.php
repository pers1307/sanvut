<?

require_once('some_content.model.php');

$Catalog = new Catalog();
$Catalog->pathId = $page['path_id'];
$Catalog->path = $page['path'];
$tree = $Catalog->getTree();
$Catalog->checkRedirect($_VARS);
$Catalog->checkTree($tree, $_VARS);

?>

<table width="100%" border="0"><tr valign="top">

<td width="220px" style="border-top:1px solid #0970d9;margin-right:20px;padding:20px 0 0 20px;">

	<?	
	
	if ( count($tree) )
		{
		
		treeHtml($tree);
		
		}
	else
		{
		
		?>Разделов нет<?
		
		}
	
	?>
	
</td>
<td width="80%" style="padding-left:20px;">

	<?
	
	if ( count($_VARS) )
		{
		
		$art = $Catalog->getArticle($_VARS);

		if ( $Catalog->isItem($_VARS[count($_VARS)-1]) )
			{
			
			if ( $item = $Catalog->getItem($art, $_VARS) )
				{
				
				$breadCrumbs->addLevel('', $item['title']);
				
				
				echo $breadCrumbs;
				?><h1 style="color:#0078c9"><?=$item['title']?></h1><?
				echo $item['comment'];
			
				}
			else
				{
				
				page404();
				
				}
			
			}
		else
			{
				
				echo '<div class="catalog_crumbs">'.$breadCrumbs.'</div>';
				
			?><h1 style="color:#428ed1"><?=$art['name']?></h1><?
			echo $art['text'];

			if ( $items = $Catalog->getItems($art['id']) )
				{
				
				?><ul><?
				
				foreach ( $items as $item )
					{

					$Catalog->setObjKey( $item[$Catalog->keyUnique] );
					
					?><li><a href="<?=$item[ $Catalog->getFieldKey() ]?><?=$Catalog->itemExt?>/"><?=$item['title']?></a></li><?
					
					}
				
				?></ul><?
				
				}

			}
		
		}
	
	?>
	
</td>

</tr></table>

<?

/**
 * Функция для построения HTML-дерева разделов каталога.
 * В переменную tree передается дерево в виде массива, все дочернии элементы должны находится в ветке childs
 * @return string
 */
function treeHtml( $tree, $level = 0, $url = array(), $bc = array() )
	{
	
	global $Catalog, $_VARS, $breadCrumbs;
	
	$level++;
	
	?><ul style="margin:0;padding:0;"><?
	
	foreach ( $tree as $t )
		{
			
			
		
		$url[$level] = $t[ $Catalog->setObjKey( $t[$Catalog->keyUnique] ) ];
		$bc[$level]['url'] = '/' . $Catalog->path . '/' . implode('/', $url) . '/';
		$bc[$level]['name'] = $t['name'];
		$__VARS = $_VARS;
		
		if ( count($__VARS) && $Catalog->isItem($__VARS[count($__VARS)-1]) )
			{
			
			unset($__VARS[count($__VARS)-1]);
			
			}
		
		if ( implode('/', $__VARS) == implode('/', $url) )
			{
			
			/** Строим хлебную крошку */
			foreach ( $bc as $crumb )
				{

				$breadCrumbs->addLevel($crumb['url'], $crumb['name']);
				
				}
			
			if($level==1){
			?>
			<li class="cat_menus_act_firsr"><?=strtoupper($t['name'])?></li>
			<?
			}
			else
			{
			?>
			<li class="cat_menus_act"><?=$t['name']?></li>
			<?
			}	
				
			}
		else
			{
				if($level==1){
				?>
				<li class="cat_menus_firsr"><a href="/<?=$Catalog->path?>/<?=implode('/', $url)?>/"><?=strtoupper($t['name'])?></a></li>
				<?
				}
				else 
				{
				?>
				<li class="cat_menus"><a href="/<?=$Catalog->path?>/<?=implode('/', $url)?>/"><?=$t['name']?></a></li>
				<?
				}
			}
		
		if ( count($t['childs']) )
			{
			
			treeHtml( $t['childs'], $level, $url, $bc );
			
			}
		
		}
	
	?></ul><?
	
	}
	
?>
