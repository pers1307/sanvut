<?

/**
 * Модель для вывода каталога
 *
 * Этот ПРИМЕР написан для демонстрации внедрения "Символьного кода" в каталоге.
 * Модель поддерживает бесконечный уровень вложенности.
 * @author Philipp Nehaev <phil@mediasite.ru>
 * @version 20110725
 */
class Catalog
	{
	
	public $pathId = 0;
	public $path = '/';
	public $tableArticles = 'mp_some_articles';
	public $tableItems = 'mp_some_items';
	public $keyPrimary = 'id';
	public $keyUnique = 'code'; // Поле с символьным кодом
	public $itemExt = '_item'; // Префикс для позиций
	public $id2code = array();
	private $key;
	private $objKey;

	/**
	 * Вернет имя поля по которому сделать выборку
	 * @return string
	 */
	private function getKey ()
		{

		if ( preg_match('/^[0-9]+$/', $this->objKey) )
			{
			
			return $this->keyPrimary;
			
			}
		
		if ( preg_match('/^[a-z0-9_]+$/i', $this->objKey) )
			{
			
			return $this->keyUnique;
			
			}
		
		return $this->keyPrimary;
		
		}
		
	/**
	 * Метод для записи приватного свойства с ключом для выборки.
	 * Сделано так, что-бы при указании ключа сразу определялось имя поля для выборки.
	 * @retrun string
	 */
	public function setObjKey ( $value )
		{
		
		$this->objKey = $value;
		$this->key = $this->getKey();
		
		return $this->key;
		
		}

	/**
	 * @return string
	 */
	public function getFieldKey ()
		{
		
		return $this->key;
		
		}
	
	/**
	 * Возвращает дерево разделов в виде массива
	 * @return array
	 */
	public function getTree ($parent = 0)
		{
		
		global $DB;		
		
		function beat ( $arts, $object )
			{
			
			global $DB;
			
			if ( count($arts) )
				{
				
				foreach ( $arts as &$art )
					{
					
					$rows = $DB->getAll( $query = 'SELECT * FROM `' . $object->tableArticles . '` WHERE `path_id` = ' . $object->pathId . ' AND `parent` = ' . $art['id'] . ' ORDER BY `order`' );
					$childs = keyAssoc($object, $rows);
					$object->id2code = array_merge($object->id2code, $rows);
					
					$art['childs'] = beat($childs, $object);
					
					}
				
				}
			
			return $arts;
			
			}
		
		/**
		 * Функция для ассоциации ключь-поле выборки, главное назначени такого подхода для легкой проверки существования урла
		 * @return array
		 */
		function keyAssoc($object, $array)
			{
			
			$temp = array();
			
			foreach ( $array as $arr )
				{
				
				$temp[ $arr[ $object->setObjKey( $arr[$object->keyUnique] ) ] ] = $arr;
				
				}
			
			return $temp;
			
			}
		
		$rows = $DB->getAll( $query = 'SELECT * FROM `' . $this->tableArticles . '` WHERE `path_id` = ' . $this->pathId . ' AND `parent` = ' . $parent . ' ORDER BY `order`' );
		$arts = beat( keyAssoc($this, $rows), $this );
		$this->id2code = array_merge($this->id2code, $rows);
		
		return $arts;
		
		}
	
	/**
	 * @return array
	 * @return boolean
	 */
	public function getArticle ($_VARS)
		{
		
		global $DB;

		if ( $this->isItem($_VARS[count($_VARS)-1]) )
			{
			
			$this->setObjKey( $_VARS[count($_VARS)-2] );
			
			}
		else
			{
			
			$this->setObjKey( $_VARS[count($_VARS)-1] );
			
			}
	
		$art = $DB->getRow( $query = 'SELECT * FROM `' . $this->tableArticles . '` WHERE `path_id` = ' . $this->pathId . ' AND `' . $this->getFieldKey() . '` = "' . $DB->pre($this->objKey) . '"' );
		
		return count($art) ? $art : false;
		
		}

	/**
	 * @return array
	 * @return boolean
	 */
	public function getItems ($artId)
		{
		
		global $DB;
		
		$items = $DB->getAll( $query = 'SELECT * FROM `' . $this->tableItems . '` WHERE `parent` = "' . $artId . '" AND `active` = 1' );

		return count($items) ? $items : false;
		
		}
	
	/**
	 * @return array
	 * @return boolean
	 */
	public function getItem ($art, $_VARS)
		{
		
		global $DB;

		$this->setObjKey( $_VARS[count($_VARS)-1] );

		$this->setObjKey( preg_replace('/' . $this->itemExt . '/', '', $this->objKey) );

		$item = $DB->getRow( $query = 'SELECT * FROM `' . $this->tableItems . '` WHERE `' . $this->getFieldKey() . '` = "' . $DB->pre($this->objKey) . '" AND `parent` = ' . $art['id'] . ' AND `active` = 1' );

		if ( count($item) )
			{
		
			$this->redirectItem($this->getFieldKey(), $item, $_VARS);
			
			return $item;
		
			}
		
		return false;
		
		}

	/**
	 * Метод для проверки существования урла
	 * @return string
	 */
	public function checkTree ($tree, $_VARS)
		{
		
		function checkTree($tree, $_VARS)
			{

			unset($_VARS[0]);
			$_VARS = array_values($_VARS);

			if ( count($_VARS) )
				{
				
				if ( isset($tree[$_VARS[0]]) )
					{
				
					checkTree($tree[$_VARS[0]]['childs'], $_VARS);
					
					}
				else
					{
					
					page404();
					
					}
				
				}
			
			}
		
		if (  count($_VARS) )
			{
			
			if ( $this->isItem($_VARS[count($_VARS)-1]) )
				{
				
				unset($_VARS[count($_VARS)-1]);
				
				}
			
			if ( isset($tree[$_VARS[0]]) )
				{
			
				checkTree($tree[$_VARS[0]]['childs'], $_VARS);
				
				}
			else
				{
				
				page404();
				
				}
			
			}
		
		}
	
	/**
	 * Определяет по урлу - позиция это или нет
	 * @return boolean
	 */
	public function isItem($var)
		{
		
		return (bool)preg_match('/^(.*)' . $this->itemExt . '$/', $var);
		
		}
	
	/**
	 * В случае, если в урле указан id, но в базе имеется символьный код - происходит редирект
	 * @return string
	 */
	public function checkRedirect ($_VARS)
		{
		
		$action = false;
		
		$this->id2code = assoc('id', $this->id2code);
		
		foreach ($_VARS as &$var)
			{
			
			$this->setObjKey( $var );
			
			if ( $this->getFieldKey() == $this->keyPrimary && isset( $this->id2code[ $this->objKey ] ) )
				{

				$this->setObjKey( $this->id2code[ $this->objKey ]['code'] );

				if ( $this->getFieldKey() == $this->keyUnique )
					{
				
					$action = true;
					$var = $this->objKey;
					
					}
				
				}
			
			}
		
		if ( $action )
			{
			
			go('/' . $this->path . '/' . implode('/', $_VARS) . '/');
			
			}
		
		}
	
	/**
	 * Редирект для позиции
	 * return boolean
	 */
	private function redirectItem ($originKey, $item, $_VARS)
		{
		
		if ( $originKey == $this->keyPrimary && $this->setObjKey( $item[$this->keyUnique] ) !== $this->keyPrimary )
			{
			
			$_VARS[count($_VARS)-1] = $item['code'];
			
			go('/' . $this->path . '/' . implode('/', $_VARS) . $this->itemExt . '/');

			return true;
			
			}
		
		return false;
		
		}
	
	}

?>