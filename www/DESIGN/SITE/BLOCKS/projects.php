<?
$cont = $_CONTENT_;
$_CONTENT_ = '';

require_once( 'catalog.model.php' );

$Catalog = new Catalog();
$Catalog->pathId = $page['path_id'];
$Catalog->path = $page['path'];

$Catalog->tableArticles = 'mp_catalog2_articles';
$Catalog->tableItems = 'mp_catalog2_items';

$tree = $Catalog->getTree();

$onPage = 9;

$cVars = count( $_VARS );
if( $cVars )
{
    $sel_page = 0;
    $art = $item = array();

    foreach( $_VARS as $k => $v )
    {
        if( isset( $tree[ $v ] ) )
        {
            //	если раздел
            $art = $tree[ $v ];
            $tree = $art['childs'];

            $breadCrumbs->addLevel( '/'.$page['path'] . '/' . fullPathArt($art['parent'], 'catalog2_articles', $v) . '/', $art['name'] );
            $_TITLEPAGE = $art['name'].' - '.$_TITLEPAGE;

            if (!empty($art['h1'])) {

                $_TITLEH1 = $art['h1'];
            }

            if (!empty($art['title_page'])) {

                $_TITLEPAGE = $art['title_page'];
            }

            if (!empty($art['meta_description'])) {

                $_META_DESCRIPTION = $art['meta_description'];
            }

            if (!empty($art['meta_keywords'])) {

                $_META_KEYWORDS = $art['meta_keywords'];
            }
        }
        elseif( $k && $k == $cVars-1 && strpos( $v, 'page' ) !== false )
        {
            //	если постраничка (всегда должен быть последним параметром)
            $sel_page = (int)preg_match( '/^page([0-9]+)$/', $v, $m ) ? $m[1] : 0;
            if( !$sel_page )
            {
                page404();
            }
        }
        elseif( !empty( $art ) && $item = $DB->getRow( 'SELECT * FROM '.PRFX.'catalog2_items WHERE code="'.$DB->pre( $v ).'" AND parent="'.(int)$art['id'].'" AND active LIMIT 1' ) )
        {
            //	товар
            if( empty( $item ) )
            {
                page404();
            }
        }
        else
        {
            page404();
        }
    }

    include( BLOCKS_DIR . 'new/projects/articles.php');

    if (empty($item)) {

    } else {
        //	страница товара

        $_TITLEH1 = $item['title'];
        $_TITLEPAGE = $item['title'].' - '.$_TITLEPAGE;
        $breadCrumbs->addLevel( '', $item['title'] );

        if (!empty($item['h1'])) {

            $_TITLEH1 = $item['h1'];
        }

        if (!empty($item['title_page'])) {

            $_TITLEPAGE = $item['title_page'];
        }

        if (!empty($item['meta_description'])) {

            $_META_DESCRIPTION = $item['meta_description'];
        }

        if (!empty($item['meta_keywords'])) {

            $_META_KEYWORDS = $item['meta_keywords'];
        }

        $gallery=unserialize($item['gallery']);

        if (!empty($gallery)){
            ?>
            <div class="thumbnails">
                <?foreach($gallery as $key => $gallimg) { ?>

                    <a class="fancybox" href="<?= $gallimg['path'][1] ?>" rel="gallery1">
                        <img title="<?= $gallimg['text'] ?>" src="<?= $gallimg['path'][0] ?>" />
                    </a>
                    <?
                }
                ?>

            </div>
            <?
        }

        $catLinks = $DB->getAll( 'SELECT * FROM '.PRFX.'catalog_articles WHERE FIND_IN_SET(`id`,"'.$item['catLinks'].'") AND visible ORDER BY `order`, id ');

        if($catLinks){
            ?>
            <div class="catLinks">
                <div class="title">Используемое оборудование</div>
                <ul>
                    <?
                    foreach($catLinks as $catLink){
                        $link='/'.path(65).fullPathArt($catLink['id']);
                        ?>
                        <li>
                            <a class="catLink" href="<?=$link?>"><?=$catLink['name']?></a>
                        </li>
                        <?
                    }
                    ?>
                </ul>
            </div>
        <?}?>
        <br>
        <div class="ind-cont2">
            <div class="cont2">
                <blockquote>
                    <p style="text-align: left;">Получить информацию о стоимости проекта.&nbsp;</p>
                </blockquote>
                <br>
                <div class="textField form1">
                    <?= getForm('itemPrice') ?>
                </div>
            </div>
        </div>
        <?
        echo $item['text'];
    }
}
else
{
    include( BLOCKS_DIR . 'new/projects/index.php');

    echo '<div>' . $cont . '</div>';
}
?>