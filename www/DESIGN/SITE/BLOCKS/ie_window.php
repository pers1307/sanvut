<?
if( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 6' ) || strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7' ) )
{
?>
<style>
#ie_overlay
{
	position:absolute;
	top:0px;
	left:0px;
	width:100%;
	height:100%;
	z-index:100;
	background:#000;
    filter:alpha(opacity=50);
}

#ie_window
{
	position:absolute;
	width:400px;
	background:#fff;
	top:50%;
	left:50%;
	margin:-200px 0px 0px -200px;
	z-index:999;
	padding:10px 20px;
}

#ie_window h3
{
	margin:30px 0px 10px 0px;
	font-size:24px;
	color:#285dbc;
}

#ie_window .text
{
	margin:21px 0px;
	font:14px arial;
	color:#383838;
}

#ie_window .br
{
	height:35px;
	margin-top:5px;
}

#ie_window .br .l
{
	float:left;
	width:31px;
	margin-right:10px;
}
</style>
<div id="ie_overlay" onclick="$('#ie_window').remove(); $('#ie_overlay').remove();"></div>
<div id="ie_window">
	<h3>Вы используете устаревший браузер Internet Exploer</h3>
	<div class="text">
		Internet Exploer не способен корректно отображать большинство сайтов.<br><br>
		Для того, чтобы использовать все возможности сайта, загрузите современную версию браузера:
	</div>
	
	<div class="br"><div class="l"><img src="/DESIGN/SITE/images/ico_firefox.png" alt=""></div><div><a href="http://www.mozilla.com/ru/firefox/">Mozilla Firefox</a></div></div>
	<div class="br"><div class="l"><img src="/DESIGN/SITE/images/ico_crome.png" alt=""></div><div><a href="http://pack.google.com/intl/ru/installer_eula.html?ci_chrome=on&hl=ru">Google Chrome</a></div></div>
	<div class="br"><div class="l"><img src="/DESIGN/SITE/images/ico_opera.png" alt=""></div><div><a href="http://www.opera.com/browser/">Opera</a></div></div>
	<div class="br"><div class="l"><img src="/DESIGN/SITE/images/ico_safari.png" alt=""></div><div><a href="http://www.apple.com/ru/safari/">Apple Safari</a></div></div>
	<div class="br"><div class="l"><img src="/DESIGN/SITE/images/ico_ie.png" alt=""></div><div><a href="http://ie8.msn.com/microsoft/internet-explorer-8/ru-ru/ie8.aspx">Internet Explorer</a></div></div>
</div>
<?
}
?>