<?

$cont = $_CONTENT_;
$_CONTENT_ = '';

//$breadCrumbs->level(1)->setActive(0);

require_once( 'catalog.model.php' );

$Catalog = new Catalog();
$Catalog->pathId = $page['path_id'];
$Catalog->path = $page['path'];

$Catalog->tableArticles = 'mp_catalog_articles';
$Catalog->tableItems = 'mp_catalog_items';


$tree = $Catalog->getTree();

$onPage = 20;

$cVars = count( $_VARS );
if( $cVars )
{
	$sel_page = 0;
	$art = $item = array();

	foreach( $_VARS as $k => $v )
	{
		if( isset( $tree[ $v ] ) )
		{
			//	если раздел
			$art = $tree[ $v ];
			$tree = $art['childs'];

			$breadCrumbs->addLevel( '/'.$page['path'].'/'.fullPathArt( $art['parent'], 'catalog_articles', $v ).'/', $art['name'] );

			if (empty($_TITLEPAGE)) {

                $_TITLEPAGE = $art['name'];
            } else {

                $_TITLEPAGE = $art['name'] . ' - ' . $_TITLEPAGE;
            }
		}
	  elseif( !empty( $art ) and $item = $DB->getRow( 'SELECT * FROM mp_catalog_items WHERE id="'.$DB->pre( $v ).'" AND parent="'.(int)$art['id'].'" AND active="1" LIMIT 1' ) )
		{
			//	товар
			if( empty( $item ) )
			{
				page404();
			}
		}
		elseif( $k && $k == $cVars-1 && strpos( $v, 'page' ) !== false )
		{
			//	если постраничка (всегда должен быть последним параметром)
			$sel_page = (int)preg_match( '/^page([0-9]+)$/', $v, $m ) ? $m[1] : 0;
			if( !$sel_page )
			{
				page404();
			}
		}
		else
		{
			page404();
		}
	}

	if(empty($item)) {
         include( BLOCKS_DIR . 'new/catalog/article.php' );
	} else {
		//	страница товара
		$_TITLEH1 = $item['title'];
		$_TITLEPAGE = $item['title'] . ' - ' . $_TITLEPAGE;
		$breadCrumbs->addLevel( '', $item['title'] );

		$i = $item;

        if ($item['mark'] === '0') {
            $i['img'] = addWatermarkToItem($i['img'], 0);
            $i['img'] = addWatermarkToItem($i['img'], 2);
            $DB->execute("UPDATE `mp_catalog_items` SET `mark` = 1 WHERE `id`=".$item['id']);
        }
        if ($item['mark_photo'] === '0') {
            $i['photo'] = addWatermarkToItem($i['photo'], 0);
            $i['photo'] = addWatermarkToItem($i['photo'], 2);
            $DB->execute("UPDATE `mp_catalog_items` SET `mark_photo` = 1 WHERE `id`=".$item['id']);
        }
    ?>
        <div>
            <div class="item-left-colon">

                <?if(getImageURL($i['img'],2)){
                    ?>
                    <a class="fancybox" href="<?= getImageURL($i['img'],2) ?>" rel="gallery1">
                        <?= getImageHTML($i['img'], 0, $i['title']) ?>
                    </a>
                    <?
                } else {?>
                    <a class="fancybox" href="<?= getImageURL($i['img'],0) ?>" rel="gallery1">
                        <?= getImageHTML($i['img'], 0, $i['title']) ?>
                    </a>
                <? } ?>

                <? if(getImageURL($i['photo'],2)) { ?>
                    <a class="fancybox" href="<?= getImageURL($i['photo'],2) ?>" rel="gallery1">
                        <?= getImageHTML($i['photo'], 0, $i['title']) ?>
                    </a>

                    <? } else if (getImageURL($i['photo'],0)) { ?>

                    <a class="fancybox" href="<?= getImageURL($i['photo'],0) ?>" rel="gallery1">
                        <?= getImageHTML($i['photo'], 0, $i['title']) ?>
                    </a>
                <? }
            ?>
            </div>
            <div class="item-right-colon">
                <?= $i['fulltext']; ?>
            </div>
        </div>
    <?
	}
}
else { ?>

    <? include( BLOCKS_DIR . 'new/catalog/index.php' );?>
<? }

	//Паджинатор титлодескрипшен фикс-костыль.

	if (preg_match('@page(\d+)/@i',$_SERVER['REQUEST_URI'],$m)) {

        $pagi_add = 'Страница '.$m[1].' — ';
	} else {

        $pagi_add = '';
	}

    if (empty($item)) {

        if (!empty($art['h1'])) {

            $_TITLEH1 = $art['h1'];
        }

        if (!empty($art['title_page'])) {

            $_TITLEPAGE = $art['title_page'];
        }

        if (!empty($art['meta_description'])) {

            $_META_DESCRIPTION = $art['meta_description'];
        }

        if (!empty($art['meta_keywords'])) {

            $_META_KEYWORDS = $art['meta_keywords'];
        }
    } else {

        if (!empty($item['h1'])) {

            $_TITLEH1 = $item['h1'];
        }

        if (!empty($item['title_page'])) {

            $_TITLEPAGE = $item['title_page'];
        } else {

            $_TITLEPAGE = $_TITLEH1 . ' купить в Екатеринбурге';
        }

        if (!empty($item['meta_description'])) {

            $_META_DESCRIPTION = $item['meta_description'];
        }

        if (!empty($item['meta_keywords'])) {

            $_META_KEYWORDS = $item['meta_keywords'];
        }
    }
?>