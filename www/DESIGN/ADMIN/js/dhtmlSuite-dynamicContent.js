if(!window.DHTMLSuite)
	var DHTMLSuite = new Object();

DHTMLSuite.dynamicContent=function()
	{
	var enableCache;
	var jsCache;
	var ajaxObjects;
	var waitMessage;

	this.enableCache=false;
	this.jsCache=new Array();
	this.ajaxObjects=new Array();
	this.waitMessage='Идет загрузка, подождите...';
	this.waitImage='dynamic-content/ajax-loader-darkblue.gif';

	try
		{
		if(!standardObjectsCreated)
			DHTMLSuite.createStandardObjects();
		}
	catch(e)
		{
		alert('You need to include the dhtmlSuite-common.js file');
		}

	var objectIndex;
	
	this.objectIndex=DHTMLSuite.vs.arrayDSObjects.length;
	
	DHTMLSuite.vs.arrayDSObjects[this.objectIndex]=this;
	}

DHTMLSuite.dynamicContent.prototype={

	loadContent:function(divId,url,functionToCallOnLoaded)
		{
		var ind=this.objectIndex;
		if(this.enableCache&&this.jsCache[url])
			{
			document.getElementById(divId).innerHTML=this.jsCache[url];
			
			this.__evaluateJs(divId);
			this.__evaluateCss(divId);

			if(functionToCallOnLoaded)
				eval(functionToCallOnLoaded);

			return;
			}

		var ajaxIndex=0;
		var waitMessageToShow='';

		if(this.waitImage)
			{
			waitMessageToShow=waitMessageToShow+'<div style="text-align:center;padding:10px"><img src="'+DHTMLSuite.configObj.imagePath+this.waitImage+'" border="0" alt=""></div>';
			}

		if(this.waitMessage)
			{
			waitMessageToShow=waitMessageToShow+'<div style="text-align:center">'+this.waitMessage+'</div>';
			}

		try
			{
			document.getElementById(divId).innerHTML=waitMessageToShow ;
			}
		catch(e)
			{
			}

		waitMessageToShow=false;
		var ajaxIndex=this.ajaxObjects.length;
		
		try
			{
			this.ajaxObjects[ajaxIndex]=new sack();
			}
		catch(e)
			{
			alert('Could not create ajax object. Please make sure that ajax.js is included');
			}

		if(url.indexOf('?')>=0)
			{
			this.ajaxObjects[ajaxIndex].method='GET';
			
			var string=url.substring(url.indexOf('?'));

			url=url.replace(string,'');
			string=string.replace('?','');
			
			var items=string.split(/&/g);
			
			for(var no=0;no<items.length;no++)
				{
				var tokens=items[no].split('=');

				if(tokens.length==2)
					{
					this.ajaxObjects[ajaxIndex].setVar(tokens[0],tokens[1]);
					}
				}

			url=url.replace(string,'');
			}

		this.ajaxObjects[ajaxIndex].requestFile=url;

		this.ajaxObjects[ajaxIndex].onCompletion=function()
			{ 
			DHTMLSuite.vs.arrayDSObjects[ind].__ajax_showContent(divId,ajaxIndex,url,functionToCallOnLoaded); 
			};

		this.ajaxObjects[ajaxIndex].onError=function()
			{ 
			DHTMLSuite.vs.arrayDSObjects[ind].__ajax_displayError(divId,ajaxIndex,url,functionToCallOnLoaded); 
			};

		this.ajaxObjects[ajaxIndex].runAJAX();
		},
	
	setWaitMessage:function(newWaitMessage)
		{
		this.waitMessage=newWaitMessage;
		},

	setWaitImage:function(newWaitImage)
		{
		this.waitImage=newWaitImage;
		},

	setCache:function(enableCache)
		{
		this.enableCache=enableCache;
		},

	__ajax_showContent :function(divId,ajaxIndex,url,functionToCallOnLoaded)
		{
		document.getElementById(divId).innerHTML='';
		document.getElementById(divId).innerHTML+=this.ajaxObjects[ajaxIndex].response;

		if(this.enableCache)
			{
			this.jsCache[url]=document.getElementById(divId).innerHTML+'';
			}

		this.__evaluateJs(divId);
		this.__evaluateCss(divId);

		if(functionToCallOnLoaded)
			eval(functionToCallOnLoaded);

		this.ajaxObjects[ajaxIndex]=null;
		
		return false;
		},

	__ajax_displayError:function(divId,ajaxIndex,url,functionToCallOnLoaded)
		{
		document.getElementById(divId).innerHTML='<h2>Message from DHTMLSuite.dynamicContent</h2><p>The ajax request for '+url+' failed</p>';
		},

	__evaluateJs:function(obj)
		{					
		var scriptTags=document.getElementById(obj).getElementsByTagName('SCRIPT');
		var string='';
		var jsCode='';
									
		for(var no=0;no<scriptTags.length;no++)
			{			  
			if(scriptTags[no].src)
				{				  
				var head=document.getElementsByTagName("head")[0];
				var scriptObj=document.createElement("script");
				
				scriptObj.setAttribute("type", "text/javascript");
				scriptObj.setAttribute("src", scriptTags[no].src);
				}
			else
				{
				if(DHTMLSuite.clientInfoObj.isOpera)
					{
					jsCode=jsCode+scriptTags[no].text+'\n';
					}
				else
					jsCode=jsCode+scriptTags[no].innerHTML;
				}
			}

		if(jsCode)this.__installScript(jsCode);
		},

	__installScript:function ( script )
		{
		try
			{
			if (!script) return;

			if (window.execScript)
				{
				window.execScript(script)
				}
			else if(window.jQuery&&jQuery.browser.safari)
				{
				window.setTimeout(script,0);
				}
			else
				{
				window.setTimeout( script, 0 );
				}
			}
		catch(e)
			{
			}
		},

	__evaluateCss:function(obj)
		{
		var cssTags=document.getElementById(obj).getElementsByTagName('STYLE');
		var head=document.getElementsByTagName('HEAD')[0];
		
		for(var no=0;no<cssTags.length;no++)
			{
			head.appendChild(cssTags[no]);
			}
		}
	}
