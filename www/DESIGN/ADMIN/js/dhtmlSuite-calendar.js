if(!window.DHTMLSuite)var DHTMLSuite = new Object();
DHTMLSuite.calendarLanguageModel=function(languageCode){
var monthArray;
var monthArrayShort;
var dayArray;
var weekString;
var todayString;
var todayIsString;
var timeString;
this.monthArray=new Array();
this.monthArrayShort=new Array();
this.dayArray=new Array();
if(!languageCode)languageCode='ru';
this.languageCode=languageCode;
this._sCalendarProperties();
}
DHTMLSuite.calendarLanguageModel.prototype={
_sCalendarProperties:function()
{
switch(this.languageCode){
case "ge":	/* German */
this.monthArray=['Januar','Februar','Mдrz','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'];
this.monthArrayShort=['Jan','Feb','Mar','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'];
this.dayArray=['Mon','Die','Mit','Don','Fre','Sam','Son'];
this.weekString='Woche';
this.todayIsString='Heute';
this.todayString='Heute';
this.timeString='';
break;
case "no":	/* Norwegian */
this.monthArray=['Januar','Februar','Mars','April','Mai','Juni','Juli','August','September','Oktober','November','Desember'];
this.monthArrayShort=['Jan','Feb','Mar','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Des'];
this.dayArray=['Man','Tir','Ons','Tor','Fre','L&oslash;r','S&oslash;n'];
this.weekString='Uke';
this.todayIsString='Dagen i dag er';
this.todayString='I dag';
this.timeString='Tid';
break;
case "nl":	/* Dutch */
this.monthArray=['Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'];
this.monthArrayShort=['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Dec'];
this.dayArray=['Ma','Di','Wo','Do','Vr','Za','Zo'];
this.weekString='Week';
this.todayIsString='Vandaag';
this.todayString='Vandaag';
this.timeString='';
break;
case "es": /* Spanish */
this.monthArray=['Enero','Febrero','Marzo','April','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
this.monthArrayShort =['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
this.dayArray=['Lun','Mar','Mie','Jue','Vie','Sab','Dom'];
this.weekString='Semana';
this.todayIsString='Hoy es';
this.todayString='Hoy';
this.timeString='';
break;
case "pt-br":  /* Brazilian portuguese (pt-br)*/
this.monthArray=['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
this.monthArrayShort=['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'];
this.dayArray=['Seg','Ter','Qua','Qui','Sex','S&aacute;b','Dom'];
this.weekString='Sem.';
this.todayIsString='Hoje &eacute;';
this.todayString='Hoje';
this.timeString='';
break;
case "fr":      /* French */
this.monthArray=['Janvier','Fйvrier','Mars','Avril','Mai','Juin','Juillet','Aoыt','Septembre','Octobre','Novembre','Dйcembre'];
this.monthArrayShort=['Jan','Fev','Mar','Avr','Mai','Jun','Jul','Aou','Sep','Oct','Nov','Dec'];
this.dayArray=['Lun','Mar','Mer','Jeu','Ven','Sam','Dim'];
this.weekString='Sem';
this.todayIsString="Aujourd'hui";
this.todayString='Aujourd';
this.timeString='';
break;
case "da": /*Danish*/
this.monthArray=['januar','februar','marts','april','maj','juni','juli','august','september','oktober','november','december'];
this.monthArrayShort=['jan','feb','mar','apr','maj','jun','jul','aug','sep','okt','nov','dec'];
this.dayArray=['man','tirs','ons','tors','fre','l&oslash;r','s&oslash;n'];
this.weekString='Uge';
this.todayIsString='I dag er den';
this.todayString='I dag';
this.timeString='Tid';
break;
case "it":	/* Italian*/
this.monthArray=['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'];
this.monthArrayShort=['Gen','Feb','Mar','Apr','Mag','Giu','Lugl','Ago','Set','Ott','Nov','Dic'];
this.dayArray=['Lun','Mar','Mer','Gio','Ven','Sab','Dom'];
this.weekString='Sett';
this.todayIsString='Oggi &egrave; il';
this.todayString='Oggi &egrave; il';
this.timeString='';
break;
case "sv":	/* Swedish */
this.monthArray=['Januari','Februari','Mars','April','Maj','Juni','Juli','Augusti','September','Oktober','November','December'];
this.monthArrayShort=['Jan','Feb','Mar','Apr','Maj','Jun','Jul','Aug','Sep','Okt','Nov','Dec'];
this.dayArray=['M&aring;n','Tis','Ons','Tor','Fre','L&ouml;r','S&ouml;n'];
this.weekString='Vecka';
this.todayIsString='Idag &auml;r det den';
this.todayString='Idag &auml;r det den';
this.timeString='';
break;
case "ru":
this.monthArray=['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декарь'];
this.monthArrayShort=['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'];
this.dayArray=['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
this.weekString='Неделя';
this.todayIsString='';
this.todayString='Сегодня';
this.timeString='Время';
break;
default:	/* English */
this.monthArray=['January','February','March','April','May','June','July','August','September','October','November','December'];
this.monthArrayShort=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
this.dayArray=['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
this.weekString='Week';
this.todayIsString='';
this.todayString='Today';
this.timeString='Time';
break;
}
}
}
DHTMLSuite.calendarModel=function(inputArray){
var initialDay;
var initialMonth;
var initialYear;
var initialHour;
var initialMinute;
var displayedDay;
var displayedMonth;
var displayedYear;
var displayedMinute;
var displayedHour;
var languageCode;
var languageModel;
var invalidDateRange;
var weekStartsOnMonday;
this.weekStartsOnMonday=true;
this.languageCode='en';
this.invalidDateRange=new Array();
this._cDefaultModel(inputArray);
}
DHTMLSuite.calendarModel.prototype=
{
setCallbackFunctionOnMonthChange:function(functionName)
{
this.callbackFunctionOnMonthChange=functionName;
}
,
addInvalidDateRange:function(fromDateAsArray,toDateAsArray)
{
var index=this.invalidDateRange.length;
this.invalidDateRange[index]=new Array();
if(fromDateAsArray){
fromDateAsArray.day=fromDateAsArray.day+'';
fromDateAsArray.month=fromDateAsArray.month+'';
fromDateAsArray.year=fromDateAsArray.year+'';
if(!fromDateAsArray.month)fromDateAsArray.month=fromDateAsArray.month='1';
if(!fromDateAsArray.day)fromDateAsArray.day=fromDateAsArray.day='1';
if(fromDateAsArray.day.length==1)fromDateAsArray.day='0'+fromDateAsArray.day;
if(fromDateAsArray.month.length==1)fromDateAsArray.month='0'+fromDateAsArray.month;
this.invalidDateRange[index].fromDate=fromDateAsArray.year+fromDateAsArray.month+fromDateAsArray.day;
}else{
this.invalidDateRange[index].fromDate=false;
}
if(toDateAsArray){
toDateAsArray.day=toDateAsArray.day+'';
toDateAsArray.month=toDateAsArray.month+'';
toDateAsArray.year=toDateAsArray.year+'';
if(!toDateAsArray.month)toDateAsArray.month=toDateAsArray.month='1';
if(!toDateAsArray.day)toDateAsArray.day=toDateAsArray.day='1';
if(toDateAsArray.day.length==1)toDateAsArray.day='0'+toDateAsArray.day;
if(toDateAsArray.month.length==1)toDateAsArray.month='0'+toDateAsArray.month;
this.invalidDateRange[index].toDate=toDateAsArray.year+toDateAsArray.month+toDateAsArray.day;
}else{
this.invalidDateRange[index].toDate=false;
}
}
,
isDateWithinValidRange:function(inputDate)
{
if(this.invalidDateRange.length==0)return true;
var month=inputDate.month+'';
if(month.length==1)month='0'+month;
var day=inputDate.day+'';
if(day.length==1)day='0'+day;
var dateToCheck=inputDate.year+month+day;
for(var no=0;no<this.invalidDateRange.length;no++){
if(!this.invalidDateRange[no].fromDate&&this.invalidDateRange[no].toDate>=dateToCheck)return false;
if(!this.invalidDateRange[no].toDate&&this.invalidDateRange[no].fromDate<=dateToCheck)return false;
if(this.invalidDateRange[no].fromDate<=dateToCheck&&this.invalidDateRange[no].toDate>=dateToCheck)return false;
}
return true;
}
,
setInitialDateFromInput:function(inputReference,format)
{
if(inputReference.value.length>0){
if(!format.match(/^[0-9]*?$/gi)){
var items=inputReference.value.split(/[^0-9]/gi);
var positionArray=new Array();
positionArray['m']=format.indexOf('mm');
if(positionArray['m']==-1)positionArray['m']=format.indexOf('m');
positionArray['d']=format.indexOf('dd');
if(positionArray['d']==-1)positionArray['d']=format.indexOf('d');
positionArray['y']=format.indexOf('yyyy');
positionArray['h']=format.indexOf('hh');
positionArray['i']=format.indexOf('ii');
var positionArrayNumeric=Array();
positionArrayNumeric[0]=positionArray['m'];
positionArrayNumeric[1]=positionArray['d'];
positionArrayNumeric[2]=positionArray['y'];
positionArrayNumeric[3]=positionArray['h'];
positionArrayNumeric[4]=positionArray['i'];
positionArrayNumeric=positionArrayNumeric.sort(this.__calendarSortItems);
var itemIndex=-1;
this.initialHour='00';
this.initialMinute='00';
for(var no=0;no<positionArrayNumeric.length;no++){
if(positionArrayNumeric[no]==-1)continue;
itemIndex++;
if(positionArrayNumeric[no]==positionArray['m']){
this.initialMonth=items[itemIndex];
continue;
}
if(positionArrayNumeric[no]==positionArray['y']){
this.initialYear=items[itemIndex];
continue;
}
if(positionArrayNumeric[no]==positionArray['d']){
tmpDay=items[itemIndex];
continue;
}
if(positionArrayNumeric[no]==positionArray['h']){
this.initialHour=items[itemIndex];
continue;
}
if(positionArrayNumeric[no]==positionArray['i']){
this.initialMinute=items[itemIndex];
continue;
}
}
this.initialMonth=this.initialMonth / 1;
tmpDay=tmpDay / 1;
this.initialDay=tmpDay;
}else{
var monthPos=format.indexOf('mm');
this.initialMonth=inputReference.value.substr(monthPos,2)/1;
var yearPos=format.indexOf('yyyy');
this.initialYear=inputReference.value.substr(yearPos,4);
var dayPos=format.indexOf('dd');
tmpDay=inputReference.value.substr(dayPos,2);
this.initialDay=tmpDay;
var hourPos=format.indexOf('hh');
if(hourPos>=0){
tmpHour=inputReference.value.substr(hourPos,2);
this.initialHour=tmpHour;
}else{
this.initialHour='00';
}
var minutePos=format.indexOf('ii');
if(minutePos>=0){
tmpMinute=inputReference.value.substr(minutePos,2);
this.initialMinute=tmpMinute;
}else{
this.initialMinute='00';
}
}
}
this._sDisplayedDateToInitialData();
}
,
_sDisplayedDateToInitialData:function()
{
this.displayedYear=this.initialYear;
this.displayedMonth=this.initialMonth;
this.displayedDay=this.initialDay;
this.displayedHour=this.initialHour;
this.displayedMinute=this.initialMinute;
}
,
__calendarSortItems:function(a,b)
{
return a/1-b/1;
}
,
setWeekStartsOnMonday:function(weekStartsOnMonday)
{
this.weekStartsOnMonday=weekStartsOnMonday;
}
,
setLanguageCode:function(languageCode)
{
this.languageModel=new DHTMLSuite.calendarLanguageModel(languageCode);
}
,
__isLeapYear:function(inputYear)
{
if(inputYear%400==0||(inputYear%4==0&&inputYear%100!=0))return true;
return false;
}
,
getWeekStartsOnMonday:function()
{
return this.weekStartsOnMonday;
}
,
_cDefaultModel:function(inputArray)
{
var d=new Date();
this.initialYear=d.getFullYear();
this.initialMonth=d.getMonth()+1;
this.initialDay=d.getDate();
this.initialHour=d.getHours();
if(inputArray){	/* Initial date data sent to the constructor?*/
if(inputArray.initialYear)this.initialYear=inputArray.initialYear;
if(inputArray.initialMonth)this.initialMonth=inputArray.initialMonth;
if(inputArray.initialDay)this.initialDay=inputArray.initialDay;
if(inputArray.initialHour)this.initialHour=inputArray.initialHour;
if(inputArray.initialMinute)this.initialMinute=inputArray.initialMinute;
if(inputArray.languageCode)this.languageCode=inputArray.languageCode;
}
this.displayedYear=this.initialYear;
this.displayedMonth=this.initialMonth;
this.displayedDay=this.initialDay;
this.displayedHour=this.initialHour;
this.displayedMinute=this.initialMinute;
this.languageModel=new DHTMLSuite.calendarLanguageModel();
}
,
_gDisplayedDay:function()
{
return this.displayedDay;
}
,
_gDisplayedHourWithLeadingZeros:function()
{
var retVal=this._gDisplayedHour()+'';
if(retVal.length==1)retVal='0'+retVal;
return retVal;
}
,
_gDisplayedMinuteWithLeadingZeros:function()
{
var retVal=this._gDisplayedMinute()+'';
if(retVal.length==1)retVal='0'+retVal;
return retVal;
}
,
_gDisplayedDayWithLeadingZeros:function()
{
var retVal=this._gDisplayedDay()+'';
if(retVal.length==1)retVal='0'+retVal;
return retVal;
}
,
_gDisplayedMonthNumberWithLeadingZeros:function()
{
var retVal=this._gDisplayedMonthNumber()+'';
if(retVal.length==1)retVal='0'+retVal;
return retVal;
}
,
_gDisplayedYear:function()
{
return this.displayedYear;
}
,
_gDisplayedHour:function()
{
if(!this.displayedHour)this.displayedHour=0;
return this.displayedHour;
}
,
_gDisplayedMinute:function()
{
if(!this.displayedMinute)this.displayedMinute=0;
return this.displayedMinute;
}
,
_gDisplayedMonthNumber:function()
{
return this.displayedMonth;
}	,
_gInitialDay:function()
{
return this.initialDay;
}
,
_gInitialYear:function()
{
return this.initialYear;
}
,
_gInitialMonthNumber:function()
{
return this.initialMonth;
}
,
_gMonthNameByMonthNumber:function(monthNumber)
{
return this.languageModel.monthArray[monthNumber-1];
}
,
__moveOneYearBack:function()
{
this.displayedYear--;
}
,
__moveOneYearForward:function()
{
this.displayedYear++;
}
,
__moveOneMonthBack:function()
{
this.displayedMonth--;
if(this.displayedMonth<1){
this.displayedMonth=12;
this.displayedYear--;
}
}
,
__moveOneMonthForward:function()
{
this.displayedMonth++;
if(this.displayedMonth>12){
this.displayedMonth=1;
this.displayedYear++;
}
}
,
_sDisplayedYear:function(year)
{
var success=year!=this.displayedYear;
this.displayedYear=year;
return success
}
,
_sDisplayedMonth:function(month)
{
var success=month!=this.displayedMonth;
this.displayedMonth=month;
return success;
}
,
_sDisplayedDay:function(day)
{
this.displayedDay=day;
}
,
_sDisplayedHour:function(hour)
{
this.displayedHour=hour/1;
}
,
_sDisplayedMinute:function(minute)
{
this.displayedMinute=minute/1;
}
,
_gPreviousYearAndMonthAsArray:function()
{
var month=this.displayedMonth-1;
var year=this.displayedYear;
if(month==0){
month=12;
year=year-1;
}
var retArray=[year,month];
return retArray;
}
,
_gNumberOfDaysInCurrentDisplayedMonth:function()
{
return this._gNumberOfDaysInAMonthByMonthAndYear(this.displayedYear,this.displayedMonth);
}
,
_gNumberOfDaysInAMonthByMonthAndYear:function(year,month)
{
var daysInMonthArray=[31,28,31,30,31,30,31,31,30,31,30,31];
var daysInMonth=daysInMonthArray[month-1];
if(daysInMonth==28){
if(this.__isLeapYear(year))daysInMonth=29;
}
return daysInMonth/1;
}
,
_gStringWeek:function()
{
return this.languageModel.weekString;
}
,
_gDaysMondayToSunday:function()
{
return this.languageModel.dayArray;
}
,
_gDaysSundayToSaturday:function()
{
var retArray=this.languageModel.dayArray.concat();
var lastDay=new Array(retArray[retArray.length-1]);
retArray.pop();
return lastDay.concat(retArray);
}
,
_gWeekNumberFromDayMonthAndYear:function(year,month,day)
{
day=day/1;
year=year /1;
month=month/1;
if(!this.weekStartsOnMonday)return this._gWeekNumberFromDayMonthAndYear_S(year,month,day);
var a=Math.floor((14-(month))/12);
var y=year+4800-a;
var m=(month)+(12*a)-3;
var jd=day+Math.floor(((153*m)+2)/5)+
(365*y)+Math.floor(y/4)- Math.floor(y/100)+
Math.floor(y/400)- 32045;
var d4=(jd+31741-(jd%7))%146097%36524%1461;
var L=Math.floor(d4/1460);
var d1=((d4-L)%365)+L;
NumberOfWeek=Math.floor(d1/7)+1;
return NumberOfWeek;
}
,
_gWeekNumberFromDayMonthAndYear_S:function(year,month,day)
{
month--;
now=Date.UTC(year,month,day+1,0,0,0);
var firstDay=new Date();
firstDay.setYear(year);
firstDay.setMonth(0);
firstDay.setDate(1);
then=Date.UTC(year,0,1,0,0,0);
var Compensation=firstDay.getDay();
if (Compensation > 3)Compensation -= 4;
else Compensation += 3;
NumberOfWeek= Math.round((((now-then)/86400000)+Compensation)/7);
return NumberOfWeek;
}
,
_gDayNumberFirstDayInYear:function(year)
{
var d=new Date();
d.setFullYear(year);
d.setDate(1);
d.setMonth(0);
return d.getDay();
}
,
_gRemainingDaysInPreviousMonthAsArray:function()
{
var d=new Date();
d.setFullYear(this.displayedYear);
d.setDate(1);
d.setMonth(this.displayedMonth-1);
var dayStartOfMonth=d.getDay();
if(this.weekStartsOnMonday){
if(dayStartOfMonth==0)dayStartOfMonth=7;
dayStartOfMonth--;
}
var previousMonthArray=this._gPreviousYearAndMonthAsArray();
var daysInPreviousMonth=this._gNumberOfDaysInAMonthByMonthAndYear(previousMonthArray[0],previousMonthArray[1]);
var returnArray=new Array();
for(var no=0;no<dayStartOfMonth;no++){
returnArray[returnArray.length]=daysInPreviousMonth-dayStartOfMonth+no+1;
}
return returnArray;
}
,
_gMonthNames:function()
{
return this.languageModel.monthArray;
}
,
_gTodayAsString:function()
{
return this.languageModel.todayString;
}
,
_gTimeAsString:function()
{
return this.languageModel.timeString;
}
}
DHTMLSuite.calendar=function(propertyArray)
{
var id;
var divElement;
var divElContent;
var divElHeading;
var divElNavBar;
var divElMonthView;
var divElMonthNInHead;
var divElYearInHeading;
var divElBtnPreviousYear;
var divElBtnNextYear;
var divElBtnPrvMonth;
var divElBtnNextMonth;
var divElYearDropdown;
var divElYearDropdownParentYears;
var divElHourDropdownParentHours;
var divElHourDropdown;
var divElMinuteDropdownParent;
var divElMinuteDropdown;
var divElTodayInNavBar;
var divElHrInTimeBar;
var divElMinInTimeBar;
var divElTimeStringInTimeBar;
var iframeEl;
var iframeElDropDowns;
var calendarModelReference;
var objectIndex;
var targetReference;
var layoutCSS;
var isDragable;
var referenceToDragDropObject;
var scrollInYearDropDownActive;
var scrollInHourDropDownActive;
var scrollInMinuteDropDownActive;
var yearDropDownOffsetInYear;
var hourDropDownOffsetInHour;
var minuteDropDownOffsetInHour;
var displayCloseButton;
var displayNavigationBar;
var displayTodaysDateInNavigationBar;
var displayTimeBar;
var posRefToHtmlEl;
var positioningOffsetXInPixels;
var positioningOffsetYInPixels;
var htmlElementReferences;
var minuteDropDownInterval;
var numberOfRowsInMinuteDropDown;
var numberOfRowsInHourDropDown;
var numberOfRowsInYearDropDown;
this.displayTimeBar=false;
this.minuteDropDownInterval=5;
this.htmlElementReferences=new Array();
this.posRefToHtmlEl=false;
this.displayCloseButton=true;
this.displayNavigationBar=true;
this.displayTodaysDateInNavigationBar=true;
this.yearDropDownOffsetInYear=0;
this.hourDropDownOffsetInHour=0;
this.minuteDropDownOffsetInHour=0;
this.minuteDropDownOffsetInMinute=0;
this.layoutCSS='calendar.css';
this.isDragable=false;
this.scrollInYearDropDownActive=false;
this.scrollInHourDropDownActive=false;
this.scrollInMinuteDropDownActive=false;
this.numberOfRowsInMinuteDropDown=10;
this.numberOfRowsInHourDropDown=10;
this.numberOfRowsInYearDropDown=10;
var callbackFunctionOnDayClick;
var callbackFunctionOnClose;
var callbackFunctionOnMonthChange;
var dateOfToday;
this.dateOfToday=new Date();
try{
if(!standardObjectsCreated)DHTMLSuite.createStandardObjects();
}catch(e){
alert('You need to include the dhtmlSuite-common.js file');
}
this.objectIndex=DHTMLSuite.vs.arrayDSObjects.length;
DHTMLSuite.vs.arrayDSObjects[this.objectIndex]=this;
if(propertyArray)this._sInitialData(propertyArray);
}
DHTMLSuite.calendar.prototype=
{
setCallbackFunctionOnDayClick:function(functionName)
{
this.callbackFunctionOnDayClick=functionName;
}
,
setCallbackFunctionOnMonthChange:function(functionName)
{
if(!this.calendarModelReference){
this.calendarModelReference=new DHTMLSuite.calendarModel();
}
this.callbackFunctionOnMonthChange=functionName;
}
,
setCallbackFunctionOnClose:function(functionName)
{
this.callbackFunctionOnClose=functionName;
}
,
setCalendarModelReference:function(calendarModelReference)
{
this.calendarModelReference=calendarModelReference;
}
,
setCalendarPositionByHTMLElement:function(refToHtmlEl,offsetXInPx,offsetYInPx)
{
refToHtmlEl=DHTMLSuite.commonObj.getEl(refToHtmlEl);
this.posRefToHtmlEl=refToHtmlEl;
if(!offsetXInPx)offsetXInPx=0;
if(!offsetYInPx)offsetYInPx=0;
this.positioningOffsetXInPixels=offsetXInPx;
this.positioningOffsetYInPixels=offsetYInPx;
}
,
addHtmlElementReference:function(key,referenceToHtmlEl)
{
referenceToHtmlEl=DHTMLSuite.commonObj.getEl(referenceToHtmlEl);
if(key){
this.htmlElementReferences[key]=referenceToHtmlEl;
}
}
,
getHtmlElementReferences:function()
{
return this.htmlElementReferences;
}
,
setDisplayCloseButton:function(displayCloseButton)
{
this.displayCloseButton=displayCloseButton;
}
,
setTargetReference:function(targetRef)
{
targetRef=DHTMLSuite.commonObj.getEl(targetRef);
this.targetReference=targetRef;
}
,
setIsDragable:function(isDragable){
this.isDragable=isDragable;
}
,
resetViewDisplayedMonth:function()
{
if(!this.divElement)return;
if(!this.calendarModelReference){
this.calendarModelReference=new DHTMLSuite.calendarModel();
}
this.calendarModelReference._sDisplayedDateToInitialData();
this._pCalHeading();
this._pMonthView();
}
,
setLayoutCss:function(nameOfCssFile)
{
this.layoutCSS=nameOfCssFile;
}
,
_i:function()
{
if(!this.divElement){
DHTMLSuite.commonObj.loadCSS(this.layoutCSS);
if(!this.calendarModelReference){
this.calendarModelReference=new DHTMLSuite.calendarModel();
}
this._cMainHtmlEls();
this._cHeadingElements();
this._cNavigationBar();
this._pNavigationBar();
this._pCalHeading();
this._cCalMonthView();
this._pMonthView();
this._cTimeBar();
this._pTimeBar();
this._cDropDownYears();
this._pDropDownYears();
this._posDropDownYears();
this._cDropDownMonth();
this._pDropDownMonths();
this._posDropDownMonths();
this._cDropDownHours();
this._pDropDownHours();
this._posDropDownHours();
this._cDropDownMinutes();
this._pDropDownMinutes();
this._posDropDownMinutes();
this._aEvents();
}else{
this.divElement.style.display='block';
this._pCalHeading();
this._pMonthView();
}
this._rPrimaryiframeEl();
}
,
display:function()
{
if(!this.divElement)this._i();
this._posCalendar();
this.divElement.style.display='block';
}
,
hide:function()
{
if(this._hCalendarCallBack('calendarClose')===false)return false;
this.divElement.style.display='none';
this.divElYearDropdown.style.display='none';
this.divElMonthDropdown.style.display='none';
}
,
isVisible:function()
{
if(!this.divElement)return false;
return this.divElement.style.display=='block'?true:false;
}
,
setInitialDateFromInput:function(inputReference,format)
{
if(!this.calendarModelReference){
this.calendarModelReference=new DHTMLSuite.calendarModel();
}
this.calendarModelReference.setInitialDateFromInput(inputReference,format);
}
,
setDisplayedYear:function(year)
{
var success=this.calendarModelReference._sDisplayedYear(year);
this._pCalHeading();
this._pMonthView();
if(success)this._hCalendarCallBack('monthChange');
}
,
setDisplayedMonth:function(month)
{
var success=this.calendarModelReference._sDisplayedMonth(month);
this._pCalHeading();
this._pMonthView();
if(success)this._hCalendarCallBack('monthChange');
}
,
setDisplayedHour:function(hour)
{
this.calendarModelReference._sDisplayedHour(hour);
this._pTimeBar();
}
,
setDisplayedMinute:function(minute)
{
this.calendarModelReference._sDisplayedMinute(minute);
this._pTimeBar();
}
,
_cDropDownMonth:function()
{
this.divElMonthDropdown=document.createElement('DIV');
this.divElMonthDropdown.style.display='none';
this.divElMonthDropdown.className='DHTMLSuite_calendar_monthDropDown';
document.body.appendChild(this.divElMonthDropdown);
}
,
_pDropDownMonths:function()
{
this.divElMonthDropdown.innerHTML='';
var ind=this.objectIndex;
var months=this.calendarModelReference._gMonthNames();
for(var no=0;no<months.length;no++){
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDownAMonth';
if((no+1)==this.calendarModelReference._gDisplayedMonthNumber())div.className='DHTMLSuite_calendar_yearDropDownCurrentMonth';
div.innerHTML=months[no];
div.id='DHTMLSuite_calendarMonthPicker'+(no+1);
div.onmouseover=this._moverMonthInDropDown;
div.onmouseout=this._moutMonthInDropDown;
div.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._sMonthFromDropdown(e); }
this.divElMonthDropdown.appendChild(div);
DHTMLSuite.commonObj._aEEl(div);
}
}
,
_cDropDownYears:function()
{
this.divElYearDropdown=document.createElement('DIV');
this.divElYearDropdown.style.display='none';
this.divElYearDropdown.className='DHTMLSuite_calendar_yearDropDown';
document.body.appendChild(this.divElYearDropdown);
}
,
_cDropDownHours:function()
{
this.divElHourDropdown=document.createElement('DIV');
this.divElHourDropdown.style.display='none';
this.divElHourDropdown.className='DHTMLSuite_calendar_hourDropDown';
document.body.appendChild(this.divElHourDropdown);
}
,
_cDropDownMinutes:function()
{
this.divElMinuteDropdown=document.createElement('DIV');
this.divElMinuteDropdown.style.display='none';
this.divElMinuteDropdown.className='DHTMLSuite_calendar_minuteDropDown';
document.body.appendChild(this.divElMinuteDropdown);
}
,
_pDropDownMinutes:function()
{
var ind=this.objectIndex;
this.divElMinuteDropdown.innerHTML='';
var divPrevious=document.createElement('DIV');
divPrevious.className='DHTMLSuite_calendar_dropDown_arrowUp';
divPrevious.onmouseover=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moverUpAndDownArrowsInDropDownMinutes(e); } ;
divPrevious.onmouseout =function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moutUpAndDownArrowsInDropDownMinutes(e); } ;
this.divElMinuteDropdown.appendChild(divPrevious);
DHTMLSuite.commonObj._aEEl(divPrevious);
this.divElMinuteDropdownParent=document.createElement('DIV');
this.divElMinuteDropdown.appendChild(this.divElMinuteDropdownParent);
this._pMinutesInsideDropDownMinutes(this.divElMinuteDropdownParent);
var divNext=document.createElement('DIV');
divNext.className='DHTMLSuite_calendar_dropDown_arrowDown';
divNext.innerHTML='<span></span>';
divNext.onmouseover=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moverUpAndDownArrowsInDropDownMinutes(e); } ;
divNext.onmouseout =function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moutUpAndDownArrowsInDropDownMinutes(e); } ;
DHTMLSuite.commonObj._aEEl(divNext);
this.divElMinuteDropdown.appendChild(divNext);
if(60 / this.minuteDropDownInterval	< this.numberOfRowsInMinuteDropDown){
divPrevious.style.display='none';
divNext.style.display='none';
}
}
,
_pMinutesInsideDropDownMinutes:function()
{
var ind=this.objectIndex;
this.divElMinuteDropdownParent.innerHTML='';
if(60 / this.minuteDropDownInterval	< this.numberOfRowsInMinuteDropDown){
startMinute=0;
}else{
var startMinute=Math.max(0,(this.calendarModelReference._gDisplayedMinute()-Math.round(this.numberOfRowsInMinuteDropDown/2)));
startMinute+=(this.minuteDropDownOffsetInMinute*this.minuteDropDownInterval)
if(startMinute<0){	/* Start minute negative-adjust it and change offset value */
startMinute+=this.minuteDropDownInterval;
this.minuteDropDownOffsetInMinute++;
}
if(startMinute+(this.numberOfRowsInMinuteDropDown*this.minuteDropDownInterval)>60){	/* start minute in drop down+number of records shown*interval larger than 60 -> adjust it */
startMinute-=this.minuteDropDownInterval;
this.minuteDropDownOffsetInMinute--;
}
}
for(var no=startMinute;no<Math.min(60,startMinute+this.numberOfRowsInMinuteDropDown*(this.minuteDropDownInterval));no+=this.minuteDropDownInterval){
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDownAMinute';
if(no==this.calendarModelReference._gDisplayedMinute())div.className='DHTMLSuite_calendar_minuteDropDownCurrentMinute';
var prefix="";
if(no<10)prefix="0";
div.innerHTML=prefix+no;
div.onmouseover=this._moverMinuteInDropDown;
div.onmouseout=this._moutMinuteInDropDown;
div.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._sMinuteFromDropdown(e); }
this.divElMinuteDropdownParent.appendChild(div);
DHTMLSuite.commonObj._aEEl(div);
}
}
,
_pDropDownHours:function()
{
var ind=this.objectIndex;
this.divElHourDropdown.innerHTML='';
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDown_arrowUp';
div.onmouseover=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moverUpAndDownArrowsInDropDownHours(e); } ;
div.onmouseout =function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moutUpAndDownArrowsInDropDownHours(e); } ;
this.divElHourDropdown.appendChild(div);
DHTMLSuite.commonObj._aEEl(div);
this.divElHourDropdownParentHours=document.createElement('DIV');
this.divElHourDropdown.appendChild(this.divElHourDropdownParentHours);
this._pHoursInsideDropDownHours(this.divElHourDropdownParentHours);
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDown_arrowDown';
div.innerHTML='<span></span>';
div.onmouseover=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moverUpAndDownArrowsInDropDownHours(e); } ;
div.onmouseout =function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moutUpAndDownArrowsInDropDownHours(e); } ;
DHTMLSuite.commonObj._aEEl(div);
this.divElHourDropdown.appendChild(div);
}
,
_pHoursInsideDropDownHours:function()
{
var ind=this.objectIndex;
this.divElHourDropdownParentHours.innerHTML='';
var startHour=Math.max(0,(this.calendarModelReference._gDisplayedHour()-Math.round(this.numberOfRowsInHourDropDown/2)));
startHour=Math.min(14,startHour);
if((startHour+this.hourDropDownOffsetInHour+this.numberOfRowsInHourDropDown)>24){
this.hourDropDownOffsetInHour=(24-startHour-this.numberOfRowsInHourDropDown);
}
if((startHour+this.hourDropDownOffsetInHour)<0){
this.hourDropDownOffsetInHour=startHour*-1;
}
startHour+=this.hourDropDownOffsetInHour;
if(startHour<0)startHour=0;
if(startHour>(24-this.numberOfRowsInHourDropDown))startHour=(24-this.numberOfRowsInHourDropDown);
for(var no=startHour;no<startHour+this.numberOfRowsInHourDropDown;no++){
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDownAnHour';
if(no==this.calendarModelReference._gDisplayedHour())div.className='DHTMLSuite_calendar_hourDropDownCurrentHour';
var prefix="";
if(no<10)prefix="0";
div.innerHTML=prefix+no;
div.onmouseover=this._moverHourInDropDown;
div.onmouseout=this._moutHourInDropDown;
div.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._sHourFromDropdown(e); }
this.divElHourDropdownParentHours.appendChild(div);
DHTMLSuite.commonObj._aEEl(div);
}
}
,
_pDropDownYears:function()
{
var ind=this.objectIndex;
this.divElYearDropdown.innerHTML='';
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDown_arrowUp';
div.onmouseover=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moverUpAndDownArrowsInDropDownYears(e); } ;
div.onmouseout =function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moutUpAndDownArrowsInDropDownYears(e); } ;
this.divElYearDropdown.appendChild(div);
DHTMLSuite.commonObj._aEEl(div);
this.divElYearDropdownParentYears=document.createElement('DIV');
this.divElYearDropdown.appendChild(this.divElYearDropdownParentYears);
this._pYearsInsideDropDownYears(this.divElYearDropdownParentYears);
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDown_arrowDown';
div.innerHTML='<span></span>';
div.onmouseover=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moverUpAndDownArrowsInDropDownYears(e); } ;
div.onmouseout =function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._moutUpAndDownArrowsInDropDownYears(e); } ;
DHTMLSuite.commonObj._aEEl(div);
this.divElYearDropdown.appendChild(div);
}
,
_pYearsInsideDropDownYears:function(divElementToPopulate)
{
var ind=this.objectIndex;
this.divElYearDropdownParentYears.innerHTML='';
var startYear=this.calendarModelReference._gDisplayedYear()-5+this.yearDropDownOffsetInYear;
for(var no=startYear;no<startYear+this.numberOfRowsInYearDropDown;no++){
var div=document.createElement('DIV');
div.className='DHTMLSuite_calendar_dropDownAYear';
if(no==this.calendarModelReference._gDisplayedYear())div.className='DHTMLSuite_calendar_yearDropDownCurrentYear';
div.innerHTML=no;
div.onmouseover=this._moverYearInDropDown;
div.onmouseout=this._moutYearInDropDown;
div.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._sYearFromDropdown(e); }
this.divElYearDropdownParentYears.appendChild(div);
DHTMLSuite.commonObj._aEEl(div);
}
}
,
_posDropDownMonths:function()
{
this.divElMonthDropdown.style.left=DHTMLSuite.commonObj.getLeftPos(this.divElMonthNInHead)+'px';
this.divElMonthDropdown.style.top=(DHTMLSuite.commonObj.getTopPos(this.divElMonthNInHead)+this.divElMonthNInHead.offsetHeight)+'px';
if(this.iframeElDropDowns){
this.iframeElDropDowns.style.left=this.divElMonthDropdown.style.left;
this.iframeElDropDowns.style.top=this.divElMonthDropdown.style.top;
this.iframeElDropDowns.style.width=(this.divElMonthDropdown.clientWidth)+'px';
this.iframeElDropDowns.style.height=this.divElMonthDropdown.clientHeight+'px';
this.iframeElDropDowns.style.display=this.divElMonthDropdown.style.display;
}
}
,
_posDropDownYears:function()
{
this.divElYearDropdown.style.left=DHTMLSuite.commonObj.getLeftPos(this.divElYearInHeading)+'px';
this.divElYearDropdown.style.top=(DHTMLSuite.commonObj.getTopPos(this.divElYearInHeading)+this.divElYearInHeading.offsetHeight)+'px';
if(this.iframeElDropDowns){
this.iframeElDropDowns.style.left=this.divElYearDropdown.style.left;
this.iframeElDropDowns.style.top=this.divElYearDropdown.style.top;
this.iframeElDropDowns.style.width=(this.divElYearDropdown.clientWidth)+'px';
this.iframeElDropDowns.style.height=this.divElYearDropdown.clientHeight+'px';
this.iframeElDropDowns.style.display=this.divElYearDropdown.style.display;
}
}
,
_posDropDownHours:function()
{
this.divElHourDropdown.style.left=DHTMLSuite.commonObj.getLeftPos(this.divElHrInTimeBar)+'px';
this.divElHourDropdown.style.top=(DHTMLSuite.commonObj.getTopPos(this.divElHrInTimeBar)+this.divElHrInTimeBar.offsetHeight)+'px';
if(this.iframeElDropDowns){
this.iframeElDropDowns.style.left=this.divElHourDropdown.style.left;
this.iframeElDropDowns.style.top=this.divElHourDropdown.style.top;
this.iframeElDropDowns.style.width=(this.divElHourDropdown.clientWidth)+'px';
this.iframeElDropDowns.style.height=this.divElHourDropdown.clientHeight+'px';
this.iframeElDropDowns.style.display=this.divElHourDropdown.style.display;
}
}
,
_posDropDownMinutes:function()
{
this.divElMinuteDropdown.style.left=DHTMLSuite.commonObj.getLeftPos(this.divElMinInTimeBar)+'px';
this.divElMinuteDropdown.style.top=(DHTMLSuite.commonObj.getTopPos(this.divElMinInTimeBar)+this.divElMinInTimeBar.offsetHeight)+'px';
if(this.iframeElDropDowns){
this.iframeElDropDowns.style.left=this.divElMinuteDropdown.style.left;
this.iframeElDropDowns.style.top=this.divElMinuteDropdown.style.top;
this.iframeElDropDowns.style.width=(this.divElMinuteDropdown.clientWidth)+'px';
this.iframeElDropDowns.style.height=this.divElMinuteDropdown.clientHeight+'px';
this.iframeElDropDowns.style.display=this.divElMinuteDropdown.style.display;
}
}
,
_sMonthFromDropdown:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
this._shHideDropDownBoxMonth();
this.setDisplayedMonth(src.id.replace(/[^0-9]/gi,''));
}
,
_sYearFromDropdown:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
this._shHideDropDownBoxYear();
this.setDisplayedYear(src.innerHTML);
}
,
_sHourFromDropdown:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
this._shHideDropDownBoxHour();
this.setDisplayedHour(src.innerHTML);
}
,
_sMinuteFromDropdown:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
this._shHideDropDownBoxMinute();
this.setDisplayedMinute(src.innerHTML);
}
,
__autoHideDropDownBoxes:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
if(src.className.indexOf('MonthAndYear')>=0||src.className.indexOf('HourAndMinute')>=0){
if(DHTMLSuite.commonObj.isObjectClicked(this.divElement,e))return;
}
this._shHideDropDownBoxMonth('none');
this._shHideDropDownBoxYear('none');
this._shHideDropDownBoxHour('none');
this._shHideDropDownBoxMinute('none');
}
,
_shHideDropDownBoxMonth:function(forcedDisplayAttribute)
{
if(!forcedDisplayAttribute){
this._shHideDropDownBoxYear('none');
this._shHideDropDownBoxHour('none');
}
if(forcedDisplayAttribute){
this.divElMonthDropdown.style.display=forcedDisplayAttribute;
}else{
this.divElMonthDropdown.style.display=(this.divElMonthDropdown.style.display=='block'?'none':'block');
}
this._pDropDownMonths();
this._posDropDownMonths();
}
,
_shHideDropDownBoxYear:function(forcedDisplayAttribute)
{
if(!forcedDisplayAttribute){
this._shHideDropDownBoxMonth('none');
this._shHideDropDownBoxHour('none');
this._shHideDropDownBoxMinute('none');
}
if(forcedDisplayAttribute){
this.divElYearDropdown.style.display=forcedDisplayAttribute;
}else{
this.divElYearDropdown.style.display=(this.divElYearDropdown.style.display=='block'?'none':'block');
}
if(this.divElYearDropdown.style.display=='none' ){
this.yearDropDownOffsetInYear=0;
}else{
this._pDropDownYears();
}
this._posDropDownYears();
}
,
_shHideDropDownBoxHour:function(forcedDisplayAttribute)
{
if(!forcedDisplayAttribute){
this._shHideDropDownBoxYear('none');
this._shHideDropDownBoxMonth('none');
this._shHideDropDownBoxMinute('none');
}
if(forcedDisplayAttribute){
this.divElHourDropdown.style.display=forcedDisplayAttribute;
}else{
this.divElHourDropdown.style.display=(this.divElHourDropdown.style.display=='block'?'none':'block');
}
if(this.divElHourDropdown.style.display=='none' ){
this.hourDropDownOffsetInHour=0;
}else{
this._pDropDownHours();
}
this._posDropDownHours();
}
,
_shHideDropDownBoxMinute:function(forcedDisplayAttribute)
{
if(!forcedDisplayAttribute){
this._shHideDropDownBoxYear('none');
this._shHideDropDownBoxMonth('none');
this._shHideDropDownBoxHour('none');
}
if(forcedDisplayAttribute){
this.divElMinuteDropdown.style.display=forcedDisplayAttribute;
}else{
this.divElMinuteDropdown.style.display=(this.divElMinuteDropdown.style.display=='block'?'none':'block');
}
if(this.divElMinuteDropdown.style.display=='none' ){
this.minuteDropDownOffsetInMinute=0;
}else{
this._pDropDownMinutes();
}
this._posDropDownMinutes();
}
,
_cMainHtmlEls:function()
{
this.divElement=document.createElement('DIV');
this.divElement.className='DHTMLSuite_calendar';
this.divElContent=document.createElement('DIV');
this.divElement.appendChild(this.divElContent);
this.divElContent.className='DHTMLSuite_calendarContent';
if(this.targetReference)this.targetReference.appendChild(this.divElement);else document.body.appendChild(this.divElement);
if(this.isDragable){
try{
this.referenceToDragDropObject=new DHTMLSuite.dragDropSimple({ elementReference: this.divElement });
}catch(e){
alert('You need to include DHTMLSuite-dragDropSimple.js for the drag feature');
}
}
if(DHTMLSuite.clientInfoObj.isMSIE&&DHTMLSuite.clientInfoObj.navigatorVersion<8){
this.iframeEl=document.createElement('<iframe src="about:blank" frameborder="0">');
this.iframeEl.className='DHTMLSuite_calendar_iframe';
this.divElement.appendChild(this.iframeEl);
this.iframeElDropDowns=document.createElement('<iframe src="about:blank" frameborder="0">');
this.iframeElDropDowns.className='DHTMLSuite_calendar_iframe';
this.iframeElDropDowns.style.display='none';
document.body.appendChild(this.iframeElDropDowns);
}
}
,
_cHeadingElements:function()
{
this.divElHeading=document.createElement('DIV');
if(this.isDragable){	/* Calendar is dragable */
this.referenceToDragDropObject.addDragHandle(this.divElHeading);
this.divElHeading.style.cursor='move';
}
this.divElHeading.className='DHTMLSuite_calendarHeading';
this.divElContent.appendChild(this.divElHeading);
this.divElHeading.style.position='relative';
this.divElClose=document.createElement('DIV');
this.divElClose.className='DHTMLSuite_calendarCloseButton';
this.divElHeading.appendChild(this.divElClose);
if(!this.displayCloseButton)this.divElClose.style.display='none';
this.divElHeadingTxt=document.createElement('DIV');
this.divElHeadingTxt.className='DHTMLSuite_calendarHeadingTxt';
if(DHTMLSuite.clientInfoObj.isMSIE){
var table=document.createElement('<TABLE cellpadding="0" cellspacing="0" border="0">');
}else{
var table=document.createElement('TABLE');
table.setAttribute('cellpadding',0);
table.setAttribute('cellspacing',0);
table.setAttribute('border',0);
}
table.style.margin='0 auto';
var tbody=document.createElement('TBODY');
table.appendChild(tbody);
this.divElHeadingTxt.appendChild(table);
var row=tbody.insertRow(0);
var cell=row.insertCell(-1);
this.divElMonthNInHead=document.createElement('DIV');
this.divElMonthNInHead.className='DHTMLSuite_calendarHeaderMonthAndYear';
cell.appendChild(this.divElMonthNInHead);
var cell=row.insertCell(-1);
var span=document.createElement('SPAN');
span.innerHTML=', ';
cell.appendChild(span);
var cell=row.insertCell(-1);
this.divElYearInHeading=document.createElement('DIV');
this.divElYearInHeading.className='DHTMLSuite_calendarHeaderMonthAndYear';
cell.appendChild(this.divElYearInHeading);
this.divElHeading.appendChild(this.divElHeadingTxt);
}
,
_cNavigationBar:function()
{
this.divElNavBar=document.createElement('DIV');
this.divElNavBar.className='DHTMLSuite_calendar_navigationBar';
this.divElContent.appendChild(this.divElNavBar);
this.divElBtnPreviousYear=document.createElement('DIV');
this.divElBtnPreviousYear.className='DHTMLSuite_calendar_btnPreviousYear';
this.divElBtnPreviousYear.innerHTML='<span></span>';
this.divElNavBar.appendChild(this.divElBtnPreviousYear);
this.divElBtnNextYear=document.createElement('DIV');
this.divElBtnNextYear.className='DHTMLSuite_calendar_btnNextYear';
this.divElBtnNextYear.innerHTML='<span></span>';
this.divElNavBar.appendChild(this.divElBtnNextYear);
this.divElBtnPrvMonth=document.createElement('DIV');
this.divElBtnPrvMonth.className='DHTMLSuite_calendar_btnPreviousMonth';
this.divElBtnPrvMonth.innerHTML='<span></span>';
this.divElNavBar.appendChild(this.divElBtnPrvMonth);
this.divElBtnNextMonth=document.createElement('DIV');
this.divElBtnNextMonth.className='DHTMLSuite_calendar_btnNextMonth';
this.divElBtnNextMonth.innerHTML='<span></span>';
this.divElNavBar.appendChild(this.divElBtnNextMonth);
this.divElTodayInNavBar=document.createElement('DIV');
this.divElTodayInNavBar.className='DHTMLSuite_calendar_navigationBarToday';
this.divElNavBar.appendChild(this.divElTodayInNavBar);
if(!this.displayNavigationBar)this.divElNavBar.style.display='none';
if(!this.displayTodaysDateInNavigationBar)this.divElTodayInNavBar.style.display='none';
}
,
_pNavigationBar:function()
{
var ind=this.objectIndex;
this.divElTodayInNavBar.innerHTML='';
var span=document.createElement('SPAN');
span.innerHTML=this.calendarModelReference._gTodayAsString();
span.onclick=function(){ DHTMLSuite.vs.arrayDSObjects[ind].__displayMonthOfToday(); }
this.divElTodayInNavBar.appendChild(span);
DHTMLSuite.commonObj._aEEl(span);
}
,
_cCalMonthView:function()
{
this.divElMonthView=document.createElement('DIV');
this.divElMonthView.className='DHTMLSuite_calendar_monthView';
this.divElContent.appendChild(this.divElMonthView);
}
,
_pMonthView:function()
{
var ind=this.objectIndex;
this.divElMonthView.innerHTML='';
var modelRef=this.calendarModelReference;
if(DHTMLSuite.clientInfoObj.isMSIE){
var table=document.createElement('<TABLE cellpadding="1" cellspacing="0" border="0" width="100%">');
}else{
var table=document.createElement('TABLE');
table.setAttribute('cellpadding',1);
table.setAttribute('cellspacing',0);
table.setAttribute('border',0);
table.width='100%';
}
var tbody=document.createElement('TBODY');
table.appendChild(tbody);
this.divElMonthView.appendChild(table);
var row=tbody.insertRow(-1);
row.className='DHTMLSuite_calendar_monthView_headerRow';
var cell=row.insertCell(-1);
cell.className='DHTMLSuite_calendar_monthView_firstColumn';
cell.innerHTML=modelRef._gStringWeek();
if(modelRef.getWeekStartsOnMonday()){
var days=modelRef._gDaysMondayToSunday();
}else{
var days=modelRef._gDaysSundayToSaturday();
}
for(var no=0;no<days.length;no++){
var cell=row.insertCell(-1);
cell.innerHTML=days[no];
cell.className='DHTMLSuite_calendar_monthView_headerCell';
if(modelRef.getWeekStartsOnMonday()&&no==6){
cell.className='DHTMLSuite_calendar_monthView_headerSunday';
}
if(!modelRef.getWeekStartsOnMonday()&&no==0){
cell.className='DHTMLSuite_calendar_monthView_headerSunday';
}
}
var row=tbody.insertRow(-1);
var cell=row.insertCell(-1);
cell.className='DHTMLSuite_calendar_monthView_firstColumn';
var week=modelRef._gWeekNumberFromDayMonthAndYear(modelRef._gDisplayedYear(),modelRef._gDisplayedMonthNumber(),1);
cell.innerHTML=week>0?week:53;
var daysRemainingInPreviousMonth=modelRef._gRemainingDaysInPreviousMonthAsArray();
for(var no=0;no<daysRemainingInPreviousMonth.length;no++){
var cell=row.insertCell(-1);
cell.innerHTML=daysRemainingInPreviousMonth[no];
cell.className='DHTMLSuite_calendar_monthView_daysInOtherMonths';
}
var daysInCurrentMonth=modelRef._gNumberOfDaysInCurrentDisplayedMonth();
var cellCounter=daysRemainingInPreviousMonth.length+1;
for(var no=1;no<=daysInCurrentMonth;no++){
var cell=row.insertCell(-1);
cell.innerHTML=no;
cell.className='DHTMLSuite_calendar_monthView_daysInThisMonth';
DHTMLSuite.commonObj._aEEl(cell);
if(cellCounter%7==0&&modelRef.getWeekStartsOnMonday()){
cell.className='DHTMLSuite_calendar_monthView_sundayInThisMonth';
}
if(cellCounter%7==1&&!modelRef.getWeekStartsOnMonday()){
cell.className='DHTMLSuite_calendar_monthView_sundayInThisMonth';
}
if(no==modelRef._gInitialDay()&&modelRef._gDisplayedYear()==modelRef._gInitialYear()&&modelRef._gDisplayedMonthNumber()==modelRef._gInitialMonthNumber()){
cell.className='DHTMLSuite_calendar_monthView_initialDate';
}
if(!modelRef.isDateWithinValidRange({year:modelRef._gDisplayedYear(),month:modelRef._gDisplayedMonthNumber(),day:no})){
cell.className='DHTMLSuite_calendar_monthView_invalidDate';
}else{
cell.onmousedown=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._mdownOnDayInCalendar(e); }
cell.onmouseover=this._moverCalendarDay;
cell.onmouseout=this._moutCalendarDay;
DHTMLSuite.commonObj._aEEl(cell);
}
if(no==this.dateOfToday.getDate()&&modelRef._gDisplayedYear()==this.dateOfToday.getFullYear()&&modelRef._gDisplayedMonthNumber()==(this.dateOfToday.getMonth()+1)){
cell.className='DHTMLSuite_calendar_monthView_currentDate';
}
if(cellCounter%7==0&&no<daysInCurrentMonth){
var row=tbody.insertRow(-1);
var cell=row.insertCell(-1)
cell.className='DHTMLSuite_calendar_monthView_firstColumn';
week++
cell.innerHTML=week;
}
cellCounter++;
}
if((cellCounter-1)%7>0){
var dayCounter=1;
for(var no=(cellCounter-1)%7;no<7;no++){
var cell=row.insertCell(-1);
cell.innerHTML=dayCounter;
cell.className='DHTMLSuite_calendar_monthView_daysInOtherMonths';
dayCounter++;
}
}
}
,
_cTimeBar:function()
{
this.divElTimeBar=document.createElement('DIV');
this.divElTimeBar.className='DHTMLSuite_calendar_timeBar';
this.divElContent.appendChild(this.divElTimeBar);
if(DHTMLSuite.clientInfoObj.isMSIE){
var table=document.createElement('<TABLE cellpadding="0" cellspacing="0" border="0">');
}else{
var table=document.createElement('TABLE');
table.setAttribute('cellpadding',0);
table.setAttribute('cellspacing',0);
table.setAttribute('border',0);
}
table.style.margin='0 auto';
this.divElTimeBar.appendChild(table);
var row=table.insertRow(0);
var cell=row.insertCell(-1);
this.divElHrInTimeBar=document.createElement('DIV');
this.divElHrInTimeBar.className='DHTMLSuite_calendar_timeBarHourAndMinute';
cell.appendChild(this.divElHrInTimeBar);
var cell=row.insertCell(-1);
var span=document.createElement('SPAN');
span.innerHTML=':';
cell.appendChild(span);
var cell=row.insertCell(-1);
this.divElMinInTimeBar=document.createElement('DIV');
this.divElMinInTimeBar.className='DHTMLSuite_calendar_timeBarHourAndMinute';
cell.appendChild(this.divElMinInTimeBar);
this.divElTimeStringInTimeBar=document.createElement('DIV');
this.divElTimeStringInTimeBar.className='DHTMLSuite_calendarTimeBarTimeString';
this.divElTimeBar.appendChild(this.divElTimeStringInTimeBar);
if(!this.displayTimeBar)this.divElTimeBar.style.display='none';
}
,
_pTimeBar:function()
{
this.divElHrInTimeBar.innerHTML=this.calendarModelReference._gDisplayedHourWithLeadingZeros();
this.divElMinInTimeBar.innerHTML=this.calendarModelReference._gDisplayedMinuteWithLeadingZeros();
this.divElTimeStringInTimeBar.innerHTML=this.calendarModelReference._gTimeAsString()+':';
}
,
_pCalHeading:function()
{
this.divElMonthNInHead.innerHTML=this.calendarModelReference._gMonthNameByMonthNumber(this.calendarModelReference._gDisplayedMonthNumber());
this.divElYearInHeading.innerHTML=this.calendarModelReference._gDisplayedYear();
}
,
_mdownOnDayInCalendar:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
this.calendarModelReference._sDisplayedDay(src.innerHTML);
this._hCalendarCallBack('dayClick');
}
,
_hCalendarCallBack:function(action)
{
var callbackString='';
switch(action){
case 'dayClick':
if(this.callbackFunctionOnDayClick)callbackString=this.callbackFunctionOnDayClick;
break;
case "monthChange":
if(this.callbackFunctionOnMonthChange)callbackString=this.callbackFunctionOnMonthChange;
break;
case "calendarClose":
if(this.callbackFunctionOnClose)callbackString=this.callbackFunctionOnClose;
break;
}
if(callbackString){
callbackString=callbackString+
'({'
+ ' year:'+this.calendarModelReference._gDisplayedYear()
+ ',month:"'+this.calendarModelReference._gDisplayedMonthNumberWithLeadingZeros()+'"'
+ ',day:"'+this.calendarModelReference._gDisplayedDayWithLeadingZeros()+'"'
+ ',hour:"'+this.calendarModelReference._gDisplayedHourWithLeadingZeros()+'"'
+ ',minute:"'+this.calendarModelReference._gDisplayedMinuteWithLeadingZeros()+'"'
+ ',calendarRef:this'
callbackString=callbackString+'})';
}
if(callbackString)return this.__evaluateCallBackString(callbackString);
}
,
__evaluateCallBackString:function(callbackString)
{
try{
return eval(callbackString);
}catch(e){
alert('Could not excute call back function '+callbackString+'\n'+e.message);
}
}
,
__displayMonthOfToday:function()
{
var d=new Date();
var month=d.getMonth()+1;
var year=d.getFullYear();
this.setDisplayedYear(year);
this.setDisplayedMonth(month);
}
,
__moveOneYearBack:function()
{
this.calendarModelReference.__moveOneYearBack();
this._pCalHeading();
this._pMonthView();
this._hCalendarCallBack('monthChange');
}
,
__moveOneYearForward:function()
{
this.calendarModelReference.__moveOneYearForward();
this._pCalHeading();
this._pMonthView();
this._hCalendarCallBack('monthChange');
}
,
__moveOneMonthBack:function()
{
this.calendarModelReference.__moveOneMonthBack();
this._pCalHeading();
this._pMonthView();
this._hCalendarCallBack('monthChange');
}
,
__moveOneMonthForward:function()
{
this.calendarModelReference.__moveOneMonthForward();
this._pCalHeading();
this._pMonthView();
this._hCalendarCallBack('monthChange');
}
,
_aEvents:function()
{
var ind=this.objectIndex;
this.divElClose.onmouseover=this._moverCalendarButton;
this.divElClose.onmouseout=this._moutCalendarButton;
this.divElClose.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind].hide(); }
DHTMLSuite.commonObj._aEEl(this.divElClose);
this.divElBtnPreviousYear.onmouseover=this._moverCalendarButton;
this.divElBtnPreviousYear.onmouseout=this._moutCalendarButton;
this.divElBtnPreviousYear.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind].__moveOneYearBack(); }
DHTMLSuite.commonObj._aEEl(this.divElBtnPreviousYear);
this.divElBtnNextYear.onmouseover=this._moverCalendarButton;
this.divElBtnNextYear.onmouseout=this._moutCalendarButton;
this.divElBtnNextYear.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind].__moveOneYearForward(); }
DHTMLSuite.commonObj._aEEl(this.divElBtnNextYear);
this.divElBtnPrvMonth.onmouseover=this._moverCalendarButton;
this.divElBtnPrvMonth.onmouseout=this._moutCalendarButton;
this.divElBtnPrvMonth.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind].__moveOneMonthBack(); }
DHTMLSuite.commonObj._aEEl(this.divElBtnPrvMonth);
this.divElBtnNextMonth.onmouseover=this._moverCalendarButton;
this.divElBtnNextMonth.onmouseout=this._moutCalendarButton;
this.divElBtnNextMonth.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind].__moveOneMonthForward(); }
DHTMLSuite.commonObj._aEEl(this.divElBtnNextMonth);
this.divElYearInHeading.onmouseover=this._moverMonthAndYear;
this.divElYearInHeading.onmouseout=this._moutMonthAndYear;
this.divElYearInHeading.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._shHideDropDownBoxYear(); }
DHTMLSuite.commonObj._aEEl(this.divElYearInHeading);
this.divElMonthNInHead.onmouseover=this._moverMonthAndYear;
this.divElMonthNInHead.onmouseout=this._moutMonthAndYear;
this.divElMonthNInHead.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._shHideDropDownBoxMonth(); }
DHTMLSuite.commonObj._aEEl(this.divElMonthNInHead);
this.divElHrInTimeBar.onmouseover=this._moverHourAndMinute;
this.divElHrInTimeBar.onmouseout=this._moutHourAndMinute;
this.divElHrInTimeBar.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._shHideDropDownBoxHour(); }
DHTMLSuite.commonObj._aEEl(this.divElHrInTimeBar);
this.divElMinInTimeBar.onmouseover=this._moverHourAndMinute;
this.divElMinInTimeBar.onmouseout=this._moutHourAndMinute;
this.divElMinInTimeBar.onclick=function(e){ DHTMLSuite.vs.arrayDSObjects[ind]._shHideDropDownBoxMinute(); }
DHTMLSuite.commonObj._aEEl(this.divElMinInTimeBar);
this.divElHeading.onselectstart=function(){ return false; };
DHTMLSuite.commonObj._aEEl(this.divElHeading);
DHTMLSuite.commonObj.addEvent(document.documentElement,'click',function(e){ DHTMLSuite.vs.arrayDSObjects[ind].__autoHideDropDownBoxes(e); },ind+'');
}
,
_rPrimaryiframeEl:function()
{
if(!this.iframeEl)return;
this.iframeEl.style.width=this.divElement.clientWidth+'px';
this.iframeEl.style.height=this.divElement.clientHeight+'px';
}
,
__scrollInYearDropDown:function(scrollDirection)
{
if(!this.scrollInYearDropDownActive)return;
var ind=this.objectIndex;
this.yearDropDownOffsetInYear+=scrollDirection;
this._pYearsInsideDropDownYears();
setTimeout('DHTMLSuite.vs.arrayDSObjects['+ind+'].__scrollInYearDropDown('+scrollDirection+')',150);
}
,
_moverUpAndDownArrowsInDropDownYears:function(e)
{
var ind=this.objectIndex;
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
var scrollDirection=(src.className.toLowerCase().indexOf('up')>=0?-1:1);
src.className=src.className+' DHTMLSuite_calendarDropDown_dropDownArrowOver';
this.scrollInYearDropDownActive=true;
setTimeout('DHTMLSuite.vs.arrayDSObjects['+ind+'].__scrollInYearDropDown('+scrollDirection+')',100);
}
,
_moutUpAndDownArrowsInDropDownYears:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
src.className=src.className.replace(' DHTMLSuite_calendarDropDown_dropDownArrowOver','');
this.scrollInYearDropDownActive=false;
}
,
__scrollInHourDropDown:function(scrollDirection)
{
if(!this.scrollInHourDropDownActive)return;
var ind=this.objectIndex;
this.hourDropDownOffsetInHour+=scrollDirection;
this._pHoursInsideDropDownHours();
setTimeout('DHTMLSuite.vs.arrayDSObjects['+ind+'].__scrollInHourDropDown('+scrollDirection+')',150);
}
,
_moverUpAndDownArrowsInDropDownHours:function(e)
{
var ind=this.objectIndex;
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
var scrollDirection=(src.className.toLowerCase().indexOf('up')>=0?-1:1);
src.className=src.className+' DHTMLSuite_calendarDropDown_dropDownArrowOver';
this.scrollInHourDropDownActive=true;
setTimeout('DHTMLSuite.vs.arrayDSObjects['+ind+'].__scrollInHourDropDown('+scrollDirection+')',100);
}
,
_moutUpAndDownArrowsInDropDownHours:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
src.className=src.className.replace(' DHTMLSuite_calendarDropDown_dropDownArrowOver','');
this.scrollInHourDropDownActive=false;
}
,
__scrollInMinuteDropDown:function(scrollDirection)
{
if(!this.scrollInMinuteDropDownActive)return;
var ind=this.objectIndex;
this.minuteDropDownOffsetInMinute+=scrollDirection;
this._pMinutesInsideDropDownMinutes();
setTimeout('DHTMLSuite.vs.arrayDSObjects['+ind+'].__scrollInMinuteDropDown('+scrollDirection+')',150);
}
,
_moverUpAndDownArrowsInDropDownMinutes:function(e)
{
var ind=this.objectIndex;
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
var scrollDirection=(src.className.toLowerCase().indexOf('up')>=0?-1:1);
src.className=src.className+' DHTMLSuite_calendarDropDown_dropDownArrowOver';
this.scrollInMinuteDropDownActive=true;
setTimeout('DHTMLSuite.vs.arrayDSObjects['+ind+'].__scrollInMinuteDropDown('+scrollDirection+')',100);
}
,
_moutUpAndDownArrowsInDropDownMinutes:function(e)
{
if(document.all)e=event;
var src=DHTMLSuite.commonObj.getSrcElement(e);
src.className=src.className.replace(' DHTMLSuite_calendarDropDown_dropDownArrowOver','');
this.scrollInMinuteDropDownActive=false;
}
,
_moverYearInDropDown:function()
{
this.className=this.className+' DHTMLSuite_calendar_dropdownAYearOver';
}
,
_moutYearInDropDown:function()
{
this.className=this.className.replace(' DHTMLSuite_calendar_dropdownAYearOver','');
}
,
_moverHourInDropDown:function()
{
this.className=this.className+' DHTMLSuite_calendar_dropdownAnHourOver';
}
,
_moutHourInDropDown:function()
{
this.className=this.className.replace(' DHTMLSuite_calendar_dropdownAnHourOver','');
}
,
_moverMinuteInDropDown:function()
{
this.className=this.className+' DHTMLSuite_calendar_dropdownAMinuteOver';
}
,
_moutMinuteInDropDown:function()
{
this.className=this.className.replace(' DHTMLSuite_calendar_dropdownAMinuteOver','');
}
,
_moverMonthInDropDown:function()
{
this.className=this.className+' DHTMLSuite_calendar_dropdownAMonthOver';
}
,
_moutMonthInDropDown:function()
{
this.className=this.className.replace(' DHTMLSuite_calendar_dropdownAMonthOver','');
}
,
_moverCalendarDay:function()
{
this.className=this.className+' DHTMLSuite_calendarDayOver';
}
,
_moutCalendarDay:function()
{
this.className=this.className.replace(' DHTMLSuite_calendarDayOver','');
}
,
_moverCalendarButton:function()
{
this.className=this.className+' DHTMLSuite_calendarButtonOver';
}
,
_moutCalendarButton:function()
{
this.className=this.className.replace(' DHTMLSuite_calendarButtonOver','');
}
,
_moverMonthAndYear:function()
{
this.className=this.className+' DHTMLSuite_calendarHeaderMonthAndYearOver';
}
,
_moutMonthAndYear:function()
{
this.className=this.className.replace(' DHTMLSuite_calendarHeaderMonthAndYearOver','');
}	,
_moverHourAndMinute:function()
{
this.className=this.className+' DHTMLSuite_calendarTimeBarHourAndMinuteOver';
}
,
_moutHourAndMinute:function()
{
this.className=this.className.replace(' DHTMLSuite_calendarTimeBarHourAndMinuteOver','');
}
,
_posCalendar:function()
{
if(!this.posRefToHtmlEl)return;
if(this.divElement.parentNode!=document.body)document.body.appendChild(this.divElement);
this.divElement.style.position='absolute';
this.divElement.style.left=(DHTMLSuite.commonObj.getLeftPos(this.posRefToHtmlEl)+this.positioningOffsetXInPixels)+'px';
this.divElement.style.top=(DHTMLSuite.commonObj.getTopPos(this.posRefToHtmlEl)+this.positioningOffsetYInPixels)+'px';
}
,
_sInitialData:function(props)
{
if(props.id)this.id=props.id;
if(props.targetReference)this.targetReference=props.targetReference;
if(props.calendarModelReference)this.calendarModelReference=props.calendarModelReference;
if(props.callbackFunctionOnDayClick)this.callbackFunctionOnDayClick=props.callbackFunctionOnDayClick;
if(props.callbackFunctionOnMonthChange)this.callbackFunctionOnMonthChange=props.callbackFunctionOnMonthChange;
if(props.callbackFunctionOnClose)this.callbackFunctionOnClose=props.callbackFunctionOnClose;
if(props.displayCloseButton||props.displayCloseButton===false)this.displayCloseButton=props.displayCloseButton;
if(props.displayNavigationBar||props.displayNavigationBar===false)this.displayNavigationBar=props.displayNavigationBar;
if(props.displayTodaysDateInNavigationBar||props.displayTodaysDateInNavigationBar===false)this.displayTodaysDateInNavigationBar=props.displayTodaysDateInNavigationBar;
if(props.minuteDropDownInterval)this.minuteDropDownInterval=props.minuteDropDownInterval;
if(props.numberOfRowsInHourDropDown)this.numberOfRowsInHourDropDown=props.numberOfRowsInHourDropDown;
if(props.numberOfRowsInMinuteDropDown)this.numberOfRowsInHourDropDown=props.numberOfRowsInMinuteDropDown;
if(props.numberOfRowsInYearDropDown)this.numberOfRowsInYearDropDown=props.numberOfRowsInYearDropDown;
if(props.isDragable||props.isDragable===false)this.isDragable=props.isDragable;
if(props.displayTimeBar||props.displayTimeBar===false)this.displayTimeBar=props.displayTimeBar;
}
}
