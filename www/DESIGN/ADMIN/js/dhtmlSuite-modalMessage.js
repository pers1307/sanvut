if(!window.DHTMLSuite)var DHTMLSuite = new Object();

var move_obj='';

var windowMinSize = [400,300];	
var moveCounter = -1;	
var startEventPos = new Array();
var startPosWindow = new Array();
var startWindowSize = new Array();
var initResizeCounter = -1;	
var activeWindow = false;
var activeWindowShadow = false;	
var windowSizeArray = new Array();
var windowPositionArray = new Array();
var currentZIndex = 9000;
var windowStateArray = new Array();	// Minimized or maximized
var activeWindowIframe = false;
var divCounter = 0;
var zIndexSet = false;
var MSIEWIN = (navigator.userAgent.indexOf('MSIE')>=0 && navigator.userAgent.indexOf('Win')>=0 && navigator.userAgent.toLowerCase().indexOf('opera')<0)?true:false;
var opera = navigator.userAgent.toLowerCase().indexOf('opera')>=0?true:false;

function cancelEvent()
	{
	return (moveCounter==-1 && initResizeCounter==-1)?true:false;
	}

function switchElement(e,inputElement)
	{
	if(!inputElement)inputElement = this;

	activeWindow = document.getElementById(move_obj + 'level2');	
	activeWindowShadow = document.getElementById(move_obj + 'level3');	
	}

DHTMLSuite.modalMessage=function(name){
	var url;
	var htmlOfModalMessage;
	var divs_transparentDiv;
	var divs_content;
	var iframeEl;
	var layoutCss;
	var width;
	var height;
	var existingBodyOverFlowStyle;
	var dynContentObj;
	var cssClassOfMessageBox;
	var shadowDivVisible;
	var shadowOffset;
	var objectIndex;

	var modalDialogName;
	var contentId;
	var button_str;
	var backContentId;



	this.url='';
	this.htmlOfModalMessage='';
	this.layoutCss='modal-message.css';
	this.height=200;
	this.width=400;
	this.cssClassOfMessageBox=false;
	this.shadowDivVisible=true;
	this.shadowOffset=5;
	
	this.modalDialogName = name;
	this.contentId = name+'_content';
	this.button_str='';
	this.backContentId='';

	try
		{
		if(!standardObjectsCreated)
			DHTMLSuite.createStandardObjects();
		}
	catch(e)
		{
		alert('You need to include the dhtmlSuite-common.js file');
		}

	this.objectIndex=DHTMLSuite.vs.arrayDSObjects.length;

	DHTMLSuite.vs.arrayDSObjects[this.objectIndex]=this;

	var ind=this.objectIndex;

	DHTMLSuite.commonObj.addEvent(
		window,"resize",function()
			{ 
			DHTMLSuite.vs.arrayDSObjects[ind]._rTransparentDiv(); 
			}
		);
	}

DHTMLSuite.modalMessage.prototype=
	{
	initWindow:function()
		{
		var obj = document.getElementById(this.modalDialogName+'caption');

		var idWin = this.modalDialogName;
		}
	,

	setButtons:function(buttons)
		{
		if (this.button_str=="")
			{
			var obj_buttons_cont = document.getElementById(this.modalDialogName+'buttons_cont');
			var obj_close_button = document.getElementById(this.modalDialogName+'close_button');

			if (obj_buttons_cont && obj_close_button)
				{
				obj_close_button.setAttribute("valign","top");

				for(var ii=0; ii<buttons.length; ii++)
					{
					var elem = document.createElement("TD");
					elem.setAttribute("valign","top");
					elem.setAttribute("width","195");
					elem.innerHTML = '<a class="button" href="#" style="float:right" onclick="'+buttons[ii][0]+'">'+buttons[ii][1]+'</a>';

					obj_buttons_cont.insertBefore(elem, obj_close_button);
					}				   
				}

			this.button_str=buttons;
			}
		}
	,

	setSource:function(urlOfSource)
		{
		this.url=urlOfSource;
		}
	,

	setHtmlContent:function(newHtmlContent)
		{
		this.htmlOfModalMessage=newHtmlContent;
		}
	,

	setSize:function(width,height)
		{
		if(width)this.width=width;
		if(height)this.height=height;
		}
	,

	setCssClassMessageBox:function(newCssClass)
		{
		this.cssClassOfMessageBox=newCssClass;
		if(this.divs_content)
			{
			if(this.cssClassOfMessageBox)
				this.divs_content.className=this.cssClassOfMessageBox;
			else
				this.divs_content.className='modalDialog_contentDiv';
			}
		}
	,

	setShadowOffset:function(newShadowOffset)
		{
		this.shadowOffset=newShadowOffset
		}
	,

	setWaitMessage:function(newMessage)
		{
		if(!this.dynContentObj)
			{
			try
				{
				this.dynContentObj=new DHTMLSuite.dynamicContent();
				}
			catch(e)
				{
				alert('You need to include dhtmlSuite-dynamicContent.js');
				}
			}
		
		this.dynContentObj.setWaitMessage(newMessage);
		}
	,

	setWaitImage:function(newImage)
		{
		if(!this.dynContentObj)
			{
			try
				{
				this.dynContentObj=new DHTMLSuite.dynamicContent();
				}
			catch(e)
				{
				alert('You need to include dhtmlSuite-dynamicContent.js');
				}
			}
		
		this.dynContentObj.setWaitImage(newImage);
		}
	,

	setCache:function(cacheStatus)
		{
		if(!this.dynContentObj)
			{
			try
				{
				this.dynContentObj=new DHTMLSuite.dynamicContent();
				}
			catch(e)
				{
				alert('You need to include dhtmlSuite-dynamicContent.js');
				}
			}
		
		this.dynContentObj.setCache(cacheStatus);
		}
	,

	display:function()
		{
		var ind=this.objectIndex;

		if(!this.divs_transparentDiv)
			{
			DHTMLSuite.commonObj.loadCSS(this.layoutCss);
			this._cDivElements();
			}

		this._rAndPositionDivElements();
		this.divs_transparentDiv.style.display='block';
		this.divs_content.style.display='block';
		this.divs_shadow.style.display='block';
		
		if(this.iframeEl){
			setTimeout('DHTMLSuite.vs.arrayDSObjects['+ind+'].iframeEl.style.display="block"',150);
		}

		this._rAndPositionDivElements();
		
		window.refToThisModalBoxObj=this;
		
		setTimeout('window.refToThisModalBoxObj._rAndPositionDivElements()',100);

		this.initWindow();

		this._aHTMLContent();
		}
	,



	setShadowDivVisible:function(visible){
		this.shadowDivVisible=visible;
	}
	,

	close:function(obj){
		document.documentElement.style.overflow='';
	
		this.divs_transparentDiv.style.display='none';
		this.divs_content.style.display='none';
		this.divs_shadow.style.display='none';
	
		if(this.iframeEl)this.iframeEl.style.display='none';
	}
	,

	closeDialog:function(name)
		{

		}
	,

	_cDivElements:function()
		{
		this.divs_transparentDiv=document.createElement('DIV');
		this.divs_transparentDiv.className='DHTMLSuite_modalDialog_transparentDivs';
		this.divs_transparentDiv.style.left='0px';
		this.divs_transparentDiv.style.top='0px';	
		this.divs_transparentDiv.id=this.modalDialogName+'level1';
		document.body.appendChild(this.divs_transparentDiv);

		
		this.divs_content=document.createElement('DIV');
		this.divs_content.className='DHTMLSuite_modalDialog_contentDiv';
		this.divs_content.id=this.modalDialogName+'level2';
		this.divs_content.style.cssText +='overflow-x:hidden; overflow-y:auto;';
		contentHeight=this.height-62;
		this.divs_content.innerHTML='\
			<div class="mp_dialog" id="'+this.modalDialogName+'caption" style="height:20px;">\
				<a href="#" onclick="closeCMSDialog(\''+this.modalDialogName+'\');">&nbsp;</a>\
			</div>\
			<div id="'+this.contentId+'" valign="top" style="overflow-x:hidden; overflow-y:auto;height:'+(contentHeight)+'px;width:100%; position:relative">&nbsp;</div>\
			<div style="height:35px;padding-top:5px;">\
				<table style="width:100%; height: 30px;" cellspacing="0" cellpadding="0" border="0">\
					<tr id="'+this.modalDialogName+'buttons_cont'+'">\
						<td>&nbsp;</td>\
						<td id="'+this.modalDialogName+'close_button'+'" width="60">\
							<a class="cancel" href="#" onclick="closeCMSDialog(\''+this.modalDialogName+'\');">Закрыть</a>\
						</td>\
					</tr>\
				</table>\
			</div>\
		';
		
		document.body.appendChild(this.divs_content);

		this.divs_shadow=document.createElement('DIV');
		this.divs_shadow.className='DHTMLSuite_modalDialog_contentDiv_shadow';
		this.divs_shadow.id=this.modalDialogName+'level3';
		document.body.appendChild(this.divs_shadow);
//
//		if(DHTMLSuite.clientInfoObj.isMSIE){
//			this.iframeEl=document.createElement('<iframe frameborder=0 src="about:blank" scrolling="no">');
//			this.iframeEl.style.filter='alpha(opacity=0)';
//			this.iframeEl.style.cssText='filter:alpha(opacity=0)';
//			this.iframeEl.style.position='absolute';
//			this.iframeEl.style.zIndex=990;
//			this.iframeEl.style.display='none';
//			this.iframeEl.style.left='0px';
//			this.iframeEl.style.top='0px';
//			this.iframeEl.id=this.modalDialogName+'level4';
//
//			document.body.appendChild(this.iframeEl);
//		}

		//var tmp = document.getElementById(this.modalDialogName+'caption');

		}
	,

	_rAndPositionDivElements:function()
		{
		var topOffset=Math.max(document.body.scrollTop,document.documentElement.scrollTop);

		if(this.cssClassOfMessageBox)
			this.divs_content.className=this.cssClassOfMessageBox;
		else
			this.divs_content.className='DHTMLSuite_modalDialog_contentDiv';

		if(!this.divs_transparentDiv)return;

		document.documentElement.style.overflow='auto';
//		document.documentElement.style.cssText +='overflow-x:hidden; overflow-y:auto;';

		var bodyWidth=DHTMLSuite.clientInfoObj.getBrowserWidth();
		var bodyHeight=DHTMLSuite.clientInfoObj.getBrowserHeight();

		this.divs_content.style.width=this.width+'px';
		this.divs_content.style.height= this.height+'px';

		var tmpWidth=this.divs_content.offsetWidth;
		var tmpHeight=this.divs_content.offsetHeight;

		this.divs_content.style.left=Math.ceil((bodyWidth-tmpWidth)/ 2)+'px';;
		this.divs_content.style.top=(Math.ceil((bodyHeight-tmpHeight)/ 2)+ topOffset)+'px';
		this.divs_shadow.style.left=(this.divs_content.style.left.replace('px','')/1+this.shadowOffset)+'px';
		this.divs_shadow.style.top=(this.divs_content.style.top.replace('px','')/1+this.shadowOffset)+'px';
		this.divs_shadow.style.height=tmpHeight+'px';
		this.divs_shadow.style.width=tmpWidth+'px';
		
		if(!this.shadowDivVisible)this.divs_shadow.style.display='none';
		
		this._rTransparentDiv();
		}
	,

	_rTransparentDiv:function()
		{
		if(!this.divs_transparentDiv)return;
		
		var divHeight=DHTMLSuite.clientInfoObj.getBrowserHeight();
		var divWidth=DHTMLSuite.clientInfoObj.getBrowserWidth();
		
		this.divs_transparentDiv.style.height=$('.body_table').height();//divHeight +'px';
		this.divs_transparentDiv.style.width='100%';

		if(this.iframeEl)
			{
			this.iframeEl.style.width=this.divs_transparentDiv.style.width;
			this.iframeEl.style.height=this.divs_transparentDiv.style.height;
			}
	}
	,

	_aHTMLContent:function()
		{
		if(!this.dynContentObj)
			{
			try
				{
				this.dynContentObj=new DHTMLSuite.dynamicContent();
				}
			catch(e)
				{
				alert('You need to include dhtmlSuite-dynamicContent.js');
				}
			}
		
		if(this.url){
			this.dynContentObj.loadContent(this.contentId,this.url);
		} else {
			var dialogHTMLContent_tpl = '\
				<table style="width:100%; height: 100%;" cellspacing="0" cellpadding="0" border="0">\
					<tr style="height: 18px;">\
						<td>\
							<div class="mp_dialog">\
								<a href="#" onclick="closeCMSDialog(\''+this.modalDialogName+'\');">&nbsp;</a>\
							</div>\
						</td>\
					</tr>\
					<tr>\
						<td id="'+this.contentId+'" valign="top">'+this.htmlOfModalMessage+'</td>\
					</tr>\
					<tr style="height: 40px;">\
						<td aligin="right" style="padding-top: 5px; border-top: 2px solid black;">\
							<table style="width:100%; height: 100%;" cellspacing="0" cellpadding="0" border="0">\
								<tr id="'+this.modalDialogName+'buttons_cont'+'">\
									<td>&nbsp;</td>\
									<td id="'+this.modalDialogName+'close_button'+'" width="60">\
										<a class="cancel" href="#" onclick="closeCMSDialog(\''+this.modalDialogName+'\');">Закрыть</a>\
									</td>\
								</tr>\
							</table>\
						</td>\
					</tr>\
				</table>';

			this.divs_content.innerHTML=dialogHTMLContent_tpl;
			}
		}
	}

//document.documentElement.onmouseup = stopMove;	
//document.documentElement.onmousemove = moveWindow;
//document.documentElement.ondragstart = cancelEvent;
//document.documentElement.onselectstart = cancelEvent;