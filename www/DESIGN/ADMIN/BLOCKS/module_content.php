<?php
global $_VARS;



/** Подключаем класс административного меню */
if (!isset ($adminmenu)) { 
    require_once CORE_DIR.'classes/admin_menu.php';
    global $adminmenu;
    $adminmenu= new Admin_Menu();
    $adminmenu->_makeMenu();
}

if (isset($_VARS[0])){	
	$admin_file= MODULES_DIR.EndSlash($_VARS[0]).'admin.php';

	if (is_file($admin_file))
	    require_once ($admin_file);

	unset($vars);
}

?>