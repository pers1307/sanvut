<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?=$_TITLEPAGE?></title>
	<link rel="stylesheet" href="/DESIGN/ADMIN/css/index.css" media="screen" type="text/css">
	<link rel="stylesheet" href="/DESIGN/ADMIN/css/module_config.css" media="screen" type="text/css">
	<link rel="stylesheet" href="/DESIGN/ADMIN/css/explorer.css" media="screen" type="text/css">
	<script type="text/javascript">
	var ROOT_PLACE = '<?=ROOT_PLACE;?>';
	</script>
	<script type="text/javascript" src="/DESIGN/ADMIN/js/mce/tiny_mce.js"></script>
	<script type="text/javascript" src="/DESIGN/ADMIN/js/mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
	<script type="text/javascript" src="/DESIGN/ADMIN/js/functions.js"></script>
	<script type="text/javascript" src="/DESIGN/ADMIN/js/ajax.js"></script>
	<script type="text/javascript" src="/DESIGN/ADMIN/js/dhtmlSuite-common.js"></script>
	<script type="text/javascript" src="/DESIGN/ADMIN/js/dhtmlSuite-modalMessage.js"></script>
	<script type="text/javascript" src="/DESIGN/ADMIN/js/dhtmlSuite-dynamicContent.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script> 
	<script type="text/javascript" src="/DESIGN/ADMIN/js/JsHttpRequest.js"></script>
</head>
<body onselectstart="return false">
<table class="body_table" cellspacing="0" cellpadding="0" border="0">

	<?if (!is_writable(DOC_ROOT.'UPLOAD') || !is_writable(DOC_ROOT.'TMP')){?>
		<tr style="height: 32px;">
			<Td style="padding: 5px;">
				<div><span style="color: red"><b>Внимание:</b></span> Директории TMP и UPLOAD, а также другие под-директории в них, должны иметь права 777 (rwx-rwx-rwx)</div>
			</td>
		</tr>
	<?}?>

	<?
	
	if( $iniMbstring = @ini_get_all('mbstring') ){
	
		if( $iniMbstring['mbstring.func_overload']['local_value'] < 2 || $iniMbstring['mbstring.internal_encoding']['local_value'] != 'UTF-8' ){
		
			?>
			<tr style="height: 32px;">
				<Td style="padding: 5px;">
					<div><span style="color: red"><b>Внимание:</b></span> Установите значения следующих параметров: <strong>mbstring.func_overload >= 2</strong>, <strong>mbstring.internal_encoding = UTF-8</strong></div>
				</td>
			</tr>
			<?
		
		}
	
	}else{
		
	?>
			<tr style="height: 32px;">
				<Td style="padding: 5px;">
					<div><span style="color: red"><b>Внимание:</b></span> Установите расширение <strong>mbstring</strong></div>
				</td>
			</tr>
	<?
		
	}
	
	?>

	<tr>
		<td class="content">
		
			<table class="top" cellspacing="0" cellpadding="0">


				<?if (!$auth_isROOT || 1){?>
				<tr class="admin_top">
					<td>

						<table class="info" cellpadding="0" cellspacing="0">
							<tr>
								<td class="col1" style="padding:10px;"><?=(LOGO ? '<img src="/DESIGN/ADMIN/images/'.LOGO.'">' : '&nbsp;');?></td>	
								<td class="col2" style="vertical-align:bottom; padding-bottom:20px;">
								</td>
								<td class="col3">&nbsp;</td>	
							</tr>
						</table>
						
					</td>
				</tr>
				<?}?>

				<tr>
					<td valign="top">

						<table style="width: 100%; height: 100%" border="0" cellspacing="0" cellpadding="0">							
							<tr style="height:22px;">
								<td class="header_tree">Управление сайтом<?/* <Span id="tree_zone" style="display: none;"></span>*/?></td>
								<td class="header_content">	   
									<span id="title_page_cont">Контент зона</span><?/* <Span id="content_zone" style="display: none;"></span>*/?>
								</td>
							</tr>	
							<tr>
								<td valign="top" id="westContent" align="center" <?/*onClick="setActiveZone('tree');"*/?>>
									<div><img src="/DESIGN/ADMIN/images/blank.gif" width="300" height="1" border="0" alt=""></div>
									<div id="tree_content" style="width: 100%;"><?=$_TREE_?></div>
								</td>
								<td valign="top" id="center" bgcolor="#EDEFEF" style="border-left: 1px solid gray; padding: 5px;"<?/* onClick="setActiveZone('page');"*/?>>&nbsp;</td>
							</tr>		 
						</table>

					</td>
				</tr>
			</table>
		
		</td>
	</tr>
</table>
<div id="ajax_working" style="position: absolute"><br><br><br><br><br><b>Пожалуйста подождите</b><br><br><a href="#" onClick="closeAjaxWorking();">Закрыть</a></div>
</body>
</html>