<?
header('HTTP/1.x 404 Not Found')
?>

<!DOCTYPE html>
<html>
<? include( BLOCKS_DIR . 'new/common/head.php' );?>
<body>
<div class="rootWrap">
    <? include( BLOCKS_DIR . 'new/common/header.php' );?>

    <main>
        <div class="centerWrap">
            <div class="seoText">


                <? if(isset($_TITLEH1)): ?>

                    <h1><?=$_TITLEH1?></h1>
                <? else: ?>

                    <h1>Страница не найдена</h1>
                <? endif; ?>



                <h2>ВОЗМОЖНЫЕ ПРИЧИНЫ ОШИБКИ</h2>

                Сервер не смог найти страницы по тому адресу, что вы запрашивали. Это могло произойти по следующим причинам:

                <ul>
                    <li>Её здесь никогда небыло;</li>
                    <li>Она была, но кто-то её удалил или переименовал;</li>
                    <li>Возможно вы опечатались вводя адрес.</li>
                </ul>

                <i>Вы можете перейти на <a href="/">главную страницу сайта</a></i>

            </div>
        </div>
    </main>

    <? include( BLOCKS_DIR . 'new/common/footer.php' );?>
</div>
<?= showJs() ?>
</body>
</html>
