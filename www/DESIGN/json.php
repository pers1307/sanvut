<?

if ( isset($_SERVER['HTTP_X_REQUESTED_WITH']) ){

	header('Content-Type:application/json; charset=utf-8');
	
	$out = array();
	$action = getParam(0);
	
	switch ($action) {
		
		case 'example':
		
			$out['elementId'] = array(
						
					'Если вы не знаете как получить максимальный эффект от рекламы — заполните заявку на рассчет рекламной кампании и наши специалисты предложат вам самый эффективный вариант.', // Content
						
					'rewrite' // rewrite || before || after
					
				);
				
		break;

	case 'GetCall':

	$out['callback'][] = "$('.modal-window-wrap').removeClass('hidden_text');";
	
	break;
	
	case 'call':

        $_err = array();
        
        $fields = array(
        						
			 'name' => array(
                'name' => 'Ваше имя',
                'oblig' => 0,
				'mes' => 'Вы не указали ваше имя',
				
            ),
					
            'phone' => array(
                'name' => 'Телефон',
                'oblig' => 1,
                'regex' => '/^(\+)?([()\d ]+)?[\d- ]{5,}+$/',
				'mes'=> 'Вы не указали телефон',
				
            ),
            'where' => array(
                'name' => 'Вопрос',
                'oblig' => 0,
				'mes'=> 'Вы не указали время когда перезвонить',
            ),
				
    					

			
					
        );
        
       if(isset($_REQUEST['feedbacks']))
		{
               
            foreach($fields as $k=>$v)
			{
                if(empty($_REQUEST['c'.$k]) && $v['oblig'])
                    $_err[$k] = $v['mes'];
                if(!empty($_REQUEST['c'.$k]) && !empty($v['regex']) && !preg_match($v['regex'],$_REQUEST['c'.$k]))
                    $_err[$k] = 'Неверно указан телефон';
					
				 if(!empty($_REQUEST['c'.$k]) && !empty($v['regex']))
				{
					     preg_match_all('/(?<!\d)(\d+)(?!\d)/', $_REQUEST['c'.$k], $m);
						 $cd=0;
						 foreach($m[1] as $c)
						 {
							if(strlen($c)>1)
							$cd=$cd+strlen($c)-1;
						 }
						 if((count($m[1])+$cd)<5)
						$_err[$k] = 'Номер слишком короткий';
				}
                                    
            }
			if(!empty($_REQUEST['surname']))
                    $_err['surname'] = 'Заполнено скрытое поле. <strong>Вы робот?</strong>';  
			
			 if(count($_err))
			 {
                
                $_out = '';
				$border='';

				 foreach($fields as $k=>$e)
				{
					$border .='$( \'.f-'.$k.'\' ).removeClass(\'err\');';
					$out['err-'.$k] = array( '', 'rewrite');
              					  
				}
				
                foreach($_err as $k=>$e)
				{
					$border .='$( \'.f-'.$k.'\' ).addClass(\'err\');';
					$out['err-'.$k] = array( $_err[$k], 'rewrite');
				}
				
//			   $out['call'] = array('<script>'.$border.'</script>','rewrite');
            }
			else
			{
                $mails = $DB->getAll('SELECT * FROM `mp_mailer` WHERE FIND_IN_SET("1",`cat`)');
				
                $emails=array();
				
                foreach ($mails as $num=>$mail)
                    $emails[] = str_replace(';',',',$mail['mail']);
				$emails = implode(',',$emails);
                foreach($fields as $k=>$v)
				{
                	if(!empty($_REQUEST['c'.$k]))
                        $_text[] = '<strong>'.$v['name'].':</strong><br>'.nl2br(htmlspecialchars($_REQUEST['c'.$k]));                                   
                }
                $text = '<html>'.implode('<br>', $_text).'</html>';
                $mail = new SendMail();
                $mail->init();
                $mail->setTo($emails);
                $mail->setSubject('Заказ обратного звонка ('.$_SERVER['SERVER_NAME'].')');
                $mail->setMessage($text);
                $mail->setFrom('noreply@'.$_SERVER['SERVER_NAME'], $_SERVER['SERVER_NAME']);
                $mail->send();

                $out['callback'][] = "window.location='/" . path(87) . "';";
            }

        }
		
		
	break;
    case 'price':
    case 'itemPrice':
    case 'feedback_shortcode':

        include(rtrim(BLOCKS_DIR,'/').'/configs/'.$action.'.php');
        $form = new FormsControl($formconfig, true);
        $form->formid = $action.'form';
        
		if(getparam(1)){
            if(getParam(1)!='get'){
                break;
            }
            echo $form->getForm(true);
            die();
        }
        
		$Forms=array(
            'price'=>array(
                'emailCat'=>3,
                'title'=>$_SERVER['HTTP_HOST'].' - Запрос прайса',
                'text'=>'<p><strong>C сайта <a href="http://'.$_SERVER['HTTP_HOST'].'/" target="_blank">'.$_SERVER['HTTP_HOST'].'</a> поступил новый запрос прайса</strong></p>',
            ),
            'itemPrice'=>array(
                'emailCat'=>2,
                'title'=>$_SERVER['HTTP_HOST'].' - Запрос стоимости товаров',
                'text'=>'<p><strong>C сайта <a href="http://'.$_SERVER['HTTP_HOST'].'/" target="_blank">'.$_SERVER['HTTP_HOST'].'</a> поступил новый запрос стоимости товара</strong></p>',
            ),
            'feedback_shortcode'=>array(
                'emailCat'=>2,
                'title'=>$_SERVER['HTTP_HOST'].' - Написать письмо',
                'text'=>'<p><strong>C сайта <a href="http://'.$_SERVER['HTTP_HOST'].'/" target="_blank">'.$_SERVER['HTTP_HOST'].'</a> поступил новое обращение</strong></p>',
            ),
        );
		
		$conf=$Forms[$action];

	    if (!empty($_POST))
	    {
			$form->from_name    = $_SERVER['SERVER_NAME'];
	    	$form->from_mail    = 'noreply@'.$_SERVER['SERVER_NAME'];
	    	$form->to_mail      = implode(', ',$DB->getcol('select mail from `'.PRFX.'mailer` WHERE FIND_IN_SET("'.$conf['emailCat'].'",`cat`)'));
		    $form->title        = $conf['title'];
		    $form->add_text     = $conf['text'];
	
	        $form->processData($_REQUEST);
	        $form->checkFields();
	
	        // Получаем значения формы
	        $errors = $form->getErrors();  
	        if (empty($errors))
	        { 
	           
               
               $fromUrl = $form->getFieldByName('url');
               if($fromUrl){
                $form->add_text.="<p>Запрос сделан со страницы <a href='{$fromUrl['value']}'>{$fromUrl['value']}</a></p>";
                $fromUrl['value']='';
                $form->setConfigData('url',$fromUrl);
               }

	            if ($form->sendMail())
	            {
	            	/*редирект на страницу Спасибо*/
	                $out['callback'][] = "window.location='/" . path(87) . "';";
	            }
	            else
	            {
	            	/*неудача отправки письма*/
	            }
	        }
	        else {
                $out['callback']= $form->getJS();
        	}
	    }
		    
		
    
        
    break;
		default:
			exit( header("HTTP/1.0 404 Not Found") );
			
	}
	
	foreach( $out as $k=>$v ) // Защита, если забыли контент поместить в массив
		$out[$k] = is_array($v) ? $v : array($v);

	echo json_encode( $out );

}else{

	/**
	Если запрос не аяксовый
	**/
		
	header('Content-Type:text/html; charset=utf-8');
	exit('Hmm. Your browser not uses AJAX <br/> <a href="http://'.DOMAIN.'">'.DOMAIN.'</a>');

}

?>