<?php

$files = array(
    'clear.php'         => array('Заглушка', ''),
    'menu_top.php'      => array('Меню верхнее', ''),
    'registred.php'     => array('Регистрация', ''),
    'user.php'          => array('Личные данные', ''),
    'orders.php'        => array('История заказов', ''),
    'catalog.php'       => array('Каталог', 'catalog'),
	'some_content.php'  => array('Реализованые проекты', 'realizovannye_proekty'),
    'basket.php'        => array('Корзина', ''),
    'search.php'        => array('Поиск по сайту', ''),
    'news.php'          => array('Новости', ''),
);
?>