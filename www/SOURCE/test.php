<p><strong>Екатеринбург<br /></strong></p>
<p><strong>Ул. Белинского, 86, 7 секция, 7 этаж</strong></p>
<p><strong>Телефон для связи:</strong> +7 <span class="wmi-callto">(343) 27-200-27</span> (23-26) <br /><strong>Отдел продаж: </strong>+7 <span class="wmi-callto">(343) 27-200-24</span> <br /><strong>Бухгалтерия:</strong> +7 <span class="wmi-callto">(343) 27-200-30</span> <br /><strong>Факс:</strong> +7 <span class="wmi-callto">(343) 27-200-28</span></p>
<p><span class="wmi-callto"><strong>Эл. Почта:</strong> </span><a href="mailto:sanvut@mail.ru" class="email">info@sanvut.ru</a></p>
<p>[map=3]</p>
<p>&nbsp;</p>
<p><strong>Москва</strong></p>
<p><strong>ул. 1-ая Магистральная 16, стр. 1</strong></p>
<p><strong>Телефон для связи:</strong> 8 (800) 555-37-53</p>
<p><strong>Эл. Почта:</strong> <span class="wmi-callto">&nbsp;</span><a href="mailto:sanvut@mail.ru" class="email">info@sanvut.ru</a></p>
<p>[map=4]</p>
<p>&nbsp;</p>
<p><strong>Новосибирск&nbsp;</strong></p>
<p><strong>&nbsp;Ул. Докучаева, 11</strong>&nbsp;</p>
<p><strong>Телефон для связи:&nbsp;</strong>+7 (383) 377-70-35</p>
<p>[map=5]</p>
<h2>Реквизиты</h2>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">Общество с&nbsp;ограниченной ответственностью <strong>&laquo;Санвут&raquo;</strong></span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">Юридический адрес: 620010, г. Екатеринбург, ул. Грибоедова 20А-7</span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">Почтовый адрес: 620024, г. Екатеринбург, ул. Новинская, 2<br /></span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">Расчетный счет: 40702810216120113529 Филиал Акционерного коммерческого банка СБ РФ (ОАО)</span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">К/с: 3010180500000000674</span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">БИК: 046577674<br /></span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">ОГРН: 2096674468702</span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">ОКПО: 95004420</span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">ИНН: 6674182460</span></p>
<p><span size="3" face="Times New Roman" style="font-family: Times New Roman; font-size: small;">КПП: 667401001</span></p>
<script type="text/javascript" charset="utf-8"></script>
<div id="_mcePaste" class="mcePaste" style="position: absolute; left: -10000px; top: 347px; width: 1px; height: 1px; overflow: hidden;"></div>
