document.createElement('aside');
document.createElement('header');
document.createElement('figcaption');
document.createElement('footer');
document.createElement('section');
document.createElement('figure');
document.createElement('article');
document.createElement('time');

$(document).ready(function(){
    $('.slider').bxSlider({
        mode: 'horizontal',
        minSlides: 4,
        maxSlides: 4,
        moveSlides: 1,
        slideWidth: 160
    });
    $( ".slider-range" ).slider({
        range: true,
        min: 0,
        max: 30,
        values: [ 2, 15 ]
    });
    $('.slider_brand').bxSlider({
        mode: 'horizontal',
        minSlides: 5,
        maxSlides: 5,
        moveSlides: 1,
        slideWidth: 160
    });
    $('.main_slider').bxSlider({
        mode: 'horizontal',
        minSlides: 1,
        maxSlides: 1,
        moveSlides: 1,
        slideWidth: 960
    });
    $('.project_slide').bxSlider({
        mode: 'horizontal',
        minSlides: 2,
        maxSlides: 2,
        moveSlides: 1,
        slideWidth: 340
    });

   $('.modal-window-close').click(function(){
        $('.modal-window-wrap').addClass('hidden_text');
    });

    $('.callback a').click(function(event){
        event.preventDefault();
        if ($('.modal-window-wrap').hasClass('hidden_text')){
            $('.modal-window-wrap').removeClass('hidden_text');
        }
    });

    $('.moar').click(function(event){
        event.preventDefault();
        $('.hidden_text').remove();
    })

});

