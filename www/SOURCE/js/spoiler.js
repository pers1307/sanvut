$(document).ready(function()
{
  
   $('.modal-window-close').click(function(){
        $('.modal-window-wrap').addClass('hidden_text');
    });

    $('.callback a').click(function(event){
        event.preventDefault();
        if ($('.modal-window-wrap').hasClass('hidden_text')){
            $('.modal-window-wrap').removeClass('hidden_text');
        }
    });
   
   
    $('.spoller_handler2').click(function(e){
        e.preventDefault();
        var $this = $(this),
            $parent = $this.closest('.spoller_container2'),
            $spoller_container = $('.spoller_container2');

        if(!$parent.hasClass('active'))
        {
            $spoller_container.removeClass('active').find('.spoller2').slideUp();
            $parent.addClass('active').find('.spoller2').slideDown();
        }
    });
  
    $('.price_form form button').submit(function(){
        if ($('.labelBlock input').val()==""){
           $('.labelBlock input').addClass('error')
           $('.errorText').removeClass('hidden_text');  
        }
    })
  
   $('.gl_slide').bxSlider({
        mode: 'horizontal',
        minSlides: 1,
        maxSlides: 1,
        moveSlides: 1,
        pagerCustom: '#bx-pager'
    });
  
    $('.spoller_handler').click(function(e){
        e.preventDefault();
        var $this = $(this),
            $parent = $this.closest('.spoller_container'),
            $spoller_container = $('.spoller_container');

        if(!$parent.hasClass('active'))
        {
            $spoller_container.removeClass('active').find('.spoller').slideUp();
            $parent.addClass('active').find('.spoller').slideDown();
        }
    });
  

    $('.moar').click(function(event){
        event.preventDefault();

        if($('.js-moreText').hasClass('hidden_text'))
        {
            $('.js-moreText').removeClass('hidden_text').slideDown();
            $('.moar').text('Скрыть');
        }
        else
        {
            $('.js-moreText').addClass('hidden_text').slideUp();
            $('.moar').text('Читать подробнее');
        }


    })
});