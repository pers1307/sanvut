<?php
	if ((strpos($_SERVER['REQUEST_URI'],'control')=== false) && 
(strpos($_SERVER['REQUEST_URI'],'ajax')=== false) && 
(strpos($_SERVER['REQUEST_URI'],'json')=== false)) ;

@set_magic_quotes_runtime(0);
@ini_set('register_globals',0);

date_default_timezone_set('Asia/Yekaterinburg');
setlocale(LC_CTYPE, array ('ru_RU.UTF-8', 'ru_SU.UTF-8', 'rus_RUS.UTF-8', 'russian', 'ru_RU', 'ru_SU', 'ru'));

$date = time();
$last_modified = gmdate('D, d M Y H:i:s', $date) . ' GMT';

if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {

	$if_modified_since = preg_replace('/;.*$/', '', $_SERVER['HTTP_IF_MODIFIED_SINCE']);

	if ($if_modified_since == $last_modified) {

		header('HTTP/1.0 304 Not Modified');
		header('Cache-Control: max-age=86400, must-revalidate');
		exit;
	}

}

header('Cache-Control: max-age=86400, must-revalidate');
header('Last-Modified: ' . $last_modified);

require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/CORE/core.php');

?>