<?php
/** Глобализуем классы и массив сессий (на всякий пожарный). */
global $_SESSION, $_MODULES, $DB;

/** Конфиг момдуля страниц */

global $PAGE_CONFIG;

/** Массив переменных ModRewrite'a */
global $_VARS;

/**
*  Информация о группах прав
*  @global array $_GROUPS
*/
global $_GROUPS;

/**
*  Информация о зонах основного шаблона
*  @global array $_ZONES
*/
global $_ZONES;

/**
*  Содержит всю информацию о текущем пользователе
*  @global array $_USER
*/
global $_USER;

/**
*  Временные настройки. Массив уничтожается до стадии генерации страницы
*  @global array $_CONFIG
*/
global $_CONFIG;

/**
*  Ключевые слова страницы
*  @global string $_META_KEYWORDS
*/
global $_META_KEYWORDS;
$_META_KEYWORDS= '';

/**
 *  Meta-description страницы
 *  @global string $_META_DESCRIPTION
 */
 global $_META_DESCRIPTION;
$_META_DESCRIPTION= '';

/**
*  Заголовок страницы (между тегами title)
*  @global string $_TITLEPAGE
*/
global $_TITLEPAGE;
$_TITLEPAGE= '';


/**
*  Заголовок страницы
*  @global string $_TITLEH1
*/
global $_TITLEH1;
$_TITLEH1= '';

global $auth_isROOT;
?>