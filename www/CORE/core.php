<?php
//----------------------------------------------------------------------------------------------------------------------------------------
define('CORE_DIR', DOC_ROOT.'/CORE/');
require_once CORE_DIR.'config.core.php';

require_once CORE_DIR.'classes/error_handling.php';

/** Функции авторизации */
require_once CORE_DIR.'includes/authorizate_functions.php';

/** Модуль дебага */
require_once CORE_DIR.'classes/error_handling.php';

/** Константы времени и даты */
require_once CORE_DIR.'configs/date.php';

/** Разные константы */
require_once CORE_DIR.'configs/other.php';

/** Константы форм */
require_once CORE_DIR.'configs/forms.php';


/** Глобальные функции */
require_once CORE_DIR.'includes/global_functions.php';
require_once CORE_DIR.'includes/images_functions.php';

//----------резалка тегов------------------------
require_once CORE_DIR.'includes/kses.php';

/** Функции модулей */
require_once CORE_DIR.'includes/mod_functions.php';


/** Пользовательские функции */
require_once CORE_DIR.'includes/user_functions.php';

/** Запустили output buffer */
ob_start('OB_handler');

// search

require_once CORE_DIR.'includes/Lingua_Stem_Ru.php';

/** XML2ARRAY */
require_once CORE_DIR.'classes/xmls.php';

/** Глобальные переменные */
require_once CORE_DIR.'configs/globals.php';

/** Подключаем класс работы с путями и ссылками */
require_once CORE_DIR.'classes/urls.php';

/** Подключаемся в БД */
require_once CORE_DIR.'classes/db.php';

/** Подключаем класс построения форм конфигураций */
require_once CORE_DIR.'classes/configs.php';

require_once CORE_DIR.'classes/sendmail.php';

/** Подключение класса для работы с картинками */
require_once(CORE_DIR.'classes/Image_Toolbox.class.php');

/** Подключение класса работы с модулями и файлами */
require_once CORE_DIR.'classes/module_director.php';

/** Подключаем класс построения "хлебных крошек" **/
require_once CORE_DIR.'classes/breadCrumbs.php';

/** Класс конфигуратора форм */
$Forms = new form_configs();

/** Класс управления путями и ссылками */
$urls= new UrlsAndPaths();

/** Класс для работы с картинками */
$Images = new Image_Toolbox();

/** Класс базы данных */
$DB= new DB_Engine('mysql', $_CONFIG['DBHOST'], $_CONFIG['DBUSER'], $_CONFIG['DBPASS'], $_CONFIG['DBNAME']);

/** Класс управления модулями */
$_MODULES= new ModuleDirector();

/** Берем информацию о модулях */
$_MODULES->getModulesInfo();



/** Уничтожаем переменные конфига */
unset ($_CONFIG, $DOC_ROOT);

addWatermarkEverywhere();

/** Работа модуля redirects */

$redirect = $DB->getRow('SELECT * FROM `'.PRFX.'redirects` WHERE `from` = "' . $DB->pre($_SERVER['REQUEST_URI']) . '" AND `active` = 1');

if ( count($redirect) )
	{

	/**
	 * TODO: Допилить функцию go, сейчас она знает только 301 редирект
	 */
	go($redirect['to'], $redirect['type']);

	}


/** Другие редиректы */
$uri = $_SERVER['REQUEST_URI'];
$path = explode('/', $uri);

if(!preg_match("#www\.#",$_SERVER['HTTP_HOST']) && substr_count($_SERVER['HTTP_HOST'], '.')<=1) {
     header("HTTP/1.x 301 Moved Permanently");
    header('Location: http://www.'.$_SERVER["HTTP_HOST"].$uri);
    die();
}

if (!sizeof($_GET) && $path[count($path)-1] != '' && !preg_match('#/'.ROOT_PLACE.'/#',$_SERVER['REQUEST_URI']) && !preg_match('#ajax#',$_SERVER['REQUEST_URI']) && !preg_match('#\.#',$_SERVER['REQUEST_URI'])) {
     header("HTTP/1.x 301 Moved Permanently");
     header('Location: http://'.$_SERVER["HTTP_HOST"].$uri.'/');
     die();
}


/** Подключаем системные модули */
if (sizeof($_MODULES->sys_modules))
	foreach ($_MODULES->sys_modules as $m)
	    if ($m != 'admin')
			UseModule($m);
foreach($_MODULES->info as $module_name => $module_data){
	if ($module_data['installed']==1){
		UseModule($module_name);
	}
}

if(preg_match('/\/\//', $_SERVER['REQUEST_URI'])) Page404(); // показываем 404 страницу, если в пути 2 или более слеша подряд

/** авторизуем */
auth_Authorization();

$auth_isROOT=auth_isROOT();

require($_SERVER['DOCUMENT_ROOT'].'/CORE/includes/exec.funcs.php');

$current_query = CURRENT_QUERY;

if(isset($_REQUEST['print_version'])){
	$GLOBALS['print_version']=true;
	$GLOBALS['print_version_']=$_REQUEST['print_version'];
	$tmp=explode("/",$current_query);
	unset($tmp[count($tmp)-1]);
	$current_query=implode("/",$tmp)."/";
	$current_query=str_Replace('?print_version=1','',$current_query);
}

$current_query = trim($current_query,'/');

/** Если переменная STOP_CMS не объявлена - продолжаем работу (защита от проникновение через визивик) */
if (!defined('STOP_CMS')){
	/** Разобрали на части и обработали текущий путь */
	//$page = $urls->Process_Current_Url(CURRENT_QUERY);
	$page = $urls->Process_Current_Url($current_query);

	/** Инициализация крошек */
	$breadCrumbs = new breadCrumbs();
	$breadCrumbs->fastGet( $page['path_id'] );

	/** Отдаем 404, если $page['notfound'] **/
	if( $page['notfound'] )
		page404();

	/** Перенаправления в случаее редиректов */
	if ( $page['root_only'] && (!auth_inGroups(array(1,2)) || !auth_getUserId() || auth_getUSerId()==1) )
		go('/'.ROOT_PLACE.'/auth/');

	if ( $page['rdr_path_id']>1)
		go("/".path($page['rdr_path_id']));

	if ( $page['rdr_url']!="")
		header("Location: ".$page['rdr_url']);

	/** Берем информацию о зонах шаблона из БД */
	$_ZONES= (array) $DB->getAll('SELECT * FROM `'.PRFX.'zones` ORDER BY id');

	/** Объявили переменную, в которую будет скинут весь html вывод страницы */
	$RESULT= '';


	/** подготовка данных для TDK */
	contentPagePrepareSys();

	/** Проходим по всем существующим зонам */
	foreach ($_ZONES as $values) {
		$zone = $tmp_zone = $values['value'];

		$search=false;
		if (isset($values['for_search']) && $values['for_search'] == 1) $search=true;


		/** Объявляем одноименную переменную зоны */
		global $$zone;

		if (!isset ($$zone)) $$zone = '';

		$cur_zone_content = $DB->getOne('SELECT content FROM `'.PRFX.'zones_content` WHERE zone_id="'.$values['value'].'" AND path_id="'.$page['path_id'].'" LIMIT 1');
		$cur_zone_content = html_entity_decode($cur_zone_content, ENT_QUOTES);

		/** Все скрипты в зоне */
		if (!isset($page['config'][$zone])) {$page['config'][$zone]=array();}

		//if ($zone == '_CONTENT_')
		//	$$zone = isset($page['content']) && $page['content']!="" ? $cur_zone_content.$page['content'] : $cur_zone_content;
		//else
			$$zone = $cur_zone_content;

			if($zone=='_CONTENT_' && isset($GLOBALS['print_version_']) && strlen($GLOBALS['print_version_'])>1)$$zone.=base64_decode($GLOBALS['print_version_']);

		$parts = $page['config'][$zone];

		/** Если зона не содержит блоков, то пропускаем */
		if (sizeof($parts) == 0 && $values['for_search']){
			$$zone = '<!--[INDEX'.(isset($values['search_links']) && $values['search_links']==1 ? '*' : '').']-->'.($$zone!="" ? $$zone : '').'<!--[/INDEX]-->';
			continue;
		}

		/** Пробегаемся по всем блокам и выполняем их код в соответствии с заданной структурой раздела. */
		foreach ($parts as $part_template) {
			$result_text= '';

			/**
			*  Если при запуске этого блока надо объявить какие-то переменные,
			*  то так и сделаем
			*  @see UrlsAndPaths::set_vars()
			*/
			if ($part_template['vars'] != ''){
				$urls->set_vars($part_template['vars']);
			}

			/** Если указан скрипт, то выполняем его */
			if ($part_template['script'] != '') {
				if (is_file(DES_DIR.$part_template['script'])) include (DES_DIR.$part_template['script']);
				$result_text= ob_get_contents();
				@ob_clean();
			}
			/**
			*  Уничтожаем недавно объявленные переменные
			*  @see UrlsAndPaths::unset_vars()
			*/
			if ($part_template['vars'] != '')
				$urls->unset_vars($part_template['vars']);

			/** Добавляем к выводу результат */
			$$zone .= $result_text;

			unset ($result_text);
			}

		if ($$zone=="")
			$$zone = isset($page['content']) && $page['content']!="" ? $page['content'] : '';

		if ($search)
			{
			$$zone = '<!--[INDEX'.(isset($values['search_links']) && $values['search_links']==1 ? '*' : '').']-->'.$$zone.'<!--[/INDEX]-->';
			}
		}

    if (empty($_TITLEPAGE)) {

        $_TITLEPAGE = !empty($page['title_page']) ? $page['title_page'] : $pagi_add.$page['title_menu'];
    }

    if (empty($_TITLEMENU)) {

        $_TITLEMENU = $page['title_menu'];
    }

    if (empty($_TITLEH1)) {

        $_TITLEH1 = !empty($page['header']) ? $page['header'] : $page['title_menu'];
    }

    if (empty($_META_DESCRIPTION)) {

        $_META_DESCRIPTION = $page['meta_description'];
    }

    if (empty($_META_KEYWORDS)) {

        $_META_KEYWORDS = $page['meta_keywords'];
    }

    $atdk=array();

    if ($_META_DESCRIPTION != "") {

        $atdk[]='<meta name="description" content="'.$_META_DESCRIPTION.'">';
    }

    if ($_META_KEYWORDS != "") {

        $atdk[]='<meta name="keywords" content="'.$_META_KEYWORDS.'">';
    }

	$print_version = false;

	if (isset($_REQUEST['print_version'])){
		/*
		ob_clean();
		 $vars['_CONTENT_'] = $_CONTENT_;
		 $vars['_TITLEPAGE'] = $_TITLEPAGE;
		 die (template('print',$vars));
		*/

		$print_version=true;
	}

	/** Подключаем основной шаблон */
	if (is_file(DES_DIR.$page['main_template'])) {
		require_once (DES_DIR.$page['main_template']);
		$RESULT= ob_get_contents();

		ob_clean();

	} else  {
		die('Не найден основной шаблон текущей страницы');
	}

	/**
	Титлотимизер
	**/

	$titleRules = $DB->GetAll( $q = 'SELECT * FROM `'.PRFX.'titles` WHERE "/'.CURRENT_QUERY.'/" LIKE  `url` ORDER by `sort` ASC' );

	if( count($titleRules) ){

		$matchTitle = preg_match('|<title>(.*)</title>|Uis', $RESULT, $match);

		if( $matchTitle ){

			$r_title = $match[1];

			foreach( $titleRules as $tr ){

				$r_title = makeChangeTitle($r_title, $tr['text'], $r_title, $tr['type_insert']);

			}

			$RESULT = preg_replace('#<title>(.*)</title>#iUs', '<title>'. trim($r_title) .'</title>', $RESULT);

		}

	}


	/** Отсылаем обязательные хедеры */
	$ct_header = 0; // флаг - был ли послан заголовок Content-Type
	$header_list = headers_list();
	foreach($header_list as $head){
	 if(substr($head,0,13) == 'Content-Type:') $ct_header = 1;
	}
	if($ct_header == 0) header('Content-Type: text/html; charset=utf-8');

	/** Вставка МЕТА тегов TDK */
	AddBeforeTag((sizeof($atdk) ? implode("\n",$atdk)."\n" : ''),'<title>');
	unset($atdk);

	/** Отрисовка страницы */
	if ((strpos($_SERVER['REQUEST_URI'],'control')=== false) && 
(strpos($_SERVER['REQUEST_URI'],'ajax')=== false) && 
(strpos($_SERVER['REQUEST_URI'],'json')=== false))
{
echo Append2HTML($RESULT);
} 
else echo Append2HTML($RESULT);
}

auth_StoreSession();
?>