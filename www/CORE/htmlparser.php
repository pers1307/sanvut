<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
define('CORE_DIR', DOC_ROOT.'/CORE/');
require_once CORE_DIR.'config.core.php';
require_once CORE_DIR.'includes/authorizate_functions.php';
require_once CORE_DIR.'configs/other.php';
require_once CORE_DIR.'includes/global_functions.php';
require_once CORE_DIR.'includes/user_functions.php';
require_once CORE_DIR.'configs/globals.php';
require_once CORE_DIR.'classes/urls.php';
require_once CORE_DIR.'classes/db.php';
require_once CORE_DIR.'classes/module_director.php';
$urls= new UrlsAndPaths();
$DB= new DB_Engine('mysql', $_CONFIG['DBHOST'], $_CONFIG['DBUSER'], $_CONFIG['DBPASS'], $_CONFIG['DBNAME']);
$_MODULES= new ModuleDirector();
$_MODULES->getModulesInfo();
unset($_CONFIG);
if (sizeof($_MODULES->sys_modules))foreach ($_MODULES->sys_modules as $m)if ($m != 'admin')UseModule($m);
auth_Authorization();
$groups=explode(",",$_USER['groups']);
if(in_array(1,$groups) || in_array(2,$groups)){
	$tmp=explode("?",$_SERVER['REQUEST_URI']);
	if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$tmp[0]))die(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$tmp[0]));
} else {
	page404();
}
?>