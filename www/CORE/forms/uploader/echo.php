<?php
global $CONFIG, $adminmenu;

$params = $adminmenu->params;
unset($params[sizeof($params)-1]);
$dialog_url = '/'.ROOT_PLACE.'/'.implode('/',$params).'/';

$config_forms = array();
$config_forms['config'] = $CONFIG; 
$config_forms['dialog_url'] = $dialog_url;
$config_forms['sysname'] = preg_replace('#[\[\]]#','',$sysname);
$config_forms['fsysname'] = $sysname;

$_SESSION['config']['uploader'] = serialize($config_forms);


$temp=array();
if (wd_check_serialization($value,$error))
	{
	$temp = unserialize($value);
	}

$sizes = '';
if (isset($w_thumbs) && isset($h_thumbs))
	{
	$sizes = prepareSizes2URL($w_thumbs, $h_thumbs);
	}
													
$s = '<fieldset style="padding: 5px;"><div id="'.preg_replace('#[\[\]]#','',$sysname).'" style="floar: left"></div>';

if (is_array($temp))
	{
	foreach ($temp as $value)
		{		
		$path_file = isset($value['path'][0]) ? $value['path'][0] : ''; 

		if (isset($path_file) && $path_file!="")
			{
			$unic = 'f'.mt_rand(0,9999).mt_rand(0,9999);

			if ($sizes!="")
				{
				$si=getSysImage($path_file);
				$w=$h=75;
				if (is_file($_SERVER['DOCUMENT_ROOT'].$si)) 
					list($w,$h) = getimagesize($_SERVER['DOCUMENT_ROOT'].$si);

				$s .= '<div id="'.$unic.'" style="border: 1px solid silver; float: left; margin: 2px; background: url('.$si.'); width: '.$w.'px; height: '.$h.'px" title="'.basename($path_file).'"><img align="right" onclick="if (confirm(\'Вы действительно хотите удалить файл?\')) {discardElement(getObj(\''.$unic.'\'));}" src="/DESIGN/ADMIN/images/delete.gif" border="0" title="Удалить файл" alt="" style="margin: 4px; cursor: pointer"> <input type="hidden" name="'.$sysname.'[path][]" value="'.htmlspecialchars(serialize($value['path'])).'"></div>';
				}
			else
				{
				$file_name = basename($path_file);

				$s .='<div id="'.$unic.'" >'.$file_name.'&nbsp;<img onclick="if (confirm(\'Вы действительно хотите удалить файл?\')) {discardElement(getObj(\''.$unic.'\'));}" src="/DESIGN/ADMIN/images/delete.gif" border="0" title="Удалить файл" alt="" style="margin-top: 3px; cursor: pointer"> <input type="hidden" name="'.$sysname.'[path][]" value="'.htmlspecialchars(serialize($value['path'])).'"></div>';
				}
			}
		}
	}

return $s.'</fieldset><div style="clear: both"><a href="#" onclick="displayMessage(\'/'.ROOT_PLACE.'/admin/mod_action/uploader/show_flash/\', 650, 570);"><b>Загрузить много файлов</b></a></div>';
?>