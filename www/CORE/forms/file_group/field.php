<?php
$value = $item[$config['field_table']]; 
$value = html_entity_decode($value, ENT_QUOTES);
if (wd_check_serialization($value, $error))
{
	$value = unserialize($value);

	if (sizeof($value) && is_array($value))
	{
		foreach ($value as $num => $f)
		{
			if (!isset($f['path']) || !is_array($f['path'])) continue;
				
            list($k, $f) = each($f['path']);
			echo '<div><a href="'.$f.'" target=_blank>'.basename($f).'</a></div>';
		}
	}
}
?>