function moveFGRow(type,mid,id)
{
	mid_el = document.getElementById(mid+'_filebox');
	
	childs=mid_el.childNodes;
	
	for (var i=0;i<childs.length;i++)
	{
		if (childs[i].id==id)
		{
			if (type==1 && childs[(i-1)])
			{
				mid_el.insertBefore(childs[i], childs[(i-1)]);
			}
			
			if (type==2)
			{
				if (childs[(i+2)])
					mid_el.insertBefore(childs[i], childs[(i+2)]);
				else
					mid_el.appendChild(childs[i]);
			}			
			
			break;
		}
	}
}

function delFGRow(id,sysname,mdiv)
{
	if (confirm('Вы действительно хотите удалить файл?')) 
	{
		path = document.getElementById('path_'+id).value;
		
		delete_input = document.createElement('input');
		delete_input.name = sysname+'[delete][]';
		delete_input.type = 'hidden';
		delete_input.value = path;
		
		document.getElementById(mdiv+'_delete').appendChild(delete_input);
		
		discardElement(document.getElementById(id));
		
		
		upload_box = document.getElementById(mdiv+'_uploadbox');
		
		if (upload_box.style.display=='none')
			upload_box.style.display = '';
		
		limit = parseInt(swfu.getSetting("file_upload_limit"))+1;
		
		swfu.destroy();	
		
		upload_box.innerHTML = '<span id="'+mdiv+'_selectbutton"></span>';
		
		swfu_options.file_upload_limit = limit;
		swfu = new SWFUpload(swfu_options);
	}
}


function preLoad() {
	if (!this.support.loading) {
		alert("Для загрузки файлов Вам необходим Flash Player версии 9.028 или выше.");
		return false;
	}
}
function loadFailed(error) {
	alert("Произошла ошибка: "+error);
	//alert("Something went wrong while loading SWFUpload. If this were a real application we'd clean up and then give you an alternative! "+error);
}



function fileQueueError(file, errorCode, message) {
	try {
		var errorName = "";

		if (errorName !== "") {
			alert(errorName);
			return;
		}

		switch (errorCode) {
		case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
			alert('Загружаемый файл имеет нулевой размер');
		break;
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			alert('Загружаемый файл привышает максимально разрешенный размер');
		break;
		case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
			alert('Загружаемый файл имеет недопустимое расширение');
		break;	
		case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
			if (!this.settings.file_upload_limit)
				alert('Вы не можете загружать больше файлов');
			else
				alert('Вы пытаетесь выбрать слишком много файлов, максимум: '+this.settings.file_upload_limit);
		break;			
		default:
			alert('Ошибка загрузки '+message);
		break;
		}
	} catch (ex) {
		this.debug(ex);
	}

}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
	try {
		if (numFilesQueued > 0) {
			swfUploaded(this);
		}
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadProgress(file, bytesLoaded) {
	try {
		var percent = Math.ceil((bytesLoaded / file.size) * 100);

		var progress = new FileProgress(file,  this.customSettings.upload_target);
		progress.setProgress(percent);
		progress.setStatus("Загрузка...");
		progress.toggleCancel(true, this);
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadSuccess(file, serverData) {
	try {
		var progress = new FileProgress(file,  this.customSettings.upload_target);

		eval(serverData);
		
		if (result_data.unic && result_data.box && result_data.content)
		{
			limit = parseInt(this.getSetting("file_upload_limit"))-1;
			this.addSetting("file_upload_limit", limit);
			
			new_field = document.createElement("div");
			new_field.id = result_data.unic;
			new_field.innerHTML = result_data.content;
			
			appendFieldData(new_field);
			
			if (limit==0)
				document.getElementById(result_data.box+'_uploadbox').style.display = 'none';
		}
		else
			alert(result_data);
		

		progress.setStatus("Успешно загружено.");
		progress.toggleCancel(false);

	} catch (ex) {
		this.debug(ex);
	}
}

function uploadComplete(file) {
	try {
		/*  I want the next upload to continue automatically so I'll call startUpload here */
		if (this.getStats().files_queued > 0) {
			swfUploaded(this);
		} else {
			var progress = new FileProgress(file,  this.customSettings.upload_target);
			progress.setComplete();
			progress.setStatus("Все файлы загружены.");
			progress.toggleCancel(false);
			
			limit = parseInt(this.getSetting("file_upload_limit"));
			this.callFlash("SetFileUploadLimit", limit);			
		}
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadError(file, errorCode, message) {
	var progress;
	try {
		switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			try {
				progress = new FileProgress(file,  this.customSettings.upload_target);
				progress.setCancelled();
				progress.setStatus("Отменено");
				progress.toggleCancel(false);
			}
			catch (ex1) {
				this.debug(ex1);
			}
		break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			try {
				progress = new FileProgress(file,  this.customSettings.upload_target);
				progress.setCancelled();
				progress.setStatus("Остановлено");
				progress.toggleCancel(true);
			}
			catch (ex2) {
				this.debug(ex2);
			}
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			alert('Превышен лимит количества одновременно загружаемых файлов');
		break;
		default:
			alert(message);
		break;
		}
	} catch (ex3) {
		this.debug(ex3);
	}
}

function appendFieldData(new_field)
{
	main_box = document.getElementById(result_data.box+'_filebox');
	main_box.appendChild(new_field);
	main_box.scrollTop='9999';
	
	fadeIn(new_field, 0);
}

function fadeIn(element, opacity) {
	var reduceOpacityBy = 5;
	var rate = 30;	// 15 fps


	if (opacity < 100) {
		opacity += reduceOpacityBy;
		if (opacity > 100) {
			opacity = 100;
		}

		if (element.filters) {
			try {
				element.filters.item("DXImageTransform.Microsoft.Alpha").opacity = opacity;
			} catch (e) {
				// If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
				element.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + opacity + ')';
			}
		} else {
			element.style.opacity = opacity / 100;
		}
	}

	if (opacity < 100) {
		setTimeout(function () {
			fadeIn(element, opacity);
		}, rate);
	}
}



/* ******************************************
 *	FileProgress Object
 *	Control object for displaying file info
 * ****************************************** */

function FileProgress(file, targetID) {
	this.fileProgressID = "divFileProgress";

	this.fileProgressWrapper = document.getElementById(this.fileProgressID);
	if (!this.fileProgressWrapper) {
		this.fileProgressWrapper = document.createElement("div");
		this.fileProgressWrapper.className = "progressWrapper";
		this.fileProgressWrapper.id = this.fileProgressID;

		this.fileProgressElement = document.createElement("div");
		this.fileProgressElement.className = "progressContainer";

		var progressCancel = document.createElement("a");
		progressCancel.className = "progressCancel";
		progressCancel.href = "#";
		progressCancel.style.visibility = "hidden";
		progressCancel.appendChild(document.createTextNode(" "));

		var progressText = document.createElement("div");
		progressText.className = "progressName";
		progressText.appendChild(document.createTextNode(file.name));

		var progressBar = document.createElement("div");
		progressBar.className = "progressBarInProgress";

		var progressStatus = document.createElement("div");
		progressStatus.className = "progressBarStatus";
		progressStatus.innerHTML = "&nbsp;";

		this.fileProgressElement.appendChild(progressCancel);
		this.fileProgressElement.appendChild(progressText);
		this.fileProgressElement.appendChild(progressStatus);
		this.fileProgressElement.appendChild(progressBar);

		this.fileProgressWrapper.appendChild(this.fileProgressElement);

		document.getElementById(targetID).appendChild(this.fileProgressWrapper);
		fadeIn(this.fileProgressWrapper, 0);

	} else {
		document.getElementById(targetID).appendChild(this.fileProgressWrapper);

		this.fileProgressElement = this.fileProgressWrapper.firstChild;
		this.fileProgressElement.childNodes[1].firstChild.nodeValue = file.name;
	}

	this.height = this.fileProgressWrapper.offsetHeight;

}
FileProgress.prototype.setProgress = function (percentage) {
	this.fileProgressElement.className = "progressContainer green";
	this.fileProgressElement.childNodes[3].className = "progressBarInProgress";
	this.fileProgressElement.childNodes[3].style.width = percentage + "%";
};
FileProgress.prototype.setComplete = function () {
	this.fileProgressElement.className = "progressContainer blue";
	this.fileProgressElement.childNodes[3].className = "progressBarComplete";
	this.fileProgressElement.childNodes[3].style.width = "";

};
FileProgress.prototype.setError = function () {
	this.fileProgressElement.className = "progressContainer red";
	this.fileProgressElement.childNodes[3].className = "progressBarError";
	this.fileProgressElement.childNodes[3].style.width = "";

};
FileProgress.prototype.setCancelled = function () {
	this.fileProgressElement.className = "progressContainer";
	this.fileProgressElement.childNodes[3].className = "progressBarError";
	this.fileProgressElement.childNodes[3].style.width = "";

};
FileProgress.prototype.setStatus = function (status) {
	this.fileProgressElement.childNodes[2].innerHTML = status;
};

FileProgress.prototype.toggleCancel = function (show, swfuploadInstance) {
	this.fileProgressElement.childNodes[0].style.visibility = show ? "visible" : "hidden";
	if (swfuploadInstance) {
		var fileID = this.fileProgressID;
		this.fileProgressElement.childNodes[0].onclick = function () {
			swfuploadInstance.cancelUpload(fileID);
			return false;
		};
	}
};