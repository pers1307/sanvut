<?
include(DOC_ROOT.'/CORE/forms/file_group/functions.php');

$temp = array();

if ( isset($result['path']) && count($result['path']) ){

foreach ($result['path'] as $key => $mas)
{
	$mas = stripslashes($mas);
	
	$temp[]= array(
		'path' => moveUploadFiles(unserialize($mas),FILES_DIR.date('Y/m/d/')),
		'text' => isset($result['text'][$key]) ? $result['text'][$key] : '',
	);
}

}

// Полное удаление файлов
if (!empty($result['delete']) && is_array($result['delete']))
{
	foreach ($result['delete'] as $group)
	{
		if (empty($group)) continue;
		
		$files = unserialize($group);
		
		foreach ($files as $file)
		{
			$cur_file = DOC_ROOT.$file;
			
			if (!is_file($cur_file)) continue;
			
			chmod($cur_file,0777);
			unlink($cur_file);
		}
	}
}


// Удаление временной папки
if (!empty($result['upload_folder']) && is_dir($result['upload_folder']))
{
	removeDir($result['upload_folder']);
}


$data['value'] = serialize($temp);
?>