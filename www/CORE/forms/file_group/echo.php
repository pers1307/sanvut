<?
ob_start();

$sys_path = '/CORE/forms/file_group/';


// Подключим основные функции для данного элемента
include(DOC_ROOT.$sys_path.'functions.php');


$temp = !empty($value) ? unserialize($value) : false;
$sizes = (isset($thumbs)) ? preSizes2URL($thumbs) : false;
$mdiv_name = preg_replace('#[\[\]]#','',$sysname);


$temp_folder = DOC_ROOT.'tmp/upload/';
// Удаление старых временных папок
cleanTempFolder($temp_folder);
$upload_folder = $temp_folder.md5(time().$mdiv_name).'/';

$text_row_enable = (isset($data['text_row']) && !$data['text_row']) ? 0 : 1;


$tmp = get_defined_constants();

if(!isset($tmp['SWFUPLOAD']))
{
	?>
	<style type="text/css"><?=file_get_contents($_SERVER['DOCUMENT_ROOT'].$sys_path.'swfupload/css/default.css')?></style>
 	<script type="text/javascript"><?=file_get_contents($_SERVER['DOCUMENT_ROOT'].$sys_path.'swfupload/js/swfupload.js')?></script>
	<script type="text/javascript"><?=file_get_contents($_SERVER['DOCUMENT_ROOT'].$sys_path.'swfupload/js/handlers.js')?></script>
	<?
    define('SWFUPLOAD',1);
}



// Допустимые расширения
if (isset($data['allowed']) && is_array($data['allowed']) && count($data['allowed']>0))
{
	$ext_desc = 'Files';
	$ext_allowed = '*.'.implode('; *.',$data['allowed']);
}
else if ($sizes)
{
	$ext_desc = 'Images';
	$ext_allowed = '*.jpg; *.jpeg; *.gif; *.png';	
}
else 
{
	$ext_desc = 'All Files';
	$ext_allowed = '*.*';	
}

$is_uploaded = ($temp && is_array($temp)) ? count($temp) : 0;

$upload_limit = isset($data['upload_limit']) ? ($data['upload_limit']-$is_uploaded) : '100';
if ($upload_limit<0) $upload_limit = 0;
?>

<script type="text/javascript">

	<?
	if ($sizes && isset($data['resize_upload']))
	{
		$res_w = !empty($data['resize_upload'][0]) ? (int)$data['resize_upload'][0] : 1500;
		$res_h = !empty($data['resize_upload'][1]) ? (int)$data['resize_upload'][1] : 1500;
		$res_q = !empty($data['resize_upload'][2]) ? (int)$data['resize_upload'][2] : 100;
		?>
		function swfUploaded(th)
		{
			th.startResizedUpload(th.getFile(0).ID, <?=$res_w?>, <?=$res_h?>, SWFUpload.RESIZE_ENCODING.JPEG, <?=$res_q?>);
		}
		<?
	}
	else 
	{
		?>
		function swfUploaded(th)
		{
			th.startUpload();
		}
		<?
	}
	?>
	var swfu_options;
	swfu_options = {
		// Backend Settings
		upload_url: "<?=$sys_path?>upload.php",
		post_params: {
			"action": "<?=($sizes?'upload_images':'upload_files')?>",
			"sizes": "<?=($sizes ? $sizes : '0')?>",
			"sysname": "<?=$sysname?>",
			"config_name": "<?=$config_name?>",
			"upload_folder": "<?=$upload_folder?>",
			"text_row_enable": <?=$text_row_enable?>
		},

		// File Upload Settings
		file_size_limit : "<?=(isset($data['max_size']) ? $data['max_size'] : '25 MB')?>",	// 2MB
		file_types : "<?=$ext_allowed?>",
		file_types_description : "<?=$ext_desc?>",
		file_upload_limit : <?=$upload_limit?>,

		// Event Handler Settings - these functions as defined in Handlers.js
		//  The handlers are not part of SWFUpload but are part of my website and control how
		//  my website reacts to the SWFUpload events.
		swfupload_preload_handler : preLoad,
		swfupload_load_failed_handler : loadFailed,
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
		upload_complete_handler : uploadComplete,

		// Button Settings
		button_image_url : "<?=$sys_path?>swfupload/img/SmallSpyGlassWithTransperancy_17x18.png",
		button_placeholder_id : "<?=$mdiv_name?>_selectbutton",
		button_width: 180,
		button_height: 18,
		button_text : '<span class="button">Выбрать файлы</span>',
		button_text_style : '.button { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; }',
		button_text_top_padding: 0,
		button_text_left_padding: 18,
		button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
		button_cursor: SWFUpload.CURSOR.HAND,
		
		// Flash Settings
		flash_url : "<?=$sys_path?>swfupload/swf/swfupload.swf",
		flash9_url : "<?=$sys_path?>swfupload/swf/swfupload_fp9.swf",

		custom_settings : {
			upload_target : "<?=$mdiv_name?>_progress"
		},
		
		// Debug Settings
		debug: false
	};
	
	var swfu;
	swfu = new SWFUpload(swfu_options);
</script>

<input type="hidden" name="<?=$sysname?>[upload_folder]" value="<?=$upload_folder?>">

<div id="<?=$mdiv_name?>_delete"></div>

<div class="file_group">
	<div id="<?=$mdiv_name?>_uploadbox" style="<?=(!$upload_limit?'display:none;':'')?>width:125px; height:18px; border:solid 1px #7FAAFF; background-color:#C5D9FF; padding:2px; margin:5px 0px;">
		<span id="<?=$mdiv_name?>_selectbutton"></span>
	</div>
	<div id="<?=$mdiv_name?>_progress"></div>
	<div id="<?=$mdiv_name?>_filebox" style="overflow:auto;max-height:200px;width:300px;"><?
	if ($temp)
	{
		foreach ($temp as $value)
		{
			if (!empty($value))
			{
				$print_func = ($sizes) ? 'fileGroupRow_image' : 'fileGroupRow_file';
				$res_data = $print_func($value,$sysname,$mdiv_name,$text_row_enable);
				
				if (!$res_data) continue;
				
				echo '<div id="'.$res_data['unic'].'">'.$res_data['content'].'</div>';
			}
		}
	}
	?></div>
</div>
<?

return ob_get_clean();
?>