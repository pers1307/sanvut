<?

if( !function_exists('fileGroupRow_image') ){


function fileGroupRow_image($value,$sysname,$mdiv_name,$text_row=true)
{
	$si = isset($value['path']['sys_thumb']) ? $value['path']['sys_thumb'] : false;
	
	if (!$si || !is_file(DOC_ROOT.$si)) return false;
	
	$unic = 'f'.mt_rand(0,9999).mt_rand(0,9999);
	list($w,$h) = getimagesize($_SERVER['DOCUMENT_ROOT'].$si);
	
	ob_start();
	?>
	<input type="hidden" name="<?=$sysname?>[path][]" value="<?=htmlspecialchars(serialize($value['path']))?>" id="path_<?=$unic?>">
	
		<div style="float:left;margin:2px;min-height:40px;"><img src="<?=$si?>" border="0" alt=""></div>
		<div style="min-height:40px;width:200px;float:left;">
		<?
		if ($text_row)
		{
		?>
		<input size="35" type="text" name="<?=$sysname?>[text][]" value="<?=(isset($value['text'])?$value['text']:'')?>">
		<br>
		<?
		}
		?>
		<a href="javascript:void(0)" onclick="delFGRow('<?=$unic?>','<?=$sysname?>','<?=$mdiv_name?>')"><img src="/DESIGN/ADMIN/images/delete.gif" border="0" title="Удалить файл" alt="" style="margin: 4px;"></a>
		<a href="javascript:void(0)" onClick="moveFGRow(1,'<?=$mdiv_name?>','<?=$unic?>')"><img src="/DESIGN/ADMIN/images/up_page.gif" border="0" alt=""></a>
		<a href="javascript:void(0)" onClick="moveFGRow(2,'<?=$mdiv_name?>','<?=$unic?>')"><img src="/DESIGN/ADMIN/images/down_page.gif" border="0" alt=""></a>
	</div><div style="clear:both"></div><?
	
	$content = ob_get_clean();
	
	return array('unic'=>$unic,'box'=>$mdiv_name,'content'=>$content);
}

function fileGroupRow_file($value,$sysname,$mdiv_name,$text_row=true)
{
	$path_file = isset($value['path'][0]) ? $value['path'][0] : false;	
	
	if (!$path_file || empty($value['path'][0]) || !is_file(DOC_ROOT.$value['path'][0])) return false;
	
	$unic = 'f'.mt_rand(0,9999).mt_rand(0,9999);
	
	ob_start();
	?><a href="<?=$path_file?>" target="_blank"><?=basename($path_file)?></a>&nbsp;
		<input type="hidden" name="<?=$sysname?>[path][]" value="<?=htmlspecialchars(serialize($value['path']))?>" id="path_<?=$unic?>">
		<div>
			<?
			if ($text_row)
			{
			?>			
			<input size="35" type="text" name="<?=$sysname?>[text][]" value="<?=(isset($value['text'])?$value['text']:'')?>">
			<?
			}
			?>
			<a href="javascript:void(0)" onclick="delFGRow('<?=$unic?>','<?=$sysname?>','<?=$mdiv_name?>')"><img src="/DESIGN/ADMIN/images/delete.gif" title="Удалить файл" border="0" alt=""></a>
			<a href="javascript:void(0)" onClick="moveFGRow(1,'<?=$mdiv_name?>','<?=$unic?>')"><img src="/DESIGN/ADMIN/images/up_page.gif" border="0" alt=""></a>
			<a href="javascript:void(0)" onClick="moveFGRow(2,'<?=$mdiv_name?>','<?=$unic?>')"><img src="/DESIGN/ADMIN/images/down_page.gif" border="0" alt=""></a>
		</div><?
	$content = ob_get_clean();
	
	return array('unic'=>$unic,'box'=>$mdiv_name,'content'=>$content);
}



function array2json($arr) 
{
	$wrd=array('\\'=>'\\\\','/'=>'\/','"'=>'\"',"\b"=>'\b',"\t"=>'\t',"\n"=>'\n',"\f"=>'\f',"\r"=>'\r');
	
	$parts = array();
	$is_list = false;
	if (!is_array($arr)) return;
	if (count($arr)<1) return '{}';

	//Find out if the given array is a numerical array
	$keys = array_keys($arr);
	$max_length = count($arr)-1;
	
	if ($keys[0]==0 && $keys[$max_length]==$max_length) {//See if the first key is 0 and last key is length - 1
		$is_list = true;
		for ($i=0; $i<count($keys); $i++) { //See if each key correspondes to its position
			if ($i != $keys[$i]) { //A key fails at position check.
				$is_list = false; //It is an associative array.
				break;
			}
		}
	}

	foreach($arr as $key=>$value) {
		
		if (is_array($value)) { //Custom handling for arrays
			if ($is_list) 
				$parts[] = array2json($value); /* :RECURSION: */
			else 
				$parts[] = '"'.$key.'":' . array2json($value); /* :RECURSION: */
		} else {
			$str = '';
			if (!$is_list) $str = '"'.$key.'":';
			
			//Custom handling for multiple data types
			if (is_numeric($value)) $str .= $value; //Numbers
			elseif ($value === false) $str .= 'false'; //The booleans
			elseif ($value === true) $str .= 'true';
			else $str .= '"'.strtr($value, $wrd).'"'; //All other things
			// :TODO: Is there any more datatype we should be in the lookout for? (Object?)
			
			$parts[] = $str;
		}
		
	}
	$json = implode(',',$parts);
	
	if ($is_list) return '[' . $json . ']';//Return numerical JSON
	return '{' . $json . '}';//Return associative JSON
}

function moveUploadFiles($files,$folder)
{
	if (empty($files) || !is_array($files)) return false;
	
	$result = array();
	
	foreach ($files as $n=>$file)
	{
		if (!empty($_SESSION['files_uploaded'][$file]) && is_file(DOC_ROOT.$_SESSION['files_uploaded'][$file]))
		{
			$result[$n] = $_SESSION['files_uploaded'][$file];
		}
		else if ($move = moveUploadFile($file,$folder)) 
		{
			$result[$n] = $move;
			$_SESSION['files_uploaded'][$file] = $move;
		}
	}
	
	return $result;
}

function moveUploadFile($file,$folder)
{
	$file_old = DOC_ROOT.trim($file,'/');
	
	if (!is_file($file_old)) return false;
	
	if (!DirExists($folder)) debug('Не могу создать директорию '.$folder, true, true);
	
	$info = pathinfo($file_old);
	
	if (strpos($folder,$info['dirname'])!==false)
		return $file;
	
	$file_new = uniqFile($folder.$info['basename']);
	
	if (copy($file_old, $file_new))
	{
		chmod($file_new, 0777);
		unlink($file_old);
		return '/'.trim(str_replace(DOC_ROOT,'',$file_new),'/');
	}
	
	return false;
}

function removeDir($path)
{
	if (!file_exists($path) || !is_dir($path)) return false;
	
    $open = opendir($path);
    while (false !== ($file = readdir($open))) 
    {
        if ($file=='.' || $file=='..') continue;
       
        $path_file = $path.'/'.$file;
        @chmod($path_file, 0777);
        
        if (is_dir($path_file))
            removeDir($path_file);
            
        else if(is_file($path_file))
			unlink($path_file);
    }
    closedir($open);
    
    rmdir($path);
}

function cleanTempFolder($folder)
{
	if (!file_exists($folder) || !is_dir($folder)) return false;
	
    $open = opendir($folder);
    while (false !== ($file = readdir($open))) 
    {
		$file_info = pathinfo($file);
    	
        if ($file=='.' || $file=='..' || strlen($file_info['basename'])!=32 || !is_dir($folder.$file)) continue;
        
        $stat = stat($folder.$file);
        
        if ((time()-$stat['mtime']) > 3600*3) 
        	removeDir($folder.$file);
    }
    closedir($open);
}

function preSizes2URL($thumbs)
{
	$sizes = array();

	if (isset($thumbs))
	{
		foreach ($thumbs as $key => $size)
			$sizes[] = $key.'-'.implode('_',$size);
	}

	return implode(':',$sizes);
}

function preURL2Sizes($data)
{
	$sizes = array();
	
	foreach (explode(':',$data) as $dat)
	{
		list($sname,$size) = explode('-',$dat);
		
		$sizes[$sname] = explode('_',$size);
	}
	
	return $sizes;
}


}

?>