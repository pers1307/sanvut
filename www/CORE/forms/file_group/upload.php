<?
define('STOP_CMS',1);
require($_SERVER['DOCUMENT_ROOT'].'/index.php');


include(DOC_ROOT.'/CORE/forms/file_group/functions.php');


$action = !empty($_POST['action']) ? $_POST['action'] : false;
$config_name = !empty($_POST['config_name']) ? $_POST['config_name'] : '';
$sysname = !empty($_POST['sysname']) ? urldecode($_POST['sysname']) : '';
$mdiv_name = preg_replace('#[\[\]]#','',$sysname);
$sizes = !empty($_POST['sizes']) ? $_POST['sizes'] : '';
$text_row_enable = (isset($_POST['text_row_enable']) && $_POST['text_row_enable']=='0') ? false : true;

$upload_folder = (!empty($_POST['upload_folder'])) ? $_POST['upload_folder'] : DOC_ROOT.'TMP/upload/';


// Загрузка выбранных файлов
if (!empty($_FILES['Filedata']))
{
	$_FILES[$config_name]=$_FILES['Filedata'];
	unset($_FILES['Filedata']);
	
	$upload_files = UploadFiles();
}

$res = array();

ob_start();
switch($action)
{
	case 'upload_files':
		{
		$files = array($upload_files[$config_name]['path']);	
		$files = moveUploadFiles($files,$upload_folder);
			
		$res = fileGroupRow_file(array('path'=>$files),$sysname,$mdiv_name,$text_row_enable);	
		}
	break;

	case 'upload_images':
		{
		$data=array(
			'thumbs' => preURL2Sizes($sizes),
			'allowed' => array('jpg','jpeg','gif','png'),
		);
		
		$files = makeImageThumbs($data,$upload_files,$config_name, true);
		$files = unserialize($files);
		$files = moveUploadFiles($files,$upload_folder);
		
		$res = fileGroupRow_image(array('path'=>$files),$sysname,$mdiv_name,$text_row_enable);
		}
	break;
}

echo 'result_data = '.array2json($res);

//echo iconv("cp1251",'utf-8',ob_get_clean());
echo ob_get_clean();
exit;
?>