<script language="javascript" type="text/javascript">tinyMCE.init({

	mode: "textareas",
	editor_selector: "wysiwyg",
	theme: "advanced",
	plugins:                                 "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,spellchecker",
                                                                                       
	spellchecker_languages : "+Russian=ru,English=en",
	spellchecker_rpc_url : "/services/tinyspell/",
	spellchecker_word_separator_chars : '\\s!"#$%&()*+,./:;<=>?@[\]^_{|}\xa7 \xa9\xab\xae\xb1\xb6\xb7\xb8\xbb\xbc\xbd\xbe\u00bf\xd7\xf7\xa4\u201d\u201c',
        <? if( empty( $data['small'] ) ){ ?>
	theme_advanced_buttons1: "fullscreen,code,|,undo,redo,formatselect,bold,italic,|,justifyleft,justifycenter,justifyright,|,pastetext",
	theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,link,unlink,|,image,anchor,file,charmap,nonbreaking,spellchecker,|,table,visualaid",
	theme_advanced_buttons3: "",
        <? }else{ ?>
	theme_advanced_buttons1: "code,|,undo,redo,formatselect,bold,italic,|,link,unlink,|,image",
	theme_advanced_buttons2: "",
	theme_advanced_buttons3: "",
        <? } ?>
	//theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,link,unlink,|,image,anchor,file,charmap,hr,nonbreaking,template,spellchecker,|,table,visualaid",

	/*template_templates: [
		{
		    title: "Описание серии",
		    src: "/DESIGN/ADMIN/js/mce/templates/catalog_desc.html",
		    description: "Типовое наполнения для страницы описания серии в каталоге"
		}
	],*/

	theme_advanced_toolbar_location: "top",
	theme_advanced_toolbar_align: "left",
	theme_advanced_statusbar_location: "bottom",
	theme_advanced_blockformats: "p,h1,h2,h3,h4,blockquote",
	//theme_advanced_styles : "Стилизованная таблица=tbl;Стилизованная таблица (упрощенная)=tbl-simple;Рамка для изображения=brd",
	theme_advanced_resizing: false,
	theme_advanced_resize_horizontal: false,
					
	language: "ru",
	content_css: "/DESIGN/SITE/CSS/styles.css",
	auto_focus: false,
	fix_table_elements: false,
	fix_list_elements: true,
	verify_html: false,
	verify_css_classes: false,
	convert_urls: false,
	paste_use_dialog: false,
	force_br_newlines: false,
	force_p_newlines: false,	
	relative_urls: true,

	//template_external_list_url: "lists/template_list.js",
	//external_link_list_url: "lists/link_list.js",
	//external_image_list_url: "lists/image_list.js",
	//media_external_list_url: "lists/media_list.js",
	
	file_browser_callback: "tinyBrowser",
	save_onsavecallback: "postForm"
        
});</script>