<?

$_constants = get_defined_constants();
$wysiwygConfig = '';

if(!isset($_constants['TINYMCE'])){
    
	ob_start();
   		require_once(CORE_DIR.'forms/wysiwyg/config.php');
	$wysiwygConfig = ob_get_clean();
	
    define('TINYMCE',1);            
    
}

return $wysiwygConfig . '<textarea name="'.$sysname.'" id="'.$sysname.'" style="width:'.(isset($data['width']) ? (int)$data['width'] : 500).'px;height:'.(isset($data['height']) ? (int)$data['height'] : 250).'px;" class="wysiwyg">'.$value.'</textarea>';

?>