<? 
/* 
GROUP_CHECKBOX

Параметры конфигуратора:

caption - название
in_list - показывать в таблице 0/1
filter - показывать в фильтрах 0/1
value - значение (дефолтное или при редактировании)

separator - разделитель записей при выводе (по дефолту ",")
from - от куда данные array('table_name'=>'', 'key_field'=>'', 'name_field'=>'', 'where'=>'', 'order'=>'')
*/

if( function_exists( 'CB_multiple' ) === false )
{
	function CB_multiple( $conf=array(), $parent=0, $level=0, $res=array(), $tree=array() )
	{
		global $DB;
		
		$items = $DB->getAll( 'SELECT '.$conf['from']['key_field'].', '.$conf['from']['parent_field'].', '.$conf['from']['name_field'].' 
			FROM '.PRFX.$conf['from']['table_name'].' 
			WHERE '.$conf['from']['parent_field'].'="'.(int)$parent.'"'.( isset( $conf['from']['where'] ) && $conf['from']['where'] ? ' AND '.$conf['from']['where'] : '' ).'
			'.( isset( $conf['from']['order'] ) && $conf['from']['order'] ? ' ORDER BY '.$conf['from']['order'] : '' ) 
		);
		
		
		if( count( $items ) )
		{
			if( isset( $conf['note_all'] ) && (int)$parent )
				$tree[] = $parent;
			
			foreach( $items as $item )
			{
				$p = $level * 20;
				$p = $p ? ' style="padding-left:'.$p.'px;"' : '';
				
				$tmp = '';
				foreach( $conf['cols'] as $k=>$c )
				{
					if( count( $conf['cols'] ) -1 == $k )
						continue;
						
					$tmp .= echoCheck( $conf, $k+1, $item[ $conf['from']['key_field'] ], $tree );
				}
				
				$res[ $item[ $conf['from']['key_field'] ] ] = '<tr>'.$tmp.'<td class="r"'.$p.'>'.$item[ $conf['from']['name_field'] ].'</td></tr>';
				$res = CB_multiple( $conf, $item[ $conf['from']['key_field'] ], $level+1, $res, $tree );
			}
		}
		
		return $res;
	}


	function echoCheck( $conf=array(), $t=0, $id=0, $tree=array() )
	{
		$class = '';
		if( count( $tree ) )
		{
			foreach( $tree as $tr )
				$class .= ' d-parent-'.$tr.' d-parent-'.$t.'-'.$tr;
		}
		
		return '<td class="c"><input type="checkbox" '.( isset( $conf['value'][ $t ][ $id ] ) ? 'checked' : '' ).' name="'.$conf['sysname'].'['.$t.']['.$id.']" value="'.$id.'" class="d-item-'.$id.$class.'" onclick="'.$conf['idBlock'].'(this, '.$t.');" /></td>';
	}
}





$value = unserialize( $value );
$idBlock = 'CB_multiple_'.$config_name;
$tmp = array( 'sysname'=>$sysname, 'value'=>$value, 'idBlock'=>$idBlock );
$rTtitle = '';

if( isset( $note_all ) && $note_all )
	$tmp['note_all'] = $note_all;
	
if( isset( $from ) && count( $from ) )
	$tmp['from'] = $from;
	
if( isset( $cols ) && count( $cols ) )
{
	$tmp['cols'] = $cols;
	
	foreach( $cols as $k=>$c )
		$rTtitle .= '<td class="'.( count( $cols ) -1 == $k  ? 't' : 'c' ).'">'.$c.'</td>';
}


return '<div class="cb-group-multiple" id="'.$idBlock.'">
	<table cellspacing="0px">
		'.( $rTtitle ? '<tr class="t">'.$rTtitle.'</tr>' : '' ).
		implode( '', CB_multiple( $tmp ) ).
	'</table>
</div>

<script type="text/javascript">
function '.$idBlock.'( obj, t )
{
	if( obj.checked )
	{
		$( \'#'.$idBlock.' .d-item-\'+obj.value ).removeAttr( \'checked\' );
		$( \'#'.$idBlock.' .d-parent-\'+obj.value ).removeAttr( \'checked\' );
		$( obj ).attr( \'checked\', \'checked\' );
		$( \'#'.$idBlock.' .d-parent-\'+t+\'-\'+obj.value ).attr( \'checked\', \'checked\' );
	}
	else
		$( \'#'.$idBlock.' .d-parent-\'+obj.value ).removeAttr( \'checked\' );
}
</script>';
?>