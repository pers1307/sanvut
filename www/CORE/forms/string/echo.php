<?
$over = (isset($over)) ? $over : '';
$out = (isset($out)) ? $out : '';

$onover = 'onmouseover="'.$over.'"';
$onout = 'onmouseout="'.$out.'"';
$to_code = isset($to_code) ? ' onkeyup="toCode(this)" onchange="toCode(this)" ': '';

return '<input class="wide" name="'.$sysname.'" type="text" value="'.htmlspecialchars($value).'" '.$onover.' '.$onout.' '.$to_code.' '.(isset($readonly)?'readonly':'').'>';
?>