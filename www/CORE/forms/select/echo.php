<? 
/**
* Шаблон поля select.
*/

$size = isset($size) ? intval($size) : 1;											 

$s = '<select class="wide" '.(isset($multi) ? 'multiple' : '').' name="'.$sysname.(isset($multi) ? '[]' : '').'" size="'.$size.'">'."\r\n";
//$s.='<option></option>';

if (isset($values) && sizeof($values))
	{	
	foreach ($values as $key=>$val)
		 $s .= '<option value="'.$key.'"'.($key==$value ? 'selected="selected"':'').'>'.$val.'</option>'."\r\n";
	}
/*
if(sizeof($values)==0 || (!isset($values) && !isset($from) && $value))
	{	
	$s .= '<option selected="selected">'.$value.'</option>';
	}
*/

if(isset($from))
	{			 
	$values = getSelectValuesFromTable(
		$from['table_name'],
		$from['key_field'],
		$from['name_field'],
		(isset($from['where']) ? $from['where'] : ''),
		(isset($from['order']) ? $from['order'] : '')
	);

	foreach ($values as $key=>$val)
		 $s .= '<option value="'.$key.'"'.($key==$value ? 'selected="selected"':'').'>'.$val.'</option>'."\r\n";
	}


$s .= '</select>';
return $s;
?>