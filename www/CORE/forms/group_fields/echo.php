<? 
/* 
GROUP_CHECKBOX

Параметры конфигуратора:

caption - название
in_list - показывать в таблице 0/1
filter - показывать в фильтрах 0/1
value - значение (дефолтное или при редактировании)

separator - разделитель записей при выводе (по дефолту ",")
from - от куда данные array('table_name'=>'', 'key_field'=>'', 'key_name'=>'', 'where'=>'', 'order'=>'')
values - данные массива array(key1=>value1, key2=>value2, ...)
*/
global $DB,$adminmenu;
$s= '';
$item_id=intval($adminmenu->params[5]);

$value = array_flip(explode(',',$value));  
/*
$names_fields = getSelectValuesFromTable(
	$from['table_name'],
	$from['key_field'],
	$from['name_field'],
	(isset($from['where']) ? $from['where'] : ''),
	(isset($from['order']) ? $from['order'] : '')
);	
*/
$names_fields = $DB->getAll("SELECT * FROM `".$from['table_name']."` WHERE `cat_id`=".$from['cat_id']);

debug($names_fields,0,1);

$values_fields_ = $DB->getAll("SELECT * FROM `".$from['table_name']."_values` WHERE `item_id`=".$item_id);
$values_fields=array();
foreach ($values_fields_ as $a){
	$values_fields[$a['tag_id']]=$a['value'];
}

$height = '';
if (sizeof($names_fields)>6) $height = 'height: 160px;';

$s.= '<input type="hidden" name="'.$sysname.'[id]" value="'.$item_id.'"><div style="width: auto; overflow-y: scroll; '.$height.' border: 1px solid silver; margin: 3px;"><table cellspacing="0" cellpadding="0">';
foreach ($names_fields as $nf){
	$s.= '<tr><td>'.$nf['tag'].'</td><td width="16"><input id="'.$sysname.'['.$nf['id'].']" type="text" name="'.$sysname.'['.$nf['id'].']" value="'.(isset($values_fields[$nf['id']])?$values_fields[$nf['id']]:'').'" style="margin:0px; padding:0px;"></td><td>'.(isset($nf['ed'])?$nf['ed']:'').'</td></tr>';
}
$s.= '</table></div>';

return $s;
?>
