<? 
/**
* Шаблон поля select.
*/

$s = '<select class="wide" '.(isset($multiple) ? 'multiple' : '').' name="'.$sysname.(isset($multiple) ? '[]' : '').'">'."\r\n";

global $months1;

$values = $months1;

foreach ($values as $key=>$val)
	$s .= '<option value="'.$key.'"'.($key==$value ? 'selected="selected"':'').'>'.$val.'</option>'."\r\n";

$s .= '</select>';
return $s;
?>