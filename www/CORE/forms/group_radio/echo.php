<? 
$s= '';

$value = array_flip(explode(',',$value));
if (is_Array($values) && (!isset($from) || !is_array($from) || sizeof($from)<2))
	{
	foreach ($values as $key => $val)
		{
		$s.= '<label for="'.$sysname.'['.$key.']['.$val.']"><input id="'.$sysname.'['.$key.']" type="radio" name="'.$sysname.'" value="'.((int)$val).'" '.(isset($value[$key]) ? 'checked' : '').'>'.$val.'</label><br>';
		}
	}
else
	{
	$values = getSelectValuesFromTable(
		$from['table_name'],
		$from['key_field'],
		$from['name_field'],
		(isset($from['where']) ? $from['where'] : ''),
		(isset($from['order']) ? $from['order'] : '')
	);

	$height = '';
	if (sizeof($values)>6) $height = 'height: 160px;';

	$s.= '<div style="width: auto; overflow-y: scroll; '.$height.' border: 1px solid silver; margin: 3px;">';
	foreach ($values as $key => $val)
		{
		$s.= '<table cellspacing="0" cellpadding="0"><tr><td width="16"><input id="'.$sysname.'['.$key.']['.$val.']" type="radio" name="'.$sysname.'" value="'.((int)$key).'" '.(isset($value[$key]) ? 'checked' : '').' style="margin:0px; padding:0px;"></td><td><label for="'.$sysname.'['.$key.']['.$val.']" style="margin:0px; padding:0px;">'.$val.'</label></td></tr></table>';
		}
	$s.= '</div>';
	}

return $s;
?>
