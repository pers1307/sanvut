<?php

$tmp = get_defined_constants();

if(!isset($tmp['JSCAL'])){
    $jscal = '
	<style type="text/css">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/css/jscal2.css')).'</style>
    <style type="text/css">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/css/border-radius.css')).'</style>
    <style type="text/css">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/css/mediasite.css')).'</style>
	<script type="text/javascript">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/js/jscal2.js')).'</script>
    <script type="text/javascript">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/js/lang/ru.js')).'</script>
	';
    define('JSCAL',1);
}

$out = '<div style="white-space:nowrap;color:#000000;">';
$out .= $jscal;
$out .= '&nbsp;&nbsp;с <input id="'.$ftitle.'" size="10" value="'.(isset($filters[$ftitle]) ? $filters[$ftitle] : '').'" type="text" name="filters['.$ftitle.']" style="background:url(/CORE/forms/calendar/images/calendar.gif) no-repeat right;cursor:pointer;padding-right:20px"><br>';
$out .= 'по <input id="'.$ftitle.'_0_2" size="10" value="'.(isset($filters[$ftitle.'_0_2']) ? $filters[$ftitle.'_0_2'] : '').'" type="text" name="filters['.$ftitle.'_0_2]" style="background:url(/CORE/forms/calendar/images/calendar.gif) no-repeat right;cursor:pointer;padding-right:20px">';

ob_start();
?>

<script type="text/javascript">
    Calendar.setup({
        inputField    : '<?=$ftitle;?>',
        trigger       : '<?=$ftitle;?>',
        animation     : false,        
        onChange      : function(cal){
            var date = cal.selection.print('%Y-%m-%d %H:%M:00', ' ... ');
            if (date.length != 0){
                date = date.join("\n");
                document.getElementById('<?=$ftitle;?>').value = date;
                setFilter('<?=$CONFIG['module_name'];?>', '<?=$path_to_reload;?>', '<?=$output_id;?>');
            }
        },
        onSelect   : function() { this.hide() }
    });
    Calendar.setup({
        inputField    : '<?=$ftitle;?>_0_2',
        trigger       : '<?=$ftitle;?>_0_2',
        animation     : false,        
        onChange      : function(cal){
            var date = cal.selection.print('%Y-%m-%d %H:%M:00', ' ... ');
            if (date.length != 0){
                date = date.join("\n");
                document.getElementById('<?=$ftitle;?>_0_2').value = date;
                setFilter('<?=$CONFIG['module_name'];?>', '<?=$path_to_reload;?>', '<?=$output_id;?>');
            }
        },
        onSelect   : function() { this.hide() }
    });
</script>

<?

$out .= ob_get_clean();
$out .= '</div>';

?>