Calendar.LANG("fr", "FranГ§ais", {

        fdow: 1,                // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.

        goToday : "Aujourd'hui",

        today: "Aujourd'hui",         // appears in bottom bar

        wk: "sm.",

        weekend: "0,6",         // 0 = Sunday, 1 = Monday, etc.

        AM: "am",

        PM: "pm",

        mn : [ "Janvier",
               "FГ©vrier",
               "Mars",
               "Avril",
               "Mai",
               "Juin",
               "Juillet",
               "AoГ»t",
               "Septembre",
               "Octobre",
               "Novembre",
               "DГ©cembre" ],

        smn : [ "Jan",
                "FГ©v",
                "Mar",
                "Avr",
                "Mai",
                "Juin",
                "Juil",
                "Aou",
                "Sep",
                "Oct",
                "Nov",
                "DГ©c" ],

        dn : [ "Dimanche",
               "Lundi",
               "Mardi",
               "Mercredi",
               "Jeudi",
               "Vendredi",
               "Samedi",
               "Dimanche" ],

        sdn : [ "Di",
                "Lu",
                "Ma",
                "Me",
                "Je",
                "Ve",
                "Sa",
                "Di" ]

});
