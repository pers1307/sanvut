// autor: Piotr kwiatkowski
// www: http://pasjonata.net

Calendar.LANG("pl", "Polish", {

        fdow: 1,                // pierwszy dzieЕ„ tygodnia; 0 = Niedziela, 1 = PoniedziaЕ‚ek, itd.

        goToday: "Idzie Dzisiaj",

        today: "DziЕ›",         

        wk: "wk",

        weekend: "0,6",         // 0 = Niedziela, 1 = PoniedziaЕ‚ek, itd.

        AM: "am",

        PM: "pm",

        mn : [ "StyczeЕ„",
               "Luty",
               "Marzec",
               "KwiecieЕ„",
               "Maj",
               "Czerwiec",
               "Lipiec",
               "SierpieЕ„",
               "WrzesieЕ„",
               "PaЕєdziernik",
               "Listopad",
               "GrudzieЕ„" ],

        smn : [ "Sty",
                "Lut",
                "Mar",
                "Kwi",
                "Maj",
                "Cze",
                "Lip",
                "Sie",
                "Wrz",
                "PaЕє",
                "Lis",
                "Gru" ],

        dn : [ "Niedziela",
               "PoniedziaЕ‚ek",
               "Wtorek",
               "Ељroda",
               "Czwartek",
               "PiД…tek",
               "Sobota",
               "Niedziela" ],

        sdn : [ "Ni",
                "Po",
                "Wt",
                "Ељr",
                "Cz",
                "Pi",
                "So",
                "Ni" ]

});
