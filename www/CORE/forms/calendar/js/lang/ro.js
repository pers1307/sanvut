Calendar.LANG("ro", "RomГўnДѓ", {

        fdow: 1,                // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.

        goToday: "AstДѓzi",

        today: "AstДѓzi",         // appears in bottom bar

        wk: "sДѓp.",

        weekend: "0,6",         // 0 = Sunday, 1 = Monday, etc.

        AM: "am",

        PM: "pm",

        mn : [ "Ianuarie",
               "Februarie",
               "Martie",
               "Aprilie",
               "Mai",
               "Iunie",
               "Iulie",
               "August",
               "Septembrie",
               "Octombrie",
               "Noiembrie",
               "Decembrie" ],

        smn : [ "Ian",
                "Feb",
                "Mar",
                "Apr",
                "Mai",
                "Iun",
                "Iul",
                "Aug",
                "Sep",
                "Oct",
                "Noi",
                "Dec" ],

        dn : [ "DuminicДѓ",
               "Luni",
               "MarЕЈi",
               "Miercuri",
               "Joi",
               "Vineri",
               "SГўmbДѓtДѓ",
               "DuminicДѓ" ],

        sdn : [ "Du",
                "Lu",
                "Ma",
                "Mi",
                "Jo",
                "Vi",
                "SГў",
                "Du" ]

});
