<?php

$id_sysname = $sysname;
$id_sysname = str_Replace("[","_",$id_sysname);
$id_sysname = str_Replace("]","_",$id_sysname);
$id_sysname = str_Replace("__","_",$id_sysname);
				
$tmp = get_defined_constants();
$jscal = '';

if(!isset($tmp['JSCAL'])){
    $jscal = '
	<style type="text/css">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/css/jscal2.css')).'</style>
    <style type="text/css">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/css/border-radius.css')).'</style>
    <style type="text/css">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/css/mediasite.css')).'</style>
	<script type="text/javascript">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/js/jscal2.js')).'</script>
    <script type="text/javascript">'.implode("\n",file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/calendar/js/lang/ru.js')).'</script>
	';
    define('JSCAL',1);
}

$value = strtotime($value);
$date_format = empty($data['show_time']) ? 'Y-m-d' : 'Y-m-d H:i:s';
$date = $value ?  $value : date($date_format, time());
$date = empty($data['empty']) ? date($date_format, ($value ? $value : time())) : ($value ? date($date_format, $value) : '');

$out = $jscal;
$out .= '<input id="'.$id_sysname.'" type="text" name="'.$sysname.'" size="'.(empty($data['size']) ? '' : (int)$data['size']).'" value="'.$date.'" style="background:url(/CORE/forms/calendar/images/calendar.gif) no-repeat right;cursor:pointer;padding-right:20px" readonly>';
$out .= empty($data['empty']) ? '' : '<a href="javascript:void(0)" onclick="javascript:$(\'#'.$id_sysname.'\').val(\'\')" style="margin-left:10px" title="Очистить"><img src="/DESIGN/ADMIN/images/delete_page.gif" alt="Удалить" width="14" height="14" style="border:0" /></a>';

ob_start();
?>

<script type="text/javascript">
    Calendar.setup({
        inputField    : '<?=$id_sysname;?>',
        trigger       : '<?=$id_sysname;?>',
        animation     : <?=(empty($data['animation']) ? 'false' : 'true');?>,
        <?=(empty($data['week_num'])) ? '' : 'weekNumbers   : true,';?>        
        <?=(empty($data['show_time'])) ? '' : 'showTime      : 24,';?>
        <?=(empty($data['multiple'])) ? '' : 'selectionType : Calendar.SEL_MULTIPLE,';?>
        <?=(empty($data['date_min'])) ? '' : 'min           : '.(int)$data['date_min'].',';?>
        <?=(empty($data['date_max'])) ? '' : 'max           : '.(int)$data['date_max'].',';?>
        <?=(empty($data['minute_step'])) ? '' : 'minuteStep   : '.(int)$data['minute_step'].',';?>
        onChange      : function(cal){
            var date = cal.selection.print('%Y-%m-%d<?=(empty($data['show_time'])) ? '' : ' %H:%M:00';?>', ' ... ');
            if (date.length != 0){
                date = date.join("\n");
                document.getElementById('<?=$id_sysname;?>').value = date;
            }
        },
        onTimeChange  : function(cal){
            var date = cal.selection.print('%Y-%m-%d %H:%M:00', ' ... ');
            if (date.length != 0){
                date = date.join("\n");
                document.getElementById('<?=$id_sysname;?>').value = date;
            }
        },
        onSelect   : function() { this.hide() }
    });
</script>

<?

$out .= ob_get_clean();
return $out;

?>