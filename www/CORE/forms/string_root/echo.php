<?
/**
* Шаблон поля boolean.
*/
global $auth_isROOT;

$over = (isset($over)) ? $over : '';
$out = (isset($out)) ? $out : '';

$onover = 'onmouseover="'.$over.'"';
$onout = 'onmouseout="'.$out.'"';

if($auth_isROOT)return $str='<input class="wide" name="'.$sysname.'" type="text" value="'.htmlspecialchars($value).'" '.$onover.' '.$onout.'>';
else return '<span>'.htmlspecialchars($value).'</span><input id="'.$sysname.'" name="'.$sysname.'" type="hidden" value="'.htmlspecialchars($value).'">';

?>