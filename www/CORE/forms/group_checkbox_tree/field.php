<?php
/* 
GROUP_CHECKBOX

Параметры конфигуратора:

caption - название
value - значение (дефолтное или при редактировании)
in_list - показывать в таблице 0/1
filter - показывать в фильтрах 0/1
separator - разделитель записей при выводе (по дефолту ",")
from - от куда данные array('table_name'=>'', 'key_field'=>'', 'key_name'=>'', 'where'=>'', 'order'=>'')
values - данные массива array(key1=>value1, key2=>value2, ...)
*/

global $DB;

$val = $item[ $config['field_table'] ];
if( $val != '' )
{
	$tmp = array();
	$items = $DB->getAll( 'SELECT '.$config['from']['name_field'].' 
		FROM '.PRFX.$config['from']['table_name'].' 
		WHERE '.$config['from']['key_field'].' IN ('.$DB->pre( $val ).')'.( isset( $config['from']['where'] ) && $config['from']['where'] ? ' AND '.$config['from']['where'] : '' ).' 
		'.( isset( $config['from']['order'] ) && $config['from']['order'] ? ' ORDER BY '.$config['from']['order'] : '' ) 
	);
	
	if( $items )
	{
		foreach( $items as $item )
			$tmp[] = $item[ $config['from']['name_field'] ];
	}
	
	echo implode( ', ', $tmp );
	unset( $tmp );
}
?>