<? 
/* 
GROUP_CHECKBOX

Параметры конфигуратора:

caption - название
in_list - показывать в таблице 0/1
filter - показывать в фильтрах 0/1
value - значение (дефолтное или при редактировании)

separator - разделитель записей при выводе (по дефолту ",")
from - от куда данные array('table_name'=>'', 'key_field'=>'', 'name_field'=>'', 'where'=>'', 'order'=>'')
values - данные массива array(key1=>value1, key2=>value2, ...)
*/
if( function_exists( 'CB_tree' ) === false )
{
	function CB_tree( $conf=array(), $parent=0, $level=0, $res=array(), $tree=array() )
	{
		global $DB;
		
		$items = $DB->getAll( 'SELECT '.$conf['from']['key_field'].', '.$conf['from']['parent_field'].', '.$conf['from']['name_field'].' 
			FROM '.PRFX.$conf['from']['table_name'].' 
			WHERE '.$conf['from']['parent_field'].'="'.(int)$parent.'"'.( isset( $conf['from']['where'] ) && $conf['from']['where'] ? ' AND '.$conf['from']['where'] : '' ).'
			'.( isset( $conf['from']['order'] ) && $conf['from']['order'] ? ' ORDER BY '.$conf['from']['order'] : '' ) 
		);
		
		if( count( $items ) )
		{
			if( (int)$parent )
				$tree[] = $parent;
			
			foreach( $items as $item )
			{
				$class = '';
				if( count( $tree ) )
				{
					foreach( $tree as $tr )
						$class .= ' item'.$tr;
				}
				
				$res[ $item[ $conf['from']['key_field'] ] ] = '<table cellspacing="0" style="margin-left:'.( $level * 20 ).'px;">
					<tr>
						<td class="cb"><input onclick="'.$conf['idBlock'].'(this);" class="item'.$parent.$class.'" id="'.$conf['sysname'].'['.$item[ $conf['from']['key_field'] ].']" type="checkbox" name="'.$conf['sysname'].'['.$item[ $conf['from']['key_field'] ].']" value="'.(int)$item[ $conf['from']['key_field'] ].'" '.( isset( $conf['value'][ $item[ $conf['from']['key_field'] ] ] ) ? 'checked' : '' ).' /></td>
						<td><label for="'.$conf['sysname'].'['.$item[ $conf['from']['key_field'] ].']">'.$item['name'].'</label></td>
					</tr>
				</table>';
				
				$res = CB_tree( $conf, $item[ $conf['from']['key_field'] ], $level+1, $res, $tree );
			}
		}
		
		return $res;
	}
}



$value = array_flip( explode( ',', $value ) );
$idBlock = 'CB_tree_'.$config_name;
$tmp = array( 'sysname'=>$sysname, 'value'=>$value, 'idBlock'=>$idBlock );

if( isset( $from ) && count( $from ) )
	$tmp['from'] = $from;

return '<div id="'.$idBlock.'" class="cb-group-tree">
	<table cellspacing="0">
		<tr>
			<td class="cb"><input onclick="'.$idBlock.'(this);" id="'.$sysname.'[0]" type="checkbox" name="'.$sysname.'[0]" value="0" /></td>
			<td><label for="'.$sysname.'[0]">Выбрать все</label></td>
		</tr>
	</table>'.
	implode( '', CB_tree( $tmp ) ).'</div>

<script type="text/javascript">
function '.$idBlock.'( obj )
{
	if( obj.checked )
	{
		if( obj.value == 0 )
			$( \'#'.$idBlock.' input\' ).attr( \'checked\', \'checked\' );
		else
			$( \'#'.$idBlock.' .item\'+obj.value ).attr( \'checked\', \'checked\' );
	}
	else
	{
		if( obj.value == 0 )
			$( \'#'.$idBlock.' input\' ).removeAttr( \'checked\' );
		else
			$( \'#'.$idBlock.' .item\'+obj.value ).removeAttr( \'checked\' );
	}
}
</script>';
?>