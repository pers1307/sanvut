<? 
/* 
GROUP_CHECKBOX

Параметры конфигуратора:

caption - название
in_list - показывать в таблице 0/1
filter - показывать в фильтрах 0/1
value - значение (дефолтное или при редактировании)

separator - разделитель записей при выводе (по дефолту ",")
from - от куда данные array('table_name'=>'', 'key_field'=>'', 'key_name'=>'', 'where'=>'', 'order'=>'')
values - данные массива array(key1=>value1, key2=>value2, ...)
*/

$s= '';

$value = !empty($value) ? unserialize($value) : array();  

if (!is_Array($values) && !isset($from) && !is_array($from) && sizeof($from))
{	
	$values = getSelectValuesFromTable(
		$from['table_name'],
		$from['key_field'],
		$from['name_field'],
		(isset($from['where']) ? $from['where'] : ''),
		(isset($from['order']) ? $from['order'] : '')
	);
}




$height = isset($height)?'height:'.$height.';':'';

if (empty($height) && sizeof($values)>6) 
	$height = 'height: 200px;';

$s.= '
<div style="width: auto; overflow-y: scroll; '.$height.' border: 1px solid silver; margin: 3px;">
<table cellspacing="0" cellpadding="0">
';
foreach ($values as $key => $val)
{
	//$val = htmlspecialchars($val);
	
	$s.= '
		<tr>
			<td width="200">'.$val.'</td>
			<td><input type="text" name="'.$sysname.'['.$val.']" value="'.(isset($value[$val]) ? $value[$val] : '').'" style="width:250px"></td>
		</tr>
	';
}
$s.= '
</table>
</div>
';

return $s;
?>