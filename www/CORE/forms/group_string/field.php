<?php
/* 
GROUP_CHECKBOX

Параметры конфигуратора:

caption - название
value - значение (дефолтное или при редактировании)
in_list - показывать в таблице 0/1
filter - показывать в фильтрах 0/1
separator - разделитель записей при выводе (по дефолту ",")
from - от куда данные array('table_name'=>'', 'key_field'=>'', 'key_name'=>'', 'where'=>'', 'order'=>'')
values - данные массива array(key1=>value1, key2=>value2, ...)
*/


$val = $item[$config['field_table']];

if (isset($config['values']) && isset($config['values'][$val])) $from_values = 1; else $from_values = 0;

if ($val!="")
	{
	$vals = explode(',',$val);

	$tmp=array();
	if ($from_values)
		{
		foreach ($vals as $val)
			{
			if ($from_values)
				$tmp[] = $config['values'][$val];
			}
		}
	else
		{
		$where = (isset($config['from']['where']) && $config['from']['where']!="" ? $config['from']['where'] : '');
		$tmp = getSelectValuesFromTable(
					$config['from']['table_name'],
					$config['from']['key_field'],
					$config['from']['name_field'],
					($val!="" ? $config['from']['key_field'].' IN ('.$val.') '.($where!="" ? ' AND '.$where : '') : $where),
					(isset($config['from']['order']) ? $config['from']['order'] : '')
				);
		}

	if (isset($config['separator'])) 
		$sep = $config['separator']; else $sep = ', ';

	echo implode($sep,$tmp);
	unset($tmp);
	}
?>