<?php
$_CONFIG= array ();

/** Уровень ошибок - все */
error_reporting(E_ALL);

/** Имя сессии котрое будет установлено в куку */
/** Имя сайта (домен) */
/** Префикс для таблиц */

define('SESS_NAME', 'mp5'.str_replace('.','',$_SERVER['HTTP_HOST']));
define('DOMAIN',$_SERVER['HTTP_HOST']);
/* Если нам нужны какие-то GET переменные то вписываем их имена */
define('ALLOW_GET', 'JsHttpRequest;_openstat;print_version;change_lang;search_string;x;y;type;ym_playback;ym_cid;ym_lang;gclid;utm_source;utm_medium;utm_campaign;utm_term;utm_content');

/** БЕЗОПАСНОСТЬ */
/** Авторизация для уникального айпи */
define('CHECK_UNIQ_IP',0);
define('ROOT_PLACE', 'control');
/** Дефолтный доступ 0 - запрещено все, 1 - разрешено все */
define('DEFAULT_ACCESS',0);

/** Настройка логотипа */
define('LOGO','logo_adm.png');

$_CONFIG['DBHOST']= CONFIG_DBHOST;
$_CONFIG['DBNAME']= CONFIG_DBNAME;
$_CONFIG['DBUSER']= CONFIG_DBUSER;
$_CONFIG['DBPASS']= CONFIG_DBPASS;




/** Запоминаем текущий запрос пользователя (+ обрезаем "/" по краям) */

define('CURRENT_QUERY', isset($_SERVER['REQUEST_URI']) ? trim($_SERVER['REQUEST_URI'], '/'): '');

//****************************************************************************************
// Далее лудше не лазать
//****************************************************************************************


/** Выключаем magic quotes */

ini_set('magic_quotes_gpc', 0);
define('MAGIC_QUOTES', ini_get("magic_quotes_gpc") ? true : false);



/** ID-ы пользователей которые являются root (т.е. имееют доступ в админку) */

//define('ROOT_USER_IDS', '2');
//define('ADMIN_USER_IDS', '5');


//****************************************************************************************
// Все пути котррые могу потребоваться для программинга в cmss
//****************************************************************************************

/** Папка содержащая ядро cms */
/** Папка с модулями */
/** Папка с версткой */
/** Папка с файлами */


define('MODULES_DIR', DOC_ROOT.'MODULES/');
define('DES_DIR',DOC_ROOT.'DESIGN/');
define('BLOCKS_DIR',DES_DIR.'SITE/BLOCKS/');
define('FILES_DIR', DOC_ROOT.'UPLOAD/');
?>