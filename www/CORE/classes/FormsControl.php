<?php

/*
 * FormsControl
 * Генератор форм
 * 
 * Yamaha-252, 09082011
 * 
 * 
 * Список возможных параметров:
 * 
 * name         string          - Системное имя
 * caption      string          - Заголовок
 * type         string          - Тип поля
 * value        string          - Значение по умолчанию
 * values       string|array    - Значения
 * many         boolean         - Множественное имя массивом - name[]
 * many_index   int             - Индекс имени, указывается при many=true
 * error        string          - Текст ошибки
 * attr         array           - Дополнительные атрибуты тега
 * send         boolean         - Отправлять или нет поле в письме
 * noedit       boolean         - Не изменять значение поля
 * req          boolean|array   - Обязательное поле
 *      Параметры:
 *      reg     string          - Проверка регулярным выражением
 *      db      array           - Проверка запросом к базе
 *          Где:
 *          query               - запрос, 
 *          result              - ожидаемый результат
 * 
 * text         string          -надпись после поля. !дописано позже. добавлено в шаблоны не всех типов
 * 
 */

class FormsControl {
    
    public  $to_mail        = '',               // e-mail для отправки
            $from_name      = '',               // имя отправителя 
            $from_mail      = '',               // e-mail отправителя
            $title          = '',               // заголовок письма
            $add_text       = '',               // дополнительный текст в письме
            $maxfilesize    = 2097152,          // максимальный размер прикрепляемого файла
            $form_action    = '',               // action формы
            $upload_dir     = 'UPLOAD/user/',   // директория загрузки
            $formid			= '',				// id формы нужно дя JS, если форм может быть несколько
            $btnname		= 'Отправить',
            $content2		= '';
            
    private $fields         = array(),          // основной конфиг полей
            $mail_text      = '',               // генерируемый текст письма
            $files          = array(),          // список прикреплённых файлов
            $protect        = false;
    		
    
    function __construct($config, $protect=false)
    {
        $this->fields = $config;
        $this->protect = $protect;
        $this->cleanWrongFields();  // Удалим неправильные поля
        $this->setManyIndexes();    // Назначем индексы
        $this->checkUploadDir();    // Проверим папку для загрузки
        $this->fieldsHTML();        // Назначим вид полей
    }
    
    
    /*
     * Проверка всех полей
     */
    public function checkFields()
    {
        global $DB;
        
        foreach ($this->fields as $num=>$conf) 
        {
            if (!isset($conf['empty']) && (!isset($conf['req']) || !$conf['req'])) continue;
            $conf['req']=!isset($conf['req'])?'' : $conf['req'];
            $value = isset($conf['value']) ? $conf['value'] : '';
            $dynamic = isset($conf['dynamic']) && $conf['dynamic'];
            
            // Для динамических полей
            if (is_array($value))
            {
                $_value = !empty($value) ? $value : array('');
            }
            else
            {
                $_value = array($value);
            }
            
            foreach ($_value as $n=>$_data)
            {
                $error = $this->checkField($conf['type'], $conf['req'], $_data, isset($conf['empty']), $conf['name']);
                // Пишем ошибку
                if ($error)
                {
                    if ($dynamic)
                        $this->fields[$num]['error'][$n] = $error;
                    else
                    	{
                   		/*
                   		if ($error=='empty'&&!isset($conf['empty']))
						   	{                    		
	                        continue;
	                        }*/
          				$this->fields[$num]['error'] = $conf[$error];
                        }
                } 
            }
            
            // Проверка размера файлов
            if ($conf['type']=='files' && !empty($value) && is_array($value) && isset($conf['size']) && (int)$conf['size']>0)
            {
                $size = 0;
                foreach ($value as $file)
                {
                    if (is_file(DOC_ROOT.$file)) $size += filesize(DOC_ROOT.$file);
                }
                
                if ((int)$conf['size'] < $size) $this->fields[$num]['error'] = 'files size';
            }
            if ($conf['type']=='file' && !empty($value) && isset($conf['size']) && (int)$conf['size'] > 0 && (int)$conf['size'] < filesize(DOC_ROOT.$value))
            {
                $this->fields[$num]['error'] = 'file size';
            }
            
        }
    }
    
    
    /*
     * Проверка поля
     * reg  - проверка регуляркой
     * db   - запросом к базе, массив, где query - запрос, result - ожидаемый результат
     */    
    private function checkField($type, $req, $value, $empty, $name)
    {
        global $DB;
        
        if ($this->protect && (!isset($_SESSION['form_protect_key_'.$this->formid]) || !isset($_REQUEST['form_protect_key']) || $_SESSION['form_protect_key_'.$this->formid]!=$_REQUEST['form_protect_key']))
        {
            exit('Отправка невозможна');
        }
        
        // Отдельная проверка полей
        switch ($type)
        {
            // Капча
            case 'code':
                if (!isset($_SESSION['captcha']) || $_SESSION['captcha']!=$value)
                {
                    return 'code';
                }
            break;

            // Пароль с повтором
            case 'password_two':
                if (!empty($value) && is_array($value) && count($value)==2)
                {
                    $pwd = array_shift($value);
                    $repwd = array_pop($value);
                }
                if (empty($pwd) || empty($repwd))
                {
                    return 'empty';
                }
                if($pwd!=$repwd){
                	return 'passwords';
                }
                if (strlen($pwd)<4)
                {
                    return 'passwords_short';
                }
            break;

            // Загруженные файлы
            case 'file':
            case 'files':
                if (empty($value) || !is_file(DOC_ROOT.$value))
                {
                    return 'files';
                }
            break;
        }
        
        // Проверка стандартная
        if ($empty && $value=='')
        {
            return 'empty';
        }

        // Проверка запросом к базе
        elseif ($value!='')
		{
			if (isset($req['db']) && !empty($req['db']['query']))
	        {
	            $query = $req['db']['query'];
	            $query = str_replace('%field%', $DB->pre($value), $query);
	            $query = str_replace('%data%', $DB->pre($value), $query);
	            $count = $DB->getOne($query);
				
	            if ($count == $req['db']['result'])
	            {
	                return 'db';
	            }
	        }
			
			
			
	        // Проверка регуляркой
	        if (!empty($req['reg']) && !preg_match($req['reg'], $value))
	        {
	            return 'regexp';
	        }
        }
        
        return false;
    }
    
    
    /*
     * Формирует список ошибок
     */
    public function getErrors()
    {
        $errors = array();
        
        foreach ($this->fields as $conf) 
        {
            if (!empty($conf['error']))
            {
                if (is_array($conf['error']))
                {
                    foreach ($conf['error'] as $n=>$error)
                    {
                        $errors[$conf['name'].'['.$n.']'] = $error;
                    }
                }
                else
                {
                    $errors[$conf['name']] = $conf['error'];
                }
            }
        }
        
        return $errors;
    }
    
    
    /*
     * Присваевает значения полям
     */
    function processData($values, $files=array())
    {
        foreach ($this->fields as $num=>$conf) 
        {
            $_data = isset($values[$conf['name']]) ? $values[$conf['name']] : '';
            
            if (isset($conf['noedit']) && $conf['noedit'])
            {
                continue;
            }
            
            switch ($conf['type'])
            {
                case 'file':
                    if (isset($files[$conf['name']]))
                    {
                        $this->fields[$num]['value'] = $this->attachFileUpload($files[$conf['name']]);
                    }
                break;
            
                case 'files':
                    
                    $this->fields[$num]['value'] = $this->attachFilesUpload($_data);
                    
                break;
            
                default:
                    
                    if (isset($conf['many']) && $conf['many'] && isset($conf['many_index']) && (!isset($conf['dynamic']) || !$conf['dynamic']))
                    {
                        $_data = isset($_data[$conf['many_index']]) ? $_data[$conf['many_index']] : '';
                    }
                    
                    $this->fields[$num]['value'] = $_data;
                break;
            }
        }
    }
    
    
    /*
     * Генерирует текст письма из значений полей
     */
    private function generateMail()
    {
        ob_start();
        
        $values = $this->getFieldsValues(true, false);
        
        foreach ($values as $title=>$value)
        {
            if (empty($value)) continue;
            
            ?>
            <p><strong><?=$title?>:</strong> <?=nl2br(trim(htmlspecialchars($value)))?></p>
            <?
        }
        $this->mail_text = ob_get_clean();
    }
    
    
    /*
     * Проверяет на доступность папку для загрузки
     */
    private function checkUploadDir()
    {
        $dir = DOC_ROOT.$this->upload_dir;
        if (!is_dir($dir)) mkdir($dir, 0777, true);
    }
    
    /*
     * Загружает файл
     */
    private function attachFileUpload($file)
    {
        if (empty($file) || !$file) return false;
        
        // Проверяем тип входящих данных
        if (is_array($file))
        {
            if ($file['error'] == 0) return false;
            
            $file_name = $file['name'];
            $file_path = $file['tmp_name'];
            $file_size = $file['size'];
        }
        else
        {
            if (!is_file($file)) return false;
            
            $info = pathinfo($file);
            $file_name = $info['basename'];
            $file_path = $file;
            $file_size = filesize($file_path);
        }
        
        $add_file = uniqFile(DOC_ROOT.$this->upload_dir.$file_name);
        if ($file_size <= $this->maxfilesize && validExt($file_name, true) && move_uploaded_file($file_path, $add_file))
        {
            return $this->files[] = str_replace(DOC_ROOT, '/', $add_file);
        }
        
        return false;
    }
    
    
    /*
     * Загружает список файлов из сессии
     */
    function attachFilesUpload($files)
    {
        $_files = array();
        
        if (!empty($files) && is_array($files))
        {
            foreach ($files as $hash)
            {
                $file = isset($_SESSION['upload_files'][$hash]) ? $_SESSION['upload_files'][$hash] : false;
                
                if ($file && is_file($file))
                {
                    $_files[] = $this->files[] = str_replace(DOC_ROOT, '/', $file);
                }
            }
        }
        
        return $_files;
    }
    
    
    /*
     * Удаляет загруженные файлы
     */
    public function attachFilesRemove()
    {
        foreach ($this->files as $file) if (is_file(DOC_ROOT.$file)) unlink(DOC_ROOT.$file);
    }

    
    /*
     * Отправка письма на почту
     */
    public function sendMail()
    {
        $this->generateMail();
        $this->assignFields();
        
        $mail = new SendMail();
        $mail->init();
        $mail->setTo($this->to_mail);
        $mail->setSubject($this->title);
        $mail->setMessage('<html><head><title>'.$this->title.'</title></head><body>'.$this->add_text.$this->mail_text.$this->content2.'</body></html>');
        $mail->setFrom($this->from_mail, $this->from_name);
        $mail->setFiles($this->files);	
        
        if ($mail->send())
        {
            return true;
        }
        
        return false;
    }
    
    
    /*
     * Обработка данных письма
     */
    private function assignFields()
    {
        $this->from_name    = $this->assignData($this->from_name);
        $this->from_mail    = $this->assignData($this->from_mail);
        $this->title        = $this->assignData($this->title);
        $this->add_text     = $this->assignData($this->add_text);        
    }
    
    
    /*
     * Сопоставляет полученные данные в форме с текстовыми данными письма
     */
    private function assignData($data)
    {
        foreach ($this->fields as $conf) 
        {
            $data = str_replace('[['.$conf['name'].']]', $conf['value'], $data);
        }
        
        return $data;
    }
    
    
    /*
     * Проверка полей в конфиге
     */
    private function cleanWrongFields()
    {
        foreach ($this->fields as $n=>$conf) 
        {
            if (empty($conf['name']) || empty($conf['type']))
            {
                unset($this->fields[$n]);
            }
        }    
    }
    
    
    /*
     * Вывод конфига полей
     */
    public function getFields()
    {
        return $this->fields;
    }
    
    
    /*
     * Получить поле по имени
     */
    public function getFieldByName($name)
    {
        $num = $this->getPosByName($name);
        
        return ($num !== false && isset($this->fields[$num])) ? $this->fields[$num] : false;
    }
    
    /*
     * Получить позицию по имени
     */
    public function getPosByName($name)
    {
        foreach ($this->fields as $num=>$conf)
        {
            if ($conf['name'] == $name)
            {
                return $num;
            }
        }
        
        return false;
    }
    
    /*
     * Изменить значение в конфиге поля
     */
    public function setConfigData($name, $values)
    {
        $pos = $this->getPosByName($name);
        
        if ($pos !== false && isset($this->fields[$pos]) && !empty($values) && is_array($values))
        {
            foreach ($values as $name=>$value)
            {
                $this->fields[$pos][$name] = $value;
            }
            
            $this->fieldsHTML();
            
            return $this->fields[$pos];
        }
        
        return false;
    }
    
    /*
     * Получить массив имён и значений
     */
    public function getFieldsValues($titles=false, $real=true, $assoc=array(), $assoc_sp=' ')
    {
        $res = array();
        
        foreach ($this->fields as $conf)
        {
            if ($real)
            {
                $value = $conf['value'];
            }
            else
            {
                if ($conf['type']=='password_two')
                {
                    $value = array_shift($conf['value']);
                }
                if (is_array($conf['value']))
                {
                    $value = implode($conf['value'],', ');
                }
                
                if (!empty($conf['values']) && is_array($conf['values']) && isset($conf['values'][$conf['value']]))
                {
                    $value = $conf['values'][$conf['value']];
                }
                else
                {
                    $value = $conf['value'];
                }
            }
            
            if ($titles)
            {
                $res[$conf['caption']] = $value;
            }
            else
            {
                $res[$conf['name']] = $value;
            }
        }
        
        /* если нужно некоторые поля склеить в одно
         */
        foreach ($assoc as $name=>$values)
        {
            $value = array();
            foreach ($values as $val)
            {
                if (!isset($res[$val])) continue;
                
                $value[] = $res[$val];
                unset($res[$val]);
            }
            
            $res[$name] = implode($value, $assoc_sp);
        }
        
        return $res;
    }
    
    public function setErrorAttr()
    {
        foreach ($this->fields as $num=>$conf) 
        {
            if (!empty($conf['error']))
            {
                if (!empty($this->fields[$num]['attr']['class']))
                {
                    $this->fields[$num]['attr']['class'] .= 'error';
                }
                else
                {
                    $this->fields[$num]['attr']['class'] = 'error';
                }
            }
        }
    }
    
    /*
     * Вывод готовой формы
     */
    public function getForm($json=false)
    {
        $this->setErrorAttr();
        $this->fieldsHTML();
        
        ob_start();
        ?>
        <div class="price_form">
            <form id="<?=$this->formid?>" action="<?=$json?'':$this->form_action?>" method="post" enctype="multipart/form-data">
                <?
                if ($this->protect)
                {
                    $key = md5(time().rand());
                    $_SESSION['form_protect_key_'.$this->formid] = $key;
                    auth_StoreSession();
                    ?>
                    <input type="hidden" name="form_protect_key" value="<?=$key?>"/>
                    <?
                }
                
                foreach ($this->fields as $conf) 
                {
                    ?>
                    <div class="item <?=$conf['name']?> <?=isset($conf['error'])&&$conf['error'] ? 'error':''?> <?=isset($conf['class'])&& $conf['class'] ? $conf['class']:''?>">
                        <?
                        if (!empty($conf['caption']))
                        {
                            ?>
                            <label class="labelBlock">
                                <span>
                                    <?= $conf['caption'] . ( isset($conf['empty']) ?'<i> *</i>':'')?>:
                                </span>
                            </label>
                            <?
                        }
                        ?>
                        <?= $conf['html'] ?>
                        <?//=(!empty($conf['comment'])?'<span class="comment">'.$conf['comment'].'</span>':'')?>
                        <div class="comment"><?=isset($conf['text']) ? $conf['text']:''?></div>
                        <div class="errormsg"><?=isset($conf['error']) ? $conf['error']:''?></div>
                    </div>
                    <?
                }
                ?>

                <br>

                <button type="submit" <?=($json&&$this->form_action? "onclick=\"getJson('{$this->form_action}', $(this).parents('form').first().serialize()); return false;\"":"")?>>
                    <?=$this->btnname?>
                </button>
            </form>
        </div>
        <?
        return ob_get_clean();
    }

    /*
     * Генерация джаваскрипта
     */
    public function getJS()
    {
		$this->setErrorAttr();
		$this->fieldsJS();
		$script='';
		foreach ($this->fields as $conf)
		{
			$script.="{$conf['JS']}";
		}
		return $script;
		
    }
    
    private function fieldsJS()
    {
        foreach ($this->fields as $num=>$conf) 
        {
            $this->fields[$num]['JS'] = $this->fieldJS($conf);
        }
    } 

    /*
     * Генерирует JS для поля
     */
    public function fieldJS($conf)
    {
   	/*$value = (isset($conf['value'])) ? htmlspecialchars($conf['value']) : '';
        $values = (!empty($conf['values']) && is_array($conf['values'])) ? $conf['values'] : array();*/

        $name = $conf['name'];
        if (isset($conf['many']) && $conf['many'])
        {
            $name = $name.'['.(isset($conf['many_index'])?$conf['many_index']:'').']';
        }
        
        /*$text=isset($conf['error']) ? '<div class="errormsg">'.$conf['error'].'</div>' 
			: (isset($conf['text']) ? '<div class="text">'.$conf['text'].'</div>' : '');*/
        
        ob_start();
        switch ($conf['type']) 
        {
            case 'string':
            case 'password':
            case 'password_two':
            case 'memo':
            	$error=isset($conf['error']) && $conf['error'] ? true : false ;
            	$formid=isset($this->formid) && $this->formid ? '#'.$this->formid :'';
            	$errormsg="$('form{$formid} .{$conf['name']} .errormsg').text('".( $error ? "{$conf['error']}" : '' )."');";
            	$errorclass="$('form{$formid} .{$conf['name']}').".($error?'add':'remove')."Class('error');";
            	
            	if ($conf['type']=='password'){
            		$errorclass.="$('form{$formid} .{$conf['name']}').val('');";
            	}
            	
            	//$text=isset($conf['text']) ? ".text({$conf['text']})" : '';
                return $errormsg.$errorclass;
            break;

            /*case 'memo':
            	return "alert('{$text}');";*/
                /*?>
                <textarea name="<?=$name?>"<?=$this->getAttr($conf, array('class'=>'memo'))?>><?=$value?></textarea>
               <?=$text?>
                <?*/
            /*break;*/

            case 'code':
            	return "alert('{$text}');";
                /*?>
                <img src="/captcha/" alt="" class="codeimg" style="display: inline-block;padding: 0 10px;vertical-align: middle;width: 25%;"/>&nbsp;<input style="width: 65%;" type="text" name="<?=$name?>"<?=$this->getAttr($conf, array('class'=>'string code'))?> />
                <?*/
            break;


        }

        return '';
    
    }
     
    /*
     * Находит все множественные элементы и определяет индексы для них
     */
    private function setManyIndexes()
    {
        $fields = array();
        foreach ($this->fields as $num=>$conf) 
        {
            if (!isset($conf['many']) || !$conf['many'] || isset($conf['many_index'])) continue;
            
            $fields[$conf['name']][] = $num;
        }
        
        foreach ($fields as $name=>$items)
        {
            foreach ($items as $index=>$num)
            {
                $this->fields[$num]['many_index'] = $index;
            }
        }
    }
    
    
    /*
     * Назначает вид полей
     */
    private function fieldsHTML()
    {
        foreach ($this->fields as $num=>$conf) 
        {
            $this->fields[$num]['html'] = $this->fieldHTML($conf);
        }
    }
    
    
    /*
     * Генерирует HTML для поля
     */
    public function fieldHTML($conf)
    {
        $value = (isset($conf['value'])) ? htmlspecialchars($conf['value']) : '';
        $values = (!empty($conf['values']) && is_array($conf['values'])) ? $conf['values'] : array();

        $name = $conf['name'];
        if (isset($conf['many']) && $conf['many'])
        {
            $name = $name.'['.(isset($conf['many_index'])?$conf['many_index']:'').']';
        }
        
        /*$text=isset($conf['error']) ? '<div class="errormsg">'.$conf['error'].'</div>' 
			: (isset($conf['text']) ? '<div class="text">'.$conf['text'].'</div>' : '');*/
        
        ob_start();
        switch ($conf['type']) 
        {
            case 'string':
                
                ?>
                <input type="text" name="<?=$name?>" value="<?=$value?>" <?=$this->getAttr($conf, array('class'=>'string'))?> />
				
                <?
            break;
			
						case 'simpletext':
                ?>
                <div <?=$this->getAttr($conf, array('class'=>'simpletext'))?>><?=$conf['content']?></div>
                <?
            break;
			
            case 'uin':
                ?>
                <input type="text" name="<?=$name?>" value="<?=$value?>"<?=$this->getAttr($conf, array('class'=>'string'))?> />&nbsp;<button type="button" class="button-small" id="uin-check">Проверка</button>
                <div id="result"></div>
                <?
            break;
        
            case 'hidden':
                ?>
                <input type="hidden" name="<?=$name?>" value="<?=$value?>"<?=$this->getAttr($conf)?> />
                <?
            break;

            case 'checkbox':
                ?>
                <input type="checkbox" name="<?=$name?>" value="<?=$value?>"<?=$this->getAttr($conf)?> />
                <?
            break;

            case 'radio':
                foreach ($values as $n=>$v)
                {
                    ?>
                    <label><input type="radio" name="<?=$name?>" value="<?=$n?>" autocomplete="off"<?=($value===$n?' checked="checked"':'')?><?=$this->getAttr($conf, array('class'=>'radio'))?> />&nbsp;—&nbsp;<?=$v?></label>
                    <?
                }
            break;

            case 'password':
                ?>
                <input type="password" name="<?=$name?>" value="<?=$value?>"<?=$this->getAttr($conf, array('class'=>/*'password'*/'string'))?> />
                <?
            break;

            case 'password_two':
                ?>
                <input type="password" name="<?=$name?>[]" value=""<?=$this->getAttr($conf, array('class'=>'password'))?> />
                <?=isset($conf['text2']) ? '<div class="text2">'.$conf['text2'].'</div>' : ''?>
                <?=isset($conf['caption2']) ? '<div class="title">'.$conf['caption2'].'</div>' : ''?>
				<input type="password" name="<?=$name?>[]" value=""<?=$this->getAttr($conf, array('class'=>'password'))?> />
                <?
            break;

            case 'files':
                ?>
                <button name="<?=$name?>"<?=$this->getAttr($conf, array('class'=>'button-small button-file'))?>><?=$conf['caption']?></button>
                <?
            break;

            case 'file':
                ?>
                <input type="file" name="<?=$name?>"<?=$this->getAttr($conf, array('class'=>'file'))?> />
                <?
            break;

            case 'memo':
                ?>
                <textarea name="<?=$name?>"<?=$this->getAttr($conf, array('class'=>'memo'))?>><?=$value?></textarea>
               	
                <?
            break;

            case 'list':
                ?>
                <select name="<?=$name?>"<?=$this->getAttr($conf, array('class'=>'list'))?>>
                    <?
                    foreach ($values as $n=>$v)
                    {
                        ?>
                        <option value="<?=$n?>"<?=($n==$value?' selected':'')?>><?=$v?></option>
                        <?
                    }
                    ?>
                </select>
                <?
            break;

            case 'code':
                ?>
                <img src="/captcha/" alt="" class="codeimg" style="display: inline-block;padding: 0 10px;vertical-align: middle;width: 25%;"/>&nbsp;<input style="width: 65%;" type="text" name="<?=$name?>"<?=$this->getAttr($conf, array('class'=>'string code'))?> />
                <?
            break;

            case 'calendar':
                ?>
                <div class="calendar-box">
                    <div class="calendar-field"><?=$value?></div>
                    <div class="calendar">
                        <span class="link"><a href="#">Календарь</a></span>
                        <div class="data">
                            <?
                            if (empty($values) || !is_array($values))
                            {
                                ?><div style="text-align:center">Свободных дней нет</div><?
                            }
                            ?>
                        </div>
                    </div>
                    <input type="hidden" name="<?=$name?>-dates" class="dates" value="<?=!empty($values) ? implode($values, ';') : ''?>"/>
                    <input type="hidden" name="<?=$name?>" value="<?=$value?>"<?=$this->getAttr($conf, array('class'=>'value', 'autocomplete'=>'off'))?> />
                </div>
                <?
            break;
        }

        return ob_get_clean();
    }
    
    
    /*
     * Генерирует атрибуты для тегов из конфига
     */
    private function getAttr($conf, $add=array())
    {
        $attr = array();
        if (!empty($conf['attr']))
        {
            foreach ($conf['attr'] as $n=>$v)
            {
                if (isset($add[$n])) 
                {
                    $v .= ' '.$add[$n];
                    unset($add[$n]);
                }
                
                $attr[] = $n.'="'.$v.'"';
            }
        }
        
        foreach ($add as $n=>$v)
        {
            $attr[] = $n.'="'.$v.'"';
        }
        
        return ' '.implode($attr,' ');
    }
    
    public function CreateXML($config){
    	//ob_start();
    	//debug($this);
    	$content='';
    	
		$xml=array();
    	foreach($this->fields as $fild){
    		$fild['InXml']=isset($fild['InXml']) ? $fild['InXml'] : $config['defaultInXml'] ;
    		
    		if (isset($fild['xmlInContent']) && $fild['xmlInContent']==true){
    				$content=$fild['value'];
    				continue;
    			}
    			
    		if( isset($fild['xmlName']) && $fild['xmlName'] ){
    			
				if ($fild['xmlName']==true){
					$fild['xmlName']=$fild['name'];
				}
				
				$xml[]=$fild['xmlName'].'="'.XMLPregReplace($fild['value']).'"';
    		}
    		
    		/*if ($fild['InXml']){
    			$xml[]=$fild['name'].'="'.$fild['value'].'"';
    		}*/
    	};
    	if (!empty($config['external']) && is_array($config['external']) ){
	    	foreach($config['external'] as $name=>$value){
	    		$xml[]=$name.'="'.XMLPregReplace($value).'"';
	    	};
    	};
    	
    	$xmlString=implode(' ',$xml);
    	return /*htmlspecialchars*/("<{$config['tagName']} $xmlString>$content</{$config['tagName']}>\r\n")/*.ob_get_clean()*/;
    }
    
}

?>
