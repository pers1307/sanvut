<?php

/**
* Paginator
* Класс для пострения постраничной навигации вида
* 1 ... 5 6 7 _8_ 9 10 11 ... 24
*/
class ModelPaginator
	{
	
	private
		$CurrentPage = 0, 
		$TotalPages = 0,		
		$PageNearby = 1;

	/**
	 * Указываем текущую страницу
	 * @return boolean
	 */
	public function setCurrentPage ( $key )
		{
			
		return $this->CurrentPage = (int)$key;

		}

	/**
	 * Указываем общее количество страниц, с нуля
	 * @return boolean
	 */
	public function setTotalPages ( $totalPage )
		{
		
		$this->TotalPages = (int)$totalPage;	

		return true;

		}

	/**
	 * Количество выводимых страниц справа и слева от активной страницы,
	 * не считая первой и последней
	 * @return boolean
	 */	
	public function setPagesNearby ( $pageNearby )
		{
		
		return $this->PageNearby = (int)$pageNearby;

		}
	
	/**
	 * Генерация результата
	 * @return array
	 */
	public function init ()
		{
		
		$this->NearbyFrom = $this->getNearbyFrom();
		$this->NearbyTo = $this->getNearbyTo();

		$Paginator = array();

		for ( $key=0; $key<=$this->TotalPages; $key++ )
			{
			
			/** Если в списке выводимых */
			if ( $this->isShow( $key ) )
				{

				$Paginator[ $key ] = $this->getPageArray( $key );

				}

			/** Если скипинатор */
			if ( $Skipinator = $this->isSkipinator( $key ) )
				{
				
				$Paginator[ $key ] = $this->getPageArray( $key, $Skipinator );

				}

			}

		return $Paginator;

		}

	/**
	 * Возвращает key начальной близлежайшей страницы
	 * @return integer
	 */
	private function getNearbyFrom ()
		{

		$from = $this->CurrentPage - $this->PageNearby;

		if ( $from < 0 )
			{
				
			$from = 0;

			}
		/** Если начальная близлежайшая страница больше суммы близлежайших страниц по обе стороны*/
		elseif ( $from > $this->TotalPages - ($this->PageNearby*2) )
			{
			
			/** Вычитаем сумму близлежайших страниц по обе стороны */
			$from = $this->TotalPages - ($this->PageNearby*2);

			}
		
		if ( $this->CurrentPage+($this->PageNearby*2) > $this->TotalPages )
			{
			
			$from = $this->CurrentPage - ($this->PageNearby*2);

			}

		return $from;

		}

	/**
	 * Возвращает key конечной близлежайшей страницы
	 * @return integer
	 */
	private function getNearbyTo ()
		{

		/** Текущяя страница, плюс количество близлежайшей страниц */
		$to = $this->CurrentPage + $this->PageNearby;
		
		/** Если начальная близлежайшая страница меньше первой страницы */
		if ( $this->CurrentPage - $this->PageNearby <= 0 )
			{

			/** Если NearbyFrom меньше нуля, плюс оличество близлежайшей страниц по обе стороны */
			$to = $this->PageNearby*2;

			}

		if ( $this->CurrentPage-($this->PageNearby*2) < 0 )
			{
			
			$to = $this->PageNearby*2;

			}

		return $to;

		}

	/**
	 * Принадлежит-ли страница числу крайних
	 * @return boolean
	 */
	private function isSide ( $key )
		{

		return in_array( $key, array( 0, $this->TotalPages ) );
			
		}

	/**
	 * Страница активна
	 * @return boolean
	 */
	private function isActive ( $key )
		{
		
		return $key === $this->CurrentPage;
			
		}

	/**
	 * Показывать данную страницу
	 */
	private function isShow ( $key )
		{

		/** Если крайняя */
		if ( $this->isSide( $key ) )
			{

			return true;

			}

		/** Если входит в пределы близлежайших */
		if ( $this->inRangeNearby( $key ) )
			{
				
			return true;

			}

		if ( $this->isSkipOne( $key ) )
			{
			
			return true;

			}

		return false;

		}

	/**
	 * Метод пропускальщик :) Определяет нужно или нет в заданную позицию
	 * вставить флаг для определения отображения свернутого списка
	 * @return boolean
	 */
	private function isSkipinator ( $key )
		{

		/** Если key равен идущему после начального элемента или перед конечным */
		if ( in_array( $key, array( 1, $this->TotalPages - 1 ) ) )
			{

			/** Данный key не входит в список выводимых */
			if ( $this->isShow( $key ) === false && $this->isSide( $key ) === false )
				{

				return true;

				}

			}

		return false;

		}

	/**
	 * Возвращает информацию о странице
	 * @return array
	 */
	private function getPageArray ( $key, $Skipinator = false )
		{

		$Array = array();

		$Array['title'] = $key+1;
		$Array['active'] = $this->isActive( $key );
		$Array['skip'] = $Skipinator;

		return $Array;

		}

	/**
	 * Элемент сверенут один
	 * @return array
	 */
	private function isSkipOne ( $key )
		{
		
		/** Проверяем первый пропуск */
		if (  $key-1 == 0 && $this->inRangeNearby( $key+1 ) )
			{
			
			return true;

			}

		/** Проверяем последний пропуск */
		if (  $key+1 == $this->TotalPages && $this->inRangeNearby( $key-1 ) )
			{
			
			return true;

			}

		return false;

		}

	/**
	 * Проверет вхождение в диапазон
	 * @return bool
	 */
	private function inRangeNearby ( $key )
		{

		return $key >= $this->NearbyFrom && $key <= $this->NearbyTo;

		}

	}

?>