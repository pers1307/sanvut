<?php

// user defined error handling function
function userErrorHandler($errno, $errmsg, $filename, $linenum, $vars){
    if (error_reporting()==0) return;
	if ($errmsg=="") return;
	static $block_debug_id;

	if (!isset($block_debug_id)) $block_debug_id = 1;

	
//	if(!auth_isROOT())$err='NOT ROOT';
	
    $dt = date("Y-m-d H:i:s (T)");
    $errortype = array (
                E_ERROR              => 'Error',
                E_WARNING            => 'Warning',
                E_PARSE              => 'Parsing Error',
                E_NOTICE             => 'Notice',
                E_CORE_ERROR         => 'Core Error',
                E_CORE_WARNING       => 'Core Warning',
                E_COMPILE_ERROR      => 'Compile Error',
                E_COMPILE_WARNING    => 'Compile Warning',
                E_USER_ERROR         => 'User Error',
                E_USER_WARNING       => 'User Warning',
                E_USER_NOTICE        => 'User Notice',
                E_STRICT             => 'Runtime Notice',
                );

	if ($errno == E_STRICT) return;

    $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);

    $err  = "\n";
    $err .= "Время: " . $dt . "\n";
    $err .= "Тип ошибки: " . $errortype[$errno] . "\n";
    $err .= "Ошибка: " . $errmsg . "\n";
    $err .= "Файл: " . $filename . "\n";
    $err .= "Строка: " . $linenum . "\n";

	if (E_ERROR != $errno && $err!="") {
		$tmp = explode('/',trim($_SERVER['REQUEST_URI'],'/'));


		if (in_array($errno, array(E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR)) )
		{
			header('HTTP/1.1 503 Service Temporarily Unavailable');
			header('Status: 503 Service Temporarily Unavailable');
		}

		if ($tmp[0]!=ROOT_PLACE){
			if ($block_debug_id==1) {
				echo '
				<script type="text/javascript">
				function showhide_errorblock(num,act){
					var obj = document.getElementById(\'errorblock\'+num);

					if (act==1){
						obj.style.display=\'\';
					} else {
						obj.style.display=\'none\';
					}
				}

				function showerror(num){
					var obj = document.getElementById(\'errorblock\'+num);
					var ttt = obj.innerHTML;
					ttt=ttt.replace("<pre>","");
					ttt=ttt.replace("</pre>","");
					ttt=ttt.replace("<PRE>","");
					ttt=ttt.replace("</PRE>","");
					alert(ttt);
				}

				</script>
				<style type="text/css">
					.err_link {text-decoration: none; color: red; font-size: 12px; font-weight: bold; font-family: Verdana;}
					.err_block { position: absolute; background: #FDFAE3; border: 1px solid black;}
				</style>
				';
			}

			echo '[<a href="#" onclick="showerror('.$block_debug_id.')" class="err_link" onmouseover="showhide_errorblock('.$block_debug_id.',1);" onmouseout="showhide_errorblock('.$block_debug_id.',0);">X</a>]<div id="errorblock'.$block_debug_id.'" class="err_block" style="display: none;"><pre>'.$err.'</pre></div>';
			$block_debug_id++;
		} else {
			echo '<div style="background: #FDFAE3; border: 1px solid black"><pre>'.$err.'</pre></div>';
		}
	}
}

error_reporting(E_ALL);
$old_error_handler = set_error_handler("userErrorHandler");
?>