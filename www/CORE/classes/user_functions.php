<?

function fast_get_catalog($type, $id)
{
    global $DB;

    $res = array();

    while (1)
    {
        if ($id=='0') break;

        $item = $DB->getRow("SELECT * FROM `mp_".$type."_articles` WHERE ".checkCode($id));

        if (empty($item)) break;

        $id = $item['parent'];

        $res[$item['id']] = $item['name'];
    }

    return array_reverse($res, true);
}

function articleSubsId($id)
{
    global $DB;

    $res = array($id);

    $subs = $DB->getAll("SELECT `id` FROM `mp_catalog_articles` WHERE `parent`=".$id);
    foreach ($subs as $sub)
    {
        $res = array_merge($res, articleSubsId($sub['id']));
    }

    return $res;
}

function prevPathId($id)
{
    global $DB;

    for($i=0;$i<255;$i++)
    {
        $item = $DB->getRow("SELECT `path_id`, `parent` FROM `mp_www` WHERE `path_id`=".(int)$id);

        if (empty($item) || $item['parent']==0 || $item['parent']==1)
        {
            return $id;
        }
        else
        {
            $id = $item['parent'];
        }
    }

    return 0;
}

function getAllEmails($cat=1) {
	global $DB;

	$mailers=$DB->getAll("SELECT `mail` FROM `mp_mailer` WHERE `cat`=".$cat);
	$mails=Array();
	foreach ($mailers as $email)
		$mails[]=$email['mail'];

	return implode(";", $mails);
}

function pageNum($data)
{
	if (strpos($data,'page')!==false)
		return (int)str_replace('page','',$data);

	return 1;
}

function pageUrl($page)
{
	$cur_path=trim(path(getPathId()),'/');

	$url = array($cur_path);
	if ((int)getParam(0)>0) $url[] = (int)getParam(0);
	if ((int)getParam(1)>0) $url[] = (int)getParam(1);
	if ((int)getParam(2)>0) $url[] = (int)getParam(2);

	$url[]='page'.$page;

	return implode('/',$url);
}

function getClientPages($page=1, $count, $onpage=10)
{
    if (!$onpage) return array('', '');

    $pages=ceil($count/$onpage);

    $get_pages=Array();
    for($i=1;$i<=$pages;$i++) {
        if ($page<($pages-7) && $pages>10 && $i==($pages-5))
            $get_pages[]='space';

        if ($i<6 || $i>($pages-5) || $i==$page || ($i>=($page-2) && ($i<$page)) || ($i<=($page+2) && ($i>$page)))
            $get_pages[]=$i;

        if ($page>8 && $i==5 && $pages>10)
            $get_pages[]='space';
    }

    $lim_start=($pages>1)?($page-1)*$onpage:0;
    $get_limit=' LIMIT '.$lim_start.','.$onpage;
    return Array($get_limit,$get_pages);
}

// Список страниц

function printPages($pages, $_page, $way='')
{
	if (count($pages)>1)
	{
		$res=Array();
	    foreach($pages as $pg)
	    {
	        if ($pg=='space')
	        	$res[]='<span>...</span>';
	        else
	        	$res[]='<a href="'.str_replace('%page%', $pg, $way).'"'.($_page==$pg?' class="select"':'').'>'.$pg.'</a>';
	    }

	    return '
        <div class="pages">
            '.($_page>1 ? '<a href="'.str_replace('%page%', ($_page-1), $way).'">«</a> ' : '').'
            <span class="items">'.implode('',$res).'</span>
            '.($_page<count($pages) ? '<a href="'.str_replace('%page%', ($_page+1), $way).'">»</a> ' : '').'
        </div>';
	}

	return '';
}

function file_size($size,$n=0)
{
	$filesizename = array(" байт", " Кб", " Мб", " Гб", " TB", " PB", " EB", " ZB", " YB");
	return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), $n) . $filesizename[$i] : '0 Bytes';
}

function getCode($item=false)
{
    if (!$item || empty($item['id'])) return '0';

    if (!empty($item['code']) && preg_match('/^[a-z0-9_]+$/i', $item['code']))
    {
        return strtolower($item['code']);
    }

    return $item['id'];
}

function checkCode($value, $not=false)
{
    global $DB;

    if (preg_match('/^[0-9]+$/', $value) && substr($value, 0, 1)!='0')
        return "`id`".($not?'!':'')."='".$DB->pre($value)."'";

    if (preg_match('/^[a-z0-9_]+$/i', $value))
        return "`code`".($not?'!':'')."='".$DB->pre($value)."'";

    page404();
}

function redirectToCode($value, $code)
{
    if (!empty($code) && $code!=$value && preg_match('/^[0-9]+$/', $value) && preg_match('/^[a-z0-9_]+$/i', $code))
        return true;

    return false;
}

function generatePassword($length = 8)
{
    $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
    $num = strlen($chars);
    $string = '';

    for ($i = 0; $i < $length; $i++)
    {
        $string .= substr($chars, rand(1, $num) - 1, 1);
    }

    return $string;
}

function getBlocksConfig($name)
{
    $config_file = BLOCKS_DIR.'configs/'.$name.'.php';

    
    if (is_file($config_file))
    {
        return include($config_file);
    }
    
    

    return false;
}

function authUserInfo()
{
    global $DB;

    if (!empty($_SESSION['auth']) && (int)$_SESSION['auth'])
    {
        $user = $DB->getRow("SELECT * FROM `mp_users_site` WHERE `id`=".(int)$_SESSION['auth']);

        if (!empty($user))
        {
            return $user;
        }
    }

    return false;
}

function getRootPathId($id)
{
    global $DB;

    for ($i=0;$i<255;$i++)
    {
        $parent = (int)$DB->getOne("SELECT `parent` FROM `mp_www` WHERE `path_id`=".(int)$id);

        if ($parent==1)
        {
            return $id;
        }

        $id = $parent;
    }

    return 0;
}

function printTags($data)
{
    global $DB;

    if (empty($data)) return '';

    $ids = explode(',',$data);
    array_map('safe', $ids);

    if (!empty($ids))
    {
        $items = $DB->getAll("SELECT * FROM `mp_tags` WHERE `id` IN (".implode($ids, ',').")");
        if (!empty($items))
        {
            ?>
            <div class="tags">
                <span>Метки:</span>
                <?
                foreach ($items as $item)
                {
                    ?>
                    <a href="/tag/<?=$item['id']?>/"><?=$item['tag']?></a>
                    <?
                }
                ?>
            </div>
            <?
        }
    }
}


function getVideoHTML($data, $width=400, $height=300)
{
    if (empty($data)) return;

    $video = unserialize($data);

    if (empty($video['id'])) return;

    ob_start();
    ?>
    <object style="height: <?=$height?>px; width: <?=$width?>px">
        <param name="movie" value="http://www.youtube.com/v/<?=$video['id']?>?version=3&feature=player_detailpage">
        <param name="allowFullScreen" value="true">
        <param name="allowScriptAccess" value="always">
        <embed src="http://www.youtube.com/v/<?=$video['id']?>?version=3&feature=player_detailpage" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="<?=$width?>" height="<?=$height?>">
    </object>
    <?
    return ob_get_clean();
}

function getVideoImage($data, $width=400, $height=300, $title='')
{
    if (empty($data)) return;

    $video = unserialize($data);

    if (empty($video['id'])) return;

    ob_start();
    ?>
    <img src="http://i.ytimg.com/vi/<?=$video['id']?>/hqdefault.jpg" alt="<?=$title?>" width="<?=$width?>" height="<?=$height?>" />
    <?
    return ob_get_clean();
}

function printAddFaq($tab_id, $_page=1, $service=false)
{
    global $DB;

    $table = ($service) ? 'mp_add_faq_service' : 'mp_add_faq';

    list($limit, $pages) = getClientPages($_page,$DB->getOne("SELECT count(*) FROM `".$table."` WHERE `tab_id`=".$tab_id." AND `active`=1"), 3);

    $items = $DB->getAll("SELECT * FROM `".$table."` WHERE `tab_id`=".$tab_id." AND `active`=1 ORDER by `date` DESC ".$limit);

    ob_start();
    if (!empty($items))
    {
        ?>
        <div class="items">
        <?
        foreach ($items as $item)
        {
            ?>
            <div class="item">
                <div class="date"><?=Sql2Date($item['date'], 10)?></div>
                <div class="name"><?=$item['name']?></div>
                <div class="text1"><?=$item['text1']?></div>
                <?
                if (!empty($item['text2']))
                {
                    ?>
                    <div class="text2">
                        <div class="title">Ответ:</div>
                        <div class="fulltext"><?=$item['text2']?>  </div>
                    </div>
                    <?
                }
                ?>
            </div>
            <?
        }
        ?>
        </div>
        <?
    }
    else
        echo '<p>Вопросы отсутствуют</p>';

    echo printPages($pages, $_page, '/json/get_faq/'.$tab_id.'/%page%/'.($service?1:0).'/');

    return ob_get_clean();
}

function catalogMainArticlesSelect($path_id, $sel=false, $parent=0)
{
    global $DB;

    $items = $DB->getAll("SELECT * FROM `mp_catalog_articles` WHERE `parent`=".$parent." AND `visible`='1' ORDER by `order`");

    $res = array();
    foreach ($items as $item)
    {
        //if (!in_array($item['id'], $show_arts)) continue;

        $res[] = '<option value="/'.path($path_id, getCode($item)).'/"'.($item['id']==$sel ? ' selected' : '').'>'.$item['name'].'</option>';
    }

    return $res;
}

function catalogArticlesSelect($path_id, $sel=false, $show_arts=array(), $parent=0, $level=0)
{
    global $DB;

    $items = $DB->getAll("SELECT * FROM `mp_catalog_articles` WHERE `parent`=".$parent." AND `visible`='1' ORDER by `order`");

    $res = array();
    $show_art = false;
    foreach ($items as $item)
    {
        list($subs, $show) = catalogArticlesSelect($path_id, $sel, $show_arts, $item['id'], ($level+1));

        if (!$show) continue;

        $show_art = true;

        $res[] = '<option value="/'.path($path_id, getCode($item)).'/"'.($item['id']==$sel ? ' selected' : '').'>'.str_repeat('&nbsp;', ($level*3)).$item['name'].'</option>';
        $res = array_merge($res, $subs);
    }

    return array($res, ($show_art || in_array($parent, $show_arts)));
}

function catalogItemsSelect($path_id, $sel, $parent, $service=false)
{
    global $DB;
    $items = $DB->getAll("SELECT * FROM `mp_catalog_items` WHERE `".($service ? "service_":"")."active`='1' AND `parent`=".$parent.($service ? " AND `service`='1'":"")." ORDER by `order`");
    $article = $DB->getRow("SELECT * FROM `mp_catalog_articles` WHERE `id`=".$parent);

    $res = array();
    foreach ($items as $item)
    {
        $res[] = '<option value="/'.path($path_id, getCode($article)).'/'.getCode($item).'/"'.($item['id']==$sel ? ' selected' : '').'>'.$item['title'].'</option>';
    }

    return $res;
}


function authBox($path_id=false)
{
    $_USER_INFO = authUserInfo();

    ob_start();

    if ($_USER_INFO)
    {
        ?>
        <div>Приветствуем Вас!</div>
        <div class="name"><a href="/user/"><?=$_USER_INFO['name']?></a></div>
        <div class="logout"><form action="/json/logout/<?=($path_id ? $path_id : getPathId())?>/" method="post" class="ajax"><button>Выйти</button></form></div>
        <?
    }
    else
    {
        ?>
        <div><a href="/registred/">Регистрация</a></div>
        <div><a href="/ajax/auth/<?=($path_id ? $path_id : getPathId())?>/" class="openwin dott login">Вход для клиентов</a></div>
        <?
    }

    return ob_get_clean();
}

function formatPrice($price)
{
	if (empty($price)) $price = 0;
    $price = number_format($price, 2, ',', ' ');
    return $price.' <span class="rur">руб</span>';
}

function orderItems($_items=false)
{
    global $DB;

    if (!$_items)
    {
        if (!empty($_SESSION['order']))
            $_items = $_SESSION['order'];
        else
            return false;
    }

    $items = $DB->getAll("SELECT * FROM `mp_catalog_items` WHERE `id` IN (".implode(array_keys($_items), ',').")");

    foreach ($items as $n=>$item)
    {
        $items[$n]['order_count'] = $_items[$item['id']];
    }

    return !empty($items) ? $items : false;
}

function orderToString($_items=false)
{
    if (!$_items)
    {
        if (!empty($_SESSION['order']))
            $_items = $_SESSION['order'];
        else
            return '';
    }

    $order = array();
    foreach ($_items as $id=>$count)
    {
        $order[] = $id.':'.$count;
    }
    return implode($order, ',');
}

function stringToOrder($items)
{
    global $DB;

    $res = array();
    foreach (explode(',', $items) as $ord)
    {
        $ord = explode(':', $ord);

        if (empty($ord[0])) continue;

        $res[$ord[0]] = !empty($ord[1]) ? $ord[1] : 1;
    }

    if (empty($res)) array();

    $items = orderItems($res);

    return $items ? $items : array();
}


function infoOrder($items, $format=true)
{
    $res_price = 0;
    $res_count = 0;
    foreach ($items as $item)
    {
        if (isset($item['order_count']))
            $count = (int)$item['order_count'] ? (int)$item['order_count'] : 1;
        else
            $count = isset($_SESSION['order'][$item['id']]) ? (int)$_SESSION['order'][$item['id']] : 1;


        $price = $item['price'];

        if (!empty($price))
        {
            $res_price += ($count*$price);
        }
        $res_count += $count;
    }

    return array($res_count, (($format) ? formatPrice($res_price) : $res_price));
}

function basketBox()
{
    $items = orderItems();

    ob_start();
    if ($items)
    {
        list($count, $price) = infoOrder($items);
        $num = substr($count, strlen($count)-1, 1);
        ?><a href="#" class="open"></a><div class="title"><a href="/basket/">Ваш заказ</a></div><div><?=$count?> позици<?=(($num==1)?'я':(($num>1 && $num<5)?'и':'й'))?></div><div><?=$price?></div><?
    }
    else
    {
        ?><a href="#" class="open"></a><div class="empty">Ваша корзина пуста</div><?
    }

    return str_replace("\r\n", "", ob_get_clean());
}

function setTitleLink($item)
{
    $item['title'] = preg_replace('/\[\[(.*)\]\]/i', '<a href="/'.path($item['path_id'], getCode($item)).'/">\\1</a>', $item['title'], 1);

    if(!preg_match('/<a href/i', $item['title']))
    {
        $item['title'] = '<a href="/'.path($item['path_id'], getCode($item)).'/">'.$item['title'].'</a>';
    }

    return $item['title'];
}

function getItemType($type)
{
    $types = array(
        1 => array('новинка',       '#2c7c23'),
        2 => array('акция',         '#ff8403'),
        3 => array('хит продаж',    '#dd4b4b'),
    );

    return (isset($types[$type]) ? '<div class="type" style="background:'.$types[$type][1].'">'.$types[$type][0].'<span></span></div>' : '');
}



function regionsList($dealers=false)
{
    global $DB;

    if ($dealers)
    {
        $res = array();
        foreach ($dealers as $dealer)
        {
            $res[$dealer['region']] = true;
        }
        $all_regions = array_keys($res);
    }
    else
    {
        $all_regions = $DB->getCol("SELECT `region` FROM `mp_dealers` WHERE `active`='1' GROUP by `region`");
    }

    return assoc('id', $DB->getAll("SELECT * FROM `mp_branch` WHERE `id` IN (".implode($all_regions, ',').") ORDER by `title`"));

}


function getDelers($id, $allow=false)
{
    global $DB;

    $dealers = $DB->getAll("SELECT * FROM `mp_dealers` WHERE `region`=".(int)$id.($allow ? " AND `id` IN (".implode(array_keys($allow), ',').")" : ""));

    debug($id, false, true);

    ob_start();
    if (!empty($dealers))
    {
        $count = count($dealers);
        ?>
        <div class="count">Вы можете купить у <strong><?=$count?> дилер<?=($count==1 ? 'а' : 'ов')?></strong></div>

        <div class="data">
            <div class="bd"><div class="b1"></div><div class="b2"></div><div class="b3"></div><div class="b4"></div></div>

            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="items">
                        <?
                        foreach ($dealers as $n=>$dealer)
                        {
                            ?>
                            <div class="item<?=(!$n?' active':'')?>"><a href="#"><?=$dealer['title']?></a></div>
                            <?
                        }
                        ?>
                    </td>
                    <td class="text">
                        <?
                        foreach ($dealers as $n=>$dealer)
                        {
                            ?>
                            <div class="box<?=(!$n?' active':'')?>"><?=replaceMapTag($dealer['text'])?></div>
                            <?
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <?
    }

    return ob_get_clean();
}

function itemDealers($id)
{
    global $DB;

    if (!$id) return array();

    $allow = $DB->getOne("SELECT `dealers` FROM `mp_catalog_items` WHERE `id`=".$id);

    if (empty($allow)) return array();

    return assoc('id', $DB->getAll("SELECT * FROM `mp_dealers` WHERE `id` IN (".$allow.")"));
}

function mapInsertNL($data)
{
    return mapInsert($data, false);
}

function replaceMapTag($HTML)
{
    return preg_replace_callback('/\[map=([\d]+)\]/i', 'mapInsertNL', $HTML);
}


function root_words($str)
{
    $stemmer = new Lingua_Stem_Ru();

    $str = explode(' ', $str);

    foreach ($str as &$s)
        $s = '+'.$stemmer->stem_word($s).'*';

    return implode(' ', $str);
}

function searchQuery($search_string, $count=false, $where='', $limit='')
{
    global $DB;

    $root_words = $DB->pre(root_words($search_string));

    $sql = '
            SELECT *

            FROM
                `'.PRFX.'search_index`

            WHERE (`title` LIKE \'%'.$search_string.'%\' OR `text` LIKE \'%'.$search_string.'%\') '.(!empty($where) ? ' AND '.$where : '').'
    ';

    if ($count) return mysql_num_rows(mysql_query($sql));

    $sql .= '  '.$limit;

    return $DB->getAll($sql);
}

function trimSearchText($text, $search_string)
{
    $words = htmlspecialchars(implode('|', explode(' ', $search_string)), ENT_QUOTES);

    preg_match('#(\s.{0,20})?[\s|[:punct:]][^\s]*('.$words.')[^\s]*[\s|[:punct:]].{0,120}#ui', ' '.$text.' ', $p);
    return !empty($p[0]) ? preg_replace('#([^ \/.]*('.$words.')[^ \/.,:;]*)#ui', '<span class="strong">\\1</span>', $p[0]) : '';
}

function processingString($string)
{
    $string = str_replace(array('+', '"', '*', '>', '<'), array('%20', '', '', '', ''), $string);
    $string = trim($string);
    //$string = addslashes($string);
    //$string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = strip_tags($string);
    $string = rawurldecode($string);

    return $string;
}


function userDataField($input)
{
    ob_start();
    ?>
    <div class="item">
        <div class="title"><?=$input['caption']?><?=(!empty($input['req'])&&$input['req'] ? '<span class="star">*</span>' : '')?><?=(!empty($input['comment']) ? '<span class="comment">('.$input['comment'].')</span>' : '')?></div>
        <div class="field"><?=$input['html']?></div>
    </div>
    <?
    return ob_get_clean();
}

function userDataFields($_fields, $form)
{
    ob_start();
    $type_value = 0;
    foreach ($_fields as $name => $data)
    {
        switch ($name)
        {
            case 'type':

                $input = $form->getFieldByName($data);
                $type_value = $input['value'];
                ?>
                <div class="check-field">
                    <?=userDataField($input)?>
                    <div class="comment"><span class="star">*</span> Поля обязательные для заполнения</div>
                </div>
                <?

            break;

            case 'checks':

                $input = $form->getFieldByName($data);
                $type_value = $input['value'];
                ?>
                <div class="check-field"><?=userDataField($input)?></div>
                <?

            break;

            case 'info':
                foreach ($data as $field)
                {
                    echo userDataField($form->getFieldByName($field));
                }
            break;
            case 'contacts':
                ?>
                <h3>Контактная информация</h3>
                <?

                foreach ($data as $num=>$types)
                {
                    $act = ($num == $type_value);

                    ?>
                    <div class="type<?=($act ? ' active' : '')?>">
                        <?
                        foreach ($types as $field)
                        {
                            $input = $form->getFieldByName($field);

                            if (!$act)
                            {
                                $input['attr']['disabled'] = 'disabled';
                                $input['html'] = $form->fieldHTML($input);
                            }

                            echo userDataField($input);
                        }
                        ?>
                    </div>
                    <?
                }
            break;
        }
    }

    return ob_get_clean();
}

function getFileType($ext)
{
    $exts = array(
        'doc' => array('doc','docx','docm'),
        'xls' => array('xls','xlsx','xlsm','xlsb','csv'),
        'pdf' => array('pdf'),
    );

    foreach ($exts as $name=>$types)
    {
        if (in_array($ext, $types)) return $name;
    }

    return 'none';
}


function printFormLogged()
{
    $_fields = array('info' => array('delivery_address', 'comment'));
    $form = new FormsControl(getBlocksConfig('order-logged'));

    return userDataFields($_fields, $form);
}


function printFormRegistred()
{
    $_fields = array(
        'type' => 'type',
        'info' => array('email', 'password', 'repassword'),
        'contacts' => array(
            0 => array('name', 'phone', 'street', 'build', 'apartament', 'city'),
            1 => array('name', 'company', 'phone', 'address1', 'address2', 'inn', 'okpo', 'rs', 'ks', 'bank', 'bik'),
        ),
    );

    $form = new FormsControl(getBlocksConfig('user'));

    return userDataFields($_fields, $form);
}

function printFormUser()
{
    $_fields = array(
        'type' => 'type',
        'contacts' => array(
            0 => array('name', 'phone', 'email', 'delivery_address', 'comment'),
            1 => array('name', 'company', 'phone', 'email', 'delivery_address', 'comment'),
        ),
    );
    $form = new FormsControl(getBlocksConfig('order'));

    return userDataFields($_fields, $form);
}


function registredUser()
{
    global $DB;

    $form = new FormsControl(getBlocksConfig('user'));

    $form->processData($_REQUEST);
    $form->checkFields();

    // Получаем значения формы
    $errors = $form->getErrors();
    $fields = $form->getFieldsValues();

    // Проверка существование адреса в базе
    if (!isset($errors['email']))
    {
        $check = (int)$DB->getOne("SELECT count(*) FROM `mp_users_site` WHERE `email` = '".$DB->pre($fields['email'])."'");

        if ($check > 0)
        {
            $errors['email'] = 'Указанный адрес уже зарегестрирован';
        }
    }

    $out['callback'][] = "$('#user-data .error-text').remove();";
    $out['callback'][] = "$('#user-data .error').removeClass('error');";

    if (empty($errors))
    {
        // Отправка письма
        $form->to_mail      = array(getAllEmails(3), $fields['email']);
        $form->title        = 'Регистрация на сайте';
        $form->add_text     = '<p><strong>На сайте <a href="http://'.$_SERVER['HTTP_HOST'].'/" target="_blank">'.$_SERVER['HTTP_HOST'].'</a> успешно прошла регистрация нового пользователя</strong></p>';
        $form->from_name    = $_SERVER['HTTP_HOST'];
        $form->from_mail    = 'noreply@'.$_SERVER['HTTP_HOST'];
        $form->sendMail();


        // Запись в базу
        $_sql = array();
        foreach ($fields as $name=>$value)
        {
            if ($name == 'password') $value = md5($value);

            $_sql[] = "`".$name."`='".safe($value)."'";
        }
        $DB->execute("INSERT INTO `mp_users_site` SET `regdate`=NOW(), ".implode($_sql, ','));

        return array($out, $DB->id);
    }
    else
    {
        foreach ($errors as $name=>$text)
        {
            $out['callback'][] = "setErrorField('".$name."','".$text."');";
        }

        return array($out, false);
    }
}
?>