<?php
/**
* Класс работы с путями и ссылками
* 
* @package Core
* @subpackage classes
* @author Xploit (Xploit@mediasite.ru)
*/
class UrlsAndPaths {

    var $tree;

    /**
    *  @access public
    *  @var $ids
    *  Содержит массив значений path_id и путей
    *  <code>
    *  array( [1] => / ) // key=Path_ID; value=path;
    *  </code>
    */
    var $ids;

    /**
    *  Текущий путь в формате /asdasd/asdasda/asdasd/asdasdas/aasd/
    *  @access public
    */
    var $current;
    
    var $path;

	var $storeAddBefTag;

    /**
    *  Конструктор класса. Объявление переменных
    *  @access private  
    */
    function UrlsAndPaths() 
    {
        global $DB;

        $this->prefix= 'http://';
        $this->tree= array ();
        $this->storeAddBefTag = array();
    }
    
    function Process_Current_Url($url){
        $this->current = $url;
        $this->getTree();
        return $this->getInfoByPath($url);
    }
    
/**
    *  Возвращает структуру раздела в шаблонной форме
    *  @access private
    *  @return array PageStructure
    *  @param string $encode_path encodePath
    */
    function GetEmptyByPath($path) {
        global $_ZONES,$DB, $GLOBALS;
        
        $tree= isset ($this->tree[$path]) ? $this->tree[$path] : array ();
        $export['title_menu']= isset ($tree['title_menu']) ? (string) $tree['title_menu'] : '';
		$export['header']= isset ($tree['header']) ? (string) $tree['header'] : '';
        $export['meta_description']= isset ($tree['meta_description']) ? (string) $tree['meta_description'] : '';
        $export['meta_keywords']= isset ($tree['meta_keywords']) ? (string) $tree['meta_keywords'] : '';
        $export['title_page']= isset ($tree['title_page']) ? (string) $tree['title_page'] : '';

		

		foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data)
			{
			$export[$field] = isset ($tree[$field]) ? (string) $tree[$field] : '';
			}

        $export['visible']= isset ($tree['visible']) ? $tree['visible'] : 0;
        $export['path_id']= isset ($tree['path_id']) ? (integer) $tree['path_id'] : $DB->nextID(PRFX.'www');
        $export['parent']= isset ($tree['parent']) ? (integer) $tree['parent'] : 0;
        $export['path']= (string) $path;
        $export['main_template']= isset ($tree['main_template']) ? (string) $tree['main_template'] : 0;
        $export['order']= isset ($tree['order']) ? (int) $tree['order'] : -1;
        $export['root_only']= isset ($tree['root_only']) ? (int) $tree['root_only'] : 1;
        $export['config'] = array();

        return $export;
    }


    /**
    *  Преобразует имя папки к разрешенному имени в файловой системе
    *  @access public
    *  @return DirectoryName
    *  @param string $folder DirectoryName
    */
    function modify_to_valid($folder) {
        return preg_replace('#[^A-Za-z0-9-_/+]#', '_', $folder);
    }

    /**
    *  Возвращает структуру раздела
    *  @access private
    *  @return array PageStructure
    *  @param string $path path

    */
    function getInfoByPath($path) {
        global $_ZONES, $_VARS;
		
        $path = (string)$path;

        if ($path === false)
            return $this->GetEmptyByPath($path);
        	
		//----отсекаем GET------------------------
		if(!empty($_GET)){
			
			/* ========================= */
			if(count(array_diff(array_keys($_GET), explode(';', ALLOW_GET))) && !preg_match('%'. ROOT_PLACE .'/(.*)%m', $path)) Page404();

			$tmp = explode('?',$path);
			$path = trim($tmp[0], '/');
			$this->current = $path;
		}
				
		$_VARS = array();

        $all_zones= $_ZONES;

		if (isset($this->tree[$path])){ $this->path = $path; return $this->tree[$path]; }
		else{
			
        	$exp = explode('/', trim($this->current,'/'));
			
        	$from = 0; $to = sizeof($exp);
        	foreach ($exp as $chunk){
				$path = implode('/',array_slice($exp,$from, $to));

				$to--;
				if (isset($this->tree[$path]) && $path != '' && sizeof($exp)>1){

					$_VARS = array_reverse($_VARS);
					$this->path = $path;

					if (HereModuleAttached($this->tree[$path])){
						return $this->tree[$path];
						
					} else page404();
				} else {
					$temp = array_reverse(explode('/', trim($path,'/')));
					$_VARS[] = $temp[key($temp)];
				}
        	}
        }
	page404();
    }
    
	/**
    *  Строит дерево всего сайта и вносит массив в $this->tree при переданом параметре $is_return=false 
	*  иначе возвращает массив в переменную не заменя $urls->tree
    *  @access private
    */
    function getTree($type=0, $is_return=false) {
        global $DB;
        $tpl=$group_tpl=array();

		usemodule('admin');
		
		$sql="SELECT * FROM `".PRFX."www` ".((int)$type>0 ? 'WHERE `www_type`='.(int)$type.'':'')." ORDER BY `path`";

        $list= $DB->getAll($sql);
        
        $tmp_tree= array ();
   		foreach($list as $path)
   		{
   			
   			$path['config'] = utf8_decode($path['config']); // иначе не работет с UTF8, возможно стопроится из-за неправильно переконвертированного символа в базе
   			
			$config_page = trim($path['config'])!="" ? 
				unserialize(trim($path['config'])) : 
				'';

   			$tmp_tree[$path['path_id']]['parent'] = $path['parent'];
   			$tmp_tree[$path['path_id']]['path'] = $path['path'];
   			$tmp_tree[$path['path_id']]['path_id'] = $path['path_id'];
   			$tmp_tree[$path['path_id']]['order'] = $path['order'];
   			$tmp_tree[$path['path_id']]['root_only'] = $path['root_only'];
   			$tmp_tree[$path['path_id']]['config'] = $config_page;
   			$tmp_tree[$path['path_id']]['main_template'] = $path['main_template'];
   			$tmp_tree[$path['path_id']]['visible'] = $path['visible'];
   			$tmp_tree[$path['path_id']]['title_menu'] = $path['title_menu'];
			$tmp_tree[$path['path_id']]['header'] = $path['header'];
   			$tmp_tree[$path['path_id']]['meta_description'] = $path['meta_description'];
   			$tmp_tree[$path['path_id']]['meta_keywords'] = $path['meta_keywords'];
   			$tmp_tree[$path['path_id']]['title_page'] = $path['title_page'];
			
			foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data)
				{
				$tmp_tree[$path['path_id']][$field] = $path[$field];
				}

   			$tmp_tree[$path['path_id']]['content'] = $path['content'];
   			$tmp_tree[$path['path_id']]['rdr_path_id'] = $path['rdr_path_id'];
   			$tmp_tree[$path['path_id']]['rdr_url'] = $path['rdr_url'];
   			$tmp_tree[$path['path_id']]['www_type'] = $path['www_type'];
   			$tmp_tree[$path['path_id']]['notfound'] = $path['notfound'];
   			
   			if (!$is_return) $this->ids[$path['path_id']]= $path['path'];			
   		}   

   		$tmp_tree = assoc('path',$tmp_tree);

		if (!$is_return)
			$this->tree=$tmp_tree;
		else
			return $tmp_tree;   		
   }
   
    /**
    *  Внести в $_REQUEST переменные из указанного GET-запроса
    *  @access public
    *  @param string $vars_get GET-query
    */
    function set_vars($vars_get) {
        $vars_get= str_replace('&amp;', '&', $vars_get);
        preg_match_all('#([^&=]*)=([^&]*)#', $vars_get, $vars= array ());
        $vars= isset ($vars[1]) ? $vars : array ();
        for ($i= 0; $i < sizeof($vars[1]); $i ++)
            $_REQUEST[$vars[1][$i]]= $vars[2][$i];
    }
    
    /**
    *  Уничтожить переменные из $_REQUEST указанные в GET-запросе
    *  @access public
    *  @param string $vars_get GET-query
    */
    function unset_vars($vars_get) {
        $vars_get= str_replace('&amp;', '&', $vars_get);
        preg_match_all('#([^&=]*)=([^&]*)#', $vars_get, $vars= array ());
        $vars= isset ($vars[1]) ? $vars : array ();
        for ($i= 0; $i < sizeof($vars[1]); $i ++)
            unset ($_REQUEST[$vars[1][$i]]);
    }

    /**
    *  Хранилище добавляемой информации к тегам.
    */
    function AddBeforeTag($text= null, $tag= null) {
        $this->storeAddBefTag[$tag][]= $text;
    }
    
   
}
?>