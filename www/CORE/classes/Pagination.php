<?

/**
 *
 * Pagination
 *
 * Класс для построения постраничной навигации
 *
 * @author Andrey Kolobov <andruha@mediasite.ru>
 * @version 2011-07-07
 *
 * @example
 * $pages = new Pagination($count, $npage, '/'.$page['path'].'/%link%/');
 *
 * $pages->number_center = 3; // количество страниц вокруг активной страницы
 * $pages->number_left_right = 2; // количество выводимых страниц справа и слева от разделителя
 * $pages->onpage = 20; // кол-во выводимых на страницу элементов
 * $pages->show_forth_back = false; // отображать или не отображать стрелки
 *
 * echo $pages; //вывод
 *
 */
class Pagination {
	
	private $current_page,
			$count_items,
			$count_pages,
			$link_mask = '',
			$after_before = true,
			$pages = array();
	
	public  $onpage = 10,
			$number_center = 2,
			$number_left_right = 1,
			$show_forth_back = true,
			$link_pref = 'page';
	
	public function __construct($count_items, $current_page, $link_mask)
	{
		$this->count_items = $count_items;
		$this->current_page = $current_page;
		$this->link_mask = $link_mask;
		
		if($this->current_page == 1) Page404();
		if(!$this->current_page) $this->current_page = 1;
	}
	
	public function __toString(){

        return $this->render();

    }
	
	private function getPages(){
		
		$this->count_pages = ceil($this->count_items / $this->onpage);
		if($this->count_items && $this->current_page > $this->count_pages) Page404();		
		
		if($this->current_page > 1 && $this->after_before && $this->show_forth_back) $this->addPage('left')->setClass('left')->setValue( '<' )->setUrl( $this->Link( $this->current_page - 1 ) );
		
		for( $i = 1; $i <= $this->count_pages; $i++ )
		{
			if( $this->count_pages > ($this->number_left_right * 2 + $this->number_center * 2 + 1) )
			{
				if( $this->current_page < $this->count_pages - $this->number_left_right - $this->number_center && $i == $this->count_pages - $this->number_left_right  )
					$this->addPage('sep')->setClass('space')->setValue( '...' );
				
				if( $i < $this->number_left_right + 1 || $i > $this->count_pages - $this->number_left_right || $i == $this->current_page || ( $i >= $this->current_page - $this->number_center && $i < $this->current_page ) || ( $i <= $this->current_page + $this->number_center && $i > $this->current_page ) )
				{
					if( $i == $this->current_page )
						$this->addPage('current')->setClass('select')->setValue( $i );
					else
						$this->addPage('link')->setUrl( $this->Link( $i ) )->setClass('link')->setValue( $i );
				}
				
				if( $this->current_page > $this->number_left_right + $this->number_center + 1 && $i == $this->number_left_right )
					$this->addPage('sep')->setClass('space')->setValue( '...' );
			}
			else
			{
				if( $i == $this->current_page )
					$this->addPage('current')->setClass('select')->setValue( $i );
				else
					$this->addPage('link')->setUrl( $this->Link( $i ) )->setClass('link')->setValue( $i );
			}
		}
		
		if($this->current_page != $this->count_pages && $this->after_before && $this->show_forth_back) $this->addPage('right')->setClass('right')->setValue( '>' )->setUrl( $this->Link( $this->current_page + 1 ) );
		
	}

	private function addPage($type){
		
		return $this->pages[] = new PaginationPage($type);
		
	}
	
	private function Link( $i ){
		
		return $i == 1 ? str_replace('/%link%', '', $this->link_mask) : str_replace('%link%', $this->link_pref.$i, $this->link_mask);
		
	}
	
	public function getLimit(){
		
		$lim_start = $this->current_page > 1 ? ($this->current_page - 1) * $this->onpage : 0;
		return ' LIMIT '.$lim_start.', '.$this->onpage;
		
	}
	
	private function render(){
		
		$this->getPages();
		
		if($this->count_pages <= 1) return '';
		
		ob_start();
		
		echo '<div class="list_in">';
		
		foreach($this->pages as $page)
			echo '<div',
				 $page->class ? ' class="'.$page->class.'"' : '',
				 '>',
				 $page->url ? '<a href="'.$page->url.'">'.$page->value.'</a>' : $page->value,
				 '</div>';
		
		echo '</div>';
		
		return ob_get_clean();
		
	}
	
}

class PaginationPage{
	
	public $url, $class, $value, $type;
	
	public function __construct($type){
		
		$this->type = $type;
		
	}
	
	public function setUrl($url){
		
		$this->url = $url;
		return $this;
		
	}
	
	public function setClass($className){
		
		$this->class = $className;
		return $this;
		
	}
	
	public function setValue($Value){
		
		$this->value = $Value;
		return $this;
		
	}
}

?>