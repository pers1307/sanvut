<?php

/**
 *
 * Breadcrumbs
 *
 * Класс для построения "хлебных крошек"
 *
 * @author Philipp Nehaev <phil@mediasite.ru>
 * @version 2011-04-12
 *
 * @example
 * $breadCrumbs = new breadCrumbs();
 * $breadCrumbs->fastGet( $page['path_id'] ); //загрузка пути из MediaPublisher
 * //предыдущие два действия по умл. выполняются при загрузке CMS
 * $breadCrumbs->level(1)=>setActive(0); //делаем не активым первый уровень
 * $breadCrumbs->level(1)=>setTitle('Название'); //меняем заголовок первого уровня
 * $breadCrumbs->level(1)=>setUrl('/test/'); //меняем урл первого уровня
 * $breadCrumbs->remove(2); //удаляем второй уровень
 * $breadCrumbs->addLevel('/url/', 'Название'); //добавляем в конец еще один уровень
 * $breadCrumbs->addLevel('/url/', 'Название', array('after', 0)); //добавляем перед нулевым (по умл. Главная) новый уровень
 * echo $breadCrumbs; //вывод
 *
 */
class BreadCrumbs {
    
    private $levels = array();
    public  $separator = '',
            $lastActive = 0;
    
    function __construct(){

        $this->addLevel( '/', 'Главная', 1 );

    }

    function __toString(){

        return $this->render();

    }

    /** Возвращаем объект уровня */
    public function level( $levelKey = 0 ){
        
        return $this->levels[ $levelKey ];
        
    }

    /** Добавляем новый уровень */
    public function addLevel( $url = '', $title = '', $active = 1, $insertTo = false ){

        $nowLevel = $this->getCountLevel();

        /** Обрабатываем запрос на добавление до или после указанного уровня,
        в результате получаем измененный $nowLevel с ключом для добавления **/
        if( is_array($insertTo) && count($insertTo) > 1 ){
            
            list( $where, $key ) = $insertTo;

            if( $key < 0 || empty( $this->levels[$key] ) )
                exit(__CLASS__ . ': invalid key');

            switch( $where ){
                
                case 'before':
                    $nowLevel = $key;
                    break;
                    
                case 'after':
                    $nowLevel = ++$key;                    
                    break;
                
                default:
                    exit(__CLASS__ . ': only before or after');
                
            }
            
            $this->levels = array_merge(
                                        array_slice($this->levels, 0, $nowLevel),
                                        array( 0 ), /** сюда запишется новый уровень */
                                        array_slice($this->levels, $nowLevel)
                                        );
            
        }
        
        $this->levels[ $nowLevel ] = new BreadCrumbsLevel();
        $this->level( $nowLevel )->setUrl( $url );
        $this->level( $nowLevel )->setTitle( $title );
        $this->level( $nowLevel )->setActive( $active );

    }
    
    /** Импорт из MediaPublisher */
    public function fastGet( $id, $again=false ){
        
        global $DB, $fastGetArray; 
        
        if( $again == false && isset( $fastGetArray ) ){
            
            unset( $fastGetArray );
            
        }

        $item = $DB->getRow( 'SELECT * FROM `mp_www` WHERE `path_id` = ' . $id );

        if( count($item) ){
         
            $fastGetArray[ $item['path_id'] ] = $item;
            
        }

        if( $item['parent'] > 1 ){
            
            return $this->fastGet( $item['parent'], true );
            
        }

        if( is_array( $fastGetArray ) ){

            $fastGetArray = array_reverse( $fastGetArray, true );

            foreach( $fastGetArray as $path_id => $item ){
                
                if( $path_id == 1 ){
                    
                    continue;
                    
                }
                
                $this->addLevel(
                                '/' . trim( path($path_id), '/' ) . '/',
                                $item['title_menu'],
                                empty($item['notfound']) ? 1 : 0
                                );
                
            }

        }
        
    }

    /** Строим HTML-код для вывода клиенту */
    public function render(){
        
        $crumbs = array();

        foreach( $this->levels as $key => $obj ){
            
            if( ! $this->lastActive && $this->getLastKey() == $key )
                $obj->setActive(0);
            
            if( $obj->getActive() )            
                $crumbs[ $key ] = '<div><a href="' . $obj->getUrl() . '">' . $obj->getTitle() . '</a></div>';
            else
                $crumbs[ $key ] = '<div><span>' . $obj->getTitle() . '</span></div>';
            
        }
        
        return implode( $crumbs, $this->separator );
        
    }

    /** Удаляем уровень с указанным ключом */
    public function remove( $levelKey ){

        unset( $this->levels[ $levelKey ] );
        
        $this->levels = array_values( $this->levels );
        
    }

    /** Получаем количество уровней */
    private function getCountLevel(){
        
        return count( $this->levels );
        
    }

    /** Получаем ключь последнего уровня */
    private function getLastKey(){
        
        return $this->getCountLevel() - 1;
        
    }

}


class BreadCrumbsLevel {

    private $url, $title, $active;

    public function getUrl(){
        
        return $this->url;
        
    }

    public function setUrl( $value ){
        
        $this->url = $value;
        
    }

    public function getTitle(){
        
        return $this->title;
        
    }

    public function setTitle( $value ){
        
        $this->title = $value;
        
    }

    public function getActive(){
        
        return $this->active;
        
    }

    public function setActive( $value = 1 ){
               
        $this->active = $value;
        
    }

}

?>