<?php


/**
* Класс построения административного меню.
* Файл подключается на стадии генерации
* административной страницы, всегда.
*
* @package Core

*/

/**
*  Класс используется для построения административного меню в файлах menu.php.
*  Класс доступен модулям только в административном разделе.
*  для работы класса используются {@link $modules}.
*  @access public
*/
class Admin_Menu {

    /**
    * Содержит массив доступного административного меню.
    * @access public
    * @var array
    * <code>
    *   [auth] => Array (
    *       [/root/auth/config/] => Array (
    *               [key] => 10
    *               [caption] => Конфигурация
    *               [path] => /root/auth/config/
    *               [childs] => Array (
    *                        [/root/auth/defaults/] => Array (
    *                               [key] => 5
    *                               [caption] => Страницы модулей по умолчанию
    *                               [path] => /root/auth/defaults/
    *                           )
    *                   )
    *           )
    *   )
    *
    *   [...] => Array ( ... )
    * </code>
    */
    var $menu;

    /**
    * Содержит директорию выбранного в администартивном меню модуля.
    * Если модуль не выбран, то содержит пустой стринг.
    * @access public
    * @var string
    */
    var $selModule;

    /**
    * Содержит ссылку на выбранный элемент меню первого уровня.
    * Если элемент не выбран, то содержит NULL.
    * @access public
    * @var array
    */
    var $selLev1;

    /**
    * Содержит ссылку на выбранный элемент меню второго уровня.
    * Если элемент не выбран, то содержит NULL.
    * @access public
    * @var array
    */
    var $selLev2;

    /**
    * Содержит дополнительные вложеные разделы из константы {@link ADD_PATH} до 2го уровня вложенности
    * @access public
    * @var array
    * <code>
    *   Array (
    *       [0] => admin
    *       [1] => errors
    *       [2] =>
    *   )
    * </code>
    */
    var $params;



    /**
    *  Конструктор.
    *  @access public
    *  @return void
    */
    function Admin_Menu() {
   		global $urls, $_VARS;

        $this->menu= array ();
        $this->cur_module= null;
        $this->CurLev1= null;
        $this->CurLev2= null;
        $this->params= $_VARS;

        /** ID пути к административной странице */
        $this->main_path_ID= 2;

        /** ID пути к административной странице */
        $this->selModule= '';

        /** Текущий путь для сравнения с выбраным */
        $this->curPath= $urls->current;

        /** Содержит ключ выбранного элемента меню первого уровня. */
        $this->selLev1= null;

        /** Содержит ключ выбранного элемента меню второго уровня. */
        $this->selLev2= null;

        /** Содержит ключ выбранного элемента меню второго уровня. */
        $this->linkToLev1= null;


    }

    /**
    *  Построение административного меню.
    *  @access private
    *  @return void
    */
    function _makeMenu() 
		{
        global $_MODULES, $adminmenu,$auth_isROOT;
        static $called= false;
							 
        if ($called)
            return;

        $called= true;

        $mods= $_MODULES->info;		 

        foreach ($mods as $module) 
			{
            if (!$module['installed'])
                continue;

			$this->cur_module= $module;
			if ($auth_isROOT && !$_MODULES->isSystem($module['module_dir']))
				$this->Option('Конфигурация', 'config');
			UseModule($module['module_dir']);
			
			$filemenu= MODULES_DIR.EndSlash($module['module_dir']).'menu.php';	
			if (is_file($filemenu)) @include_once $filemenu;

			$this->cur_module= null;
			$this->CurLev1 === null;
			$this->CurLev2 === null;
	        }
		}

    /**
    *  Добавление пункта меню первого уровня
    *  @access public
    *  @param string $caption Название пункта меню
    *  @param string $path Относительный путь
    *  @param string $rights Необходимые права для доступа в раздел. Перечисление через запятую.
    *  @return void
    */
    function Option($caption, $path, $rights= '') {

        $caption= (string) $caption;
        $path= (string) $path;

        if (!$caption || !$path)
            return;

        $rights=true;
        $path= $this->_makePath($path);


        $selected= strpos($this->curPath, rtrim($path,'/')) === 0;

        if (!$rights)
            return;

        $this->CurLev1= & $this->menu[$this->cur_module['module_dir']][$path];

        if ($selected) {
            $this->selModule= $this->cur_module['module_dir'];
            $this->selLev1= & $this->CurLev1;
            $this->selLev2= null;
        }

        $this->CurLev1['key']= $this->_key();
        $this->CurLev1['caption']= $caption;
        $this->CurLev1['path']= $path;
        $this->CurLev1['childs']= array ();
    }






    /**
    *  Проверяет выбран ли сейчас указаный пункт меню.
    *  Если выбран, то в переменную {@link $this->selected_module} записывает директорию выбранного модуля,
    *  @access private
    *  @param string $path Путь опции меню
    *  @param integer $level Уровень опции меню
    *  @return boolean
    */
    function _checkForSelect($path, $level) {

        if (strpos($this->curPath, $path) === 0) {
            $this->selModule= $this->cur_module['module_dir'];
            unset ($this->selLev1);
            $this->selLev1= & $this->CurLev1;
            if ($level == 2) {
                unset ($this->selLev2);
                $this->selLev2= & $this->CurLev2;
                
            }

            return true;
        }

        return false;
    }

    /**
    *  Возвращает инкрементируемый ключ.
    *  @access private
    *  @return integer
    */
    function _key() {
        static $i= 0;
        return ++ $i;
    }

    /**
    *  Создать путь к разделю меню.
    *  @access private
    *  @param string $path Путь опции меню
    *  @return string HREF-путь
    */
    function _makePath($path) {
		global $urls;
        $path= (string) $path;
        $path= str_replace('?', '&', str_replace('&amp;', '&', $path));
        $path= trim($path, '&');

        if (preg_match('#^([^&]*)&(.*)$#', $path, $matches= null)) {
            $path= $matches[1];
            $get= $matches[2];
        }
        else
            $get= '';

        return EndSlash(EndSlash($urls->path).$this->cur_module['module_dir']).EndSlash($path);

    }

}
?>