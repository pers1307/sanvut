<?php
function makeImageThumbs($data, $upload_files, $config_name, $create_sys_thumbs=false)
{
	global $Images;

	if (isset($upload_files[$config_name]['error']) && $upload_files[$config_name]['error']== 4){
		$data['value'] = isset($result['path']) ? serialize($result['path']) : '';
		return false;
	}
	
	if (isset($upload_files[$config_name]['error']) || (isset($data['allowed']) && !in_array(ExtractExt($upload_files[$config_name]['path']),$data['allowed'])))
	{
		if (isset($upload_files[$config_name]['error']) && $upload_files[$config_name]['error']!="")
			debug($upload_files[$config_name]['error'],1,0,'',false);
		else
			debug('В конфигурационном поле "<b>'.$config_name.'</b>", загружаемый файл имеет не допустимое расширение. Доступны только: <b>'.implode(',',$data['allowed']).'</b>',1,0,'',false);

		$data['value']='';

		return false;
	}
	
	$temp=array();
	
	$upload_file_name=$upload_files[$config_name]['path'];
	
	$upload_file_name_fs= relativeToAbsolutePath($upload_file_name);

	$folder = explode(':',date('Y:m:d',NOWTIME));
	$folder = FILES_DIR.EndSlash(implode('/',$folder));
	
	//-------исходные размеры---------
	list($w0,$h0)=getimagesize($upload_file_name_fs);

	foreach($data['thumbs'] as $th_name => $th)
	{
        $imgForResize    = $upload_file_name;
        $imgForResize_fs = $upload_file_name_fs;

//	    if(empty($th['watermark'])) {
//	        $imgForResize = $upload_file_name;
//	        $imgForResize_fs = $upload_file_name_fs;
//        } else{
//            $watermark = "/UPLOAD/big-watermark.png";
//            $watermarkedImagePath = copyAndWatermarkedOriginalImage($upload_file_name,$watermark);
//
//            $imgForResize = $watermarkedImagePath;
//            $imgForResize_fs = relativeToAbsolutePath($imgForResize);
//        }

		//----создаем превьюшки аплоадного файла
		$w1=(int)$th[0];
		$h1=(int)$th[1];
		
		$crop=!empty($th[2])?true:false;
		
		$new_name = explode('.',basename($upload_files[$config_name]['path']));
		$new_name[0] = $new_name[0].'_'.$w1.'_'.$h1;
		$new_name = uniqFile($folder.implode('.',$new_name));
		
		//--------условия, при которых изменять размер изображения не надо
		if(
			($w1==0 && $h1==0) || 							//----указаны нули - сохранение оригинала
			($w1!=0 &&  $h1!=0 && $h0<=$h1 && $w0<=$w1) || 	//----указан прямоугольник для масшатбирования, но исходное изображение меньше прямоугольника
			($h1==0 && $w0<=$w1) || 						//----указано масштабирование по ширине, но она меньше указанной
			($w1==0 && $h0<=$h1) 							//----то же, только по высоте
		){
			
			//-----сохраняем оригинал----------
			$z=copy($imgForResize_fs,$new_name);
			
		} else {
			//------изменим размер---------------------------
			
			if($w1!=0 && $h1!=0 && !$crop){
				//-----надо вписывать в прямоугольник-------
				
				
	
				//-----пропорции изображения-----------------
				$p0=$w0/$h0;
				$p1=$w1/$h1;
				
				if($p0>$p1)$h1=0;
				else $w1=0;
				
			}
			
			if(class_exists('Imagick'))
			{
				
				$thumb = new Imagick();
				$thumb->readImage($imgForResize_fs);
				if($crop) $r = $thumb->cropThumbnailImage($w1, $h1);
				else $r = $thumb->thumbnailImage($w1, $h1);
				$thumb->writeImage($new_name);
				$thumb->destroy();
				
			}else{
			
				$Images->newImage($imgForResize_fs);
				$Images->newOutputSize($w1,$h1,$crop, false);
				
				$r = $Images->save($new_name,'jpg');
				
			}
		}
		
		chmod($new_name,0777);
		$temp[$th_name] = str_replace(DOC_ROOT,'/',$new_name);
	}
	
	if(!empty($watermarkedImagePath)){
		unlink(relativeToAbsolutePath($watermarkedImagePath));
	}

	//----создаем системное изображение если надо
	if($create_sys_thumbs)
	{
		$temp['sys_thumb'] = str_replace(DOC_ROOT,'/',makeSysImageThumb($upload_file_name));
	}
	
	//----удаляем ориджинал
	unlink($upload_file_name_fs);
	
	$data['value'] = serialize($temp);		
	
	if (isset($temp) && is_array($temp))
		$data['value'] = serialize($temp);

	return $data['value'];
}

/**
 * @param $upload_file_name
 * @param $upload_file_name_fs
 * @param $watermark
 * @return string
 */
function copyAndWatermarkedOriginalImage($upload_file_name,  $watermark)
{
    $upload_file_name_fs = relativeToAbsolutePath($upload_file_name);

    $watermarkedImagePath = watermarkedFileName($upload_file_name);
    $watermarkedImagePath_fs = relativeToAbsolutePath($watermarkedImagePath);
    copy($upload_file_name_fs, $watermarkedImagePath_fs);

    AddWatermark($watermarkedImagePath, $watermark, null, DOC_ROOT);
    return $watermarkedImagePath;
}

/**
 * @param $temp
 * @param $th_name
 * @return string
 */
function watermarkedFileName($name)
{
    $basename = basename($name);
//	$filepath = dirname($name);

    return "/TMP/$basename";
//	return "$filepath/watermark_{$basename}";
}

/**
 * @param $relative
 * @return string
 */
function relativeToAbsolutePath($relative)
{
    return DOC_ROOT . $relative;
}


function deleteAllimagesBySerialize($str){
	$images=unserialize($str);
	foreach ($images as $img){
		if($img!='' && file_exists($_SERVER['DOCUMENT_ROOT'].$img))unlink($_SERVER['DOCUMENT_ROOT'].$img);
	}
}


function getImageURL($images, $num)
{
	if (!empty($images))
    {
		$image = unserialize(html_entity_decode($images,ENT_QUOTES));

        if (!empty($image[$num]) && is_file(DOC_ROOT.$image[$num])) 
            return $image[$num];
	}

    return false;
}

function getImageHTML($images, $num=1, $title='', $params='')
{
	$image = getImageURL($images, $num);

    if (!$image) return false;
    
	list($w,$h,$p) = getimagesize(DOC_ROOT . $image);
	return '<img src="'.$image.'" width="'.$w.'" height="'.$h.'" alt="'.$title.'" title="'.$title.'"'.$params.' />';
}

function getGroupImagesURL($images, $num=0, $text_enable=false)
{
    $res = array();
    
	if (!empty($images))
    {
		$images = unserialize(html_entity_decode($images,ENT_QUOTES));
        
        foreach ($images as $n=>$image)
        {
            if (!empty($image['path'][$num]) && is_file(DOC_ROOT.$image['path'][$num])) 
            {
                if ($text_enable)
                {
                    $res[$n]['path'] = $image['path'][$num];
                    $res[$n]['text'] = isset($image['text']) ? $image['text'] : '';
                }
                else
                {
                    $res[$n] = $image['path'][$num];
                }
            }
        }
	}
    
    return $res;
}

function getGroupImagesHTML($images, $num=0, $text_enable=false, $title='', $params='')
{
    $images = getGroupImagesURL($images, $num, true);
    
    $res = array();
    foreach ($images as $n=>$image)
    {
        if ($text_enable) $title = $image['text'];
        
        list($w,$h,$p) = getimagesize(DOC_ROOT.$image['path']);
        $res[$n] = '<img src="'.$image['path'].'" width="'.$w.'" height="'.$h.'" alt="'.$title.'" title="'.$title.'" border="0" '.$params.' />';
    }

	return $res;
}

function getGroupImageURL($images, $num=0, $col=0)
{
    $images = getGroupImagesURL($images, $num);
	return isset($images[$col]) ? $images[$col] : false;
}

function getGroupImageHTML($images, $num=0, $col=0, $text_enable=false, $title='', $params='')
{
    $images = getGroupImagesHTML($images, $num, $text_enable, $title, $params);
	return isset($images[$col]) ? $images[$col] : false;
}

function getImageURLFromCollection($images,$item_id,$num){
	if ($images!=""){
		$s = html_entity_Decode($images,ENT_QUOTES);
		$s = unserialize($s);  
		return isset($s[$item_id]['path'][$num]) ? $s[$item_id]['path'][$num] : '';
	} else return '';
}

function makeSysImageThumb($orig_filename){
	global $Images;

	$folder = explode(':',date('Y:m:d',NOWTIME));
	$folder = FILES_DIR.EndSlash(implode('/',$folder));
	
	/** Делаем првеьюшку для вывда в конфигураторе в админке */
	$new_name = explode('.',basename($orig_filename));
	$new_name[0] = $new_name[0].'_75_0';
	$new_name = uniqFile($folder.implode('.',$new_name));
	   
	if(class_exists('Imagick'))
	{
		
		$thumb = new Imagick();
		$thumb->readImage(DOC_ROOT.$orig_filename);
		$thumb->thumbnailImage(75, 0);
		$thumb->writeImage($new_name);
		$thumb->destroy();
		
	}else{
	
		$Images->newImage(DOC_ROOT.$orig_filename);
		$Images->newOutputSize(75,0,false,false);
		$Images->save($new_name,'jpg');
		
	}   
	 
	chmod($new_name,0777);
	
	return $new_name;
}

/**
 * @param string $serializeGallery
 *
 * @return string
 */
function addWatermarkToGallery($serializeGallery)
{
    $gallery = unserialize($serializeGallery);

    foreach ($gallery as &$galleryItem) {
        if(empty($galleryItem['mark'])){
            foreach($galleryItem['path'] as $k=>$path){
                autoWaterMarking($galleryItem['path'][$k]);
            }
            $galleryItem['mark'] = true;
        }
    }

    return serialize($gallery);
}

/**
 * @param $serializeItem
 */
function addWatermarkToAll($serializeItem)
{
    $paths = unserialize($serializeItem);
    foreach($paths as $key=>$path) {
        $imgPath = $paths[$key];
        autoWaterMarking($imgPath);
    }
}

/**
 * @param $serializeItem
 * @param $key
 * @return string
 */
function addWatermarkToItem($serializeItem, $key)
{
    $paths = unserialize($serializeItem);
    $imgPath = $paths[$key];
    $activeWatermark = autoWaterMarking($imgPath);

    if(!$activeWatermark){
        return $serializeItem;
    }

    return serialize($paths);
}

/**
 * @param $imgPath
 * @return bool
 */
function autoWaterMarking($imgPath) {
    $activeWatermark = determinateWatermark($imgPath);
    if($activeWatermark) {
        AddWatermark($imgPath, $activeWatermark);
    }
    return $activeWatermark;
}

function determinateWatermark($imgPath){

    $watermarks = [
        '/UPLOAD/big-watermark.png',
        '/UPLOAD/watermark.png',
    ];
    $doc_root = rtrim(DOC_ROOT,'/\\');

    list($imageW,$imageH) = getimagesize($doc_root.$imgPath);
    if(!($imageW && $imageH)){
        return false;
    }

    $activeWatermark = false;

    foreach($watermarks as $k=>$watermark) {
        list($waterW,$waterH) = getimagesize($doc_root.$watermark);

        if(!($waterW && $waterH)) {
            continue;
        }

        if($waterH<$imageH && $waterW<$imageW){
            $activeWatermark = $watermark;
            break;
        }

    }

    return $activeWatermark;
}

/**
 * Для водянных знаков
 *
 * @param string $img_path
 * @param string $mark_path
 * @param string $new_file
 * @param string $root_path
 *
 * @return bool
 */
function AddWatermark($img_path, $mark_path, $new_file='', $root_path='')
{
    $doc_root = rtrim(DOC_ROOT, '/');
    $root_path = $root_path ? $root_path : $doc_root;

    $img_full_path = $root_path.$img_path;

    if(!is_writable($img_full_path)){
        return false;
    };
	
	if (preg_match('/\.jpg|\.jpeg|\.JPG|\.JPEG/',$img_path)) {
        $im = imagecreatefromjpeg($img_full_path);
	} elseif (preg_match('/\.png|\.PNG/',$img_path)) {
		$im = imagecreatefrompng($img_full_path);
	} else {
		return false;
	}
	
	if (preg_match('/\.jpg|\.jpeg|\.JPG|\.JPEG/',$mark_path)) {
		$stamp = imagecreatefromjpeg($root_path.$mark_path);
	}
	elseif (preg_match('/\.png|\.PNG/',$mark_path)) {
		$stamp = imagecreatefrompng($root_path.$mark_path);
	} else {
		return false;
	}

	imagealphablending($im,1);
	imagealphablending($stamp,1);

	imagesavealpha($im,1);
	imagesavealpha($stamp,1);
	
	$sx = imagesx($stamp);
	$sy = imagesy($stamp);
	$ix = imagesx($im);
	$iy = imagesy($im);
	$minx = $sx < $ix ? $sx :$ix ;
	$miny = $sy < $iy ? $sy :$iy ;
	$return=imagecopyresampled($im, $stamp, $ix/2-$sx/2, $iy/2-$sy/2, 0, 0, $sx, $sy, $minx, $miny);
	if (!$return){return $return;}	
	
	$new_file=$new_file ? $doc_root.$new_file : $img_full_path ;
	if(preg_match('/\.jpg|\.jpeg|\.JPG|\.JPEG/',$img_path)){
	   $return=imagejpeg($im, $new_file);
	} elseif (preg_match('/\.png|\.PNG/',$img_path)) {
	   $return=imagepng($im, $new_file);
	}
	if (!$return){return $return;}	
	
	imagedestroy($im);
	imagedestroy($stamp);
	
	return true;	
}
?>