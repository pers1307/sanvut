<?

/**
 * @param int $parent
 * @param int $wwwType
 *
 * @return array
 */
function getMenu($parent, $wwwType = 2)
{
    global $DB;

    $items = $DB->getAll(
            'SELECT 
            title_menu, 
            path, 
            path_id, 
            class FROM '.PRFX.'www 
            WHERE www_type = ' . $wwwType . ' 
            AND parent=' . (int)$parent . '
            AND visible 
            ORDER BY `order`, path_id'
    );

    foreach ($items as &$item) {

        if ($item['path'] == '/' . $_SERVER['REQUEST_URI'] . '/') {

            $item['here'] = true;
        } else {

            $item['here'] = false;
        }
    }

    return $items;
}

/**
 * @return array
 */
function getFooterCatalog()
{
    global $DB;

    $items = $DB->getAll('
        SELECT 
        id,
        path_id,
        parent,
        `name`, 
        code
        FROM ' . PRFX . 'catalog_articles 
        WHERE footer = 1 
        AND visible = 1 
        ORDER BY `order` ASC'
    );

    $result = [];

    /**
     * 1 level
     */
    foreach ($items as &$item) {

        if ($item['parent'] == 0) {

            $item['path'] = '/' . path($item['path_id']) . $item['code'] . '/';

            $result[] = $item;
        }
    }

    foreach ($result as &$resultItem) {

        foreach ($items as &$item) {

            if ($resultItem['id'] == $item['parent']) {

                $item['path'] = $resultItem['path'] . $item['code'] . '/';

                $resultItem['child'][] = $item;
            }
        }
    }

    return $result;
}

/**
 * @return array
 */
function getMainCatalog()
{
    global $DB;

    $items = $DB->getAll('
        SELECT 
        id,
        path_id,
        parent,
        `name`, 
        code
        FROM ' . PRFX . 'catalog_articles 
        WHERE visible = 1 
        AND ind = 1
        ORDER BY `order` ASC'
    );

    $result = [];

    /**
     * 1 level
     */
    foreach ($items as &$item) {

        if ($item['parent'] == 0) {

            $item['path'] = '/' . path($item['path_id']) . $item['code'] . '/';

            $result[] = $item;
        }
    }

    /**
     * two level
     */
    foreach ($result as &$resultItem) {

        foreach ($items as &$item) {

            if ($resultItem['id'] == $item['parent']) {

                $item['path'] = $resultItem['path'] . $item['code'] . '/';

                $resultItem['child'][] = $item;
            }
        }
    }

    /**
     * tree level
     */
    foreach ($result as &$resultItem) {

        if (!empty($resultItem['child'])) {

            foreach ($resultItem['child'] as &$child) {

                foreach ($items as &$item) {

                    if ($child['id'] == $item['parent']) {

                        $item['path'] = $child['path'] . $item['code'] . '/';

                        $child['child'][] = $item;
                    }
                }
            }
        }
    }

    return $result;
}

/**
 * @param array $items
 * @param int   $columns
 *
 * @return array
 */
function arrayHorizontal(array $items, $columns)
{
    $count = count($items);

    $colons = [];

    for ($i = 0; $i < $columns; $i++) {

        $colons[$i] = [];
    }

    $countForColons = 0;

    for ($i = 0; $i < $count; $i++) {

        array_push($colons[$countForColons], $items[$i]);

        ++$countForColons;

        if ($countForColons > $columns - 1) {
            $countForColons = 0;
        }
    }
    return $colons;
}

/**
 * @param int $last
 *
 * @return array
 */
function getNews($last = 4)
{
    global $DB;

    $items = $DB->getAll('
        SELECT 
        id,
        path_id,
        title,
        code,
        announce,
        image
        FROM ' . PRFX . 'news 
        ORDER BY `date` DESC
        LIMIT ' . $last
    );

    foreach ($items as &$item) {

        $item['path'] = '/' . path($item['path_id']) . $item['code'] . '/';
    }

    return $items;
}

function getSliders()
{
    global $DB;

    $items = $DB->getAll(
        'SELECT * 
        FROM '.PRFX.'slider
        ORDER BY `order` ASC'
    );

    return $items;
}

/**
 * @return array
 */
function getProjectCatalog()
{
    global $DB;

    $items = $DB->getAll('
        SELECT 
        id,
        path_id,
        `name`, 
        code,
        img
        FROM ' . PRFX . 'catalog2_articles 
        WHERE visible = 1 
        AND ind = 1
        AND parent = 0
        ORDER BY `order` ASC
        LIMIT 2'
    );

    /**
     * 1 level
     */
    foreach ($items as &$item) {

        $item['path'] = '/' . path($item['path_id']) . $item['code'] . '/';

        $result[] = $item;
    }

    return $items;
}

function getWWW( $p=1 )
{
	global $DB;
	return $DB->getAll( 'SELECT title_menu, path, path_id, class FROM '.PRFX.'www WHERE www_type=2 AND parent='.(int)$p.' AND visible ORDER BY `order`, path_id' );
}

function getParent ($p_id) {

  global $DB;

  return $DB->getRow('select * from mp_www where path_id="'.$p_id.'"');

}


function getArts( $t='catalog_articles', $p=0, $w='' )
{
	global $DB;
	return $DB->getAll( 'SELECT id, name, code, img FROM '.PRFX.$t.' WHERE parent='.(int)$p.' AND visible '.$DB->pre( $w ).' ORDER BY `order`, id' );
}

function getProjectsArt( $w='' )
{
	global $DB;
	return $DB->getRow( 'SELECT id, name, code FROM '.PRFX.'catalog2_articles WHERE visible '.$w.' LIMIT 1' );
}



function fullPathArt( $id=0, $t='catalog_articles', $path='' )
{
	global $DB;
    
    static $rows;
    if(!isset($rows[$t])){
        $rows[$t] = $DB->getall( 'SELECT id,parent, code FROM '.PRFX.$t.' ' );
        if(is_array($rows[$t])){
            //debug($rows[$t],0);                                    
            $rows[$t] = assoc('id',$rows[$t]);
        }                        
    }        
    $r=isset($rows[$t][$id]) ? $rows[$t][$id] : false ;        
 
                        
	//$r = $DB->getRow( 'SELECT parent, code FROM '.PRFX.$t.' WHERE id="'.(int)$id.'" LIMIT 1' );
	if( !empty( $r ) )
	{
		$path = $r['code'].'/'.$path;

		if( $r['parent'] )
		{
			$path = fullPathArt( $r['parent'], $t, $path );
		}
	}

	return $path;
}




function treePages( $id=0, $arr=array() )
{
	global $DB;

	if( $id )
	{
		$arr[] = $id;
		$cat = $DB->getOne( 'SELECT parent FROM '.PRFX.'www WHERE path_id="'.(int)$id.'"' );

		if( $cat )
			$arr = treePages( $cat, $arr );
	}

	return $arr;
}


function is_valid_string( $str='' )
{
	return $str ? preg_match( "/[а-яА-Я,a-zA-Z,\s,\-]/", $str ) : false;
}



function echoCatItems( $items=array(), $art=array(), $count, $sel_page, $onPage, $textBottom='' )
{
	global $_VARS,$DB;

	ob_start();

	$parents = array();

	$parents[] = $art['code'];

	$art2 = array();
	$art3 = array();

	if ($art['parent'] > 0) {

        $art2 = $DB->getRow('select * from mp_catalog_articles where id="'.$art['parent'].'" limit 1');
        $parents[] = $art2['code'];

        if ($art2['parent'] > 0) {

          $art3 = $DB->getRow('select * from mp_catalog_articles where id="'.$art2['parent'].'" limit 1');
          $parents[] = $art3['code'];
        }

	}

	$parents = array_reverse($parents);

	$path = '/catalog/'.implode('/',$parents).'/';

	if ($sel_page > 0) {

		$path .= 'page' . $sel_page . '/';
	}
	?>

	<? if(!empty($items)): ?>

	    <div class="wrap">

            <? foreach($items as $item): ?>

                <?
                $v1 = isset($item['pcode'] ) ? $item['pcode'] : $_VARS[1];

                $imageUrl = getImageURL($item['img'], 1);

                if (empty($imageUrl)) {
                    $imageUrl = '/DESIGN/SITE/images/noImage/no-image_200_200.jpg';
                }elseif ($item['mark'] === '0') {
                        $item['img'] = addWatermarkToItem($item['img'], 0);
                        $item['img'] = addWatermarkToItem($item['img'], 2);
                        $DB->execute("UPDATE `mp_catalog_items` SET `mark` = 1 WHERE `id`=".$item['id']);
                    }

                ?>

                <div>
                    <a href="<?=preg_replace('@page(\d+)/@i','',$path).(isset($item['id'])?$item['id']:'');?>/"><?= $item['title'] ?></a>

                    <div class="wrapImg">
                        <div>
                            <a href="<?=preg_replace('@page(\d+)/@i','',$path) . (isset($item['id'])?$item['id']:'');?>/">

                                <img src="<?= $imageUrl ?>" alt="<?= $item['title'] ?>"/>
                            </a>
                        </div>
                    </div>

                    <div class="description">
                        <?= $item['text']; ?>
                    </div>
                </div>
            <? endforeach; ?>
        </div>

        <script>
            $('.wrap > div').each(function() {

                $(this).css('height', '270px');
            });
        </script>

        <?

        if ($sel_page == 0) {

            echo '<div class="both">'.$textBottom.'</div>';
        }

        $pages = new Pagination( $count, $sel_page, '/catalog/'.fullPathArt( $art['parent'], 'catalog_articles', $art['code'] ).'/%link%/' );
        $pages->onpage = $onPage;
        $pages->number_center = 3;
        echo $pages;

        ?>

	<? endif; ?>

    <?

    return ob_get_clean();
}

/**
 * @param array $folders
 * @param array $art
 * @param $count
 * @param string $textBottom
 * @return string
 */
function echoCatFolders($folders=array(), $art=array(), $count, $textBottom='')
{
	global $_VARS;

	ob_start(); ?>

    <? if(!empty($folders)): ?>

        <div class="wrap">

            <? foreach($folders as $item): ?>

                <?

                $imageUrl = getImageURL($item['img'], 0);

                if (empty($imageUrl)) {

                    $imageUrl = '/DESIGN/SITE/images/noImage/no-image_200_200.jpg';
                }

                ?>

                <div>
                    <a href="<?= preg_replace('@page(\d+)/@i','',$_SERVER['REQUEST_URI']) . $item['code'];?>/"><?= $item['name'] ?></a>

                    <div class="wrapImg">
                        <div>
                            <img src="<?= $imageUrl ?>" alt="<?= $item['name'] ?>"/>
                        </div>
                    </div>

                    <?= $item['text2']; ?>
                </div>
            <? endforeach; ?>
        </div>
    <? endif; ?>

<?
	return ob_get_clean();
}

function getForm($configName){
    global $_VARS;

	include(BLOCKS_DIR.'configs/'.$configName.'.php');

    $form = new FormsControl($formconfig, true);
    $form->form_action='/json/'.$configName.'/';
    $form->formid = $configName.'form';

	return $form->getForm(true);
}

function addWatermarkEverywhere(){

    global $DB;
    /** catalog */
    $catalog_articles = $DB->getAll("SELECT * FROM `mp_catalog_articles` WHERE `mark`=0");

    if(!empty($catalog_articles)){
        foreach($catalog_articles as $catalog_article) {
            if($catalog_article['mark'] == 0) {
                addWatermarkToAll($catalog_article['img']);
                $DB->execute("UPDATE `mp_catalog_articles` SET `mark` = 1 WHERE `id`={$catalog_article['id']}");
            }
        }
    }

    $catalog_items = $DB->getAll("SELECT * FROM `mp_catalog_items` WHERE `mark`=0 or `mark_photo`=0");

    if(!empty($catalog_items)){
        foreach($catalog_items as $catalog_item) {
            if($catalog_item['mark'] == 0) {
                addWatermarkToAll($catalog_item['img']);
                $DB->execute("UPDATE `mp_catalog_items` SET `mark` = 1 WHERE `id`={$catalog_item['id']}");
            }
            if($catalog_item['mark_photo'] == 0) {
                addWatermarkToAll($catalog_item['photo']);
                $DB->execute("UPDATE `mp_catalog_items` SET `mark_photo` = 1 WHERE `id`={$catalog_item['id']}");
            }
        }
    }
    /** /catalog */

    /** project */
    $project_articles = $DB->getAll("SELECT * FROM `mp_catalog2_articles` WHERE `img_mark`=0");
    if(!empty($project_articles)) {
        foreach($project_articles as $project_article) {
            if($project_article['img_mark'] == 0) {
                addWatermarkToAll($project_article['img']);
                $DB->execute("UPDATE `mp_catalog2_articles` SET `img_mark` = 1 WHERE `id`={$project_article['id']}");
            }
        }
    }

    $project_items = $DB->getAll("SELECT * FROM `mp_catalog2_items` WHERE `img_mark`=0 OR `marck` =0");
    if(!empty($project_items)) {
        foreach($project_items as $project_item) {
            if($project_item['img_mark'] == 0) {
                addWatermarkToAll($project_item['img']);
                $DB->execute("UPDATE `mp_catalog2_items` SET `img_mark` = 1 WHERE `id`={$project_item['id']}");
            }
            if($project_item['marck'] == 0) {

                $project_item['gallery'] = addWatermarkToGallery($project_item['gallery']);
                $DB->execute("UPDATE `mp_catalog2_items` SET `marck` = 1, `gallery`='".$DB->pre($project_item['gallery'])."'WHERE `id`={$project_item['id']}");
            }
        }
    }
    /** /project */


}