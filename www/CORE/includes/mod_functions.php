<?php
function checkModuleIntegrity(){
	ob_start();

	global $_CONFIG, $DB, $CONFIG, $output_id;

	$result_out='';

	$tables = array();
	$allt = $DB->getAll('SHOW TABLES');
	foreach($allt as $key => $name)
		$tables[] = $name['Tables_in_'.$DB->config_params['dbname']];


	foreach($CONFIG['tables'] as $table => $table_data)
		{
		$table_db_name = PRFX.$table_data['db_name'];


		
		if (!in_array($table_db_name, $tables)){
			echo '<div style="border: 1px dashed black; background: #FFFDEF; padding: 10px; margin-bottom: 10px;">';
			echo '<div>В Базе данных не обнаружена таблица <b>'.$table_db_name.'</b>, которая используется в модуле, в конфигурационном файле.</div>';
			echo '<br><div><b>Раздел конфига:</b> [tables]=>['.$table.']=>[db_name]=>['.$table_db_name.']</div>';
			echo '</div>';
		} else {
			foreach ($table_data['config'] as $field => $field_data){
				$all_fs = $DB->GetAll('DESC '.$table_db_name.'');
				$fs=array();
				foreach ($all_fs as $key => $data)
					$fs[] = $data['Field'];

				if (!in_array($field,$fs)){
					echo '<div style="border: 1px dashed black; background: #FFFDEF; padding: 10px; margin-bottom: 10px;">';
					echo '<div>В таблице базы данных не обнаружено поле <b>'.$field.'</b>, которое используется в модуле, в конфигурационном файле.</div>';
					echo '<br><div><b>Раздел конфига:</b> [tables]=>['.$table.']=>[config]=>['.$field.']</div>';
					echo '</div>';
				}
			}
		}
	}	

	$result_out = ob_get_clean();

	if ($result_out!=""){
		if ($output_id!='center'){
			UseModule('ajax');
			$JsHttpRequest &= new JsHttpRequest("utf-8");		
			$addon='';
		} else
			$addon = '<h3 Style="margin-left: 0px;">Ошибки</h3>';
	
		die($addon.$result_out);
	}
}

function prepareLinkPath($CONFIG)
	{
	if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path']) && $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1]['inmodule']==$CONFIG['module_name'])
		{
		$last = $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1];
		$output_id = $last['to_output'];

		unset($last);
		}
	else
		{
		$output = (int)blockOnPage($CONFIG['module_name']);
		$output_id = ($output>0 ? 'fast_table_'.$CONFIG['module_name'] : 'center');

		unset($output);
		}

	return array($output_id);
	}

function generateVars(){
	global $table_name, $path_id, $isorder, $page, $CONFIG, $Forms, $output_id, $key_field, $not_show, $auth_isROOT;

	$path_id = !isset($path_id) ? 0 : (int)$path_id;
	$page = !isset($page) ? 1 : (int)$page;
	$isorder = !isset($isorder) || $isorder==0 ? false : true;

	if ($table_name=="") 
		alert('Имя таблицы не известно. Путь: /'.implode('/'.$adminmenu->params).'/');

	$not_in = (isset($CONFIG['tables']['items']['not_show']) && is_array($CONFIG['tables']['items']['not_show']) && !$auth_isROOT) ? $CONFIG['tables']['items']['not_show'] : array();
	
	/* CREATE VIEW */ 
	$filter = generateFilterWhereSQL();

	// Проверяется вложеность модуля в другой модуль
	$and ='';
	if ($filter!="") $and = ' AND ';

	if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path'])){
		$link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1];
		if (isset($link_path['inmodule']) && $link_path['inmodule']==$CONFIG['module_name']){
			$filter.=$and.'`'.$link_path['to_key'].'`="'.$link_path['from_id'].'"';
		}
	}
	do {
		list($items, $num_pages) = getDataFromSQL(
									'*', 
									$table_name, 
									$filter,
									$path_id, 
									$isorder ? '`order` ASC' : (isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : '`'.$key_field.'` DESC'), 
									'', 
									$page, 
									$CONFIG['tables']['items']['onpage'],
									$not_in
								);
		if ($page>$num_pages) {$page=$num_pages;} else {break;}
	}
	while($page<=$num_pages);
									
	$vars=array();		
	$vars['Forms']		= $Forms;
	$vars['CONFIG']		= $CONFIG;
	$vars['output_id']	= $output_id;
	$vars['isorder']	= $isorder;
	$vars['path_id']	= (int)$path_id;

	$vars['items']		= $items;
	$vars['page']		= $page;
	$vars['num_pages']	= $num_pages;

	$vars['pager']		= getPagerTemplate('fastview', '', $page, $num_pages);
	/* CREATE VIEW */

	return $vars;
}

function saveItem($showdberr = true)
	{
	global $CONFIG, $path_id, $DB, $Forms, $table_name, $isorder, $key_field;

	$CONFIG['tables']['items']['config'] = $Forms->save($CONFIG['tables']['items']['config']);
   
	$sql_ = array("`path_id` = '".$path_id."'");  	
	foreach($CONFIG['tables']['items']['config'] as $field => $info)
		{
		
		if (isset($info['value'])){
		
			if ( $info['type'] == 'code' && empty($info['value']) )
				{
				
				$info['value'] = "NULL";
				
				}
			else
				{
				
				$info['value'] = "'".$DB->pre($info['value'])."'";
				
				}
		
			$sql_[] = "`".$field."` = " . $info['value'];
			
		}
		
		}


	if ( (int)$_REQUEST[$key_field] > 0)
		{
		$sql = "UPDATE `".PRFX.$table_name."` SET ".implode(', ',$sql_)." WHERE `".$key_field."` = '".(int)$_REQUEST[$key_field]."'";
		}
	else
		{
		if ($isorder)
			{
				
				/*$last_order = $DB->getOne('SELECT `order` FROM `'.PRFX.$table_name.'` ORDER BY `order` DESC LIMIT 1');
	
				$last_order = $last_order=="" ? 1 : $last_order;
	
				$sql_[] = "`order` = '".($last_order+1)."'";*/
				
				//-----------уккажем сортировку в зависимости от того что указано в конфиге
				if($CONFIG['tables']['items']['add_new_on_top']){
					$order=$DB->GetOne("SELECT MIN(`order`) FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `path_id`='".$path_id."'");
					$sql_[]="`order`=".((int)$order-1);
				} else {
					$order=$DB->GetOne("SELECT MAX(`order`) FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `path_id`='".$path_id."'");
					$sql_[]="`order`=".((int)$order+1);
				}
				
			}

		$sql = "INSERT INTO `".PRFX.$table_name."` SET ".implode(', ',$sql_);
		}
		
		if( $DB->execute($sql, $showdberr) )
			{
			return $inserted_id = ($_REQUEST[$key_field])?intval($_REQUEST[$key_field]):$DB->id;
			}
		else
			{
			return false;
			}
			
	}

function generateConfigValues($new_item_id)
	{
	global $CONFIG, $DB, $table_name, $key_field;

	if($new_item_id>0)
		{
		$list = $DB->getRow("SELECT * FROM `".PRFX.$table_name."` WHERE `".$key_field."`=".$new_item_id);

		foreach($CONFIG['tables']['items']['config'] as $k => $v)
			if (isset($list[$k]))
				$CONFIG['tables']['items']['config'][$k]['value'] = $list[$k];
		}

	if (isset($_SESSION['link_path']))
		{
		if (isset($_SESSION['link_path'][sizeof($_SESSION['link_path'])-1]))
			{
			$link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1];
			if (isset($link_path['inmodule']) && $link_path['inmodule']==$CONFIG['module_name'])
				{
				$CONFIG['tables']['items']['config'][$link_path['to_key']]['value'] =$link_path['from_id'];
				}
			}
		}	

	return $CONFIG;
	}

/**
* Выбор итема для смени позиции
*/
function getItemsToSwap($table_name, $order = 0, $up = true, $parent=0)
	{
	global $DB, $path_id;

	if ($order == 0 ) return false;
	
	if ($up)
		$sql="SELECT `id` FROM `".PRFX.$table_name."` WHERE `order`<".$order." ".((int)$path_id>0 ? ' AND `path_id`="'.(int)$path_id.'"' : '').($parent!==false?" AND `parent`=".$parent:"")." ORDER BY `order` DESC LIMIT 1";
	else
		$sql="SELECT `id` FROM `".PRFX.$table_name."` WHERE `order`>".$order." ".((int)$path_id>0 ? ' AND `path_id`="'.(int)$path_id.'"' : '').($parent!==false?" AND `parent`=".$parent:"")." ORDER BY `order` ASC LIMIT 1";

		
	debug($sql,0,1);
	$id = $DB->getOne($sql);

	return $id;	
	}

/**
* Смена позиции сортировки
*/
function setSwapItemsOrder($table_name, $item_id, $action)
	{
	global $DB, $CONFIG;

	$item = $DB->getRow('SELECT * FROM `'.PRFX.$table_name.'` WHERE `id`="'.(int)$item_id.'"');	

	if (!isset($item['parent'])) $item['parent'] = false;
	
	switch ($action)
		{
		// Двигаем вверх
		case 1:
			{
			$to_swap = getItemsToSwap($table_name, $item['order'], true, $item['parent']);
			
			if ((int)$to_swap>0)
				{
				$target = $DB->getOne('SELECT `order` FROM `'.PRFX.$table_name.'` WHERE `id`="'.(int)$to_swap.'"');

				$sql1 = "UPDATE `".PRFX.$table_name."` SET `order` = '".((int)$target)."' WHERE `id` = '".$item_id."'";					
				$sql2 = "UPDATE `".PRFX.$table_name."` SET `order` = '".((int)$item['order'])."' WHERE `id` = '".(int)$to_swap."'";

				$DB->execute($sql1,true,false);
				$DB->execute($sql2,true,true);
				}
			}
		break;
		
		// Двигаем вниз
		case 0:
			{
			$to_swap = getItemsToSwap($table_name, $item['order'], false, $item['parent']);

			if ((int)$to_swap>0)
				{
				$sql1 = "UPDATE `".PRFX.$table_name."` SET `order` = '".((int)$item['order']+1)."' WHERE `id` = '".$item_id."'";
				$sql2 = "UPDATE `".PRFX.$table_name."` SET `order` = '".((int)$item['order'])."' WHERE `id` = '".(int)$to_swap."'";

				$DB->execute($sql1,true,false);
				$DB->execute($sql2,true,true);
				}				
			}
		break;
		}
	}

/**
* Сброс сортировки
*/
function resetSwapItemsOrder($table_name) 
	{
	global $DB, $path_id;

	$all = $DB->getAll('SELECT `id` FROM `'.PRFX.$table_name.'` '.($path_id>0 ? 'WHERE `path_id`="'.(int)$path_id.'"': '').'');
	if (sizeof($all))
		{
		$order=0;
		foreach ($all as $num => $item)
			{
			$order++;
			$sql='UPDATE `'.PRFX.$table_name.'` SET `order`="'.(int)$order.'" WHERE `id`="'.$item['id'].'"';   
			$DB->execute($sql,true,($order == 1) ? true : false);
			}
		}  

	return (boolean)$order;
	}

############################################################

/**
* Создание условия фильра из памяти в SQL
*/
function generateFilterWhereSQL($table_db_name=''){
	
	global $table_name;

	if ($table_db_name!="")
		$table_name = $table_db_name;

				
	if (isset($_REQUEST['filters'])) 
		$data = $_REQUEST['filters']; 
	elseif (isset($_SESSION['filters'][$table_name])) 
		$data = unserialize($_SESSION['filters'][$table_name]);
	else $data ='';
	
	auth_StoreSession();					

	if( !empty($data) ){

		foreach( $data as $k=>$v ){
			
			if( $v == '' || $v == -1 ){
				
				unset($data[$k]);
				
			}
			
		}
	
	}

	$where=array();
	if (sizeof($data) && is_array($data)) {
		foreach ($data as $key => $value){
			$tmp = explode('_0_',$key);
			$table = $tmp[0];
			$field = $tmp[1];
			$ftype = $tmp[2];
			if(isset($tmp[3]))continue;
			
			if ($table!="" && $field!="") {
				
				if ($value=="") continue;
				
				if($ftype == 'select') $where[]='`'.$field.'`="'.$value.'"';
				else if($ftype=='calendar'){
					$value2=$data[$key.'_0_2'];
					$where[]='(`'.$field.'` BETWEEN "'.$value.'" AND "'.$value2.'")';
					
				} else $where[]='`'.$field.'` LIKE "%'.$value.'%"';
				
			} else {
				continue;
			}
			
		}

		return implode(' AND ', $where);
	} else
		return '';
}

/**
* Очистка фильтров
*/
function clearFilters($table_name='')
	{
	global $CONFIG;

	if ($table_name!="")
		$name = $table_name;
	else
		$name = $CONFIG['module_name'];

	if(isset($_SESSION['filters']) && isset($_SESSION['filters'][$name]))
		unset($_SESSION['filters'][$name]);
		auth_StoreSession();
	}

#################################################################

/**
* Имеет ли модуль блок вывода
**/
function blockOnPage($module_name)
	{
	global $_MODULES;

	$output=0;

	foreach($_MODULES->info[$module_name]['output'] as $zone => $block) 
		if ($block!="" && is_file($_SERVER['DOCUMENT_ROOT'].'/DESIGN/'.$block)) 
			$output++;

	return $output>0 ? true : false;
	}

/**
* Проверяется и создается поле для сортировки, если в таблице создано поле `ORDER`
**/
function moduleOrderField($table_config){
	global $DB;

	$table_name = PRFX.$table_config['db_name'];
	
	$order_field = false;
	$CONF=array();

	$tmp = $DB->getRow('SHOW COLUMNS FROM `'.$table_name.'` LIKE "order"');

	if (isset($tmp['Field']) && $tmp['Field']=="order"){  		  
		$table_config['config']['order'] = array(
				'caption' => '',
				'value' => '',
				'type' => 'order',
				'in_list' => 1,
			);

		$order_field=true;
	}

	return array($order_field, $table_config);
}

/**
*  Подгружается конфиг модуля
**/
function loadConfig()
	{
	global $CONFIG, $_MODULES, $adminmenu, $log_file, $log_config;

	$tmp_config = ($_MODULES->by_dir($adminmenu->params[0]));

	$file = $_SERVER['DOCUMENT_ROOT'].'/MODULES/'.$tmp_config['module_name'].'/config.php';
	include($file);

	$log_file = $_SERVER['DOCUMENT_ROOT'].'/MODULES/'.$tmp_config['module_name'].'/log.php';
	if(file_exists($log_file)) include($log_file);

	unset($tmp_config);
	}

####################################################################################################################

/**
*  Функция, для выборки данных из таблицы
**/
function getDataFromSQL($fields, $table, $where=1, $path_id=0, $order='order', $order_pos='DESC', $_page=0, $onpage=0 , $not_in = array())
	{
    global $DB;

	if ($where=='') $where=1;

    if ($fields != "*") 
		{
		$fields = str_Replace('`','',$fields);
        $fields = explode(",",$fields);
        $fields = "`".implode("`,`",$fields)."`";
	    }     

	if ($where==1 && $path_id>0)
		{
		$where = '`path_id`='.(int)$path_id;
		}
	elseif ($where!=1 && $where!="" && $path_id>0)
		{
		$where.=' AND `path_id`='.(int)$path_id;
		}

	if(count($not_in))
	{
		foreach($not_in as $not_field => $not_value)
		{
			$where.=' AND `'.$not_field.'` not in (\''.implode("','",$not_value).'\')';
		}
	}

	$num_pages = 0;
	
	if ($onpage>0)
		{
	    if ($_page == 0 || !isset($_page)) $_page = 1;
		$from = ($_page-1)*$onpage;
		}

	$sql="SELECT {$fields} FROM `".PRFX.$table."` WHERE {$where} ORDER BY {$order} {$order_pos} ".($onpage>0 ? "LIMIT $from,$onpage" : "")." ";
//debug($sql,false,true);
    $result = $DB->getAll($sql);

	if ($onpage>0)
		{
		$count = $DB->getOne("SELECT count(*) FROM `".PRFX.$table."` WHERE {$where}");
		$num_pages = ceil($count/$onpage);
		}

    return array($result, $num_pages);
	}


/**
*  Шаблон постранички
**/
function prepareOutput($output_id)
	{
	$output_id = trim($output_id, '"');
	$output_id = trim($output_id, "'");
	if (!preg_match("#\(|\)#",$output_id)) 
		{
		$output_id = "'".$output_id."'";
		$output_id = str_Replace("''","'",$output_id);
		$output_id = str_Replace('""','"',$output_id);
		} 

	return $output_id;
	}

/** 
* Рисовалка постраничности для модулей
*/
function mod_pager($page, $pages, $link)
	{
	if (!is_numeric($page)) $page=0;
	$out = array();
	
	if($pages <= 1)
		{
		return "";
		}
	
	for($i = 1; $i <= $pages; $i++)
		{
		if (($i>$page+5) || ($i<$page-5)) continue;	

		if($i == $page)
			{
			$out[] = '<td class="selected">'.$i.'</td>';
			}
		else
			{
			$out[] = '<td class="normal"><a href="#" onclick="'.str_replace("%%", $i, $link).'" class="page_num">'.$i.'</a></td>';
			}
		}
	
	if($page > 1)
		{
		$out = array_merge(array('prev' => "<td><a href=\"#\" onclick=\"".str_replace("%%", "1", $link)."\" class=\"first_page\" title=\"Назад\"><img src=\"/DESIGN/ADMIN/images/arback.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"В начало\"></a></td>"), $out);
		}
	
	if($page < $pages)
		{
		$out[] = "<td><a href=\"#\" onclick=\"".str_replace("%%", ($pages), $link)."\" class=\"last_page\" title=\"Вперед\"><img src=\"/DESIGN/ADMIN/images/arforw.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"В конец\"></a></td>";
		}
	
	return '<table class="module_pager" border="0"><tr>'.implode("", $out).'</tr></table>';
	}

function getPagerTemplate($action='fastview', $output='', $page, $num_pages, $uri_params=''){
	global $CONFIG, $output_id,  $path_id;

	if ($output!="") $output_id = $output;	

	if (!isset($path_id)) $path_id = 0;	

	$output_id = prepareOutput($output_id);
												
	$ajax=false;
	if ($action!="") {$act = $action; $ajax=true;} else $act='fastview';

	$s='';
		   
	if ($page>0 && $num_pages>1 && $CONFIG['module_name']!=""){
		if ($uri_params=="") $uri_params = '%%';

		$s = ''.
				mod_pager(
				$page,
				$num_pages,
				"doLoad('','/".ROOT_PLACE."/".$CONFIG['module_name']."/".$act."/".(int)$path_id."/".$uri_params."/".($ajax ? '1/' : '')."', ".$output_id.")"
				).
			 '';
	}

	return $s;
}

/**
*	Фильтр для обычного спиского модуля
*/
function getFilterData($field, $table_name, $path_id, $filter_type, $fc)
	{
	global $DB;

	$ftitle = $table_name.'_0_'.$field.'_0_'.$filter_type;

	$filters=array();
	if (isset($_SESSION['filters'][$table_name]) && $_SESSION['filters'][$table_name]!="") 
		{
		$filters = unserialize($_SESSION['filters'][$table_name]);
		}

	$out='';

	if(is_file($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/'.$fc['type'].'/filter.php') && $filter_type == 1) 
	{ 
	 include_once($_SERVER['DOCUMENT_ROOT'].'/CORE/forms/'.$fc['type'].'/filter.php'); 
	} 
	 else 
	{
	 $sql = 'SELECT `'.$field.'` FROM `'.PRFX.$table_name.'` '.($path_id>0 ? 'WHERE `path_id`="'.$path_id.'"' : '').' GROUP BY `'.$field.'` ORDER BY `'.$field.'`';
	}
	
		if($out != '') return $out;
	
		$all = $DB->getAll($sql);
	
		if($filter_type == 1)
		{
			$out.='<select name="filters['.$ftitle.']">';
			$out.='<option value="">Любое</option>';
			foreach ($all as $num => $val)
				{
				if (isset($filters[$ftitle]) && $filters[$ftitle]==$val[$field]) $sel='selected'; else $sel='';
		
				$out.='<option value="'.$val[$field].'" '.$sel.'>'.(strlen($val[$field]) > 24 ? substr($val[$field],0,20).'...' : $val[$field]).'</option>';
				}
			$out.='</select>';
		}else{
			$out.='<input type="text" name="filters['.$ftitle.']" value="'.$filters[$ftitle].'" />';
		}

	return $out;
	}

/**
*  Подгружается конфиг конфигуратора формы для поля при выводе в виде ячейки таблицы
**/
function execConfField($type)
	{
	$conf_width=$conf_align='';	

	$file_config = $_SERVER['DOCUMENT_ROOT'].'/CORE/forms/'.$type.'/config.php';
	if (is_file($file_config)) {require($file_config);}

	$s=''.($conf_align!="" ? 'align="'.$conf_align.'"' : '').' '.($conf_width!="" ? 'width="'.$conf_width.'"' : '').'';

	return $s;
	}

/**
*  Подготовка пути для выполнения действия для модуля
**/
function setActionInLink($act, $link){	
	return str_replace('%%ACTION%%',$act,$link);
}

function copy_mod($from,$to) // копирование содержимого из одной директории в другую (копирование модулей)
{
	global $dirs;
	if(!is_dir($to))
	{
		mkdir($to);
		chmod($to,0777);
	}
	
	if($dh = opendir($from))
	{
		while (($file = readdir($dh)) !== false)
		{
			if($file != '.' && $file != '..' && $file != 'info.php')
			{
				if(is_dir($from.'/'.$file))	copy_mod($from.$file,$to.'/'.$file);
				else
				{
					copy($from.'/'.$file,$to.'/'.$file);
					chmod($to.'/'.$file,0666);
				}
			}
		}
		closedir($dh);
	}
}
?>