<?php
$mod_name = $CONFIG['module_name'];
$d_width = (int)$CONFIG['tables']['items']['dialog']['width'];
$d_height = (int)$CONFIG['tables']['items']['dialog']['height'];
$table_config = $CONFIG['tables']['items'];
$key_field = $CONFIG['tables']['items']['key_field'];

global $auth_isROOT;

Echo '<fieldset style="margin-bottom: 5px;">';
echo $Forms->writeModule_LinksPath();
echo $Forms->writeModuleTableStart();
echo $Forms->writeModuleTableHeader($CONFIG['tables']['items'], true);



foreach ($items as $item){
	/**
	* Подготовка конфига для ячейки
	*/
	$item['sub_config']=array(
		'path2item'=>'/'.ROOT_PLACE.'/'.$mod_name.'/%%ACTION%%/'.(int)$path_id.'/'.(int)$page.'/'.(int)$item[$key_field],
		'output_id'=>$output_id
		);

	/**
	* Кнопки дейтсивий Удаление / редактирование
	*/
	$ed_click="displayMessage('/".ROOT_PLACE."/".$mod_name."/add/".(int)$path_id."/".(int)$page."/".(int)$item[$key_field]."/', ".(int)$d_width.", ".(int)$d_height.")";
	
	if(!in_array((int)$item[$key_field],array(1,2,3)))$del_click="if (confirm('Удалить запись?')) doLoad('','/".ROOT_PLACE."/".$mod_name."/delete/".(int)$path_id."/".(int)$page."/".(int)$item[$key_field]."/','".$output_id."')";					

	$act_buttons='';
	$ed_act_dbclick='';
	if ($auth_isROOT || auth_Access($CONFIG['module_name'], 'edit')){
		$act_buttons.=writeSmallEditButton($ed_click);
		$ed_act_dbclick='ondblclick="'.$ed_click.'"';
	}
	
	if(!in_array((int)$item[$key_field],array(1,2,3))){
		if ($auth_isROOT || auth_Access($CONFIG['module_name'], 'del')){
			$act_buttons.=writeSmallDeleteButton($del_click);
		}
	}

	if ($act_buttons=='') $act_buttons='&nbsp;';
	
	/**
	* Доп. параметры ячейки таблицы
	*/
	$addon_params_tag='ondblclick="'.$ed_click.'" onmouseover="this.style.backgroundColor=\'#FBFCE2\';" onmouseout="this.style.backgroundColor=\'white\'"';

	/**
	* Вывод ячейки таблицы
	*/
	echo $Forms->moduleTableCell($table_config, $item, $act_buttons, $addon_params_tag);
}

echo $Forms->writeModuleTableEnd($pager);
echo '</fieldset>';
?>

<div align="center">

	<a style="float: left" onclick="displayMessage('/<?=ROOT_PLACE;?>/<?=$mod_name;?>/add/<?=(int)$path_id?>/<?=(int)$page;?>', <?=$d_width;?>, <?=$d_height;?>)" href="#" class="button">Добавить</a>

	<?if ($auth_isROOT){?>
		<a href="#" class="button" style="float: right" onclick="displayMessage('/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/manage_groups/', <?=(int)$d_width;?>,<?=(int)$d_height;?>)">Управление группами</a>
	<?}?>

</div>
