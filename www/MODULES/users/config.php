<?php

$module_name='users';
$module_caption = 'Пользователи';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,
			'key_field'=>'user_id',
			'dialog'=> array('width'=>700, 'height'=>550),			  		
			'onpage' =>	20,	 
			
			'links' => array(			 
					array(				
						'target_module'=>'users_access',
						'target_key'=>'user_id',
						'main_key'=>'id',
					),			
					array(				
						'target_module'=>'tarifs',
						'target_key'=>'user_id',
						'main_key'=>'id',
					),						
				),

			'config'=>	array(

				'username' => array(
							'caption' => 'Логин',
							'value' => '',
							'height'=>100,
							'type' => 'string',
							'in_list' => 1,
							'filter'=>1,
							),

				'useremail' => array(
							'caption' => 'E-Mail',
							'value' => '',
							'height'=>100,
							'type' => 'string',
							'in_list' => 1,
							),

				'pass' => array(
							'caption' => 'Пароль',
							'value' => '',
							'height'=>100,
							'type' => 'password',
							'in_list' => 0,
							),

				'groups' => array(
							'caption' => 'Принадлежность группам',
							'value' => '',
							'from'=> array(
									'table_name'=>PRFX.'users_groups', 
									'key_field'=>'group_id',
									'name_field'=>'name',
									'where' => 'group_id NOT IN(1,3)',
							),
							'type' => 'group_checkbox',
							'in_list' => 1,
							'filter'=>1,
							),

				),
			),


		'groups_table'=>array(

			'db_name' => $module_name.'_groups',		
			'key_field'=>'group_id',
			'dialog'=> array('width'=>600,'height'=>350),
			'onpage' =>	23,				

			'config'=>	array(

				'name' => array(
							'caption' => 'Имя группы',
							'value' => '',
							'type' => 'string',
							'in_list' => 1,
							),

				),
			),
	
		),
);
?>