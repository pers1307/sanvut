<?php
/** 
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name='images';
$module_caption = 'Баннер в шапке';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		
	
	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>440,'height'=>300),
			'key_field'=>'id',
			//	'order_field' => '`title`',
			'onpage' =>	20,
			
			'config'=>	array(
			
				'title' => array(
					'caption' => 'Название',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					'filter'=>1,
					),
					
				'img' => array(
					'caption' => 'Изображение',
					'value' => '',
					'type' => 'image',
					'thumbs' => array( 0=>array( 1349, 540, 1 ) ),
					'in_list' => 1,
					),

				'active' => array(
					'caption' => 'Опубликовано',
					'value' => 1,
					'type' => 'boolean',
					'in_list'=>1,
					'filter'=>1,
					),

				'title_twe' => array(
					'caption' => 'Название_второе',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					'filter'=>1,
				),
					
			),							
		),
	),
);
?>