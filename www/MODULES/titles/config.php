<?php
/** 
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name='titles';
$module_caption = 'Заголовки (titles)';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>700,'height'=>550),
			'key_field'=>'id',
			'log_title_field' => 'text',
			'order_field' => '`sort`',
			'onpage' =>	9,

			'config'=>	array(

				'url' => array(
							'caption' => 'Маска URLа (% - любая замена)',
							'value' => '',
							'type' => 'string',
							'in_list' => 1,
							'filter' => 0,
							),
				'type_insert' => array(
							'caption' => 'Место вставки',
							'type' => 'select',
							'values' => array(
								0 => 'вместо',
								1 => 'до оригинала',
								2 => 'после оригинала',
								3 => 'до результата',
								4 => 'после результата',
							),
							'in_list' => 1,
							
				),
				'text' => array(
							'caption' => 'Текст',
							'value' => '',
							'height'=>70,
							'type' => 'memo',
							'filter' => 0,
							'in_list' => 1,
							),
				'sort' => array(
					'caption' => 'Сила',
					'type' => 'string',
					'value' => 0,
					'in_list' => 1,
				),
				'active' => array(
					'caption' => 'Включено',
					'type' => 'boolean',
					'value' => 1,
					'in_list' => 1,
					'filter' => 0,
				),
				),
			),
		),
);
?>