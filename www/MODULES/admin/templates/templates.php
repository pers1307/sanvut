<form id="templates_form">
<table width="100%" border="0" style="height: 100%;">
	<tr>
		<td width="280" style="padding: 10px; height: 100%;" valign="top">
			
			<div id="tree_div_templates" style="overflow-y: scroll; height: 100%; border: 1px solid #919B9C; background: #E5E7E7;">
			<table width="100%">
				<?php
				echo($tree);
				?>
			</table>
			</div>

			<div id="template_complite" align="center" style="padding: 3px"></div>

			<div id="copy_complite" align="center" style="padding: 3px"></div>
		
		</td>
		<td valign="top" style="padding: 10px">	
		
			<h2>Галочками нужно выбрать страницы на которые применяется шаблон</h2>

			<br>
			
			<div style="border: 1px solid #919B9C; background: #E5E7E7;">
				<table width="100%">
					<tr>
						<td width="60">Шаблон: </td>
						<td style="padding: 2px;">
							<select name="tpl_id" style="width: 100%;">
<!--								<option value="0">-- Без шаблона --</option>-->
								<?php
								foreach($templates as $index => $item)
									{
									echo '<option value="'.$item['id'].'">'.$item['caption'].'</option>';
									}
								?>
							</select>															
						</td>
						<td width="20">&nbsp;</td>
						<td width="90" align="center">
							<div align="left"><a href="#" onclick="doLoad(getObj('templates_form'),'/<?=ROOT_PLACE;?>/ajax/apply_template/','template_complite','POST');" class="save">Применить</a></div>							
						</td>
					</tr>
				</table>
				
			</div>		
			
			<br>

			<div style="border: 1px solid #919B9C; background: #E5E7E7;">
				<div style="padding: 5px;"><b>Копирование блока на страницы</b></div>
				
				<table width="100%">
					<tr>
						<td width="60">Блок: </td>
						<td>
							<select name="block_file" style="width: 100%">
								<option value="">-- Без блока --</option>
							<?php
							foreach($result as $block)
								{
								$block['file']=str_replace($_SERVER["DOCUMENT_ROOT"].'/DESIGN/','',$block['file']);
								echo '<option value="'.$block['file'].'">'.$block['caption'].'</option>';
								}
							?>
							</select>
						
						</td>
					</tr>
					<tr>
						<td valign="top">В Зону:</td>
						<td>
							<select name="zones[]" style="width: 100%" size="7" multiple="multiple">
								<?php 
								foreach($zones as $zone)
									{
									echo '<option value="'.$zone['value'].'">'.$zone['caption'].' [ '.$zone['value'].' ]</option>';
									}
								?>
							</select>
						</td>
					</tR>
					<tr>
						<td valign="top">Верстка:</td>
						<td>
							<select name="verstka" style="width: 100%;" size="4">
								<?php 
								foreach($verstkas as $verstka)
									{
									echo '<option value="'.$verstka.'">'.$verstka.'</option>';
									}
								?>
							</select>
						</td>
					</tR>
					<tr>
						<td valign="top">Модуль:</td>
						<td>
							<select name="module" style="width: 100%;" size="5">
								<option value="">Без модуля</option>
								<?php 
								foreach($inst['elements'] as $module)
									{
									echo '<option value="'.$module['module_dir'].'">'.$module['module_caption'].'</option>';
									}
								?>
							</select>
						</td>
					</tR>
				</table>
				<hr>
				<table width="100%">
					<tr>
						<td width="60">&nbsp;</td>
						<td valign="top">&nbsp;Очистка от блоков выбранных зон</td>
						<td>&nbsp;</td>
						<td width="60" align="right"><div align="left"><a href="#" onclick="doLoad(getObj('templates_form'),'/<?=ROOT_PLACE;?>/ajax/clean_zones/','copy_complite','POST');" class="save">Очистить</a></div></td>
					</tr>
				</table>
				<hr>
				<table width="100%">
					<tr>
						<td width="60">&nbsp;</td>
						<td valign="top">
							<div>&nbsp;Копирование блоков в зоны, привязка модуля и шаблона</div>
							<div><input type="checkbox" name="is_clear_zone" value="1"> очищать каждую выбранную зону перед копированием?</div>
						</td>
						<td>&nbsp;</td>
						<td width="60" align="right"><div align="left"><a href="#" onclick="doLoad(getObj('templates_form'),'/<?=ROOT_PLACE;?>/ajax/copy_block/','copy_complite','POST');" class="save">Сохранить</a></div></td>
					</tr>
				</table>

			</div>

		</td>
	</tr>
</table>

<div style="margin:10px;">
	<a href="#" onclick="doLoad(getObj('templates_form'),'/<?=ROOT_PLACE;?>/ajax/nodel/','nodel','POST');" class="button">Заблокировать</a>
</div>
</form>
