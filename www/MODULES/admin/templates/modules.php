<div id="modules">


	<? if (isset($inst['elements']) && sizeof($inst['elements'])):?>
		<h3>Установленные модули</h3>

		<table class="table">
			<tr>
				<th width="32" align="center">№</th>
				<th align="center">Модуль</th>
				<th width="80" align="center">Версия</th>
				<th width="80" align="center">Действие</th>
			</tr>
		<? $i=0;foreach($inst['elements'] as $module):$i++;?>	
			<tr>
				<td width="24" align="center"><?=$i;?>.</td>
				<td style="padding: 4px;">
					<?=$module['module_caption']?>
				</td>
				<td align="center"><?=$module['version']?></td>
				<td align="center">
					<?if(!in_array($module['module_name'],array('users','users_access'))){?>
						<a href="#" onclick="doLoad('','/<?=ROOT_PLACE;?>/ajax/delete_mod/<?=$module['module_dir']?>/','modules;menu_div;left_tab2','post')" style="color: red"><b>Удалить</b></a>
					<?}?>
				</td>
			</tr>
		<?endforeach;?>
		</table>

	<? endif;?>



	<? if (isset($syst['elements']) && sizeof($syst['elements'])):?>
	<h3>Системные модули</h3>
		<table class="table">
			<tr>
				<th width="32" align="center">№</th>
				<th align="center">Модуль</th>
				<th width="80" align="center">Версия</th>
				<?/*<th width="80" align="center">Действие</th>*/?>
			</tr>
		<? 
			$i=0;
			foreach($syst['elements'] as $module){
				$i++;
				?>
				<tr>
					<td width="24" align="center"><?=$i;?>.</td>
					<td style="padding: 4px;">
						<?=$module['module_caption']?>
					</td>
					<td align="center"><?=$module['version']?></td>
					<?/*<td align="center"><a href="#" onclick="doLoad('','/<?=ROOT_PLACE;?>/ajax/delete_mod/<?=$module['module_dir']?>/','modules;menu_div;left_tab2','post')" style="color: red"><b>Удалить</b></a></td>*/?>
				</tr>
	
			<?}?>
		</table>
	<? endif;?>	



	<? if (isset($notinst['elements']) && sizeof($notinst['elements'])):?>
	<h3>Неустановленные модули</h3>
		<table class="table">
			<tr>
				<th width="32" align="center">№</th>
				<th align="center">Модуль</th>
				<th width="80" align="center">Версия</th>
				<th width="80" align="center">Действие</th>
			</tr>
		<? $i=0;foreach($notinst['elements'] as $module):$i++;?>

			<tr>
				<td width="24" align="center"><?=$i;?>.</td>
				<td style="padding: 4px;">
					<?=$module['module_caption']?>
				</td>
				<td align="center"><?=$module['version']?></td>
				<td align="center"><a href="#" onclick="doLoad('','/<?=ROOT_PLACE;?>/ajax/install/<?=$module['module_dir']?>/','modules;menu_div;left_tab2','post')" style="color: green"><b>Установить</b></a></td>
			</tr>			

		<? endforeach;?>
		</table>
	<? endif;?>


</div>