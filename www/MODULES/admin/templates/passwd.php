<?
global $_USER;
?>
<h3>Изменение пароля пользователя <?=$_USER['username'];?></h3>
<div style="padding: 10px;">
	<form method="post" id="passwd_form" enctype="multipart/form-data">
		<table cellspacing="0" cellpadding="5">
			<tr>
				<td width="20%">Текущий пароль</td>
				<td><input type="password" name="oldpass" style="width:100%;"></td>
			</tr>
			<tr>
				<td>Новый пароль</td>
				<td><input type="password" name="newpass" style="width:100%;"></td>
			</tr>
			<tr>
				<td>Подтверждение нового пароля</td>
				<td><input type="password" name="newpass2" style="width:100%;"></td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="5">
			<tr>
				<td style="height:50px;"><div id="passwd_report" style="font-weight: bold">&nbsp;</div></td>
				<td width="120" align="right"><a href="#" onclick="doLoad(document.getElementById('passwd_form'), '/<?=ROOT_PLACE;?>/ajax/passwd_change/','passwd_report','post','rewrite'); closeAjaxWorking();" class="save">Изменить</a></td>
			</tr>
		</table>  		
	</form>
</div>