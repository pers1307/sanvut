<?
global $_ZONES,$_MODULES,$_VARS,$DB;

$des_file="";
if (file_exists(DES_DIR.$info['main_template']) && is_file(DES_DIR.$info['main_template'])){
	$des_file = file_get_contents(DES_DIR.$info['main_template']);
}

preg_match_all('#<\?=\$(_[a-z_A-Z0-9]+_)\?>#',$des_file, $exzone);

//--------получаем имя зоны данной верстки-----------
if (isset($exzone[1]) && sizeof($exzone[1])) $exzone = $exzone[1];
else $exzone = false;


$assigned_modules=array();
if (sizeof($info['config']) && is_array($info['config'])){
	foreach ($info['config'] as $zone_config){
		foreach ($zone_config as $value){
			if ($value['module'] && isset($_MODULES->info[$value['module']]) && blockOnPage($value['module'])){
				$assigned_modules[$_MODULES->info[$value['module']]['module_caption']] = array($_MODULES->info[$value['module']]['fastcall'],$_MODULES->info[$value['module']]['module_name']);
			}
		}
	}
}		

$tabs = array();

if (isset($assigned_modules)){
	foreach($assigned_modules as $name => $value){
		$tabs[] = $name;
	}
}

$temp = assoc('value',$_ZONES);	 
if($exzone && count($exzone)){
	$exzone = array_unique($exzone);
	foreach($exzone as $zone){
		if (isset($temp[$zone])){
//			if($zone=="_CONTENT_" || auth_isROOT()) 
			$tabs[] = $temp[$zone]['caption'];
		}
	}
}

//------------табы---------------------------------------------------------------------------------------------------------
$i=0;
$act_tab = 1;
$s_tabs = '<table cellspacing="0" cellpadding="0" border="0">';
foreach ($tabs as $tab){
	$i++;
	$tab = trim($tab,"'");

	$c1 = 'tab_item_inactive';
	$c2 = 'tab_item2_inactive';
	if ($i==1) {
		$c1 = 'tab_item_active';
		$c2 = 'tab_item2_active';
	}

	$s_tabs .= '
		<td style="cursor: pointer">
			<table cellpadding="0" cellspacing="0" border="0" style="height: 27px; width:100%" id="center_tab_item'.(int)$i.'" class="'.$c1.'">
				<tr>
					<td align="center" style="padding-left: 5px;" onclick="showCenterTab('.(int)$i.','.count($tabs).');">'.$tab.'</td>
					<td style="width: 5px" id="center_tab_item'.(int)$i.'_img" class="'.$c2.'"><img src="/DESIGN/ADMIN/images/blank.gif" width="5" height="1" border="0" alt=""></td>
				</tr>
			</table>
		</td>
	';
}
$s_tabs.= '</table>';
//------------табы---------------------------------------------------------------------------------------------------------

?>


<table cellspacing="0" cellpadding="0" width="100%" style="height: 100%;" border="0">
	<tr style="height: 27px;">
		<td>
			<?=$s_tabs;?>
		</td>
	</tr>
	<tr>
		<td valign="top">
			<?
				//-----------каунтер для айдишников блоков--------------
				$i=0;
				$tab_num=1;
				
				//------вывод вывода модулей--------------------------------------------------------------------------------------------------------
				if (isset($assigned_modules) && sizeof($assigned_modules)){
					foreach ($assigned_modules as $name => $link){
						$i++;
						$links[$name] = $link[0];
						?>
						<div class="tab_item_cont" id="center_tab<?=$tab_num++;?>" <?=($i==1 ? '' : 'style="display: none;"');?>>
							<div id="fast_table_<?=$link[1]?>"></div>
						</div>
						<?
					}
				}
				//------вывод вывода модулей--------------------------------------------------------------------------------------------------------
			
			$zact_tab = 1;
			if (isset($exzone) && is_array($exzone))foreach($exzone as $zone){
				//	if ($zone!="_CONTENT_" && !auth_isROOT()) continue;
					$i++;
					if($i>$zact_tab)$s='display: none;';else $s='';
					?>
					<div id="center_tab<?=$tab_num++;?>" style="background: white;border: 1px solid #919b9c; <?=$s?>">
						<?
							//--------item_content--------------------------------------------------------------------------------------------------------------
							
							$edit_action="displayMessage('/".ROOT_PLACE."/admin/zone_wysiwyg_message/".$zone."/".$info['path_id']."/'+(getObj('td_content".$zone."').offsetHeight-15)+'/', '100%', '100%',0,1); return false;";
							?>
							<table cellpadding="0" cellspacing="0" border="0" width="100%" style="height:100%;" id="tab_cont111">
								
								<tr>
									<td id="td_content<?=$zone?>" valign="top" style="padding-left:4px; padding-top:4px; padding-bottom:10px;opacity: 0.5;filter: alpha(opacity=50);cursor:pointer;" valign="top" ondblclick="<?=$edit_action?>">	  												
										<div style="border:1px solid #696969;height:100%;">
											<div style="border-bottom:1px solid #696969;height:53px;background:#f0f0ee;">
												<img src="/DESIGN/ADMIN/images/wysiwyg.jpg" alt="">
											</div>
											<div style="padding:10px;cursor:pointer;" id="content_div_<?=$zone?>_2" ondblclick="<?=$edit_action?>">
												<?=strip_tags(html_entity_decode($DB->getOne('SELECT `content` FROM `'.PRFX.'zones_content` WHERE `zone_id`="'.$zone.'" AND `path_id`='.(int)$info['path_id']), ENT_QUOTES),'<img><p><h1><h2><h3><h4><h5><h6><span><div><br><iframe><b><i><small><strong><table><tr><td><tbody><thead><ol><ul><li>');?>&nbsp;
											</div>
										</div>
							
							
									</td>
								</tr>
								<tr style="height: 32px;">
									<td valign="top">
										<?
										//------------нижние кнопки---------------------------------------------------------------------------------------------------------
										?>
										<table cellspacing="0" cellpadding="5" border="0" width="100%">
											<tr>
												<? if(auth_isROOT()){?>
												<td width="130" align="center"><a class="save" href="#" onclick="displayMessage('/<?=current_path('/admin/structure/add_module/'.$info['path_id'].'/'.$zone)?>', 500, 400); return false;">Модуль</a></td>
												<td width="130" align="right"><a class="save" href="#" onclick="displayMessage('/<?=current_path('/admin/structure/add_block/'.$info['path_id'].'/'.$zone)?>',600,400); return false;">Блок</a></td>
												<td width="250" id="zone_save_report">&nbsp;</td>
												<td>&nbsp;</td>
										
													<td width="130">
													<?
														//------------управление блоками вывода---------------------------------------------------------------------------------------------------------
														if(auth_isROOT()){
															?>
															<div id="<?=$info['path_id'].$zone?>">
																
															<?
															if (isset($info['config'][$zone]) && sizeof($info['config'][$zone])){
																												
																$info['config'][$zone] = array_values($info['config'][$zone]);
															?>
																<table>
																<?
																foreach($info['config'][$zone] as $key => $data){
																?>
																	<tr style="clear:both;">  															
																		<td width="5">
																			<a href="#" title="Удалить блок" onclick="doLoad('<?=$key?>','/<?=ROOT_PLACE;?>/ajax/delete_script/<?=$info['path_id']?>/<?=$key?>/<?=$zone?>/','<?=$info['path_id'].$zone?>');" class="delete"><img src="/DESIGN/ADMIN/images/blank.gif" width="1" height="1" alt="" border="0"></a>
																		</td>
																		<td width="5">
																			<a href="#" title="Переместить вверх" onclick="doLoad('<?=$key?>','/<?=ROOT_PLACE;?>/ajax/swap_scripts/<?=$info['path_id']?>/<?=$key?>/up/<?=$zone?>/','<?=$info['path_id'].$zone?>');" class="up"><img src="/DESIGN/ADMIN/images/blank.gif" width="1" height="1" alt="" border="0"></a>
																		</td>
																		<td width="5">
																			<a href="#" title="Переместить вниз" onclick="doLoad('<?=$key?>','/<?=ROOT_PLACE;?>/ajax/swap_scripts/<?=$info['path_id']?>/<?=$key?>/down/<?=$zone?>/','<?=$info['path_id'].$zone?>');" class="down"><img src="/DESIGN/ADMIN/images/blank.gif" width="1" height="1" alt="" border="0"></a>
																		</td>
																		<td title="<?=$data['script'];?>" style="cursor: pointer">
																			<?=getScriptName($data['script'])?>
																		</td>
																	</tr>
																<?}?>
																</table>
															<?}?>
															</div>
														<?
														}
														//------------управление блоками вывода---------------------------------------------------------------------------------------------------------
													
													?>
													</td>
												<?}?>
												<td></td>
												<td width="120" id="zone_<?=$zone;?>_text_save"><a class="save2" href="#" onclick="<?=$edit_action?>">Редактировать</a></td>
											</tr>
										</table>
										<?
										//------------нижние кнопки---------------------------------------------------------------------------------------------------------
										?>
									</td>
								</tr>
							</table>
							<?
							//--------item_content--------------------------------------------------------------------------------------------------------------
						
						?>
					</div>
			<?}?>
			
		</td>
	</tr>
</table>

<script type="text/javascript">
//var active_center_tab = <?=sizeof($assigned_modules)>0 ? 2 : 1; ?>;
var active_center_tab = 1;
<? if(isset($assigned_modules)){?>
	<? foreach($assigned_modules as $name => $link){?>
		doLoad('','<?=$link[0].$info['path_id'].'/1/'?>','fast_table_<?=$link[1]?>');
	<?}?>
<?}?>
</script>




