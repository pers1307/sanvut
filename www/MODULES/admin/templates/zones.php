<div id="zone_error"></div>	  

<br/><table cellpadding="5" cellspacing="0" border="0" class="table">
	<tr>
		<th>Название</th>
		<th width="200" align="center">Системное имя</th>
		<th width="90" align="center">Для поиска</th>
		<th width="90" align="center">Блок ссылок (для поиска)</th>
		<th width="20"></th>
		<th width="20"></th>
	</tr>
	<?foreach ($zones as $key => $zone):?>
	<tr>
		<td><?=$zone['caption']?></td>
		<td align="center"><?=$zone['value']?></td>
		<td align="center"><?=(isset($zone['for_search']) && $zone['for_search']==1 ? 'Да': 'Нет');?></td>
		<td align="center"><?=(isset($zone['search_links']) && $zone['search_links']==1 ? 'Да': 'Нет');?></td>
		<td><?if (!isset($exclude[$zone['value']])):?><a href="#" class="edit" onclick="displayMessage('/<?=ROOT_PLACE;?>/admin/edit_zone/<?=$zone['id']?>/',400,300)">&nbsp;</a><?endif;?></td>
		<td><?if (!isset($exclude[$zone['value']])):?><a href="#" class="delete" onclick="if (confirm('Вы действительно хотите удалить зону?')){doLoad('','/<?=ROOT_PLACE;?>/admin/delete_zone/<?=$zone['id']?>/','center')}">&nbsp;</a><?endif;?></td>				
	</tr>
	<?endforeach;?>
</table>
<a href="#" style="float: right;" class="button" onclick="displayMessage('/<?=ROOT_PLACE;?>/admin/edit_zone/0/',400,300)">Добавить зону</a>
