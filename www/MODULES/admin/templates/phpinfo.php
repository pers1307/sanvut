<?

ob_start();

?>
<style type="text/css"> 
.phpinfo
	{
	background:#fff;
	padding:10px;
	}
.phpinfo img
	{
	float:right;
	border:0px;
	}
.phpinfo hr
	{
	width:600px;
	background-color:#E7E7E8;
	border:0px;
	height:1px;
	color:#000000;
	text-align:left;
	}
.phpinfo .p
	{
	text-align:left;
	}
.phpinfo .e
	{
	background-color:#E7E7E8;
	font-weight:bold;
	color:#000000;
	}
.phpinfo .h{
	background-color:#E7E7E8;
	font-weight:bold;
	color:#000000;
	}
.phpinfo .v{
	background-color:#E7E7E8;
	color:#000000;
	}
.phpinfo .vr{
	background-color:#E7E7E8;
	text-align:right;
	color:#000000;
	}
.phpinfo pre
	{
	margin:0px;
	font-family:monospace;
	}
.phpinfo table
	{
	border-collapse:collapse;
	}
.phpinfo  th
	{
	text-align:center !important;
	}
.phpinfo td, .phpinfo th
	{
	border:1px solid #fff;
	}
</style>
<?

$customStyle = ob_get_clean();

ob_start();

	phpinfo();

$out = ob_get_clean();
	
$out = preg_replace('/<style type="text\/css">(.*?)<\/style>/si', $customStyle, $out);
$out = preg_replace('/<div class="center">/', '<div class="center phpinfo">', $out);

echo $out;

?>