<table cellpadding="5" cellspacing="0" border="0" class="table">
	<tr>
		<th width="32">id</th>
		<th>Название</th>
		<th width="100">Порядок</th>
		<th width="32"></th>
	</tr>
	<?foreach ($modules_types as $key => $type){
	
		$ed_click="displayMessage('/".ROOT_PLACE."/admin/edit_modules_type/".$type['id']."/',400,220)";

		$del_click="if (confirm('Вы действительно хотите удалить этот тип модулей?')){doLoad('','/".ROOT_PLACE."/admin/delete_modules_type/".$type['id']."/','center')}";

	?>
	<tr ondblclick=<?=$ed_click?>" style="cursor: pointer" onmouseover="this.style.backgroundColor='#FBFCE2';" onmouseout="this.style.backgroundColor='white'">
		<td align="center"><?=$type['id']?></td>
		<td align="center"><?=$type['title']?></td>
		<td align="center"><?=$type['sortir'];?></td>
		<td align="center">
			<?=writeSmallEditButton($ed_click);?>
			<a href="#" class="delete" onclick="if(confirm('Вы действительно хотите удалить тип?')){doLoad('','/<?=ROOT_PLACE;?>/admin/delete_modules_type/<?=$type['id']?>/','center')}">&nbsp;</a>
		</td>
	</tr>
	<?}?>
</table>
<br>
<a href="#" class="button" onclick="displayMessage('/<?=ROOT_PLACE;?>/admin/edit_modules_type/-1/',400,320)">Добавить тип модулей</a>
