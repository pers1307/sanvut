<form id="templates_form">
<table width="100%" border="0" style="height: 100%;">
	<tr>
		<td width="250" style="padding: 10px; height: 500px;" valign="top">
			
			<div><B>Из:</B></div>
			<div id="tree_from_div" style="overflow: auto; height: 100%; border: 1px solid #919B9C; background: #E5E7E7;">
			<table width="100%">
				<?php
				echo($tree_from);
				?>
			</table>
			</div>
		
		</td>
		<td width="250" style="padding: 10px; height: 500px;" valign="top">
			
			<div><B>В:</B></div>
			<div id="tree_to_div" style="overflow: auto; height: 100%; border: 1px solid #919B9C; background: #E5E7E7;">
			<table width="100%">
				<?php
				echo($tree_to);
				?>
			</table>
			</div>
		
		</td>
		<td valign="top" style="padding: 10px">		

			<div>
			<fieldset>
				<legend>Копирование целого раздела</legend>

				<TABLE>
					<TR>
						<TD valign="top"><INPUT TYPE="radio" NAME="www_pages_action" id="copy_struct" value="quick"></TD>
						<TD><label for="copy_struct" style="cursor: pointer">Скопировать только структуру</label></TD>
					</TR>
					<TR>
						<TD valign="top"><INPUT TYPE="radio" NAME="www_pages_action" id="copy_struct_config" value="config"></TD>
						<TD><label for="copy_struct_config" style="cursor: pointer">Скопировать структуру и конфигурацию</label></TD>
					</TR>
					<TR>
						<TD valign="top"><INPUT TYPE="radio" NAME="www_pages_action" id="copy_struct_config_data" value="all"></TD>
						<TD><label for="copy_struct_config_data" style="cursor: pointer">Скопировать структуру, конфигурацию и содержимое (Текст страницы)</label></TD>
					</TR>
				</TABLE>
			</fieldset>

			<br>

			<fieldset>
				<legend>Копирование содержимого раздела</legend>

				<TABLE>
					<TR>
						<TD valign="top"><INPUT TYPE="radio" NAME="www_pages_action" id="copy_struct" value="childs_quick"></TD>
						<TD><label for="copy_struct" style="cursor: pointer">Скопировать только структуру</label></TD>
					</TR>
					<TR>
						<TD valign="top"><INPUT TYPE="radio" NAME="www_pages_action" id="copy_struct_config" value="childs_config"></TD>
						<TD><label for="copy_struct_config" style="cursor: pointer">Скопировать структуру и конфигурацию</label></TD>
					</TR>
					<TR>
						<TD valign="top"><INPUT TYPE="radio" NAME="www_pages_action" id="copy_struct_config_data" value="childs_all"></TD>
						<TD><label for="copy_struct_config_data" style="cursor: pointer">Скопировать структуру, конфигурацию и содержимое (Текст страницы)</label></TD>
					</TR>
				</TABLE>
			</fieldset>

			<br>

			<A href="#" class="save" onclick="doLoad(document.getElementById('templates_form'),'/<?=ROOT_PLACE;?>/ajax/www_pages_copy/','report_www_pages;tree_from_div;tree_to_div','POST');">Копировать</a>
				<div id="report_www_pages"></div>
			</div>
			

		</td>
	</tr>
</table>
</form>
