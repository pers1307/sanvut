<h3>Свойства страницы: <?=$title_menu;?></h3>
<div style="padding: 10px;">
	
	<?if($form_tdk!=""){?>		
	<fieldset>
	<h2>Основные параметры страницы</h2>
	<div id="tdk_form_div">
	<?=$form_tdk;?>
	</div>
	</fieldset>		  
	<br>
	<?}?>

	<?if($template_form!=""){?>		
	<fieldset>
	<h2>Верстка страницы</h2>
	<form method="post" id="template_form" enctype="multipart/form-data">
		<?=$template_form;?>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td><a href="#" onclick="doLoad(document.getElementById('template_form'), '/<?=ROOT_PLACE;?>/ajax/update_template/','template_report','post','rewrite'); closeAjaxWorking();" class="save">Сохранить</a></td>
				<td width="80" align="right"><div id="template_report" style="font-weight: bold"></div></td>
			</tr>
		</table>  		
	</form>
	</fieldset>
	<br>
	<?}?>

	<?if($tpl_form!=""){?>
	<fieldset>
	<h2>Сохранение шаблона страницы</h2>
	<form method="post" id="tpl_form" enctype="multipart/form-data">
		<?=$tpl_form;?>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td><a href="#" onclick="doLoad(document.getElementById('tpl_form'), '/<?=ROOT_PLACE;?>/ajax/save_tpl_page/','tpl_report'); closeAjaxWorking();" class="save">Сохранить</a></td>
				<td align="right"><div id="tpl_report" style="font-weight: bold"></div></td>
			</tr>
		</table>
	</form>
	</fieldset>
	<br>
	<?}?>
	<?if($pagepath_form!=""){?>
	<fieldset>
	<h2>Системное имя страницы</h2>
	<form method="post" id="pagepath_form" enctype="multipart/form-data">
		<?=$pagepath_form;?>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td><a href="#" onclick="doLoad(document.getElementById('pagepath_form'), '/<?=ROOT_PLACE;?>/ajax/save_pagepath_page/','pagepath_report');  closeAjaxWorking();" class="save">Сохранить</a></td>
				<td width="80" align="right"><div id="pagepath_report" style="font-weight: bold"></div></td>
			</tr>
		</table>
	</form>
	</fieldset>
	<br>
	<?}?>

	<?if($redirect_form!=""){?>
	<fieldset>
	<h2>Перенаправления</h2>
	<form method="post" id="rdr_form" enctype="multipart/form-data">
		<?=$redirect_form;?>
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td><a href="#" onclick="doLoad(document.getElementById('rdr_form'), '/<?=ROOT_PLACE;?>/ajax/update_rdr/', 'rdr_report','POST');  closeAjaxWorking();" class="save">Сохранить</a></td>
				<td width="80" align="right"><div id="rdr_report" style="font-weight: bold"></div></td>
			</tr>
		</table>
	</form>
	</fieldset>
	<?}?>
</div>