<div id="explorer" style="height:100%;">
	<TABLE cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="height: 100%">
		<TR style="height: 24px">
			<TD>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr>
						<td class="address" width="30" align="right"><a href="#" onclick="doLoad('','<?=$backlink?>','explorer','POST');" class="back">&nbsp;</a></td>
						<td class="address" align="center">
							<form method="get" id="explore">
								<input type="text" name="path" value="<?=$path?>" / class="path">
								<input type="hidden" name="basepath" value="<?=request('basepath','')?>">
								<input type="hidden" name="path_id" value="<?=request('path_id','')?>">
								<input type="hidden" name="zone" value="<?=request('zone','')?>">
								<input type="hidden" name="conf" value="<?=request('conf','')?>"/>
								<input type="hidden" name="ajax" value="1"/>
							</form>
						</td>
						<td class="address" width="24" align="right"><a href="#" onclick="closeDialog();" class="close">&nbsp;</a></td>
					</tr>
				</table>			
			</TD>
		</TR>
		<TR>
			<TD>

				<div class="container" style="height: 100%">
					<table cellpadding="0" cellspacing="0" border="0" class="folders">
						<tr>
							<th width="80%">Имя</th>
							<th width="10%">Размер</th>
							<th width="10%">Права</th>
						</tr>
						<?foreach($folders['folders'] as $folder):?>
						<tr>
							<td style="padding: 1px 5px;"><img src="/DESIGN/ADMIN/images/address_folder.png" alt="Папка: <?=$folder['file']?>"/>&nbsp;<a href="#" onclick="doLoad('','/<?=ROOT_PLACE;?>/ajax/explorer/?path=<?=EndSlash(request('path','').$folder['file'])?>&basepath=<?=request('basepath','')?>&path_id=<?=request('path_id','')?>&zone=<?=request('zone','')?>&conf=<?=request('conf','')?>','explorer','POST');"><?=$folder['file']?></a></td>
							<td align="center"><?=($folder['filesize'])? $folder['filesize']: '&nbsp;'?></td>
							<td align="center"><?=$folder['chmod']?></td>
						</tr>
						<?endforeach;?>
						<?foreach($folders['files'] as $file):?>
						<tr>
							<td style="padding: 1px 5px;">
							<?if (file_exists(DOC_ROOT.'DESIGN/ADMIN/images/file_types/'.$file['type'].'.png')):?>
								<img src="/DESIGN/ADMIN/images/file_types/<?=$file['type']?>.png" alt=""/>
							<?else:?>
								<img src="/DESIGN/ADMIN/images/file_types/file.png" alt=""/>
							<?endif;?>
							&nbsp;
			<?if (!request('conf','')):?>
			<a href="#" onclick="return_path('<?=request('path','').$file['file']?>','<?=request('path_id',0)?>','<?=request('zone','')?>');"><?=$file['caption'] ? $file['caption'] : $file['file']?></a></td>
			<?else:?>
			<a href="#" onclick="return_path('<?=request('path','').$file['file']?>','<?=request('conf','')?>','<?=request('zone','')?>','1');"><?=$file['caption'] ? $file['caption'] : $file['file']?></a></td>			
			<?endif;?>
							<td align="center"><?=($file['filesize'])? $file['filesize']: '&nbsp;'?></td>
							<td align="center"><?=$file['chmod']?></td>
						</tr>
						<?endforeach;?>
					</table>
					
				</div>

			</TD>
		</TR>
	</TABLE>
</div>