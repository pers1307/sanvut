<?php
header('Content-type: text/html; charset=utf-8');
global $adminmenu, $urls, $DB, $Forms, $_MODULES;

$tmp_config = ($_MODULES->by_dir($adminmenu->params[0]));
include($_SERVER['DOCUMENT_ROOT'].'/MODULES/'.$tmp_config['module_name'].'/config.php');
unset($tmp_config);

switch($adminmenu->params[1]){ 

	case'phpinfo':
	
		if ( $auth_isROOT )
			{
			
			echo template('phpinfo',array());
			
			}
		
	break;

	case'serv_info':
		echo template('serv_info',array());
	break;

	case 'import_csv':
		$module_name = (isset($adminmenu->params[2])) ? $adminmenu->params[2] : '';
		$db_name = (isset($adminmenu->params[3])) ? $adminmenu->params[3] : '';
		$path_id = (isset($adminmenu->params[4])) ? (int)$adminmenu->params[4] : 0;
		
		
		
		if (!isset($_FILES['csv']) && $module_name!=""){
			echo '
				<h3>Импорт данных</h3>
				<div style="padding: 10px;">			
				<form action="#" method="POST" id="import_csv" enctype="multipart/form-data">
				<div><b>Файл CSV</b></div>
				<input type="file" value="" name="csv">
				</form>
				</div>
				
				<SCRIPT type="text/javascript">
				dialogAddButtons(
					new Array(
						new Array("doLoad(getObj(\'import_csv\'),\'/'.ROOT_PLACE.'/admin/import_csv/'.$module_name.'/'.$db_name.'/'.(int)$path_id.'/\', \'\'); closeDialog();","Импортировать")
					)
				);
				</SCRIPT>			
			';
		} elseif (isset($_FILES['csv']) && $module_name!="") {
			$file = $_FILES['csv'];

			if (is_file($file['tmp_name'])){
				$data = file($file['tmp_name']);
				foreach ($data as $num => $d)
					$data[$num] = trim($d);
					
					
					
				$CUR_CONFIG = $CONFIG;
				include(DOC_ROOT.'MODULES/'.$module_name.'/config.php');
				$mod_config = $CONFIG;
				$CONFIG = $CUR_CONFIG;
				unset($CUR_CONFIG);
				
				
				
				$main_table_config=0;
				$table_name='';
				$ok = false;
				foreach ($mod_config as $mparams => $data_config)
					{
					if ($mparams == 'tables')
						{
						$ok = true;
						break;
						}
					}
					
				if ($ok)
					{
					foreach ($data_config as $tparams => $table_params_config)
						{
						if (isset($table_params_config['db_name']) && $table_params_config['db_name']==$db_name)
							{
							$table_name = $tparams;
							$main_table_config = $table_params_config;
							
							break;
							}
						}
						
					
					
					$DB->execute('TRUNCATE '.PRFX.$db_name.'');
				
					$sql=array();
					foreach ($data as $dnum => $line)
						{
						$line_values = explode(';',$line);
						
						$num_f=0;
						
						$asql=array();
						foreach ($main_table_config['config'] as $field => $field_config)
							{
							if (isset($line_values[$num_f]) && $line_values[$num_f]!="")
								$asql[] = '`'.$field.'` = "'.$DB->pre(htmlspecialchars($line_values[$num_f],ENT_QUOTES)).'"';
								
							$num_f++;	
							}
							
						$DB->execute('INSERT INTO '.PRFX.$db_name.' SET path_id='.(int)$path_id.', '.implode(', ',$asql));
						}
					}				
				}
		} else {
			debug('error import cvs',0,1,'a+');
		}
	break;
	
	case 'clear_linkpath':
		$module = (isset($adminmenu->params[2])) ? $adminmenu->params[2] : '';
		$output = (isset($adminmenu->params[3])) ? $adminmenu->params[3] : '';

		$_SESSION['link_path'] = array();
		auth_StoreSession();

		$fastcall = $_MODULES->info[$module]['fastcall'];

//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$_RESULT=array('content'=>'&nbsp;', 'fastcall'=>$fastcall, 'output'=>$output);
	break;
	
	/* Каскадно перемещение вперед по модулям */
	case 'link2module':
		{
		$to_module = (isset($adminmenu->params[2])) ? $adminmenu->params[2] : '';
		$from_module = (isset($adminmenu->params[3])) ? $adminmenu->params[3] : '';
		$from_id = (isset($adminmenu->params[4])) ? $adminmenu->params[4] : '';
		$to_key = (isset($adminmenu->params[5])) ? $adminmenu->params[5] : '';
		$from_key = (isset($adminmenu->params[6])) ? $adminmenu->params[6] : '';
		$path_id= (isset($adminmenu->params[7])) ? $adminmenu->params[7] : '';
		$from_output = (isset($adminmenu->params[8])) ? $adminmenu->params[8] : '';

		$imod = $_MODULES->info[$to_module];

		$link_path = isset($_SESSION['link_path']) ? $_SESSION['link_path'] : array();

//		$to_output = str_Replace($from_module,$to_module,$from_output);
		$to_output = substr($to_module,0,strlen($from_module)) != $from_module ? str_replace($from_module,$to_module,$from_output) : $from_output;
		
		$array = array(
				'inmodule'		=> $to_module,
				'back2module'	=> $from_module,
				'from_id'		=> $from_id,
				'from_key'		=> $from_key,
				'to_key'		=> $to_key,
				'path_id'		=> $path_id,
				'form_output'	=> $from_output,
				'to_output'		=> $to_output,
				);	 
		
		$link_path[] = $array;														  
		$_SESSION['link_path'] = $link_path;

		auth_StoreSession();	

//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");  

		$_RESULT=array('content'=>'&nbsp;', 'fromModule'=>$from_module, 'toModule'=>$to_module, 'fastcall'=>$imod['fastcall'].(int)$path_id.'/');
		}
	break;
	
	/* Каскадно перемещение назад по открытым модулям */
	case 'back2module':
		$to_module = (isset($adminmenu->params[2])) ? $adminmenu->params[2] : '';
		$from_module = (isset($adminmenu->params[3])) ? $adminmenu->params[3] : '';

		$array = $_SESSION['link_path'];
		$link_path = $array[sizeof($array)-1]; 
		unset($array[sizeof($array)-1]);	   
		$_SESSION['link_path'] = $array;

		auth_StoreSession();

		$imod = $_MODULES->info[$to_module];
		
		$path_id='';
		if (isset($link_path['path_id'])) $path_id = (int)$link_path['path_id'].'/';

//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");

		$_RESULT=array('content'=>'&nbsp;', 'fromModule'=>$from_module, 'toModule'=>$to_module, 'fastcall'=>$imod['fastcall'].$path_id);
	break;


	case 'passwd':
		echo template('passwd',array());
	break;

	
	/** Какие либо действия отдельного конфигуратора через главный модуль Админ */
	case 'mod_action':
		$form_item = (isset($adminmenu->params[2])) ? $adminmenu->params[2] : '';
		$params = $adminmenu->params;
		unset($params[0]);
		unset($params[1]);

		$tmp_params=array();
		foreach($params as $num => $par)$tmp_params[]=$par;
		$params = $tmp_params;

		$file = $_SERVER['DOCUMENT_ROOT'].'/CORE/forms/'.$form_item.'/action.php';

		if ($form_item!="" && is_file($file)){
			require_once($file);
		}
	break;

	/**
	* Редактирование настроек страницы
	*/
	case 'page_options':
		$path_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : 0;

		$vars=array();							
		$vars['info'] = ($path_id !== false) ? $urls->tree[$urls->ids[$path_id]] : false;
		$vars['path_id'] = $path_id;
		$vars['title_menu'] = $urls->tree[$urls->ids[$path_id]]['title_menu'];

		/* Конфиг для редактирования TDK страницы */
		$config = array(
			'path_id'=>array(
				'value' => $path_id,
				'type' => 'hidden',
				),
				
			'title_menu'=>array(
				'caption' => 'Заголовок в меню',
				'value' => html_entity_decode($vars['info']['title_menu']),
				'type' => 'string',
				),
				
			'header'=>array(
				'caption' => 'Заголовок страницы',
				'value' => html_entity_decode($vars['info']['header']),
				'type' => 'string',
			),
				
			'title_page'=>array(
				'caption' => 'Тег TITLE',
				'value' => html_entity_decode($vars['info']['title_page']),
				'type' => 'string',
			),

			'description'=>	array(
				'caption' => 'Описание страницы (DESCRIPTION)',
				'value' => $vars['info']['meta_description'],
				'type' => 'string',
				),

			'keywords'=> array(
				'caption' => 'Ключевые слова страницы (KEYWORDS)',
				'value' => $vars['info']['meta_keywords'],
				'type' => 'string',
				),

			);
			
		if($path_id==1)unset($config['title_menu']);
		
		foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data){
			$config[$field] = $data;
			$config[$field]['value'] = $vars['info'][$field];
		} 
		$vars['form_tdk'] =  write_tdk_Form($Forms->make($config,'',FORM_ID_TDK_FORM));


		if ($auth_isROOT){
			/* Верстка страницы */
			$files = array();	 		
			$files = GetFilesFromDir(DOC_ROOT.'DESIGN/',false);
			foreach ($files as $key => $info)
				if (in_array($key,array('admin.php','auth.php','ajax.php','exit.php','captcha.php','404.php','403.php', 'json.php')))
					unset($files[$key]);

			$config_template=array(
					'html'=>array(
						'caption' => 'Верстка страницы',
						'value' => $vars['info']['main_template']!="" ? $vars['info']['main_template'] : 'inner.php',
						'values' => $files,
						'type' => 'select',
					),

					'path_id'=>array(
						'value' => $path_id,
						'type' => 'hidden',
					)
				);		
			$vars['template_form'] = $Forms->make($config_template, '', FORM_ID_TEMPLATE);
		} else {
			$vars['template_form']='';
		}

		if ($auth_isROOT){
			/* Сохранение блоков как шаблон (зона контент исключается) */	 
			$config_tpl=array(
				'caption' => array(
					'caption' => 'Название шаблона',
					'value' => '',
					'type' => 'string',
				),
				'path_id' => array(
					'value' => $path_id,
					'type' => 'hidden',
				) 						
			);	   					
			$vars['tpl_form'] = $Forms->make($config_tpl,'',FORM_ID_SAVETPL);
		} else {
			$vars['tpl_form'] = '';
		}


		if ($auth_isROOT){	
			/* Конфиг для редактирования SYSNAME страницы */
			$path_cur_page = $DB->getOne('SELECT path FROM mp_www WHERE path_id="'.(int)$path_id.'"');
			if ($path_cur_page!=""){
				$path_cur_page = trim($path_cur_page,'/');
				$path_cur_page = str_Replace("//","/",$path_cur_page);
				$path_cur_page = end(explode('/',$path_cur_page));
			}
			$config_pagepath=array(
				'page_path' => array(
					'caption' => 'Системное имя страницы',
					'value' => $path_cur_page,
					'height'=>100,
					'type' => 'string',
					),
				'path_id' => array(
					'value' => $path_id,
					'type' => 'hidden',
				)						
			);
			$vars['pagepath_form'] = $Forms->make($config_pagepath, '', FORM_ID_PAGEPATH_FORM);
		} else {
			/* Конфиг для редактирования SYSNAME страницы */
			$path_cur_page = $DB->getOne('SELECT path FROM mp_www WHERE path_id="'.(int)$path_id.'"');
			$config_pagepath=array(
				'page_path' => array(
                    'caption' => 'Адрес страницы',
                    'value' => '/'.$path_cur_page.'/',
                    'height'=>100,
                    'type' => 'string',
                    'readonly'=>true,
                ),						
			);
			$vars['pagepath_form'] = $Forms->make($config_pagepath, '', FORM_ID_PAGEPATH_FORM);
		}

		/* Конфиг для редактирования RDR страницы */
		$vac_out=generateTreeSelectArray();		  
		$config_rdr=array(
			'rdr_path_id' => array(
				'caption' => 'Перевод на страницу сайта',
				'value' => $vars['info']['rdr_path_id'],
				'values' => $vac_out,
				'height'=>100,
				'type' => 'select',
			),

			'rdr_url' => array(
				'caption' => 'Или на другой URL',
				'value' => $vars['info']['rdr_url'],
				'height'=>100,
				'type' => 'string',
				),

			'path_id' => array(
				'value' => $path_id,
				'type' => 'hidden',
				)			
			);					  
		$vars['redirect_form'] = $Forms->make($config_rdr, '', FORM_ID_REDIRECT);

		echo template('page_options',$vars);
	break;



	/** Показ главной страницы функционала по копированию разделов*/
	case 'www_pages':
		$vars=array();
		
		$vars['tree_from'] = getTypesNode_templates(true, 'from');
		$vars['tree_to'] = getTypesNode_templates(true, 'to');

		echo template('www_pages',$vars);
	break;



	/**
	 * Визивик на весь экран
	 */
	case 'fullscreen_w':
		$sysname = urldecode((isset($adminmenu->params[2])) ? $adminmenu->params[2] : "");
		$height = (isset($adminmenu->params[3])) ? (int)$adminmenu->params[3] : '200';

		$vars=array();
		$vars['sysname']=$sysname;
		$vars['height']=$height;

		echo template('fullscreen_w',$vars);
		
	break;

	case 'zone_wysiwyg_message':

		$zone = urldecode((isset($adminmenu->params[2])) ? $adminmenu->params[2] : "");
		$path_id = urldecode((isset($adminmenu->params[3])) ? $adminmenu->params[3] : 0);

		$vars=array();
		$vars['zone']=$zone;
		$vars['path_id']=$path_id;
		$vars['height']='100%';

		$content = $DB->getOne('SELECT content FROM `'.PRFX.'zones_content` WHERE zone_id="'.$zone.'" AND path_id="'.(int)$path_id.'"');

		$vars['content'] = html_entity_decode($content,ENT_QUOTES);

		echo template('zone_wysiwyg',$vars);
	break;

	case 'zone_wysiwyg_save2':
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$_RESULT = array('content' => 'Сохранено');
		die();
	break;
	
	case 'zone_wysiwyg_save':
		debug($_REQUEST,0,1);
//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");

		$zone = urldecode((isset($adminmenu->params[2])) ? $adminmenu->params[2] : "");
		$path_id = urldecode((isset($adminmenu->params[3])) ? $adminmenu->params[3] : "");

		$id = $DB->getOne('SELECT id FROM `'.PRFX.'zones_content` WHERE  zone_id="'.$zone.'" AND path_id="'.$path_id.'"');

		if($_REQUEST['content'.$zone]=='<br type="_moz" />')$_REQUEST['content'.$zone]='';
		$allowed = array('b' => array(),
                 'em' => array( 'class'=>1,'style'=>1,),
                 'i' => array( 'class'=>1,'style'=>1,),
                 'a' => array( 'class'=>1,'style'=>1,'href' => 1, 'title' => 1, 'alt'=>1),
                 'p' => array( 'class'=>1,'style'=>1,'align' => 1),
                 'br' => array( 'class'=>1,'style'=>1,),
                 'h1' => array( 'class'=>1,'style'=>1,),
                 'h2' => array( 'class'=>1,'style'=>1,),
                 'h3' => array( 'class'=>1,'style'=>1,),
                 'h4' => array( 'class'=>1,'style'=>1,),
                 'h5' => array( 'class'=>1,'style'=>1,),
                 'h6' => array( 'class'=>1,'style'=>1,),
                 'h7' => array( 'class'=>1,'style'=>1,),
                 'iframe' => array( 'class'=>1,'style'=>1,
                 	'width'=>1,
                 	'height'=>1,
                 	'frameborder'=>1,
                 	'scrolling'=>1,
                 	'marginheight'=>1,
                 	'marginwidth'=>1,
                 	'src'=>1,
                 ),
                 'small' => array( 'class'=>1,'style'=>1,),
                 'strong' => array( 'class'=>1,'style'=>1,),
                 'img' => array( 'class'=>1,'style'=>1,'title' => 1, 'alt'=>1, 'border'=>1,'width'=>1,'height'=>1,'src'=>1,'align'=>1),
                 'table' => array( 'class'=>1,'style'=>1,'title' => 1, 'alt'=>1, 'border'=>1,'width'=>1,'height'=>1,'cellpadding'=>1,'cellspacing'=>1),
                 'thead' => array( 'class'=>1,'style'=>1,'title' => 1, 'alt'=>1, 'border'=>1,'width'=>1,'height'=>1,),
                 'tbody' => array( 'class'=>1,'style'=>1,'title' => 1, 'alt'=>1, 'border'=>1,'width'=>1,'height'=>1,),
                 'tr' => array('style'=>1,'title' => 1, 'alt'=>1, 'border'=>1,'width'=>1,'height'=>1, 'class'=>1,'colspan'=>1,'rowspan'=>1,'valign'=>1,'align'=>1,),
                 'td' => array('style'=>1,'title' => 1, 'alt'=>1, 'border'=>1,'width'=>1,'height'=>1, 'class'=>1,'colspan'=>1,'rowspan'=>1,'valign'=>1,'align'=>1,),
                 'div' => array('style'=>1,'class'=>1, 'width'=>1, 'align'=>1),
                 'span' => array('style'=>1,'class'=>1, 'width'=>1, 'align'=>1),
				 'ul' => array( 'class'=>1,'style'=>1, 'width'=>1, 'align'=>1),
				 'li' => array( 'class'=>1,'style'=>1, 'width'=>1, 'align'=>1),
				 'ol' => array( 'class'=>1,'style'=>1, 'width'=>1, 'align'=>1),
				 'u' => array( 'class'=>1,'style'=>1, 'width'=>1, 'align'=>1),
				 'strike' => array( 'class'=>1,'style'=>1, 'width'=>1, 'align'=>1),
                 );

//		$_REQUEST['content'.$zone]=cleanTextFromWord($_REQUEST['content'.$zone]);
//		$_REQUEST['content'.$zone]=kses($_REQUEST['content'.$zone],$allowed);

		if ($id>0)
			$DB->execute('UPDATE `'.PRFX.'zones_content` SET content="'.htmlspecialchars($_REQUEST['content'.$zone], ENT_QUOTES).'" WHERE id="'.$id.'"');
		else
			$DB->execute('INSERT INTO `'.PRFX.'zones_content` SET content="'.htmlspecialchars($_REQUEST['content'.$zone], ENT_QUOTES).'", zone_id="'.$zone.'", path_id="'.$path_id.'"');

		//$_RESULT = array('content' => ' Текст сохранен');
		
		$res_text=strip_tags($_REQUEST['content'.$zone],'<img><p><h1><h2><h3><h4><h5><h6><span><div><br><iframe><b><i><small><strong><table><tr><td><tbody><thead><ol><ul><li>');
		$_RESULT = array('content' => $res_text);
		
		
	break;


	/**
	 * Запросили список зон см. Управление зонами
	 */
	case 'zones':
		ob_clean();

		$zones = $DB->getAll('SELECT * FROM `'.PRFX.'zones`  WHERE is_admin=0  ORDER BY id');
				
		$vars['zones'] = $zones;
		echo template('zones',$vars);
	break;

	/**
	 * Редактирование зон сайта
	 */
	case 'edit_zone':
		ob_clean();
		
		$config['caption'] = array(
			'caption'=> 'Название зоны',
			'value'=> '',
			'type'=> 'string',
		);

		$config['value'] = array(
			'caption'=> 'Системное имя',
			'value'=> '',
			'type'=> 'string',
		);

		$config['for_search'] = array(
			'caption'=> 'Для поиска',
			'value'=> '',
			'type'=> 'boolean',
		);	
		
		$config['search_links'] = array(
			'caption'=> 'Блок ссылок (для поиска)',
			'value'=> '',
			'type'=> 'boolean',
		);		

		$zone_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : 0;

		
		
		if ($zone_id >0){
			$zones = $DB->getRow('SELECT * FROM `'.PRFX.'zones` WHERE id="'.(int)$zone_id.'" ORDER BY id'); 

			$config['caption']['value'] = $zones['caption'];
			$config['value']['value'] = $zones['value'];
			$config['for_search']['value'] = isset($zones['for_search']) ? $zones['for_search'] : 0;
			$config['search_links']['value'] = isset($zones['search_links']) ? $zones['search_links'] : 0;
		}
		
		$vars['_FORM_'] = $Forms->make($config);
		$vars['zone_id'] = $zone_id;
		
		echo template('add_zone',$vars);
	break;
	
	/**
	 * Сохранение зоны
	 */
	case 'save_zone':
		ob_clean();
//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$zone_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;
		
		if ($zone_id >0){
			$DB->execute('UPDATE `'.PRFX.'zones` SET caption="'.$_REQUEST['conf'][1]['caption'].'", value="'.$_REQUEST['conf'][1]['value'].'", for_search="'.(int)$_REQUEST['conf'][1]['for_search'].'", search_links="'.(int)$_REQUEST['conf'][1]['search_links'].'" WHERE id="'.(int)$zone_id.'"');
		} else {
			$DB->execute('INSERT INTO `'.PRFX.'zones` SET caption="'.$_REQUEST['conf'][1]['caption'].'", value="'.$_REQUEST['conf'][1]['value'].'", for_search="'.(int)$_REQUEST['conf'][1]['for_search'].'", search_links="'.(int)$_REQUEST['conf'][1]['search_links'].'"');
		}
		
		$zones = $DB->getAll('SELECT * FROM `'.PRFX.'zones`  WHERE is_admin=0  ORDER BY id');

		$vars=array('zones'=>$zones); 
		$_RESULT = array('content' => template('zones',$vars));
	break;
	

	/**
	 * Удаление зоны
	 */
	case 'delete_zone':
		$zone_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;
		
		if ($zone_id>0)
			{
			$DB->execute('DELETE FROM `'.PRFX.'zones` WHERE id="'.$zone_id.'"');
			}

//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");

		$vars=array();
		$vars['zones'] = $DB->getAll('SELECT * FROM `'.PRFX.'zones`  WHERE is_admin=0  ORDER BY id');

		$_RESULT = array('content' => template('zones',$vars));		
	break;




#######################################################################################################################


	

	/**
	 * Запросили список типов страниц см. Управление типами страниц
	 */
	case 'www_types':
		ob_clean();
		
		$vars['www_types'] = $DB->getAll('SELECT * FROM `'.PRFX.'www_types` WHERE `id`!=1 ORDER BY `sortir`');
		
		echo template('www_types',$vars);
	break;	

	/**
	 * Редактирование зон сайта
	 */
	case 'edit_www_type':
		ob_clean();
		
		$config['title'] = array(
			'caption'=> 'Название типа',
			'value'=> '',
			'type'=> 'string',
		);

		$config['flag_addroot'] = array(
			'caption'=> 'Возможность добавлять в корень',
			'value'=> '',
			'type'=> 'boolean',
		);

		$config['flag_addsub'] = array(
			'caption'=> 'Возможность добавлять под-разделы',
			'value'=> '',
			'type'=> 'boolean',
		);
		
		$config['sortir'] = array(
			'caption'=> 'Порядок вывода',
			'value'=> 0,
			'type'=> 'string',
		);

		$www_type_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;
		if ($www_type_id>0){
			$www_types = $DB->getRow('SELECT * FROM `'.PRFX.'www_types` WHERE `id`='.(int)$www_type_id.'');
		
			$config['title']['value'] = $www_types['title'];
			$config['flag_addroot']['value'] = (int)$www_types['flag_addroot'];
			$config['flag_addsub']['value'] = (int)$www_types['flag_addsub'];
			$config['sortir']['value'] = (int)$www_types['sortir'];
		}
		
		$vars['_FORM_'] = $Forms->make($config);
		$vars['www_type_id'] = $www_type_id;
		
		echo template('add_www_type',$vars);
	break;

	/**
	 * Сохранение типа
	 */
	case 'save_www_type':
		ob_clean();
//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$www_type_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;
		
		$title=$_REQUEST['conf'][1]['title'];
		$flag_addroot=$_REQUEST['conf'][1]['flag_addroot'];
		$flag_addsub=$_REQUEST['conf'][1]['flag_addsub'];
		$sortir=$_REQUEST['conf'][1]['sortir'];

		if ($www_type_id>0){
			$sql = "UPDATE `".PRFX."www_types` SET `title` = '".$DB->pre($title)."', `flag_addroot` = '".$DB->pre($flag_addroot)."', `flag_addsub` = '".$DB->pre($flag_addsub)."', `sortir` = '".$DB->pre($sortir)."' WHERE `id`='".$www_type_id."'";
		} else {
			$sql = "INSERT `".PRFX."www_types` SET `title` = '".$DB->pre($title)."', `flag_addroot` = '".$DB->pre($flag_addroot)."', `flag_addsub` = '".$DB->pre($flag_addsub)."', `sortir` = '".$DB->pre($sortir)."'";
		}


		$DB->execute($sql);
		
		$vars['www_types'] = $DB->getAll('SELECT * FROM `'.PRFX.'www_types` WHERE `id`!=1 ORDER BY `sortir`');
		
		$_RESULT['content'] = template('www_types',$vars);
	break;

	/**
	 * Удаление типа
	 */
	case 'delete_www_type':
		$www_type_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;

		if ($www_type_id>0){
			$DB->execute("DELETE FROM `".PRFX."www_types` WHERE `id`=".(int)$www_type_id);
		}

//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$vars['www_types'] = $DB->getAll('SELECT * FROM `'.PRFX.'www_types` WHERE `id`!=1 ORDER BY `sortir`');
		$_RESULT['content'] = template('www_types',$vars);
	break;



#######################################################################################################################
#######################################################################################################################


	

	/**
	 * Запросили список типов страниц см. Управление типами страниц
	 */
	case 'modules_types':
		ob_clean();
		
		$vars['modules_types'] = $DB->getAll('SELECT * FROM `'.PRFX.'modules_types` ORDER BY `sortir`');
		
		echo template('modules_types',$vars);
	break;	

	/**
	 * Редактирование зон сайта
	 */
	case 'edit_modules_type':
		ob_clean();
		
		$config['title'] = array(
			'caption'=> 'Название типа',
			'value'=> '',
			'type'=> 'string',
		);

		$config['sortir'] = array(
			'caption'=> 'Порядок вывода',
			'value'=> 0,
			'type'=> 'string',
		);

		$modules_type_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;
		if ($modules_type_id>0){
			$modules_types = $DB->getRow('SELECT * FROM `'.PRFX.'modules_types` WHERE `id`='.(int)$modules_type_id.'');
		
			$config['title']['value'] = $modules_types['title'];
			$config['sortir']['value'] = (int)$modules_types['sortir'];
		}
		
		$vars['_FORM_'] = $Forms->make($config);
		$vars['modules_type_id'] = $modules_type_id;
		
		echo template('add_modules_type',$vars);
	break;

	/**
	 * Сохранение типа
	 */
	case 'save_modules_type':
		ob_clean();
		//UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$modules_type_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;
		
		$title=$_REQUEST['conf'][1]['title'];
		$sortir=$_REQUEST['conf'][1]['sortir'];

		if ($modules_type_id>0){
			$sql = "UPDATE `".PRFX."modules_types` SET `title` = '".$DB->pre($title)."', `sortir` = '".$DB->pre($sortir)."' WHERE `id`='".$modules_type_id."'";
		} else {
			$sql = "INSERT `".PRFX."modules_types` SET `title` = '".$DB->pre($title)."', `sortir` = '".$DB->pre($sortir)."'";
		}


		$DB->execute($sql);
		
		$vars['modules_types'] = $DB->getAll('SELECT * FROM `'.PRFX.'modules_types` ORDER BY `sortir`');
		
		$_RESULT['content'] = template('modules_types',$vars);
	break;

	/**
	 * Удаление типа
	 */
	case 'delete_modules_type':
		$modules_type_id = (isset($adminmenu->params[2])) ? (int)$adminmenu->params[2] : -1;

		if ($modules_type_id>0){
			$DB->execute("DELETE FROM `".PRFX."modules_types` WHERE `id`=".(int)$modules_type_id);
		}

//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$vars['modules_types'] = $DB->getAll('SELECT * FROM `'.PRFX.'modules_types` ORDER BY `sortir`');
		$_RESULT['content'] = template('modules_types',$vars);
	break;



#######################################################################################################################


	/**
	 * Запросили список типов страниц см. Управление типами страниц
	 */
	case 'templates':
		ob_clean();
		
		$vars=array();
		
		$vars['tree'] = getTypesNode_templates();
		$vars['templates'] = $DB->getAll('SELECT * FROM `'.PRFX.'tpl` ORDER BY `id`');
		$vars['zones'] = $DB->getAll('SELECT * FROM `'.PRFX.'zones`  WHERE is_admin=0  ORDER BY id');

		$verstka = GetFilesFromDir(DOC_ROOT.'DESIGN/',false);
		foreach ($verstka as $key => $info)
			{
			if ($key == 'admin.php' || $key == '403.php' || $key == '404.php' || $key == 'auth.php')
				unset($verstka[$key]);
			}
		$vars['verstkas']=$verstka;


		$blocks = GetBlocks(DES_DIR);		
		foreach ($blocks as $key => $info)
			{
			$vars['result'][] = $info;
			}
		@asort($vars['result']);	
		

		$temp_vars=manage_modules();
		$vars=array_merge($temp_vars,$vars);


		
		echo template('templates',$vars);

	break;	


#######################################################################################################################

	/**
	 * Управление шаблонами
	 */

	case 'manage_tpls':
		{			
		$config_tpl = array();
		$config_tpl['caption'] = array(
			'caption'=> 'Название шаблона',
			'value'=> '',
			'type'=> 'string',
		);
		
		if (isset($adminmenu->params[2]))
			{
			switch($adminmenu->params[2])
				{
				case 'edit':
					{
					$id = isset($adminmenu->params[3]) && $adminmenu->params[3]>0 ? (int)$adminmenu->params[3] : 0;

					$vars=array();
					$vars['DB'] = $DB;
					$vars['config'] = $config_tpl;
					$vars['Forms'] = $Forms;
					$vars['id']=$id; 

					if (isset($_REQUEST['id']) && $_REQUEST['id']>0)
						{
//						UseModule('ajax');
						$JsHttpRequest = new JsHttpRequest("utf-8");

						$config_tpl = $Forms->save($config_tpl);
						$sql_=array();
						foreach($config_tpl as $field => $info)
							{
							$sql_[] = "`".$field."` = '".$DB->pre($info['value'])."'";
							}

						$DB->execute('UPDATE `'.PRFX.'tpl` SET '.implode(',',$sql_).' WHERE id="'.(int)$_REQUEST['id'].'"');	  
						
						$_RESULT=array('content'=> template('manage_tpls',$vars));
						}
					else
						{														 
						$config_tpl['caption']['value'] = $DB->getOne('SELECT caption FROM `'.PRFX.'tpl` WHERE id="'.(int)$id.'"');
						$vars['_FORM_'] = $Forms->make($config_tpl);

						echo template('add_manage_tpls',$vars);
						}

					}
				break;

				case 'delete':
					{
//					UseModule('ajax');
					$JsHttpRequest = new JsHttpRequest("utf-8");

					$id = isset($adminmenu->params[3]) && $adminmenu->params[3]>0 ? (int)$adminmenu->params[3] : 0;

					if ($id>0)
						{
						$DB->execute('DELETE FROM `'.PRFX.'tpl` WHERE id="'.(int)$id.'"');
						}

					$vars=array();
					$vars['DB'] = $DB;

					$_RESULT=array('content'=>template('manage_tpls',$vars));
					}
				break;
				}
			}
		else
			{
			$vars=array();
			$vars['DB'] = $DB;

			echo template('manage_tpls',$vars);
			}


		}
	break;

#######################################################################################################################

	/**
	 * Изменение видимости раздела
	 */
	case 'toggle_vis':
		ob_clean();
//		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");

		$path_id = (int)$adminmenu->params[2];
		
		if (isset($urls->tree[$urls->ids[$path_id]])){
			$visible = $urls->tree[$urls->ids[$path_id]]['visible'] == 0 ? 1 : 0;
			$sql = "UPDATE `".PRFX."www` SET `visible` = '".(int)$visible."' WHERE `path_id` = ".(int)$path_id;
			$DB->execute($sql);
		}

		$content='<img border="0" src="/DESIGN/ADMIN/images/'.((int)$visible==0 ? 'invis' : 'vis').'.gif" alt=""/>';
		
		$_RESULT['content'] = array($content);
	break;
	
	/**
	 * Отдаем содержание конфигуратора exporer и далее все данные отдает модуль Ajax
	 */
	case 'explorer':
		ob_clean();
		
		$path = str_replace('//', '/',EndSlash(request('path','')));
		$basepath = str_replace('//', '/',EndSlash(request('basepath','')));

		//if (strpos($path,$basepath) === false) die('Попытка взлома');	

		/** Создадим ссыку для кнопки назад*/
		$backlink = explode('/',trim($path,'/'));
		if (sizeof($backlink)>1)
		{
			$backlink = array_reverse($backlink);
			unset($backlink[key($backlink)]);
			$backlink = array_reverse($backlink);
		}
		$backlink = '/'.ROOT_PLACE.'/ajax/explorer/?path='.EndSlash('/'.implode('/',$backlink)).'&basepath='.$basepath.'&path_id='.request('path_id','').'&zone='.request('zone','').'&conf='.request('conf','');
				
		$vars['path'] = $path;
		$vars['folders']=cmsGetFoldersAndFiles(str_replace('//', '/', DOC_ROOT.$vars['path']));
		$vars['backlink'] = $backlink;
		 
		
		die(template('explorer',$vars)); 

	break;
	
	/**
	 * Работа со структурой сайта
	 */
	case 'structure':
		/** Проверка на вызов дочернего пути */
		if (isset($adminmenu->params[2])){

			switch($adminmenu->params[2]){
				/**
				 * Создание нового пути
				 */
				case 'add_path':
				    ob_clean();
				    $data_path_id = str_replace('node','',$adminmenu->params[3]);
					$data_path_id = explode('_',$data_path_id);
					list($path_id, $type_id) = $data_path_id;


					$sql='SELECT `id`, `title` FROM `'.PRFX.'www_types` WHERE `id`='.(int)$type_id;
					$cur_type=$DB->getRow($sql);

					$config['static_www_type']['caption'] = 'Тип страницы:';
					$config['static_www_type']['value'] = $cur_type['title'];
					$config['static_www_type']['type'] = 'static';

					$config['sysname']['caption'] = 'Системное имя:';
					$config['sysname']['value'] = '';
					$config['sysname']['type'] = 'string';
					
					$config['header']['caption'] = 'Заголовок страницы:';
					$config['header']['value'] = '';
					$config['header']['type'] = 'string';
					
					$config['title_menu']['caption'] = 'Заголовок в меню:';
					$config['title_menu']['value'] = '';
					$config['title_menu']['type'] = 'string';
					
					$config['title']['caption'] = 'Тег TITLE:';
					$config['title']['value'] = '';
					$config['title']['type'] = 'string';

					$tpl = array();
					$sql = "SELECT `id`,`caption` FROM `".PRFX."tpl`";									

					foreach( $DB->getAll( $sql ) as $info )
						$tpl[ $info['id'] ] = $info['caption'];
										
					if( count( $tpl ) > 1 )
					{
						$config['html'] = array(
							'caption' => 'Шаблон:',
							'value' => 0,
							'values' => $tpl,
							'type' => 'select',
						);
					}
					else
					{
						$config['html'] = array(
							'value' => array_shift(array_keys($tpl)),
							'type' => 'hidden',
						);
					}
					
					$config['www_type']['value'] = (isset($type_id)) ? $type_id : 0;
					$config['www_type']['type']= 'hidden';	

					$config['root']['value'] = (isset($path_id)) ? $path_id : 0;
					$config['root']['type']= 'hidden';	


					foreach( $GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data )
					{
						$config[$field] = $data;
					}

					$vars['__FORM__'] = $Forms->make($config);

					die( template( 'add_path', $vars ) );
				break;
				
				/**
				 * Запросили редактирование структуры страницы выдаем все что можно
				 */
				case 'edit_path':
					ob_clean();
					global $_ZONES;
					
					$path_id = (isset($adminmenu->params[3])) ? $adminmenu->params[3] : false;
					$vars['info'] = ($path_id !== false) ? $urls->tree[$urls->ids[$path_id]] : false;
					$vars['path_id'] = $path_id;

					$_SESSION['link_path'] = array();
					auth_StoreSession();
					
					die(template('edit_path',$vars));
				break;
				

				/**
				 * Изменение титла страницы из дерева
				 */
				case 'rename':
						ob_clean();
						if((int)$adminmenu->params[3] && trim($adminmenu->params[4])){
							$id = (int)$adminmenu->params[3];
							$name = trim($adminmenu->params[4]);
							ob_clean();
							$inf = $DB->getRow("SELECT * FROM `".PRFX."www` WHERE `path_id`='".$id."'");

							if (sizeof($inf)){
								$DB->execute("UPDATE `".PRFX."www` SET `title_menu` = '".$DB->pre($name)."' WHERE `path_id`='".$inf['path_id']."'") or die("NOT OK");
								die("OK");
							}
							die("NOT OK");

						}
				break;
				



				/**
				 * Удаление прикрепленного блока/модуля из зоны страницы
				 */
				case 'delete_cont':
					$path_id = (int)$adminmenu->params[3];
					$info = ($path_id !== 0) ? $urls->tree[$urls->ids[$path_id]] : false;
					$zone = $adminmenu->params[4];
					$script = (int)$adminmenu->params[5];
					if (isset($info['config'][$zone][$script]))
					{
						unset($info['config'][$zone][$script]);
						$sql = "
							UPDATE `".PRFX."www` SET `config` = '".$DB->pre(serialize($info['config']))."'
							WHERE `path_id` = '".$DB->pre($path_id)."'
						";
						$DB->execute($sql);
					}
					go(current_path('/admin/structure/edit_path/'.$path_id.'/'));
				break;
				
				/**
				 * Добавление модуля на страницу
				 */
				case 'add_module':
					ob_clean();
					$vars=array();
					
					$path_id = (int)$adminmenu->params[3];
					$zone = $adminmenu->params[4];

					$modules=array();
					
					foreach ($_MODULES->info as $dir => $info){
						if (array_search($dir,$_MODULES->sys_modules)) continue;
						$modules[$dir] = $info['module_caption'];
					}
					
					$config['module'] = array(
						'caption' => 'Выберите модуль:',
						'value'=>'',
						'values'=> $modules,
						'type'=>'select',
						'size'=>15,
					);
					
					$config['get'] = array(
						'caption' => 'Параметры в виде GET запроса: ',
						'value'=>''
					);
					
					$config['path_id'] = array(
						'value'=>$path_id,
						'type'=>'hidden',
					);
					
					$config['zone'] = array(
						'value'=>$zone,
						'type'=>'hidden',
					);
					
					$vars['_FORM_'] = $Forms->make($config);

					die(template('add_module',$vars));
					
				break;
				
				/**
				 * Добавление блока на страницу
				 */
				case 'add_block':
					ob_clean();
					$vars=array();
					
					$vars['path_id'] = (int)$adminmenu->params[3];
					$vars['zone'] = $adminmenu->params[4];
					/**
					 * Пробегаемся по всем папкам в DESIGN и ищем папку BLOCKS с файлом config.php
					 */
					$blocks = GetBlocks(DES_DIR);
					
					foreach ($blocks as $key => $info)
					{
						if( $info['caption'] )
						{
							$name = ($info['module'] && isset($_MODULES->info[$info['module']])) ? $_MODULES->info[$info['module']]['module_caption'] : 'Другие';
							$vars['result'][$name][] = $info;
						}
					}
					@asort($vars['result']);
					die(template('add_block',$vars));
				break;

			}
			return;
		}
	break;
	
	/**
	 * Запросили управление модулями
	 */
	case 'modules':
		ob_clean();
        $vars=manage_modules();
		echo template('modules',$vars);
        die();
	break;
	
	case 'robotstxt':
		die(template('robotstxt',array()));
	break;

}
die();
?>