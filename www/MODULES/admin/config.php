<?php
$CONFIG = array(
	'version' => '1.0.0.0',
	'module_name' => 'Admin',
	'module_caption' => 'Дерево страниц сайта',

	'pages'=> array(
		'config'=> array (

//						'image' => array(
//						'caption' => 'File',
//						'value' => '',
//						'type' => 'file',
//						),
		)
	),
);

if(auth_isROOT())
{
	$CONFIG['pages']['config']['nodel'] = array(
							'caption' => 'Не удалять',
							'value' => '0',
							'type' => 'checkbox',
							'in_list' => 1,
							'filter' =>0,
							);
							
	$CONFIG['pages']['config']['noadd'] = array(
							'caption' => 'Не добавлять подразделы',
							'value' => '0',
							'type' => 'checkbox',
							'in_list' => 1,
							'filter' =>0,
							);
							
	$CONFIG['pages']['config']['noview'] = array(
							'caption' => 'Не менять видимость',
							'value' => '0',
							'type' => 'checkbox',
							'in_list' => 1,
							'filter' =>0,
							);														
							
	$CONFIG['pages']['config']['notfound'] = array(
							'caption' => 'Страница 404',
							'value' => '0',
							'type' => 'checkbox',
							'in_list' => 1,
							'filter' =>0,
							);
							
	$CONFIG['pages']['config']['getvars'] = array(
							'caption' => 'Разрешенные GET переменные',
							'value' => '',
							'type' => 'string',
							'in_list' => 0,
							'filter' =>0,
							);						
							
}

$GLOBALS['PAGE_CONFIG'] = $CONFIG;
?>