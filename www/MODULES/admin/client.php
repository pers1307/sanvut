<?php
function getPages($type_id=0, $show_hidden=false){
	global $DB;

	$wheres=array();
	if ($type_id>0) $wheres[]='`www_type` = "'.(int)$type_id.'"';
	if (!$show_hidden) $wheres[]='`visible` = 1';

	$wheres[]='`path_id`>10';

	$all = $DB->getAll('SELECT * FROM `'.PRFX.'www` '.(sizeof($wheres) ? 'WHERE '.implode(' AND ', $wheres).'' : '').'');

	return $all;
}

function generateMainPage(){
	global $auth_isROOT;
	return '
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="main_tree">
			<tr class="main_page">
				<td><a href="#" class="top" onclick="openPage(\'center\',\'edit_path1\',\'/'.ROOT_PLACE.'/admin/structure/edit_path/1/\',\'<a href=/ target=\_blank>Главная страница</a>\',\'Главная страница\'); return false;">Главная страница</a></td>
				
				'.($auth_isROOT || auth_Access('admin', 'edit') ? '
				
				<td width="57">
					<!-- Редактирование -->

					&nbsp;<img src="/DESIGN/ADMIN/images/edit_page.gif" width="14" height="14" border="0" onclick="displayMessage(\'/'.ROOT_PLACE.'/admin/page_options/1/\', 760, 600);" alt="Настройки страницы" title="Настройки страницы" style="cursor: pointer" />
				</td>

				' : '').'			
				
			</tr>				
		</table>
		';	
}	

function writeTreeMenu($type){
	global $added;

	if (is_array($type)){
		$out='';
	
		$out.='<div id="tree_'.$type['id'].'">';

		ob_start();
		writeContentTree($type,1);
		$s = ob_Get_contents();
		ob_end_Clean();

		$out.= $s;

		$out.='</div>';
	
		return $out;
	} else {
		return '';
	}
}



function getTypesNode(){
	global $DB, $added, $auth_isROOT;
	$out='';

	$types=$DB->getAll('SELECT * FROM `'.PRFX.'www_types` WHERE `id`!=1 ORDER BY `sortir` ASC');
	foreach($types as $type)
		{
		if (!$auth_isROOT && $type['id']==1) continue;
		$out.= '
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="main_tree">
				<tr class="main">
					<td class="www_type">'.$type['title'].'</td>
					<td class="www_type_act">'.($type['flag_addroot']==1 && ($auth_isROOT || auth_Access('admin', 'add')) ? '<a href="#" onclick="displayMessage(\'/'.ROOT_PLACE.'/admin/structure/add_path/node1_'.$type['id'].'/\',\'400\',\'360\');"><img src="/DESIGN/ADMIN/images/new_page.gif" width="14" height="14" border="0"/></a>' : '&nbsp;').'</td>
				</tr>
			</table>
			';

		$out.=writeTreeMenu($type);
		}
	
	return $out;
}




//------------------------------------------------------------------------------------------------------------------------------------------------------------

function writeContentTree($type, $pid=1, $level=-1){
	global $DB, $auth_isROOT;

	if (isset($_COOKIE['tree_sel_childs'])) $cookie = $_COOKIE['tree_sel_childs']; else $cookie='';

	$childs_array_cookie = explode(',',$cookie);

	$all = $DB->getAll('SELECT * FROM '.PRFX.'www WHERE www_type="'.$type['id'].'" AND parent="'.$pid.'" ORDER BY `order`');

	if (sizeof($all)){
		
		foreach ($all as $num => $info){

			//if (strpos($info['path'], 'mod')!==false) continue;

			$level++;
			$style = $level*15;

			$chcnt = $DB->getOne('SELECT count(*) FROM `'.PRFX.'www` WHERE parent="'.$info['path_id'].'"');

			$title_menu = htmlspecialchars($info['title_menu'],ENT_QUOTES);


			if ($auth_isROOT)
				$root_alt = ''.$title_menu.'&#13;&#10;ID:'.$info['path_id'].'&#13;&#10;Путь: /'.path($info['path_id']).'';
			else
				$root_alt = ''.$title_menu.'&#13;&#10;Путь: /'.path($info['path_id']).'';
			
			?>
			<table cellspacing="0" cellpadding="0" width="100%" class="main_tree" border="0">
				<tr id="tree_node<?=$info['path_id']?>" <?=($level>0 ? 'class="child_trs"' : '')?>>
					<td style="padding-left: <?=$style?>px;width:<?=($style+15);?>px;">
						<?if($chcnt>0){?>
							<a href="#" onclick="show_hide2('<?=$info['path_id']?>', 'tree')"><img src="/DESIGN/ADMIN/images/<?=(!in_array($info['path_id'], $childs_array_cookie) ? 'plus' : 'minus')?>.gif" hspace="5" width="10" height="10" border="0" id="tree_colapsik<?=$info['path_id']?>"></a>
						<?} else {?>
							<img src="/DESIGN/ADMIN/images/blank.gif" hspace="5" width="10" height="10">
						<?}?>
					</td>
					<td style="width:17px;" valign="middle">
						<?
						//---------видимость-------------------------------------------------------------------
						if(($auth_isROOT || auth_Access('admin', 'vis')) && !$info['noview']){?>
							<a href="#" onclick="doLoad('','/<?=ROOT_PLACE?>/admin/toggle_vis/<?=$info['path_id']?>/','vis_img_<?=$info['path_id']?>');">
								<span id="vis_img_<?=$info['path_id']?>">
									<img border="0" src="/DESIGN/ADMIN/images/<?=(isset($info['visible']) && $info['visible']==0 ? 'invis' : 'vis')?>.gif" alt=""/>
								</span>
							</a>&nbsp;
						<? }
						//---------видимость-------------------------------------------------------------------
						?>
					</td>
					<td align="left">
						<?
							if($title_menu=='')$title_menu='<small>---</small>';
						?>
						<a id="node<?=$info['path_id']?>" href="#" onclick="openPage('center','edit_path<?=$info['path_id']?>','/<?=current_path('/admin/structure/edit_path/'.$info['path_id'])?>','<?=addslashes(htmlspecialchars( $info['title_menu'] ))?>','<?=addslashes(htmlspecialchars( $info['title_menu'] ))?>'); return false;" title="<?=$root_alt?>"><?=$title_menu?></a>
					</td>
					<td width="80">

						<?
						//---------Перемещение-------------------------------------------------------------------
						if($type['id']!=1 && ($auth_isROOT || auth_Access('admin', 'move'))){?>

							<a href="#" onclick="doLoad('<?=$info['path_id']?>','/<?=ROOT_PLACE?>/ajax/swap_page/up/<?=(int)$type['id']?>/','tree_<?=(int)$type['id']?>','POST');"><img src="/DESIGN/ADMIN/images/up_page.gif" width="8" height="14" border="0" alt="Перенести раздел вверх" title="Перенести раздел вверх"/></a>
							<a href="#" onclick="doLoad('<?=$info['path_id']?>','/<?=ROOT_PLACE?>/ajax/swap_page/down/<?=(int)$type['id']?>/','tree_<?=(int)$type['id']?>','POST');"><img src="/DESIGN/ADMIN/images/down_page.gif" width="8" height="14" border="0" alt="Перенести раздел вниз" title="Перенести раздел вниз"/></a>
						
						<? } else echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						//---------Перемещение-------------------------------------------------------------------
						?>

						
						<?
						//-------------Редактирование-------------------------------------------------------------
							if($auth_isROOT || auth_Access('admin', 'edit')){?>
							&nbsp;<img src="/DESIGN/ADMIN/images/edit_page.gif" width="14" height="14" border="0" onclick="displayMessage('/<?=ROOT_PLACE?>/admin/page_options/<?=$info['path_id']?>/', 760, 600);" alt="Настройки страницы" title="Настройки страницы" style="cursor: pointer" />
						<? }
						//-------------Редактирование-------------------------------------------------------------
						?>

						
						<?
						//-------------Добавление-------------------------------------------------------------
							if($type['id']!=1 && ($auth_isROOT || auth_Access('admin', 'add')) && !$info['noadd'] && $type['flag_addsub']){?>
								<a href="#" onclick="displayMessage('/<?=ROOT_PLACE?>/admin/structure/add_path/node<?=$info['path_id']?>_<?=$type['id']?>/','400','320');"><img src="/DESIGN/ADMIN/images/new_page.gif" width="14" height="14" border="0"  alt="Добавить раздел" title="Добавить раздел"/></a>
						<? }
						//-------------Добавление-------------------------------------------------------------
						?>

						
						<?
						//-------------удаление-------------------------------------------------------------
						if( ( $auth_isROOT || auth_Access( 'admin', 'del' ) ) && !array_search( $info['path_id'], array( 1,2,3,4,5 ) ) && !$info['nodel'] )
						{?>
							<a href="#" onclick="if (confirm('Вы действительно хотите удалить раздел &laquo;<?=addslashes(htmlspecialchars( $info['title_menu'] ))?>&raquo;?')) {doLoad('node<?=$info['path_id']?>','/<?=ROOT_PLACE?>/ajax/delete/','tree_div','POST');}"><img src="/DESIGN/ADMIN/images/delete_page.gif" width="14" height="14"border="0" alt="Удалить раздел" title="Удалить раздел"/></a>
						<? }
						//-------------удаление-------------------------------------------------------------
						?>


					</td>
				</tr>
			</table>

			<div id="tree_childs<?=$info['path_id']?>" <?=(($chcnt>0) && !in_array($info['path_id'], $childs_array_cookie) ? 'style="display: none"' : '')?>>
			<?
			if ($chcnt>0) writeContentTree($type, $info['path_id'], $level);

			echo '</div>';
			
			$level--;
			}
		}
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------




##############################################################################
# Дерево в админке слева (основное дерево управления сайтом
##############################################################################

function writeTree(){
	ob_start();
	global $auth_isROOT;
?>

	<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="height: 100%">
		<tr style="height: 10px;">
			<Td></td>
			<Td><img src="/DESIGN/ADMIN/images/blank.gif" width="1" height="1" border="0" alt=""></td>
			<Td></td>
		</tR>
		<tr>
			<td width="5">&nbsp;</td>
			<td valign="top">  		

				<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
					<tr>
						<td align="left">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td style="cursor: pointer" onclick="showLeftTab(1);">
										<table cellpadding="0" cellspacing="0" border="0" style="height: 27px; width:100%" id="tab_item1" class="tab_item_active">
											<tr>
												<td align="center" style="padding-left: 5px;">Страницы</td>
												<td style="width: 5px" id="tab_item1_img" class="tab_item2_active"><img src="/DESIGN/ADMIN/images/blank.gif" width="5" height="1" border="0" alt=""></td>
											</tr>
										</table>
									</td>
									<td style="cursor: pointer" onclick="showLeftTab(2);">
										<table cellpadding="0" cellspacing="0" border="0" style="height: 27px; width:100%" id="tab_item2" class="tab_item_inactive">
											<tr>
												<td align="center" style="padding-left: 5px;">Управление</td>
												<td style="width: 5px" id="tab_item2_img" class="tab_item2_inactive"><img src="/DESIGN/ADMIN/images/blank.gif" width="5" height="1" border="0" alt=""></td>
											</tr>
										</table>			
									</td>
									
									<?if($auth_isROOT){?>
									<td style="cursor: pointer" onclick="showLeftTab(3);">
										<table cellpadding="0" cellspacing="0" border="0" style="height: 27px; width:100%" id="tab_item3" class="tab_item_inactive">
											<tr>
												<td align="center" style="padding-left: 5px;">ROOT</td>
												<td style="width: 5px" id="tab_item3_img" class="tab_item2_inactive"><img src="/DESIGN/ADMIN/images/blank.gif" width="5" height="1" border="0" alt=""></td>
											</tr>
										</table>														
									</td>
									<?}?>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
						
							<div id="left_tab1">
								<div id="tree_div">			
									<?=generateMainPage();?>
									<?=getTypesNode();?>
								</div>

							</div>

							<div id="left_tab2" style="display: none;">
								<?=getModulesList();?>				
							</div>

							<?if($auth_isROOT){?>
								<div id="left_tab3" style="display: none;">
									<div id="menu_div" style="width:100%">
										<?=generateMenuDiv();?>
									</div>						
								</div>
							<?}?>
						
						</td>
					</tr>
				</table>

			</td>
			<td width="5">&nbsp;</td>
		</tr>
	</table>
<?
	return ob_get_clean();
}




function generateMenuDiv(){
	global $adminmenu,$_MODULES;

	$temp = assoc('module_dir',$_MODULES->info);

	$out = '
	<TABLE width="100%" class="main_tree" cellspacing="0" cellpadding="0">
		<TR>
			<TD class="www_type">Основные настройки</TD>
		</TR>

	';

	foreach($adminmenu->menu['admin'] as $path => $iinfo){
		$out.='<TR class="main_page"><TD>';
		$out .=  "<a href=\"#\" onclick=\"openPage('center','".str_replace('/','',$path)."','/".$path."','".$iinfo['caption']."','".$iinfo['caption']."')\">".$iinfo['caption']."</a>";
		$out.='</TD></TR>';
	}
			
	$out .= '  
		<TR class="main">
			<TD class="www_type">Модули</TD>
		</TR>
		<TR>
			<TD>
		';

	foreach($_MODULES->info as $path => $info){
		if ($path=='admin' || $info['fastcall']=='' || in_array($path, $_MODULES->sys_modules) || $info['installed']==0) continue;
		$path_to_config ='/'.ROOT_PLACE.'/'.$path.'/config/'; 

		$out .= '<div class="option_menu_item"><a href="#" onclick="openPage(\'center\',\''.str_replace("/","",$path_to_config)."','".$path_to_config."','".$info['module_caption'].'\',\''.$info['module_caption'].'\')"><B>'.$info['module_caption'].'</B></a></div>';
	}
	
	$out .= '
			</td>
		</tr>
	</TABLE>
	';

	return $out;
}

function getModulesList(){
	global $DB;
	$out='';

	ob_Start();
	?>
	<table cellpadding="0" cellspacing="0" border="0" width="100%" class="main_tree">	
		<?
		echo getModulesListType();
	
		$modules_types=$DB->GetAll("SELECT * FROM `".PRFX."modules_types` ORDER by `sortir`");
		foreach($modules_types as $mt){
			?>
			<tr class="main">
				<td class="www_type"><?=$mt['title']?></td>
			</tr>
			<?
			echo getModulesListType($mt['id']);
			
		}
	
	//----------действия------------------------------------	
	?>
		<tr class="main">
			<td class="www_type">Действия</td>
		</tr>
		<tr>
			<td>
				<div class="option_menu_item">
					<?echo '<a href="#" onclick="displayMessage(\'/'.ROOT_PLACE.'/admin/robotstxt/\',\'600\',\'500\');">Редактировать robots.txt</a>';?>
				</div>
				<div class="option_menu_item">
					<a href="#" onclick="displayMessage('/<?=ROOT_PLACE?>/admin/passwd/','400','300');">Сменить пароль</a>											
				</div>
			
				<div class="option_menu_item">
					<a href="/<?=ROOT_PLACE;?>/exit/">Выход</a>
				</div>
			</td>
		</tr>
	<?
	//----------действия------------------------------------	
	?>
	
	</table>
	<?
	$out = ob_get_contents();
	ob_end_clean();	
	return $out;
}


function getModulesListType($type=0){
	global $_MODULES,$auth_isROOT;
	$mods = array();
	
	foreach ($_MODULES->info as $mod_name => $mod_info){
		if (!$auth_isROOT && !auth_access2Module($mod_info['module_id'])) continue;

		$cancel = false;
		if(isset($mod_info['output']))
			foreach ($mod_info['output'] as $out_script)
				if (strlen($out_script)>0) {$cancel = true; break;}

		if ( $cancel === false && $mod_info['fastcall']
			&& (array_search($mod_name,$_MODULES->sys_modules) === false || array_search($mod_name,$_MODULES->sys_modules_inlist)))
				$mods[$mod_name] = $mod_info;

	}

	$out='';
	if (sizeof($mods)){
		ob_Start();	
	?>
					   
			<? foreach ($mods as $mod_info){
				
				if(isset($mod_info['type']) && $mod_info['type']!=$type)continue;

				if (!$auth_isROOT && !auth_access2Module($mod_info['module_id'])) continue;

				require($_SERVER['DOCUMENT_ROOT'].'/MODULES/'.$mod_info['module_name'].'/config.php');
				if (isset($CONFIG['show']) && $CONFIG['show']==false) continue;						  
				unset($CONFIG);
			?>

			<tr class="main_page">
				<td style="padding-left: 26px;"><a class="top" href="#" onclick="clearLinkPaths_and_loadQuickModule('<?=$mod_info['module_name']?>', 'center','<?=$mod_info['module_caption']?>');"><?=$mod_info['module_caption']?></a></td>
			</tr>

			<?}?>

	<?
		$out = ob_get_contents();
		ob_end_clean();

	} else {

		$out='
			<tr>
				<td style="padding-left: 26px;">
					Нет модулей
				</td>
			</tr>
		';
	}

	return $out;
}


function generateTreeSelectArray($type=0){
	global $vac_out, $DB;
	$temp=array();
	$temp[]=array('id'=>1,'parent'=>0,'name'=>' ');

	$nodes=$DB->getAll('SELECT * FROM `'.PRFX.'www` WHERE `www_type`>1 ORDER BY `order`');

	if (sizeof($nodes)) 
	   foreach ($nodes as $key => $value)
	   {
			$cur =& $temp[];
			$cur['id'] = (int)$value['path_id'];
			$cur['parent'] = (int)$value['parent'];
			$cur['name'] = (string)$value['title_menu'];				
	   }

	$temp = array2tree($temp, 0);
	$vac_out=array();
	prepareLevel($temp); 

	return $vac_out;
}

function checkCodeUniq($param)
	{
	
	/**
	 * Чекалка уникальности символьного урла
	 */

	global $DB;
	 
	$opts = $sqlWheres = array();

	if ( isset($param['table']) && preg_match('/^[a-z_]+$/', $param['table']) )
		{
		
		$opts['table'] = $param['table'];
		
		}
	
	if ( isset($param['path_id']) && preg_match('/^[0-9]+$/', $param['path_id']) )
		{
		
		$opts['path_id'] = $param['path_id'];
		
		}

	if ( isset($param['parent']) && preg_match('/^[0-9]+$/', $param['parent']) )
		{
		
		$opts['parent'] = $param['parent'];
		
		}
		
	if ( isset($param['code']) && preg_match('/^[a-zA-Z_]+$/', $param['code']) )
		{
		
		$opts['code'] = $param['code'];
		
		}
	
	if ( isset($opts['table']) && isset($opts['code']) )
		{
		
		if ( isset($opts['path_id']) )
			$sqlWheres[] = ' `path_id` = ' . $opts['path_id'];

		if ( isset($opts['parent']) )
			$sqlWheres[] = ' `parent` = ' . $opts['parent'];
			
		$row = $DB->getRow('SELECT * FROM `' . $opts['table'] . '` WHERE `code` = "' . $opts['code']  . '"' . (count($sqlWheres) ? ' AND ' : '') . implode(' AND ', $sqlWheres));
		
		return count($row) ? 1 : 0;
		
		}
	
	}

?>
