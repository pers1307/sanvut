<?php
/** 
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name='texts';
$module_caption = 'Тексты';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>610,'height'=>410),
			'key_field'=>'id',
			'log_title_field' => 'title',
			'order_field' => '`title`',
			'onpage' =>	20,

			'config'=>	array(

				'title' => array(
							'caption' => 'Название',
							'value' => '',
							'type' => 'string_root',
							'in_list' => 1,
							),
				'text' => array(
							'caption' => 'Текст',
							'value' => '',
							'height'=>260,
							'type' => 'wysiwyg',
							'in_list' => 0,
							),
				),
			),
		),
);
?>