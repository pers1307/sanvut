<?
set_time_limit(0);

	function report($str){

			$GLOBALS['REPORT'][] = $str;

		return true;
	}

	function safe($str){
		return htmlspecialchars(html_entity_decode($str, ENT_QUOTES), ENT_QUOTES);
	}

	function normalize($str){
		return html_entity_decode($str, ENT_QUOTES);
	}

	function url($url){
		if(!trim($url)){
			$url = '/';
		}

		$url =
			preg_replace('#/+#', '/',
			preg_replace('#\\\+#', '/', //WARNING возможно не стабильно =)
			preg_replace('#^([a-z0-9]+://)?(www\.)?'.HTTP_HOST.'#i', '',
			$url)));

		if(strpos($url, '?') === false){
			$url = preg_replace('#/+$#i', '', $url);
		}

		if($url == ''){
			$url = '/';
		}

		return $url;
	}

    function _mctime() {
        list ($sec, $msec)= explode(' ', microtime());
        return $sec + $msec;
    }


	$REPORT = array();
	$TIME = array();

	$host = isset($_REQUEST['host']) ? $_REQUEST['host'] : 'localhost';
	$user = isset($_REQUEST['user']) ? $_REQUEST['user'] : 'sanvut';
	$pass = isset($_REQUEST['pass']) ? $_REQUEST['pass'] : 'va3Lepach';
	$dbname = isset($_REQUEST['dbname']) ? $_REQUEST['dbname'] : 'sanvut';
	$sitehost = isset($_REQUEST['sitehost']) ? $_REQUEST['sitehost'] : 'sanvut.mediasite.ru';

	if ($host=='' || $user=='')
		{
		echo 'Не указан доступ к БД MySQL';
		exit;
		}

	$link = mysql_connect($host, $user, $pass)
        or die("Could not connect to DB");
    	mysql_select_db($dbname) or die("Could not select DB");

    define('DB_TABLE_SEARCH', 'mp_search_index');
    define('HTTP_HOST', $sitehost);

	//Время, до которого будет работать скрипт.
	//На всякий случай только 80% разрешенного времени
	$TIME['stop'] = _mctime() + ( ( ($met = ini_get('max_execution_time') )? $met : 40)*0.8);
	//report('HOST: '.HTTP_HOST);
	//report('MAX_EXECUTION_TIME: '.ini_get('max_execution_time'));


/*======================================================================*/

	$main_SQL = '
		SET NAMES `utf8`;
	';

	$main = mysql_query($main_SQL);

	$main_SQL = '
		SELECT `uri` FROM `'.DB_TABLE_SEARCH.'`
			ORDER BY
				`time`
			LIMIT
				300
	';

	$main = mysql_query($main_SQL);

	if(mysql_num_rows($main)){
		$r = array('uri' => '/');
	}else{
		$r = mysql_fetch_assoc($main);
	}

	$i = 1;
	$new_links = 0;
	$size = 0;

	do
	{
	   
		report('==========================================');
		$url = 'http://'.HTTP_HOST.normalize($r['uri']);
		$sql_uri = safe( url( $url ) );


		report('URL: '.$url);

		//Открываем
		$c = @file_get_contents( $url );
		//Читаем титл
		if(!preg_match('#\<!--\[TITLE\]--\>(.*?)\<!--\[/TITLE\]--\>#', $c, $p))
		{
			preg_match('#\<title>(.*?)<\/title\>#', $c, $p);
		}

		$title = @$p[1];
		report('TITLE: '.$title);

		if(!$c || preg_match('#\<!--\[404 - not found\]--\>#', $c)){
			report('404 - DELETING PAGE');
			mysql_query('DELETE FROM `'.DB_TABLE_SEARCH.'` WHERE `uri` = "'.$sql_uri.'"');
			continue;
		}


		//Берем список ссылок и приводим их к стандарту
	    preg_match_all('#\<a[^\>]+href="([^http,^https,^javascript,^mailto,^ftp,^\#].*?)"#sim', $c, $p2);
	    $links = array_unique(array_map('url', $p2[1]));
		
		//Берем индекс-зоны
	    preg_match_all('#\<!--\[INDEX(\*)?\]--\>(.*)\<!--\[/INDEX\]--\>#si', $c, $p);
	    //report('Найдено '.count($p[2]).' индексируемых блоков');
	    $c = implode(' ', $p[2]);
		
	    //	report('LINKS FOUND: '.count($links));

	    //Патч для релкома:
	    if(strtolower('a') == 'A'){
	    	$strtolower_func = 'strtoupper';
	    }else{
	    	$strtolower_func = 'strtolower';
	    }


	    //Избавляемся от ссылок на левые ресурсы (картинки, архивы и пр..)
	    foreach( $links as $_k => $_l )
		{
	    	if( preg_match( '#\.([a-z0-9]+)$#i', $_l, $p3 ) )
			{
	    		if( !in_array( $strtolower_func( $p3[1] ), array( 'htm', 'html', 'php', 'phtml', 'php3', 'php4', 'txt' ) ) )
				{
	    			unset( $links[ $_k ] );
				}
	    	}
	    }

	    unset( $p2 );
	    unset( $c );


	    //Берем только нужные контент зоны
	    $content = '';
	    foreach( $p[1] as $_k => $_v )
		{
	    	if( $_v )
			{
	    		continue;
			}

	    	$content .= ' '.$p[2][ $_k ];
	    }
		
	    unset( $p );

	    //Убираем тэги и всякую фигню из индекса
		$content = preg_replace('#\<script(.*?)<\/script\>#si','',$content); // режем скрипты (c) Andruha
		$content = preg_replace('#\<style(.*?)<\/style\>#si','',$content); // режем стили (c) Andruha
	    $content = preg_replace('/(\s+)|(&[a-zA-Z0-9#]+;)/', ' ', strip_tags($content));



	    //Сохраняем НОВЫЕ ссылочки в базу
	    if(count($links) > 0){
	    	//Берем из ссылочек только новые и суем их в БД
	    	$sql = '
	    		SELECT `uri` FROM `'.DB_TABLE_SEARCH.'`
	    			WHERE
	    				`uri` IN ("'.implode('","', array_map('safe', $links)).'")
	    	';

	    	$exists = array();
	    	$res = mysql_query($sql);
	    	while( $r = mysql_fetch_array($res) ){
	    		$exists[] = $r[0];
	    	}

	    	if(count($exists)){
	    		$links = array_diff($links, array_map('normalize', $exists));
	    		unset($exists);
	    	}

	    	report('NEW LINKS: '.count($links));

	    	if( $links )
			{
				$sql = 'INSERT INTO '.DB_TABLE_SEARCH.' (uri) VALUES ("'.implode( '"),("', array_map( 'safe', $links ) ).'")';
				
		    	if( mysql_query( $sql ) )
				{
		    		//report('LINKS ('.count($links).') SUCCEFULLY ADDED');
		    		$new_links += count($links);
		    	}
				else
				{
		    		//report('ERROR');
		    	}
	    	}
	    }


	    //Прибиваем ненужное (старое), короче - обновляем.

	    if( mysql_query( 'DELETE FROM `'.DB_TABLE_SEARCH.'` WHERE `uri` = "'.$sql_uri.'"' ) )
		{
	    	//report('"'.$sql_uri.'" INFO DELETED');
	    }
		else
		{
	    	//report('ERROR');
	    }

	    if( mysql_query( 'INSERT INTO '.DB_TABLE_SEARCH.' SET uri="'.$sql_uri.'", title="'.safe($title).'", text="'.safe($content).'", time=UNIX_TIMESTAMP()' ) )
		{
			$size = $size + (strlen($title) + strlen($content));
	    	//report('NEW INFO FOR "'.$sql_uri.'" SUCCEFULLY ADDED/UPDATED ');
	    }
		else
		{
	    	//report('ERROR');
	    }

	    //report('LOOP END.');

	    if( $i++ < 5 && is_resource( $main ) && ( mysql_num_rows( $main ) < 30 ) )
		{
	    	$main = mysql_query( $main_SQL );
	    }

	    //report('TIME: ' ._mctime(). ' / '.$TIME['stop'] );
	
	}
	//	while( (_mctime() < $TIME['stop']) && ($r = mysql_fetch_array($main)) );
	while( $r = mysql_fetch_array( $main ) );


	//report('==============================================');
	//report('==============================================');
	//report('==============================================');

	if(mysql_query('OPTIMIZE TABLE `'.DB_TABLE_SEARCH.'`')){
		//report('TABLE OPTIMIZED');
	}else{
		//report('ERROR');
	}

	if(is_array($REPORT)){
		ob_start();
			foreach ($REPORT as $_r){
				echo '* '.$_r."\n";
			}
			$report = ob_get_clean();
	}
	
mysql_query( 'DELETE FROM mp_search_index WHERE uri="/service"' );
?>

&nbsp;&nbsp;&nbsp;&nbsp;<b>Индексация произведена</b>

<pre>
<?=$report;?>
</pre>

