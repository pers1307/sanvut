<?php
/** Версия модуля */
$CONFIG['version'] = '1.0.0.0';

/** Системное название модуля */
$CONFIG['module_name'] = 'search';
$CONFIG['module_caption'] = 'Модуль поиска';
$CONFIG['fastcall'] = '/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/start/';
$CONFIG['show'] = true;
?>