<?php
/** 
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name='news';
$module_caption = 'Новости';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		
	
	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>660,'height'=>410),
			'key_field'=>'id',
			'order_field' => '`date` desc',
			'onpage' =>	20,
			
			'config'=>	array(
			
				'title' => array(
					'caption' => 'Название',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					'to_code' => true,
					),
				'code' => array(
					'type' => 'code',
					'caption' => 'Символьный код',
					'in_list' => 0,
					'value' => '',
					),
				'date' => array(
					'type' => 'calendar',
					'caption' => 'Дата',
					'in_list' => 1,
					'value' => '',
					),
				'image' => array(
					'caption' => 'Картинка',
					'value' => '',
					'type' => 'image',
					'thumbs' => array(
						0 => array(100,100,2),
						1 => array(160,120,2),
					),
					'in_list'=>1,
					'filter'=>0,
					),
				'announce' => array(
					'caption' => 'Анонс',
					'value' => '',
					'height'=>260,
					'type' => 'wysiwyg',
					'in_list' => 0,
					),
				'text' => array(
					'caption' => 'Текст',
					'value' => '',
					'height'=>260,
					'type' => 'wysiwyg',
					'in_list' => 0,
					),
			),
		),
	),
);
?>