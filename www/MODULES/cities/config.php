<?php
/** 
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name='cities';
$module_caption = 'Города';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		
	
	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>660,'height'=>410),
			'key_field'=>'id',
			'order_field' => '`order` desc',
			'onpage' =>	20,
			
			'config'=>	array(
			
				'title' => array(
					'caption' => 'Название',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					'to_code' => true,
					),


				'url' => array(
					'caption' => 'Ссылка',
					'value' => '',
					'type' => 'string',
				),
					
			),							
		),
	),
);
?>