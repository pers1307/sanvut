<?php
global $auth_isROOT;
$key_field = $CONFIG['tables']['items']['key_field'];
?>

<h3>Добавление / редактирование объекта</h3>

<form id="add" enctype="multipart/form-data">
<div id="div_inserted_id">
<input id="inserted_id" type="hidden" name="<?=$key_field;?>" value="<?=${$key_field}?>">
</div>

<?=$_FORM_?>
</form>


<SCRIPT type="text/javascript">
function postForm()	{
  doLoad(getObj('add'),'/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add/<?=(int)$path_id;?>/<?=(int)$page;?>/', <?=prepareOutput($output_id);?>+';div_inserted_id');
}


dialogAddButtons(
	new Array(
		new Array("doLoad(getObj('add'),'/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add/<?=(int)$path_id;?>/<?=(int)$page;?>/0/0/', <?=prepareOutput($output_id);?>+';div_inserted_id', null, 'rewrite');","Ok"),
		new Array("doLoad(getObj('add'),'/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add/<?=(int)$path_id;?>/<?=(int)$page;?>/0/1/', <?=prepareOutput($output_id);?>+';div_inserted_id');","Применить")
	)
);
</SCRIPT>
