<?php
/*AddBeforeTag('<script type="text/javascript" src="/DESIGN/ADMIN/js/JsHttpRequest.js"></script>'."\n",'</head>');*/
include_once('inc/JsHttpRequest.php');

/**
 * Рекурсивное удаление детей страницы
 *
 * @param integer $parent
 */
function delete_childs($parent = 0){
  global $DB;

  $cur_del = $DB->getAll("SELECT `path_id` FROM `".PRFX."www` WHERE `parent`='".$DB->pre( $parent )."'");
  if(sizeof( $cur_del )){
    foreach($cur_del as $cd){
      $DB->execute("DELETE FROM `".PRFX."www` WHERE `path_id`='".$DB->pre( $cd['path_id'] )."'");
      delete_childs( $cd['path_id'] );
    }
  }
  else return;
}


/*
ERROR_CODES:
0 - успешно сохранено
1 - задано пустое системное имя
2 - указано существующее системное имя
3 - системное имя содержит недопустимые символы

*/
function add_new_path( $parent_id=0, $sys_name='', $header='нет заголовка', $title='', $title_menu = '', $design=0, $www_type )
{
	global $urls, $DB, $_ZONES, $nodel, $noadd, $noview, $notfound;
	
	if( $sys_name == '' )
		return 1;
	

	$allowed_chars=' qwertyuiopasdfghjklzxcvbnm01234567890_-.,';
	for( $p = 0; $p < strlen( $sys_name ); $p++ )
	{
		if( strpos( $allowed_chars,$sys_name[$p] ) == false )
			return 3;
	}
	
	
	$new_path = EndSlash( $urls->tree[$urls->ids[$parent_id]]['path'] ).strtolower( $sys_name );
    
	
	$title_menu = $title_menu ? $title_menu : $header;
	$title = $title ? $title : $header;
	
	if( isset( $urls->tree[ trim($new_path, '/') ] ) )
		return 2;
		
	if( $new_path[0] == '/' )
		$new_path[0] = '';
	
	
	//return $new_path;
	
	$sql = "SELECT `structure` FROM `".PRFX."tpl` WHERE `id` = ".(int)$design;
	$temp = unserialize( $DB->getOne( $sql ) );
	
	// Зададим ордер
	$sql = "SELECT `order` FROM `".PRFX."www` WHERE `parent` = '".(int)$parent_id."' AND `path_id` NOT IN (2,3,4,5) ORDER BY `order` DESC LIMIT 1";
	$order = $DB->getOne( $sql );

	$sql = array();
	$sql[] = "`parent` = '".$DB->pre( $parent_id )."'";
	$sql[] = "`path` = '".$DB->pre( $new_path )."'";
	$sql[] = "`config` = '".$DB->pre( serialize( $temp['config'] ) )."'";
	$sql[] = "`www_type` = '".$DB->pre( $www_type )."'";
	$sql[] = "`order` = '".( (int)$order + 1 )."'";
	$sql[] = "`visible` = 0";
	$sql[] = "`header` = '".$DB->pre( $header )."'";
	$sql[] = "`title_page` = '".$DB->pre( $title )."'";
	$sql[] = "`title_menu` = '".$DB->pre( $title_menu )."'";
	$sql[] = "`meta_description` = ''";
	$sql[] = "`meta_keywords` = ''";
	$sql[] = "`main_template` = '".$DB->pre( $temp['main_template']!="" ? $temp['main_template'] : 'inner.php' )."'";
	
	$sql[] = "`noadd` = ".(isset($noadd) ? 1 : 0);
	$sql[] = "`noview` = ".(isset($noview) ? 1 : 0);
	$sql[] = "`nodel` = ".(isset($nodel) ? 1 : 0);
	$sql[] = "`notfound` = ".(isset($notfound) ? 1 : 0);
	
	$sql = "INSERT INTO `".PRFX."www` SET ".implode( ',', $sql );
	$DB->execute( $sql );
	
	return 0;
}

function unic_path($cur_path){
   global $urls;
   $cur_path .= mt_rand(0,9999);
   if (isset($urls->tree[$cur_path])) unic_path($cur_path);
   return $cur_path;
}


function get_modules_content(){
    global $_MODULES;
	$_MODULES->getModulesInfo();
	$_MODULES->getNotInstalledModules();

	/** Пробегаемся по всем модулям и отстраиваем содержимое страницы */
	foreach ($_MODULES->info as $module_name => $values) {
		$system = $_MODULES->isSystem($module_name);
		if ($values['installed'] && !$system)
			$cur = & $vars_installed[];
		elseif ($values['installed'] && $system) $cur = & $vars_system[];
		else
			$cur = & $vars_notinstalled[];
		
		$cur['module_caption'] = $values['module_caption'];
		$cur['module_name'] = $values['module_name'];
		$cur['module_dir'] = $values['module_dir'];
		$cur['version'] = $values['version'];
		$cur['installed'] = $values['installed'];
		$cur['system'] = $_MODULES->isSystem($module_name);
	}
   
	$content = '';
	if (isset($vars_installed) && sizeof($vars_installed))
	{
		$content .= '
					<h3>Установленные модули</h3>
					<ol>
		';
	    foreach($vars_installed as $module){
	      $content .='<li>'.$module['module_caption'].', версия '.$module['version'].'&nbsp;&nbsp; <a href="#" onclick="doLoad(\'\',\'/'.ROOT_PLACE.'/ajax/delete_mod/'.$module['module_dir'].'/\',\'modules\',\'post\')">Удалить</a></li>';
	    }
		$content .= '</ol>';
	}
	
	if (isset($vars_system) && sizeof($vars_system))
	{
		$content .= '
					<h3>Системные модули</h3>
					<ol>
		';
	     foreach($vars_system as $module){
	         $content .='<li>'.$module['module_caption'].', версия '.$module['version'].'&nbsp;&nbsp; <a href="#" onclick="doLoad(\'\',\'/'.ROOT_PLACE.'/ajax/delete_mod/'.$module['module_dir'].'/\',\'modules\',\'post\')">Удалить</a></li>';
	     }
		$content .= '</ol>';	
	}	
		
	if (isset($vars_notinstalled) && sizeof($vars_notinstalled))
	{
		$content .= '
					<h3>Неустановленные модули</h3>
					<ol>
		';
	   foreach($vars_notinstalled as $module){
	      $content .='<li>'.$module['module_caption'].', версия '.$module['version'].'&nbsp;&nbsp; <a href="#" onclick="doLoad(\'\',\'/'.ROOT_PLACE.'/ajax/install/'.$module['module_dir'].'/\',\'modules\',\'post\')">Установить</a></li>';
	   }
		$content .= '</ol>';
	}	     

	return $content;
}

/**
 * Возвращает ID страницы с которой необходимо поменять местами
 *
 * @param integer $order
 * @param boolean $up
 * @return integer
 */
function get_page_to_swap($order = 0, $parent = 0, $up = true)
{
	global $DB, $urls;
	if ($order == 0 || $parent == 0) return false;
	$order = ($up) ? $order-1 : $order+1;
	
	$sql = "
		SELECT `path_id` FROM `".PRFX."www` WHERE `order` = '".(int)$order."' AND `parent` = '".(int)$parent."';
	";
	return $DB->getOne($sql);	
}

function reset_orders($parent = 1)
{
	global $DB,$already,$urls;
	
	$sql = "
		SELECT `path_id` FROM `".PRFX."www` WHERE `parent` = '".(int)$parent."' AND `path_id` NOT IN (2,3,4,5) ORDER BY `order`;
	";
	
	$paths = $DB->getCol($sql);
	if (sizeof($paths))
	{
		$start = 0;
		foreach ($paths as $path_id)
		{
			if (isset($already[$path_id]) && $already[$path_id]) continue;
			
			$already[$path_id] = 1;
			
			$start++;
			
			$sql = "
				UPDATE `".PRFX."www` SET `order` = '".$start."' WHERE `path_id` = '".(int)$path_id."';
			";
			
			$DB->execute($sql,true,false);
			$urls->tree[$urls->ids[$path_id]]['order'] = $start;
			reset_orders($path_id);
		}
	}
}

?>