<?php

/**
* Модуль авторизации и регистрации пользователя
* Конфигурационный файл
* 
* @package ObligatoryModules

*/
/** Версия модуля */
$CONFIG['version'] = '0.0.0.1';

/** Системное название модуля */
$CONFIG['module_name'] = 'ajax';
$CONFIG['module_caption'] = 'Динамическая подгрузка данных';

?>