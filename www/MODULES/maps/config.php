<?php
/** 
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name='maps';
$module_caption = 'Карты';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>555,'height'=>550),
			'key_field'=>'id',
			'order_field' => '`id`',
			'onpage' =>	20,

			'config'=>	array(

				'id' => array(
					'caption' => 'ID',
					'value' => '',
					'type' => 'static',
					'in_list' => 1,
					),

				'coord' => array(
					'caption' => 'Название или координаты',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					),

				'description' => array(
					'caption' => 'Описание',
					'value' => '',
					'type' => 'wysiwyg',
                                        'small' => 1,
                                        'width' => '370',
                                        'height' => '100',
					'in_list' => 0,
					),						

				'icon' => array(
					'caption' => 'Иконка',
					'value' => '',
					'type' => 'image',
					'thumbs' => array(
						'original' => array(0,0),
					),
					'in_list'=>0,
					'filter'=>0,				
					),

				'map_width' => array(
					'caption' => 'Ширина карты (px)',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					),

				'map_height' => array(
					'caption' => 'Высота карты (px)',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					),

				'scale' => array(
					'caption' => 'Масштаб',
					'value' => '85',
					'type' => 'select',
                                        'values' => array(
                                                        100 => 'Крупный',
                                                        85 => 'Средний',
                                                        70 => 'Мелкий',
                                                          ),
					'in_list' => 1,
					),
      
			),							
		),
	),
);
?>
