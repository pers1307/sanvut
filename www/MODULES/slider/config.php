<?php
/**
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
 */

$module_name = 'slider';
$module_caption = 'Слайдер на главной';

$CONFIG=array(

    'module_name'=>$module_name,
    'module_caption'=>$module_caption,
    'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
    'version'=>'1.1.0.0',

    'tables'=>array(

        'items'=> array (

            'db_name' => $module_name,
            'dialog'=> array('width'=>660,'height'=>410),
            'key_field'=>'id',
            'order_field' => '`order` asc',
            'onpage' =>	20,

            'config'=>	array(

                'title' => array(
                    'caption' => 'Заголовок',
                    'value' => '',
                    'type' => 'string',
                    'in_list' => 1,
                    'to_code' => true,
                ),
                'title_two' => array(
                    'caption' => 'Подзаголовок',
                    'value' => '',
                    'type' => 'string',
                    'in_list' => 1,
                    'to_code' => true,
                ),
                'image' => array(
                    'caption' => 'Картинка',
                    'value' => '',
//                    'type' => 'loader',
                    'type' => 'image',
                    'thumbs' => array(
                        0 => array(1400, 545, 2),
                        1 => array(90, 60, 2),
                    ),
                    'in_list'=>1,
                    'filter'=>0,
                ),

            ),
        ),
    ),
);
?>