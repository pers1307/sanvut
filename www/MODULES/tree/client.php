<?

function tree_tree_generateVars(){		
	global $Forms, $DB, $CONFIG, $path_id, $tree_isorder;

	$filter='';
	if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path'])){
		$link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1];
		if (isset($link_path['inmodule']) && $link_path['inmodule']==$CONFIG['module_name']){
			$filter='`'.$link_path['to_key'].'`="'.$link_path['from_id'].'"';
		}
	}
	
	if(!isset($CONFIG['tables']['tree']['order_field']))$CONFIG['tables']['tree']['order_field']='`order`';
	
	ob_start();
	$tmp=$CONFIG['module_name'].'_writeCatalogTree';
	$tmp($CONFIG['tables']['tree'], $CONFIG['tables']['tree']['title_field'], 0, -1, $path_id, $filter , $CONFIG['tables']['tree']['order_field'],'tree','items');
	$tree_html = ob_get_clean();
	

	$vars = array();

	$vars['CONFIG']		= $CONFIG; 
	$vars['Forms']		= $Forms; 
	$vars['path_id']	= $path_id;	  
	$vars['tree_html'] = $tree_html;
	
	return $vars;
}

//-------------------------------------------------------------------------------------------------------------------------------------

function tree_writeCatalogTree($table_config, $title_field, $pid=0, $level=-1, $path_id=0, $where=1, $order, $tree_act='tree', $items_act='items'){
	global $DB, $CONFIG, $auth_isROOT;

	if (isset($_COOKIE['catalog'.(int)$path_id.'_sel_childs'])) $cookie = $_COOKIE['catalog'.(int)$path_id.'_sel_childs']; 
	else $cookie='';

	$childs_array_cookie = explode(',',$cookie);

	$all = $DB->getAll($q='SELECT * FROM '.PRFX.$table_config['db_name'].' WHERE `parent`='.$pid.' AND `path_id`='.(int)$path_id.($where!=''?' AND '.$where:'').' ORDER BY '.$order);
	if (sizeof($all)){
		foreach ($all as $num => $info){
			$level++;
			$style = $level*15;

			$chcnt = $DB->getOne('SELECT count(*) FROM '.PRFX.$table_config['db_name'].' WHERE `parent`="'.$info['id'].'" AND `path_id`='.(int)$path_id);

			$title_page=htmlspecialchars($info[$title_field],ENT_QUOTES);
			$title_page_small = substr($title_page,0,32).( strlen($title_page)>32 ? '...' : '');
			
			if($title_page_small=='')$title_page_small="<small>-----------------</small>";

			if ($chcnt>0)$collapse = '<a href="#" onclick="show_hide2(\''.$info['id'].'\', \'catalog'.(int)$path_id.'\')"><img src="/DESIGN/ADMIN/images/'.(!in_array($info['id'], $childs_array_cookie) ? 'plus' : 'minus').'.gif" hspace="5" width="10" height="10" border="0" id="catalog'.(int)$path_id.'_colapsik'.$info['id'].'"></a>';
			else $collapse = '<img src="/DESIGN/ADMIN/images/blank.gif" hspace="5" width="10" height="10">';

			$root_alt = '';
			if ($auth_isROOT) $root_alt = 'ID: '.$info['id'].'';

			$id_toggle_vis = 'catalog'.(int)$path_id.'_vis_img_'.$info['id'].'';

			
			$move_up			= "doLoad('', '/".ROOT_PLACE."/".$CONFIG['module_name']."/swap_".$tree_act."/".(int)$path_id."/up/".$info['id']."/', '".$tree_act."');";
			$move_down			= 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/swap_'.$tree_act.'/'.(int)$path_id.'/down/'.$info['id'].'/\', \''.$tree_act.'\');';
			$visible_tree	= 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/toggle_vis_'.$tree_act.'/'.(int)$path_id.'/'.(int)$info['id'].'/\', \''.$id_toggle_vis.'\');';
			$show_items			= 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/show_'.$items_act.'/'.(int)$path_id.'/1/'.(int)$info['id'].'/\', \''.$items_act.'\');';
			$add_sub_tree	= 'displayMessage(\'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/add/'.(int)$path_id.'/0/'.(int)$info['id'].'/'.($level+1).'/\', '.$table_config[($level+2==$CONFIG['tables']['tree']['max_levels']?'dialog_last':'dialog')]['width'].', '.$table_config[($level+2==$CONFIG['tables']['tree']['max_levels']?'dialog_last':'dialog')]['height'].');';
			$show_items = $edit_tree	= 'displayMessage(\'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/add/'.(int)$path_id.'/'.(int)$info['id'].'/0/'.$level.'/\', '.$table_config[($level<$CONFIG['tables']['tree']['max_levels']-1?'dialog':'dialog_last')]['width'].', '.$table_config[($level<$CONFIG['tables']['tree']['max_levels']-1?'dialog':'dialog_last')]['height'].');';
			$del_tree		= 'if (confirm(\'Вы действительно хотите удалить раздел &laquo;'.$info[$title_field].'&raquo;?\')) {doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/del_'.$tree_act.'/'.(int)$path_id.'/'.(int)$info['id'].'/\', \''.$tree_act.'\');}';
			
			
			$btn_vis=($auth_isROOT || auth_Access($CONFIG['module_name'], 'vis'))?'<a href="#" onclick="'.$visible_tree.'"><span id="'.$id_toggle_vis.'"><img border="0" src="/DESIGN/ADMIN/images/'.(isset($info['visible']) && $info['visible']==0 ? 'invis' : 'vis').'.gif" alt=""/></span></a>&nbsp;':'';
			
			$btn_move=$order=='`order`' && ($auth_isROOT || auth_Access($CONFIG['module_name'], 'move'))?'
					<a href="#" onclick="'.$move_up.'"><img src="/DESIGN/ADMIN/images/up_page.gif" width="8" height="14" border="0" alt="Перенести раздел вверх" title="Перенести раздел вверх"/></a>
					<a href="#" onclick="'.$move_down.'"><img src="/DESIGN/ADMIN/images/down_page.gif" width="8" height="14" border="0" alt="Перенести раздел вниз" title="Перенести раздел вниз"/></a>
				':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			
			$btn_edit=$auth_isROOT || auth_Access($CONFIG['module_name'], 'edit') ?'<a id="node'.$info['id'].'" href="#" onclick="'.$edit_tree.'" title="Редактировать раздел"><img src="/DESIGN/ADMIN/images/edit_page.gif" width="14" height="14" border="0" alt="Редактировать раздел" title="Редактировать раздел"/></a>':'';
			
			$btn_add=(($auth_isROOT || auth_Access($CONFIG['module_name'], 'add')) && $level<$CONFIG['tables']['tree']['max_levels']-1) ?'<a href="#" onclick="'.$add_sub_tree.'"><img src="/DESIGN/ADMIN/images/new_page.gif" width="14" height="14" border="0" alt="Добавить под-раздел" title="Добавить под-раздел"/></a>':'';
			
			$btn_del=$auth_isROOT || auth_Access($CONFIG['module_name'], 'del')?'<a href="#" onclick="'.$del_tree.'"><img src="/DESIGN/ADMIN/images/delete_page.gif" width="14" height="14"border="0" alt="Удалить раздел" title="Удалить раздел"/></a>':'';
			
			echo '	 			
				<table cellspacing="0" cellpadding="0" width="100%" class="main_tree">
					<tr '.($level>0 ? 'class="child_trs"' : '').' style="height: 24px">
						<td style="padding-left: '.$style.'px;">
							'.$collapse.$btn_vis.'
							<a href="#" onclick="'.$show_items.'" title="'.$root_alt.'">'.$title_page_small.'</a>
						</td>
						<td width="80">
							'.$btn_move.$btn_edit.$btn_add.$btn_del.'
						</td>
					</tr>
				</table>
	
				<div id="catalog'.(int)$path_id.'_childs'.$info['id'].'" '.(($chcnt>0) && !in_array($info['id'], $childs_array_cookie) ? 'style="display: none"' : '').'>
			';

			if ($chcnt>0) {
				$tmp=$CONFIG['module_name'].'_writeCatalogTree';
				$tmp($table_config, $title_field, $info['id'], $level, $path_id, $where, $order, $tree_act, $items_act);
			}
			echo '</div>';
			
			$level--;
			}
		}
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------

function tree_tree_saveItem($showdberr = true){
	global $Forms, $DB, $CONFIG, $table_tree;

	$CONFIG['tables']['tree']['config'] = $Forms->save($CONFIG['tables']['tree']['config']);

	$sql_=array();

	foreach($CONFIG['tables']['tree']['config'] as $field => $info)
		{
		
		if (isset($info['value'])){
		
			if ( $info['type'] == 'code' && empty($info['value']) )
				{
				
				$info['value'] = "NULL";
				
				}
			else
				{
				
				$info['value'] = "'".$DB->pre($info['value'])."'";
				
				}
		
			$sql_[] = "`".$field."` = " . $info['value'];
			
		}
		
		}
	
	if ($_REQUEST['id']>0){
		$sql = "UPDATE `".PRFX.$CONFIG['tables']['tree']['db_name']."` SET ".implode(', ',$sql_)." WHERE `id` = ".(int)$_REQUEST['id'];
	} else  {
		
		if(!isset($CONFIG['tables']['tree']['order_field'])){
		
			//-----------уккажем сортировку в зависимости от того что указано в конфиге
			if($CONFIG['tables']['tree']['add_new_on_top']){
				$order=$DB->GetOne("SELECT MIN(`order`) FROM `".PRFX.$CONFIG['tables']['tree']['db_name']."` WHERE `path_id`=".(int)$CONFIG['tables']['tree']['config']['path_id']['value']." AND `parent`=".(int)$CONFIG['tables']['tree']['config']['parent']['value']);
				$sql_[]="`order`=".((int)$order-1);
			} else {
				$order=$DB->GetOne("SELECT MAX(`order`) FROM `".PRFX.$CONFIG['tables']['tree']['db_name']."` WHERE `path_id`=".(int)$CONFIG['tables']['tree']['config']['path_id']['value']." AND `parent`=".(int)$CONFIG['tables']['tree']['config']['parent']['value']);
				$sql_[]="`order`=".((int)$order+1);
			}
		}
		
		$sql = "INSERT INTO `".PRFX.$CONFIG['tables']['tree']['db_name']."` SET ".implode(', ',$sql_);
	}

	if( $DB->execute($sql, $showdberr) )
		{
		return ($_REQUEST['id'])?intval($_REQUEST['id']):$DB->id;
		}
	else
		{
		return false;
		}
		
}

//-------------------------------------------------------------------------------------------------------------------------------------

/**
* Смена позиции сортировки
*/
function tree_setSwapItemsOrder($table_name, $item_id, $action, $with_path_id=true){
	global $DB, $CONFIG;

	$item = $DB->getRow('SELECT * FROM `'.PRFX.$table_name.'` WHERE `id`='.(int)$item_id);

	$tmp=$CONFIG['module_name'].'_getItemsToSwap';
	$to_swap = $tmp($table_name, $item['order'], $action, $item['parent'], $with_path_id);
	
	if ((int)$to_swap>0){
		$target = $DB->getOne('SELECT `order` FROM `'.PRFX.$table_name.'` WHERE `id`='.(int)$to_swap);

		$sql1 = "UPDATE `".PRFX.$table_name."` SET `order` = ".(int)$target." WHERE `id`=".$item_id;
		$sql2 = "UPDATE `".PRFX.$table_name."` SET `order` = ".(int)$item['order']." WHERE `id`=".(int)$to_swap;

		$DB->execute($sql1);
		$DB->execute($sql2);
	}	
	
}	

//-------------------------------------------------------------------------------------------------------------------------------------
	
// Выбор итема для смены позиции-----------------------------------

function tree_getItemsToSwap($table_name, $order = 0, $up = true, $parent=0, $with_path_id=true){
	global $DB, $path_id,$CONFIG;

	//--------учитывать или нет path_id, для итемов path_id нет
	if($with_path_id)
		$where = " AND `path_id`=".(int)$path_id." AND `parent`=".$parent;
	else 
		$where = " AND `parent`=".(int)$parent;
		
	//--------если есть повторяющиеся значения поля ордер надо их по порядку выстроить----------------------------
	$count_unique=$DB->GetOne("SELECT COUNT(*) FROM `".PRFX.$table_name."` WHERE `order`=".$order.$where);
	if ($count_unique>1) {

		$tmp=$CONFIG['module_name'].'_resetSwapItemsOrder';
		$tmp($table_name,$path_id,$parent,'`order`',$with_path_id);
		
		$tmp=$CONFIG['module_name'].'_getItemsToSwap';
		return $tmp($table_name,$order,$up,$parent);
	}
	//---------------------------------------------------------------------------------------------------------
	
	if ($up)
		$sql="SELECT `id` FROM `".PRFX.$table_name."` WHERE `order`<".$order.$where." ORDER BY `order` DESC LIMIT 1";
	else
		$sql="SELECT `id` FROM `".PRFX.$table_name."` WHERE `order`>".$order.$where." ORDER BY `order` ASC LIMIT 1";

	$id = $DB->getOne($sql);

	return $id;	
}

//-------------------------------------------------------------------------------------------------------------------------------------

function tree_tree_generateConfigValues($tree_id, $parent, $path_id){
	
	global $CONFIG, $DB;

	$tmp=$CONFIG['module_name'].'_getTree';
	$arts=$tmp($CONFIG['tables']['tree']['db_name'], 'name', (int)$path_id); 

	$CONFIG['tables']['tree']['config']['path_id']['caption'] = '';
	$CONFIG['tables']['tree']['config']['path_id']['value'] = $path_id;
	$CONFIG['tables']['tree']['config']['path_id']['type'] = 'hidden';		

	$CONFIG['tables']['tree']['config']['parent']['caption'] = '';
	$CONFIG['tables']['tree']['config']['parent']['value'] = $parent;
	$CONFIG['tables']['tree']['config']['parent']['type'] = 'hidden';		

	if($tree_id>0){
		$list = $DB->getRow("SELECT * FROM `".PRFX.$CONFIG['tables']['tree']['db_name']."` WHERE `id`=".$tree_id);
		foreach($CONFIG['tables']['tree']['config'] as $k => $v)
			if (isset($list[$k]))
				$CONFIG['tables']['tree']['config'][$k]['value'] = $list[$k];
	}

	if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path'])){
		$link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1];
		if (isset($link_path['inmodule']) && $link_path['inmodule']==$CONFIG['module_name']){
			$CONFIG['tables']['tree']['config'][$link_path['to_key']]['value'] =$link_path['from_id'];
		}
	}	

	return $CONFIG;
}

//-------------------------------------------------------------------------------------------------------------------------------------

function tree_deleteTree($id){
	global $DB, $CONFIG;

	$all = $DB->getAll('SELECT `id` FROM `'.PRFX.$CONFIG['tables']['tree']['db_name'].'` WHERE `parent`='.(int)$id);
	if (sizeof($all)){
		foreach ($all as $tree){
			if((int)$tree['id']!=0){
				$tmp=$CONFIG['module_name'].'_deleteTree';
				$tmp($tree['id']);
			}
		}
	}

	$deleteAbstractItem=$CONFIG['module_name'].'_deleteAbstractItem';

	//-----удалим сам раздел------------------
	$deleteAbstractItem('tree',(int)$id);
	
	//---------удалим все позиции в этом разделе----------------
	foreach ($DB->GetAll("SELECT `id` FROM `".PRFX.$CONFIG['tables']['tree']['db_name']."` WHERE `parent`=".$id) as $i)$deleteAbstractItem('items',$i['id']);
}

//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//----------удаление позиции или раздела  с учетом удаления картинок-------------------------------------------------------------------
function tree_deleteAbstractItem($table,$id){
	global $DB, $CONFIG;
	
	foreach ($CONFIG['tables'][$table]['config'] as $fname => $field){
		if($field['type']=='image'){
			$item=$DB->GetRow("SELECT * FROM `".PRFX.$CONFIG['tables'][$table]['db_name']."` WHERE `id`=".(int)$id);

			if(isset($item[$fname])){
				$images=unserialize($item[$fname]);
			
				foreach ($images as $img){
					if($img!='' && file_exists($_SERVER['DOCUMENT_ROOT'].$img))unlink($_SERVER['DOCUMENT_ROOT'].$img);
				}
			}

			
		}
	}
	$DB->execute('DELETE FROM `'.PRFX.$CONFIG['tables'][$table]['db_name'].'` WHERE `id`='.(int)$id);
	
}
//---------------------------------------------------------------------------------------------------
/**
 * создаем приятный вывод дерева разделов в SELECT при добавлении
 */
function tree_getTree($table, $title_field='name', $path_id=0)
	{
	global $vac_out, $DB;

	$tree_array=array();

	function writeTreeArts($table, $title_field, $parent, $level=0)
		{
		global $DB, $tree_array, $path_id;
		
		$sql = "SELECT * FROM `".PRFX.$table."` WHERE `parent`=".$parent." AND `path_id`=".$path_id." ORDER BY `id`";

		$all = $DB->getAll($sql);
		if (sizeof($all))
			{
			foreach ($all as $num => $item)
				{
				$level++;				
				$tree_array[$item["id"]]=str_Repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $level-1).$item[$title_field];
				writeTreeArts($table, $title_field, $item["id"], $level);
				$level--;
				}
			}

		return $tree_array;
		}	

	$tree_array = writeTreeArts($table, $title_field, 0);

	$a = array(0=>"В корне разделов");
	$tree_array=(array)$a + (array)$tree_array;

	return $tree_array;
	}
//---------------------------------------------------------------------------------------------------
?>