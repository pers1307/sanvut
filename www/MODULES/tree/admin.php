<?php

header('Content-type: text/html; charset=utf-8');
global $CONFIG, $adminmenu, $DB, $Forms;
ob_clean();

global $CONFIG;
loadConfig();

$output = blockOnPage($CONFIG['module_name']);
$output_id = ($output>0 ? 'fast_table_'.$CONFIG['module_name'] : 'center');

list($tree_isorder,$CONFIG['tables']['tree'])=moduleOrderField($CONFIG['tables']['tree']);
//$tree_isorder=true;

checkModuleIntegrity();

switch($adminmenu->params[1]){
	
	case 'config':
		ob_clean();
		$config = $_MODULES->by_dir($adminmenu->params[0]);
		global $_ZONES;
		$config['config'] = array();
		foreach ($_ZONES as $_zone){
			$config['config']['mod_'.$_zone['value']] = array(
				'caption' => $_zone['value'],
				'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
				'module'=>$adminmenu->params[0],
				'zone'=>$_zone['value'],
				'type' => 'explorer',
				);
		}
		
		$vars['_FORM_'] = $Forms->make($config['config']);
		$vars['mod'] = $adminmenu->params[0];
		
		die (template('module_config',$vars));
    break;	

	case 'fastview':
		$path_id = isset($adminmenu->params[2]) ? $adminmenu->params[2] : 0;
		$sel_page = isset($adminmenu->params[3]) ? $adminmenu->params[3] : 0;

		$tmp=$CONFIG['module_name'].'_tree_generateVars';
		$tree_vars = $tmp();

		$vars = array();
		$vars['CONFIG']		= $CONFIG; 
		$vars['DB']			= $DB; 
		$vars['Forms']		= $Forms; 
		$vars['path_id']	= $path_id;
		$vars['page']		= 1;
		$vars['tree']		= template('tree', $tree_vars);

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");		
		$_RESULT = array('content' => template('fast',$vars));
	break;


	case 'swap_tree':
		$path_id = isset($adminmenu->params[2]) ? $adminmenu->params[2] : 0;
		$action = (isset($adminmenu->params[3]) && $adminmenu->params[3] == 'up') ? 1:0;
		$tree_id = isset($adminmenu->params[4]) ? $adminmenu->params[4] : 0;

		$tmp=$CONFIG['module_name'].'_setSwapItemsOrder';
		$tmp($CONFIG['tables']['tree']['db_name'], $tree_id, $action);	
		
		$tmp=$CONFIG['module_name'].'_tree_generateVars';
		$tree_vars = $tmp();

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$_RESULT = array('content' => template('tree',$tree_vars));
	break;

	case 'toggle_vis_tree':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$tree_id = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;

		$active = $DB->getOne('SELECT `visible` FROM '.PRFX.$CONFIG['tables']['tree']['db_name'].' WHERE id='.(int)$tree_id);

		$DB->execute('UPDATE '.PRFX.$CONFIG['tables']['tree']['db_name'].' SET `visible`='.(int)!(boolean)$active.' WHERE id='.(int)$tree_id);

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$_RESULT = array('content' => '<img border="0" src="/DESIGN/ADMIN/images/'.((int)!(boolean)$active==0 ? 'invis' : 'vis').'.gif" alt=""/>');
	break;

	case 'add':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$tree_id = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$parent = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
		$level = (isset($adminmenu->params[5]))?(int)$adminmenu->params[5] : 0;
	
		$tmp=$CONFIG['module_name'].'_tree_generateConfigValues';
		$CONFIG = $tmp($tree_id, $parent,$path_id);

		if (isset($_REQUEST['conf'])) {

			$tmp = $CONFIG['module_name'].'_tree_saveItem';

			if ( $inserted_id = $tmp(false) )
				{
				
				$tmp=$CONFIG['module_name'].'_tree_generateVars';
				$tree_vars = $tmp();
				
				$tree_vars['apply'] = isset($adminmenu->params[6]) && $adminmenu->params[6] > 0 ? 1 : 0;
				
				UseModule('ajax');
				$JsHttpRequest = new JsHttpRequest("utf-8");
				
				$inserted_id = '<input id="inserted_id" type="hidden" value="'.$inserted_id.'" name="id"/>';
				
				$_RESULT = array('content' => array(template('tree', $tree_vars), $inserted_id));
			
				}
			else
				{
				
				$JsHttpRequest = new JsHttpRequest("utf-8");
				
				echo '<i style="display:none">Fatal error: </i>Введенный "Символьный код" уже занят';
				
				/**
				 * TODO: Сейчас на все ошибки одна причина, исправить :)
				 */
				
				}
			
		} else {
			
			foreach( $CONFIG['tables']['tree']['config'] as $k => $v ){
				
				$v['where'] = empty($v['where']) ? 0 : $v['where'];

				switch( $v['where'] ){
					
					case 0:
						# code...
					break;

					case 1:
						if( $level==$CONFIG['tables']['tree']['max_levels']-1 )
							unset($CONFIG['tables']['tree']['config'][$k]);
					break;

					case 2:
					
						if( $level<$CONFIG['tables']['tree']['max_levels']-1 )
							unset($CONFIG['tables']['tree']['config'][$k]);
							
					break;
						
				}
				
			}
			
			$vars['_FORM_'] = $Forms->make($CONFIG['tables']['tree']['config']);
			$vars['CONFIG'] = $CONFIG;

			$vars['id'] = (int)$tree_id;
			$vars['path_id'] = (int)$path_id;
			$vars['output_id'] = $output_id;

			echo template('add',$vars);
		}
	break;

	case 'del_tree':
		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$parent = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		
		if ($parent != 0) {
			$tmp=$CONFIG['module_name'].'_deleteTree';
			$tmp($parent);
		}
		
		$tmp=$CONFIG['module_name'].'_tree_generateVars';
		$tree_vars = $tmp();

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$_RESULT = array('content' => template('tree',$tree_vars));
	break;

}  

die();	

?>		