<?php

$module_name='tree';
$module_caption = 'Древовидная лента';

$CONFIG = array (

	'module_name'  => $module_name,
	'module_caption' => $module_caption,
	'fastcall' => '/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'  => '1.0',

	'tables'=>array(
	
		'tree' => array (

			'db_name'=>$module_name,
			'dialog' => array('width'=>600,'height'=>200),
			'dialog_last' => array('width'=>710,'height'=>440), // размеры окна редактирования в конечном разделе
			//'order_field' => '`name`', // если ордер не указан то сортировка будет вручную			
			'add_new_on_top' => false, // указывает куда добавлять новые элементы, вверх или вниз по сортировке
			'max_levels' => 2, // масимальное количество уровней вложенности
			'title_field' => 'name', // поле, которое будет выводиться в списке
			'is_order' => true,
			

			'config' => array(

				/*
				В этом модуле используется особый параметр "where"
					where = 0 : Поле используется во всех разделах
					where = 1 : Поле используется во всех разделах, кроме конечного
					where = 2 : Поле используется только в конечном разделе
				*/

				'name' => array(
					'caption' =>		'Название',
					'value' =>			'',
					'type' =>			'string',
					'where' =>			0,
					'to_code' => true,
					),	

				'code' => array(
					'type' => 'code',
					'caption' => 'Символьный код',
					'in_list' => 1,
					'value' => '',
					),
					
				'title_page' => array(
					'caption' =>		'Заголовок страницы (TITLE)',
					'value' =>			'',
					'type' =>			'string',
					'where' =>			2,
					),	

				'comment' => array(
					'caption' =>		'Описание',
					'value' =>			'',
					'height' =>			'50',
					'type' =>			'memo',
					'where' =>			1,
					),

				'text' => array(
					'caption' =>		'Текст',
					'value' =>			'',
					'type' =>			'wysiwyg',
					'where' =>			2,
					),

			),
			
		),

	),	 

);

?>
