<fieldset style="margin-bottom: 5px;">

<?php
global $auth_isROOT;
$d_tree_width = $CONFIG['tables']['tree']['dialog']['width'];
$d_tree_height = $CONFIG['tables']['tree']['dialog']['height'];

echo $Forms->writeModule_LinksPath();
?>

<table cellpadding="0" cellspacing="0" border="0" align="center" class="catalog_table">
	<tr>
		<th style="border-right:0">

			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="no_border">
				<tr>
					<td style="color: white;"><b></b></td>
					<td width="32">

						<?if ($auth_isROOT || auth_Access($CONFIG['module_name'], 'add')){?>
					
							<a href="#" onclick="displayMessage('/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add/<?=(int)$path_id?>/0/', <?=$d_tree_width;?>, <?=$d_tree_height;?>)"><img src="/DESIGN/ADMIN/images/new_page.gif" width="14" height="14" border="0" alt="Добавить раздел" title="Добавить раздел"/></a>

						<?}?>

					</td>
				</tr>
			</table>
		
		</th>
	</tr>
	<tr>
		<td class="tree" id="tree">

			<?=$tree;?>
			
		</td>
	</tr>
</table>

</fieldset>
