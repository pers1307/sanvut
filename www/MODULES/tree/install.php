<?php

$config_module = $_SERVER['DOCUMENT_ROOT'].'/MODULES/'.$module['module_name'].'/config.php';

if( is_file($config_module) ){
	
	$DB->moduleType = 'catalog';
	
	include($config_module);

	if (isset($CONFIG['tables']['tree'])){
		
		$CONFIG['tables']['tree']['config']['visible'] = array( 'caption' => 'visible', 'value' => 0, 'type' => 'boolean', );
		
		$DB->createModuleTable($CONFIG['tables']['tree']);
		
	}

	unset($CONFIG);
	unset($module_name);
	
}

?>