<?
//     выбирает все письма $cat_id категории
function mailer_forsendByCat( $cat_id=0 )
{
     global $DB;
     if( $cat_id )
     {
          $mailers = $DB->getAll( 'SELECT * FROM '.PRFX.'mailer WHERE CONCAT( ",",cat,"," ) LIKE "%,'.(int)$cat_id.',%"' );

          $to_mail = array();
          foreach( $mailers as $num => $mailer )
               $to_mail[] = $mailer['mail'];

          return implode( ',', $to_mail );
     }
     else
          return false;
}
?>