<?php
header('Content-type: text/html; charset=utf-8');
global $CONFIG, $adminmenu, $DB, $Forms, $auth_isROOT;
ob_clean();

global $CONFIG;
loadConfig();	   

$table_name = $CONFIG['tables']['items']['db_name'];
list($isorder,$CONFIG['tables']['items'])=moduleOrderField($CONFIG['tables']['items']);
$key_field = $CONFIG['tables']['items']['key_field'];

list($output_id) = prepareLinkPath($CONFIG);

checkModuleIntegrity();

switch($adminmenu->params[1])
	{	
	case 'config':
		{
		ob_clean();

		$config = $_MODULES->by_dir($adminmenu->params[0]);

		global $_ZONES;

		$config['config'] = array();

		foreach ($_ZONES as $_zone)
			{
			$config['config']['mod_'.$_zone['value']] = array(
				'caption' => $_zone['value'],
				'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
				'module'=>$adminmenu->params[0],
				'zone'=>$_zone['value'],
				'type' => 'explorer',
				);		
			}	

		$vars['_FORM_'] = $Forms->make($config['config']);
		$vars['mod'] = $adminmenu->params[0];
		

		die (template('module_config',$vars));
		}
	break;	



	case 'swap':
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 0;
		$item_id= isset($adminmenu->params[4]) && is_numeric($adminmenu->params[4]) ? $adminmenu->params[4] : 0;
		$action = (isset($adminmenu->params[5]) && $adminmenu->params[5] == 'up') ? 1:0;

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");		
		
		/* ACTION */
		setSwapItemsOrder($table_name, $item_id, $action);		
		$vars=generateVars();
		/* ACTION */

		$_RESULT = array('content' => template('fast',$vars)); 
		}
	break;



	case 'filter':
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 1;

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");		

		/* ACTION */	
		$_SESSION['filters'][$table_name] = serialize($_REQUEST['filters']);
		$vars=generateVars();
		/* ACTION */

		$_RESULT = array('content' => template('fast',$vars)); 
		}
	break;


	case 'clear_filter':

		clearFilters();	


	case 'fastview': 
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 1;

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");		

		/* ACTION */
		$vars=generateVars();
		/* ACTION */


		$_RESULT = array('content' => template('fast',$vars)); 
		}
	break;



	case 'reset_order':
		{
		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");	

		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$art_id= isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 0;

		/* ACTION */
		resetSwapItemsOrder($table_name); 
		$vars=generateVars();
		/* ACTION */

		$_RESULT = array('content' => template('fast',$vars));
		}
	break;




	case 'add':
		{			
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$new_item_id = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;

		/* ACTION */	  
		$CONFIG = generateConfigValues($new_item_id);

		if (isset($_REQUEST['conf']))
			{	
			UseModule('ajax');
			$JsHttpRequest = new JsHttpRequest("utf-8");

			saveItem();			
			list($isorder, $CONFIG['tables']['items'])=moduleOrderField($CONFIG['tables']['items']);			
			$vars=generateVars();


			$_RESULT = array('content' => template('fast',$vars));
			}
		else
			{
			$vars['CONFIG']		= $CONFIG;
			$vars['Forms']		= $Forms;

			$vars[$key_field]	= (int)$new_item_id;
			$vars['path_id']	= (int)$path_id;
			$vars['page']		= $page;	   
			$vars['output_id']	= $output_id;
			$vars['isorder']	= $isorder;

			$vars['_FORM_']		= $Forms->make($CONFIG['tables']['items']['config']);

			echo template('add',$vars);
			}
		/* ACTION */

		die();
		}
	break;	


	case 'delete':
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page= (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$id = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
		
		/* ACTION */
		if($id>0)
			{
			UseModule('ajax');
			$JsHttpRequest = new JsHttpRequest("utf-8");

			$DB->execute("DELETE FROM `".PRFX.$table_name."` WHERE `".$key_field."`=".$id);
			$vars=generateVars();

			$_RESULT = array('content' => template('fast',$vars));
			}
		/* ACTION */		

		exit;		
		}
	break;
}

die();
?>