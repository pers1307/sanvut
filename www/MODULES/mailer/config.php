<?php
/** 
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name='mailer';
$module_caption = 'Адресаты отправки';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',


	'tables'=>array( 

		'items'=>array(	  

			'db_name' => $module_name,				   
			'dialog'=> array('width'=>400,'height'=>310),
			'key_field'=>'id',						   
			'onpage' =>	20,	

			'config'=>	array(
				'name' => array(
							'caption' => 'ФИО',
							'value' => '',
							'type' => 'string',
							'oblig'=> 1,
							'in_list' => 1,
							),
		
				'mail' => array(
							'caption' => 'Электронная почта',
							'value' => '',
							'type' => 'string',
							'oblig'=> 1,
							'in_list' => 1,
							),
							
							
				'cat' => array(
							'caption' => 'Категория',
							//'value' => '',
							'values' => array( 1=>'Заказать звонок', 2=>'Запрос стоимости товара',3=>'Запрос прайса' ),
							'type' => 'group_checkbox',
							'in_list' => 1,
							),
			),
		),
	)
);
?>
