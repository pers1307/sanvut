<?
$log_file = true;
$log_config = array(
	'edit' => $module_caption.'. Внесение изменений',
	'add' => $module_caption.'. Добавление позиции.',
	'delete' => $module_caption.'. Удаление позиции',
	'swap_up' => $module_caption.'. Перемещение вверх',
	'swap_down' => $module_caption.'. Перемещение вниз',
	'reset_order' => $module_caption.'. Сброшена сортировка'
);
?>