<?php

$module_name='redirects';
$module_caption = 'Перенаправления';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,
			'dialog'=> array('width'=>610,'height'=>260),
			'key_field'=>'id',
			'onpage' =>	50,

			'config'=>	array(

				'type' => array(
					'caption' => 'Тип',
					'value' => '301',
					'type' => 'select',
					'in_list' => 0,
					'values' => array(
						301 => '301',
						//302 => '302',
						//303 => '303',
						//307 => '307',
						),
					'filter'=>1,
					),

				'from' => array(
					'caption' => 'Старый URL',
					'value' => '',
					'type' => 'string',
					'in_list' => 1,
					'filter'=>1,
					),

				'to' => array(
						'caption' => 'Новый URL',
						'value' => '',
						'type' => 'string',
						'in_list' => 1,
						'filter'=>1,
						),

				'active' => array(
						'caption' => 'Активно',
						'value' => 1,
						'type' => 'boolean',
						'in_list'=>1,
						'filter'=>1,
						),
						
			),
		),
	),
);
?>
