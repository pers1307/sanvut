<?php
/*
СПИСОКОВЫЙ МОДУЛЬ, используется для создания на его основе модулей новости, статьи, faq и т.д.
 */
header('Content-type: text/html; charset=utf-8');
global $CONFIG, $adminmenu, $DB, $Forms;
ob_clean();

global $CONFIG;
loadConfig();	   

/* Подкотовительные работы для модуля */
$table_name = $CONFIG['tables']['items']['db_name'];
$key_field = $CONFIG['tables']['items']['key_field'];

if(!isset($log_file)) $log_file = false; // Проверка, есть ли файл с фразами для логирования (log.php)

list($isorder,$CONFIG['tables']['items'])=moduleOrderField($CONFIG['tables']['items']); // проверка на наличия поля сортировки order
list($output_id) = prepareLinkPath($CONFIG);

checkModuleIntegrity();

/* Начало работы модуля - действия, реакции */
switch($adminmenu->params[1])
	{	
	case 'config':
		{
		ob_clean();

		$config = $_MODULES->by_dir($adminmenu->params[0]);

		global $_ZONES;

		$config['config'] = array();

		foreach ($_ZONES as $_zone)
			{
			$config['config']['mod_'.$_zone['value']] = array(
				'caption' => $_zone['value'],
				'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
				'module'=>$adminmenu->params[0],
				'zone'=>$_zone['value'],
				'type' => 'explorer',
				);		
			}	

		$vars['_FORM_'] = $Forms->make($config['config']);
		$vars['mod'] = $adminmenu->params[0];

		die (template('module_config',$vars));
		}
	break;	

	case 'swap':
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 0;
		$item_id= isset($adminmenu->params[4]) && is_numeric($adminmenu->params[4]) ? $adminmenu->params[4] : 0;
		$action = (isset($adminmenu->params[5]) && $adminmenu->params[5] == 'up') ? 1:0;

		/* Логирование событий */
		/*$title = $DB->getOne("select `".(isset($CONFIG['tables']['items']['log_title_field']) ? $CONFIG['tables']['items']['log_title_field'] : 'title')."` from `".PRFX.$table_name."` where id=".$item_id);
		if ($action > 0) {					/* Логирование событий - "Сдвиг вверх" * /
			if($log_file && isset($log_config['swap_up']) && $log_config['swap_up'] != '')
				$log_action = $log_config['swap_up'].' "'.$title.'"';
		}else{ 								/* Логирование событий - "Сдвиг вниз" * /
			if($log_file && isset($log_config['swap_down']) && $log_config['swap_down'] != '')
				$log_action = $log_config['swap_down'].' "'.$title.'"';
		}*/

		setSwapItemsOrder($table_name, $item_id, $action);		
		$vars=generateVars();

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");		
		$_RESULT = array('content' => template('fast',$vars)); 
		}
	break;

	case 'filter':
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 1;
	
		$_SESSION['filters'][$table_name] = serialize($_REQUEST['filters']);
		$vars=generateVars();

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");		
		$_RESULT = array('content' => template('fast',$vars)); 
		}
	break;

	case 'clear_filter':
		clearFilters();

	case 'fastview': 
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 1;

		$vars=generateVars();

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");		
		$_RESULT = array('content' => template('fast',$vars)); 
		}
	break;

	case 'reset_order':
		{
	

		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$art_id= isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 0;

		/* ACTION */

		/* Логирование событий - сброс сортировки */
		//if($log_file && isset($log_config['reset_order']) && $log_config['reset_order'] != '')
			//$log_action = $log_config['reset_order'];

		resetSwapItemsOrder($table_name); 
		$vars=generateVars();
		/* ACTION */

		UseModule('ajax');
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$_RESULT = array('content' => template('fast',$vars));
		}
	break;

	case 'add':
		{			
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$new_item_id = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;

		/* ACTION */
		$OUT_CONFIG = $CONFIG;
		$CONFIG = generateConfigValues($new_item_id);

		if (isset($_REQUEST['conf']))
			{

			/*if ((int)$_REQUEST[$key_field] > 0)
			{
				/* Логирование событий - "Редактирование" * /
				if($log_file && isset($log_config['edit']) && $log_config['edit'] != '')
				{
					$log_action = $log_config['edit'].' "'.$_REQUEST['conf'][1]['title'].'"';
				}
				/* ====================================== * /

			}else{
				/* Логирование событий - "Добавление" * /
				if($log_file && isset($log_config['add']) && $log_config['add'] != '')
				{
					$log_action = $log_config['add'].' "'.$_REQUEST['conf'][1]['title'].'"';
				}
				
			}*/

			$inserted_id = saveItem();
			$CONFIG = $OUT_CONFIG;
			$vars=generateVars();
			UseModule('ajax');
			$JsHttpRequest = new JsHttpRequest("utf-8");
			$inserted_id = '<input id="inserted_id" type="hidden" value="'.$inserted_id.'" name="id"/>';
			$_RESULT = array('content' => array(template('fast', $vars), $inserted_id));
			}
		else
			{
			$vars['CONFIG']		= $CONFIG;
			$vars['Forms']		= $Forms;

			$vars[$key_field]	= (int)$new_item_id;
			$vars['path_id']	= (int)$path_id;
			$vars['page']		= $page;	   
			$vars['output_id']	= $output_id;
			$vars['isorder']	= $isorder;

			$vars['_FORM_']		= $Forms->make($CONFIG['tables']['items']['config']);

			echo template('add',$vars);
			}
		/* ACTION */

		die();
		}
	break;	

	case 'delete':
		{
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page= (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$id = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
		
		/* ACTION */
		if($id>0)
			{
			UseModule('ajax');
			$JsHttpRequest = new JsHttpRequest("utf-8");
			
			/*if($log_file && isset($log_config['delete']) && $log_config['delete'] != '') { /* Логирование событий - "Удаление" * /
				$title = $DB->getOne("select `".(isset($CONFIG['tables']['items']['log_title_field']) ? $CONFIG['tables']['items']['log_title_field'] : 'title')."` from `".PRFX.$table_name."` where id=".$id);
				$log_action = $log_config['delete'].' "'.$title.'"';
			}*/

			$DB->execute("DELETE FROM `".PRFX.$table_name."` WHERE `".$key_field."`=".$id);
			$vars=generateVars();
			$_RESULT = array('content' => template('fast',$vars));
			}
		/* ACTION */		

		exit;		
		}
	break;
}

die();
?>