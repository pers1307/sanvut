
<fieldset style="margin-bottom: 5px;">

<?php
global $auth_isROOT;
$d_articles_width = $CONFIG['tables']['articles']['dialog']['width'];
$d_articles_height = $CONFIG['tables']['articles']['dialog']['height'];

echo $Forms->writeModule_LinksPath();
?>

<table cellpadding="0" cellspacing="0" border="0" align="center" class="catalog_table">
	<tr>
		<th>

			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="no_border">
				<tr>
					<td style="color: white;"><b>Разделы</b></td>
					<td width="32">

						<?if ($auth_isROOT || auth_Access($CONFIG['module_name'], 'add')){?>
					
							<a href="#" onclick="displayMessage('/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add_article/<?=(int)$path_id?>/0/', <?=$d_articles_width;?>, <?=$d_articles_height;?>)"><img src="/DESIGN/ADMIN/images/new_page.gif" width="14" height="14" border="0" alt="Добавить раздел" title="Добавить раздел"/></a>

						<?}?>

					</td>
				</tr>
			</table>
		
		</th>
		<th id="items_caption" align="left">Позиции раздела</th>
	</tr>
	<tr>
		<td width="317" class="tree" id="articles">

			<?=$articles;?>
			
		</td>
		<td valign="top" class="catalog_content" id="items">
		
			Не выбран раздел каталога

		</td>
	</tr>
</table>

</fieldset>
