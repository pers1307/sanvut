<?php
/*
родительский элемент и у разделов и у элементов называется parent
управление видимостью - поле visible (только у разделов)
для элементов управление видимостью можно сделать отдельным полем
*/


$module_name='catalog2';
$module_caption = 'Реализованные проекты';

$CONFIG = array (

	'module_name'  => $module_name,
	'module_caption' => $module_caption,
	'fastcall' => '/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'  => '2.0.0.0',

	'tables'=>array(
		/*
			Названия таблиц "articles" и "items" обязательны, эти имена используются в различных функциях
		*/
	
	
		//------------------------------Разделы-------------------------------------------------------
		'articles' => array (

			'db_name'=>$module_name.'_articles',
			'dialog' => array ('width'=>600,'height'=>440),
			'onpage'=>20,
			
			//------если ордер не указан то сортировка будет вручную
//			'order_field' => '`name`',
			
			//---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
			'add_new_on_top' => false,
			
			//------масимальное количество уровней вложенности----------
			'max_levels' => 2,

			//------поле, которое будет выводиться в списке
			'title_field' => 'name',
			'is_order' => true,

			'config' => array(
				'name' => array(
					'caption' =>		'Название',
					'value' =>			'',
					'type' =>			'string',
					'to_code' => true,
				),
				'code' => array(
					'type' => 'code',
					'caption' => 'URL',
					'value' => '',
				),
				'ind' => array(
					'caption' => 'Выводить на главную',
					'value' => 0,
					'type' => 'boolean',
					//'level' => array( 0 ),
				),
				'img' => array(
					'caption' => 'Изображение',
					'value' => '',
					'type' => 'image',
					'thumbs' => array(
						0 => array( 150, 150),
						1 => array( 260, 185),
					),
					//'level' => array( 0 ),
				),
                'img_mark' => array(
                    'caption' => 'Применен водный знак',
                    'value' => 1,
                    'type' => 'boolean',
                ),
				'text' => array(
					'caption' =>		'Текст',
					'value' =>			'',
					'type' =>			'wysiwyg',
				),
                'h1' => array(
                    'caption' => 'H1',
                    'value' => '',
                    'type' => 'string',
                ),
                'title_page' => array(
                    'caption' => 'Page title',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_description' => array(
                    'caption' => 'Meta description',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_keywords' => array(
                    'caption' => 'Meta keywords',
                    'value' => '',
                    'type' => 'string',
                )
			),
		),
		//------------------------------Разделы-------------------------------------------------------

		//------------------------------Товары-------------------------------------------------------
		'items' => array (

			'db_name'=>$module_name.'_items',
			'dialog' => array ('width'=>720,'height'=>500),
			'onpage'=>20,
			//------если ордер не указан то сортировка будет вручную
//			'order_field' => '`title`',

			//---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
			'add_new_on_top' => false,

			'config' => array (
			
				'title' => array(
					'caption' => 'Заголовок',
					'value' => '',
					'type' => 'string',
					'in_list'=>1,
					'filter'=>1,
					'to_code' => true,
					),
					
				'code' => array(
					'type' => 'code',
					'caption' => 'URL',
					'value' => '',
					),
					
				'img' => array(
					'caption' => 'Изображение',
					'value' => '',
					'type' => 'image',
					'thumbs' => array( 0=>array( 200, 150) ),
					'in_list'=>1,
					),
                'img_mark' => array(
                    'caption' => 'Применен водный знак',
                    'value' => 1,
                    'type' => 'boolean',
                ),
				'text' => array(
					'caption' =>		'Текст',
					'value' =>			'',
					'type' =>			'wysiwyg',
					),
				'active' => array(
					'caption' => 'Опубликовано',
					'value' => 1,
					'type' => 'boolean',
					'in_list'=>1,
					'filter'=>1,
					),
				'gallery' => array(
					'caption' => 'Галерея',
					'value' => '',
					'type' => 'file_group',
					'thumbs' => array(
						0 => array(200, 150),
						1 => array(0, 0)
					),
					'in_list'=>1,
					'width' => '650',
				),
                'marck' => array(
                    'caption' => 'Применен водный знак',
                    'value' => 1,
                    'type' => 'boolean',
                ),
                'catLinks'=>array(
					'caption' => 'Ссылки на каталог',
					//'value' => '',
                    'from' => array(
                		'table_name' => 'catalog_articles',
                		'key_field' => 'id',
                		'parent_field' => 'parent',
                		'name_field' => 'name',
                		'where' => '',
                		'order' => 'name'
                	),
					'type' => 'group_checkbox_tree',
					'in_list' => 0,
				),
                'h1' => array(
                    'caption' => 'H1',
                    'value' => '',
                    'type' => 'string',
                ),
                'title_page' => array(
                    'caption' => 'Page title',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_description' => array(
                    'caption' => 'Meta description',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_keywords' => array(
                    'caption' => 'Meta keywords',
                    'value' => '',
                    'type' => 'string',
                )
			),
		),
		//------------------------------Товары-------------------------------------------------------
	),
	);
?>
