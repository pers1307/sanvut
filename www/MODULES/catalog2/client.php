<?
function catalog2_articles_generateVars(){		
	global $Forms, $DB, $CONFIG, $path_id, $articles_isorder;

	$filter='';
	if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path'])){
		$link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1];
		if (isset($link_path['inmodule']) && $link_path['inmodule']==$CONFIG['module_name']){
			$filter='`'.$link_path['to_key'].'`="'.$link_path['from_id'].'"';
		}
	}
	
	if(!isset($CONFIG['tables']['articles']['order_field']))$CONFIG['tables']['articles']['order_field']='`order`';
	
	ob_start();
	$tmp=$CONFIG['module_name'].'_writeCatalogTree';
	$tmp($CONFIG['tables']['articles'], isset($CONFIG['tables']['articles']['lavels_names']) ? $CONFIG['tables']['articles']['lavels_names'] : $CONFIG['tables']['articles']['title_field'], 0, -1, $path_id, $filter , $CONFIG['tables']['articles']['order_field'],'article','items');
	$articles_html = ob_get_clean();
	

	$vars = array();

	$vars['CONFIG']		= $CONFIG; 
	$vars['Forms']		= $Forms; 
	$vars['path_id']	= $path_id;	  
	$vars['articles_html'] = $articles_html;
	
	return $vars;
}

//-------------------------------------------------------------------------------------------------------------------------------------

function catalog2_writeCatalogTree($table_config, $title_field, $pid=0, $level=-1, $path_id=0, $where=1, $order, $article_act='article', $items_act='items'){
	global $DB, $CONFIG, $auth_isROOT;

	if (isset($_COOKIE['catalog'.(int)$path_id.'_sel_childs'])) $cookie = $_COOKIE['catalog'.(int)$path_id.'_sel_childs']; 
	else $cookie='';

	$childs_array_cookie = explode(',',$cookie);
	
	$disabled_lavels = isset($CONFIG['tables']['articles']['disabled_levels']) ? $CONFIG['tables']['articles']['disabled_levels'] : array();

	$all = $DB->getAll($q='SELECT * FROM '.PRFX.$table_config['db_name'].' WHERE `parent`='.$pid.' AND `path_id`='.(int)$path_id.($where!=''?' AND '.$where:'').' ORDER BY '.$order);
	if (sizeof($all)){
		foreach ($all as $num => $info){
			$level++;
			$style = $level*15;

			$chcnt = $DB->getOne('SELECT count(*) FROM '.PRFX.$table_config['db_name'].' WHERE `parent`="'.$info['id'].'" AND `path_id`='.(int)$path_id);
			
			$title_page = htmlspecialchars( $info[ $title_field ], ENT_QUOTES );
			if( strlen( $title_page ) > 30-( $level * 3 ) )
			{
				$title_page_small = substr( $title_page, 0, 30-( $level * 3 ) );
				
				if( strrpos( $title_page_small, ' ' ) !== false )
					$title_page_small =  substr( $title_page_small, 0, strrpos( $title_page_small, ' ' ) );
				
				$title_page_small .= '...';
			}
			else
				$title_page_small = $title_page;
			
			if( !$title_page_small )
				$title_page_small = "<small>-----------------</small>";

			if ($chcnt>0)$collapse = '<a href="#" onclick="show_hide2(\''.$info['id'].'\', \'catalog'.(int)$path_id.'\')"><img src="/DESIGN/ADMIN/images/'.(!in_array($info['id'], $childs_array_cookie) ? 'plus' : 'minus').'.gif" hspace="5" width="10" height="10" border="0" id="catalog'.(int)$path_id.'_colapsik'.$info['id'].'"></a>';
			else $collapse = '<img src="/DESIGN/ADMIN/images/blank.gif" hspace="5" width="10" height="10">';

			$root_alt = '';
			if ($auth_isROOT) $root_alt = 'ID: '.$info['id'].'';

			$id_toggle_vis = 'catalog'.(int)$path_id.'_vis_img_'.$info['id'].'';

			
			$move_up			= "doLoad('', '/".ROOT_PLACE."/".$CONFIG['module_name']."/swap_".$article_act."/".(int)$path_id."/up/".$info['id']."/', '".$article_act."s');";
			$move_down			= 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/swap_'.$article_act.'/'.(int)$path_id.'/down/'.$info['id'].'/\', \''.$article_act.'s\');';
			$visible_article	= 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/toggle_vis_'.$article_act.'/'.(int)$path_id.'/'.(int)$info['id'].'/\', \''.$id_toggle_vis.'\');';
			$show_items			= 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/show_'.$items_act.'/'.(int)$path_id.'/1/'.(int)$info['id'].'/\', \''.$items_act.'\');';
			$add_sub_article	= 'displayMessage(\'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/add_'.$article_act.'/'.(int)$path_id.'/0/'.(int)$info['id'].'/'.($level + 1).'/\', '.$table_config['dialog']['width'].', '.$table_config['dialog']['height'].');';
			$edit_article		= 'displayMessage(\'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/add_'.$article_act.'/'.(int)$path_id.'/'.(int)$info['id'].'/0/'.($level).'/\', '.$table_config['dialog']['width'].', '.$table_config['dialog']['height'].');';
			$del_article		= 'if (confirm(\'Вы действительно хотите удалить раздел &laquo;'.$info[is_array($title_field) ? $title_field[$level] : $title_field].'&raquo;?\')) {doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/del_'.$article_act.'/'.(int)$path_id.'/'.(int)$info['id'].'/\', \''.$article_act.'s\');}';
			
			
			$btn_vis=($auth_isROOT || auth_Access($CONFIG['module_name'], 'vis'))?'<a href="#" onclick="'.$visible_article.'"><span id="'.$id_toggle_vis.'"><img border="0" src="/DESIGN/ADMIN/images/'.(isset($info['visible']) && $info['visible']==0 ? 'invis' : 'vis').'.gif" alt=""/></span></a>&nbsp;':'';
			
			$btn_move=$order=='`order`' && ($auth_isROOT || auth_Access($CONFIG['module_name'], 'move'))?'
					<a href="#" onclick="'.$move_up.'"><img src="/DESIGN/ADMIN/images/up_page.gif" width="8" height="14" border="0" alt="Перенести раздел вверх" title="Перенести раздел вверх"/></a>
					<a href="#" onclick="'.$move_down.'"><img src="/DESIGN/ADMIN/images/down_page.gif" width="8" height="14" border="0" alt="Перенести раздел вниз" title="Перенести раздел вниз"/></a>
				':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			
			$btn_edit=$auth_isROOT || auth_Access($CONFIG['module_name'], 'edit') ?'<a id="node'.$info['id'].'" href="#" onclick="'.$edit_article.'" title="Редактировать раздел"><img src="/DESIGN/ADMIN/images/edit_page.gif" width="14" height="14" border="0" alt="Редактировать раздел" title="Редактировать раздел"/></a>':'';
			
			$btn_add=(($auth_isROOT || auth_Access($CONFIG['module_name'], 'add')) && $level<$CONFIG['tables']['articles']['max_levels']-1) ?'<a href="#" onclick="'.$add_sub_article.'"><img src="/DESIGN/ADMIN/images/new_page.gif" width="14" height="14" border="0" alt="Добавить под-раздел" title="Добавить под-раздел"/></a>':'';
			
			$btn_del=$auth_isROOT || auth_Access($CONFIG['module_name'], 'del')?'<a href="#" onclick="'.$del_article.'"><img src="/DESIGN/ADMIN/images/delete_page.gif" width="14" height="14"border="0" alt="Удалить раздел" title="Удалить раздел"/></a>':'';

			echo '	 			
				<table cellspacing="0" cellpadding="0" width="100%" class="main_tree">
					<tr '.($level>0 ? 'class="child_trs"' : '').' style="height: 24px">
						<td style="padding-left: '.$style.'px;">
							'.$collapse.$btn_vis
							.(!in_array($level, $disabled_lavels) ? '<a href="#" onclick="'.$show_items.'" title="'.$root_alt.'" id="catalogArt'.$info['id'].'">'.$title_page_small.'</a>' : $title_page_small).'
						</td>
						<td width="80">
							'.$btn_move.$btn_edit.$btn_add.$btn_del.'
						</td>
					</tr>
				</table>
	
				<div id="catalog'.(int)$path_id.'_childs'.$info['id'].'" '.(($chcnt>0) && !in_array($info['id'], $childs_array_cookie) ? 'style="display: none"' : '').'>
			';

			if ($chcnt>0) {
				$tmp=$CONFIG['module_name'].'_writeCatalogTree';
				$tmp($table_config, $title_field, $info['id'], $level, $path_id, $where, $order, $article_act, $items_act);
			}
			echo '</div>';
			
			$level--;
			}
		}
	}

//-------------------------------------------------------------------------------------------------------------------------------------
	
// Выбор итема для смены позиции-----------------------------------

function catalog2_getItemsToSwap($table_name, $order = 0, $up = true, $parent=0, $with_path_id=true){
	global $DB, $path_id,$CONFIG;

	//--------учитывать или нет path_id, для итемов path_id нет
	if($with_path_id)
		$where = " AND `path_id`=".(int)$path_id." AND `parent`=".$parent;
	else 
		$where = " AND `parent`=".(int)$parent;
		
	//--------если есть повторяющиеся значения поля ордер надо их по порядку выстроить----------------------------
	$count_unique=$DB->GetOne("SELECT COUNT(*) FROM `".PRFX.$table_name."` WHERE `order`=".$order.$where);
	if ($count_unique>1) {

		$tmp=$CONFIG['module_name'].'_resetSwapItemsOrder';
		$tmp($table_name,$path_id,$parent,'`order`',$with_path_id);
		
		$tmp=$CONFIG['module_name'].'_getItemsToSwap';
		return $tmp($table_name,$order,$up,$parent);
	}
	//---------------------------------------------------------------------------------------------------------
	
	if ($up)
		$sql="SELECT `id` FROM `".PRFX.$table_name."` WHERE `order`<".$order.$where." ORDER BY `order` DESC LIMIT 1";
	else
		$sql="SELECT `id` FROM `".PRFX.$table_name."` WHERE `order`>".$order.$where." ORDER BY `order` ASC LIMIT 1";

	$id = $DB->getOne($sql);

	return $id;	
}
//-------------------------------------------------------------------------------------------------------------------------------------

/**
* Смена позиции сортировки
*/
function catalog2_setSwapItemsOrder($table_name, $item_id, $action, $with_path_id=true){
	global $DB, $CONFIG;

	$item = $DB->getRow('SELECT * FROM `'.PRFX.$table_name.'` WHERE `id`='.(int)$item_id);

	$tmp=$CONFIG['module_name'].'_getItemsToSwap';
	$to_swap = $tmp($table_name, $item['order'], $action, $item['parent'], $with_path_id);
	
	if ((int)$to_swap>0){
		$target = $DB->getOne('SELECT `order` FROM `'.PRFX.$table_name.'` WHERE `id`='.(int)$to_swap);

		$sql1 = "UPDATE `".PRFX.$table_name."` SET `order` = ".(int)$target." WHERE `id`=".$item_id;
		$sql2 = "UPDATE `".PRFX.$table_name."` SET `order` = ".(int)$item['order']." WHERE `id`=".(int)$to_swap;

		$DB->execute($sql1);
		$DB->execute($sql2);
	}	
	
}	
//-------------------------------------------------------------------------------------------------------------------------------------

function catalog2_resetSwapItemsOrder($table_name,$path_id,$parent,$order_by='`order`', $with_path_id=true) {
	global $DB;

	//--------учитывать или нет path_id, для итемов path_id нет
	if($with_path_id)
		$where = "`path_id`=".(int)$path_id." AND `parent`=".(int)$parent;
	else 
		$where = "`parent`=".(int)$parent;
	
	$all = $DB->getAll('SELECT `id` FROM `'.PRFX.$table_name.'` WHERE '.$where.' ORDER by '.$order_by);
	if (sizeof($all)){
		$order=0;
		foreach ($all as $num => $item){
			$order++;
			$sql='UPDATE `'.PRFX.$table_name.'` SET `order`="'.(int)$order.'" WHERE `id`='.$item['id'];
			$DB->execute($sql,true,($order == 1) ? true : false);
		}
	}  
}
//-------------------------------------------------------------------------------------------------------------------------------------

function catalog2_articles_generateConfigValues($article_id, $parent, $path_id){
	
	global $CONFIG, $DB;

	$tmp=$CONFIG['module_name'].'_getTreeArticles';
	$arts=$tmp($CONFIG['tables']['articles']['db_name'], isset($CONFIG['tables']['articles']['lavels_names']) ? $CONFIG['tables']['articles']['lavels_names'] : $CONFIG['tables']['articles']['title_field'], (int)$path_id); 

	$CONFIG['tables']['articles']['config']['path_id']['caption'] = '';
	$CONFIG['tables']['articles']['config']['path_id']['value'] = $path_id;
	$CONFIG['tables']['articles']['config']['path_id']['type'] = 'hidden';		

	$CONFIG['tables']['articles']['config']['parent']['caption'] = '';
	$CONFIG['tables']['articles']['config']['parent']['value'] = $parent;
	$CONFIG['tables']['articles']['config']['parent']['type'] = 'hidden';		

	if($article_id>0){
		$list = $DB->getRow("SELECT * FROM `".PRFX.$CONFIG['tables']['articles']['db_name']."` WHERE `id`=".$article_id);
		foreach($CONFIG['tables']['articles']['config'] as $k => $v)
			if (isset($list[$k]))
				$CONFIG['tables']['articles']['config'][$k]['value'] = $list[$k];
	}

	if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path'])){
		$link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path'])-1];
		if (isset($link_path['inmodule']) && $link_path['inmodule']==$CONFIG['module_name']){
			$CONFIG['tables']['articles']['config'][$link_path['to_key']]['value'] =$link_path['from_id'];
		}
	}	

	return $CONFIG;
}

//-------------------------------------------------------------------------------------------------------------------------------------

function catalog2_articles_saveItem(){
	global $Forms, $DB, $CONFIG, $table_articles;

	$CONFIG['tables']['articles']['config'] = $Forms->save($CONFIG['tables']['articles']['config']);

	$sql_=array();

	foreach($CONFIG['tables']['articles']['config'] as $field => $info)
		if (isset($info['value']))
			$sql_[] = "`".$field."` = '".$DB->pre($info['value'])."'";
	
	if ($_REQUEST['id']>0){
		$sql = "UPDATE `".PRFX.$table_articles."` SET ".implode(', ',$sql_)." WHERE `id` = ".(int)$_REQUEST['id'];
	} else  {
		
		if(!isset($CONFIG['tables']['articles']['order_field'])){
		
			//-----------уккажем сортировку в зависимости от того что указано в конфиге
			if($CONFIG['tables']['articles']['add_new_on_top']){
				$order=$DB->GetOne("SELECT MIN(`order`) FROM `".PRFX.$table_articles."` WHERE `path_id`=".(int)$CONFIG['tables']['articles']['config']['path_id']['value']." AND `parent`=".(int)$CONFIG['tables']['articles']['config']['parent']['value']);
				$sql_[]="`order`=".((int)$order-1);
			} else {
				$order=$DB->GetOne("SELECT MAX(`order`) FROM `".PRFX.$table_articles."` WHERE `path_id`=".(int)$CONFIG['tables']['articles']['config']['path_id']['value']." AND `parent`=".(int)$CONFIG['tables']['articles']['config']['parent']['value']);
				$sql_[]="`order`=".((int)$order+1);
			}
		}
		
		$sql = "INSERT INTO `".PRFX.$table_articles."` SET ".implode(', ',$sql_);
	}

	$DB->execute($sql);			
	$inserted_id = ($_REQUEST['id'])?intval($_REQUEST['id']):$DB->id;
	return $inserted_id;
}

//-------------------------------------------------------------------------------------------------------------------------------------

function catalog2_deleteArticle($id){
	global $DB, $CONFIG;

	$all = $DB->getAll('SELECT `id` FROM `'.PRFX.$CONFIG['tables']['articles']['db_name'].'` WHERE `parent`='.(int)$id);
	if (sizeof($all)){
		foreach ($all as $article){
			if((int)$article['id']!=0){
				$tmp=$CONFIG['module_name'].'_deleteArticle';
				$tmp($article['id']);
			}
		}
	}

	$deleteAbstractItem=$CONFIG['module_name'].'_deleteAbstractItem';

	//-----удалим сам раздел------------------
	$deleteAbstractItem('articles',(int)$id);
	
	//---------удалим все позиции в этом разделе----------------
	foreach ($DB->GetAll("SELECT `id` FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `parent`=".$id) as $i)$deleteAbstractItem('items',$i['id']);
}

//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------итемы-----------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------

function catalog2_showItems($CONFIG,$parent,$sel_page){
	
	global $DB,$Forms;

	$filter = generateFilterWhereSQL($CONFIG['tables']['items']['db_name']);

	$order=isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : '`order`';
	
	$count_items=$DB->GetOne("SELECT COUNT(*) FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `parent`=".(int)$parent . ( empty($filter) ? '' : ' AND '.$filter ) );
	
	$num_pages=ceil($count_items/$CONFIG['tables']['items']['onpage']);
	$items=$DB->GetAll("SELECT * FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `parent`=".(int)$parent . ( empty($filter) ? '' : ' AND '.$filter ) . " ORDER by ".$order." LIMIT ".($CONFIG['tables']['items']['onpage']*($sel_page-1)).",".$CONFIG['tables']['items']['onpage']);

	$items_vars=array();
	$items_vars['CONFIG']		= $CONFIG; 
	$items_vars['DB']			= $DB; 
	$items_vars['Forms']		= $Forms;

	$items_vars['items']		= $items;
	$items_vars['parent']		= $parent;
	$items_vars['page']			= $sel_page;
	$items_vars['num_pages']	= $num_pages;
	$items_vars['output_id']	= 'items';
	$items_vars['path_id']		= empty($path_id) ? 0 : (int)$path_id;

	$items_vars['pager']		= getPagerTemplate('show_items', 'items', $sel_page, $num_pages, '%%/'.$parent.'');
	
	return $items_vars;
}

//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//----------удаление позиции или раздела  с учетом удаления картинок-------------------------------------------------------------------
function catalog2_deleteAbstractItem($table,$id){
	global $DB, $CONFIG;
	
	foreach ($CONFIG['tables'][$table]['config'] as $fname => $field){
		if($field['type']=='image'){
			$item=$DB->GetRow("SELECT * FROM `".PRFX.$CONFIG['tables'][$table]['db_name']."` WHERE `id`=".(int)$id);

			if(isset($item[$fname])){
				$images=unserialize($item[$fname]);
			
				foreach ($images as $img){
					if($img!='' && file_exists($_SERVER['DOCUMENT_ROOT'].$img))unlink($_SERVER['DOCUMENT_ROOT'].$img);
				}
			}

			
		}
	}
	$DB->execute('DELETE FROM `'.PRFX.$CONFIG['tables'][$table]['db_name'].'` WHERE `id`='.(int)$id);
	
}
//---------------------------------------------------------------------------------------------------
/**
 * создаем приятный вывод дерева разделов в SELECT при добавлении
 */
function catalog2_getTreeArticles($table, $title_field='name', $path_id=0)
	{
	global $vac_out, $DB;

	$tree_array=array();
	
	function writeTreeArts($table, $title_field, $parent, $level=0)
		{
		global $DB, $tree_array, $path_id;
		
		$sql = "SELECT * FROM `".PRFX.$table."` WHERE `parent`=".$parent." AND `path_id`=".$path_id." ORDER BY `id`";

		$all = $DB->getAll($sql);
		if (sizeof($all))
			{
			foreach ($all as $num => $item)
				{
				$level++;
				
				$tree_array[$item["id"]]=str_Repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $level-1).$item[is_array($title_field) ? $title_field[$level-1] : $title_field];
				writeTreeArts($table, $title_field, $item["id"], $level);
				$level--;
				}
			}

		return $tree_array;
		}	

	$tree_array = writeTreeArts($table, $title_field, 0);

	$a = array(0=>"В корне разделов");
	$tree_array=(array)$a + (array)$tree_array;

	return $tree_array;
	}
//---------------------------------------------------------------------------------------------------
?>