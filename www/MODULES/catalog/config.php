<?php
/*
родительский элемент и у разделов и у элементов называется parent
управление видимостью - поле visible (только у разделов)
для элементов управление видимостью можно сделать отдельным полем
*/

$module_name='catalog';
$module_caption = 'Каталог';

$CONFIG = array (

	'module_name'  => $module_name,
	'module_caption' => $module_caption,
	'fastcall' => '/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'  => '2.0.0.0',

	'tables'=>array(
		/*
			Названия таблиц "articles" и "items" обязательны, эти имена используются в различных функциях
		*/

		//------------------------------Разделы-------------------------------------------------------
		'articles' => array (

			'db_name'=>$module_name.'_articles',
			'dialog' => array ('width'=>650,'height'=>400),
			'onpage'=>20,

			//------если ордер не указан то сортировка будет вручную
//			'order_field' => '`name`',

			//---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
			'add_new_on_top' => false,

			//------масимальное количество уровней вложенности----------
			'max_levels' => 3,

			//------поле, которое будет выводиться в списке
			'title_field' => 'name',
			'is_order' => true,

			//	'lavels_names' => array(0 => 'season_title', 1 => 'tournament_title', 2 => 'tour_title'),
			'disabled_levels' => array( 0 ),

			'config' => array(

				'name' => array(
					'caption' =>		'Название',
					'value' =>			'',
					'type' =>			'string',
					'to_code' => true,
				),
				'code' => array(
					'type' => 'code',
					'caption' => 'Символьный код',
					'in_list' => 1,
					'value' => '',
				),
				'img' => array(
					'caption' => 'Изображение',
					'value' => '',
					'type' => 'image',
					'thumbs' => array(
						0 => array(200, 200),
					),
					'level' => array( 0, 1, 2 ),
				),
                'mark' => array(
                    'caption' => 'Применен водный знак',
                    'value' => 1,
                    'type' => 'boolean',
                    'level' => array( 0, 1, 2 ),
                ),
				'ind' => array(
                    'caption' => 'Выводить на главную',
                    'value' => 0,
                    'type' => 'boolean',
//                    'level' => array(0),
                ),
                'footer' => array(
                    'caption' => 'Выводить в подвал сайта',
                    'value' => 0,
                    'type' => 'boolean',
//                    'level' => array(0),
                ),
				'text' => array(
					'caption' =>		'Текст',
					'value' =>			'',
					'type' =>			'wysiwyg',
				),
				'text2' => array(
					'caption' =>		'Текст анонса',
					'value' =>			'',
					'type' =>			'wysiwyg',
				),
                'h1' => array(
                    'caption' => 'H1',
                    'value' => '',
                    'type' => 'string',
                ),
                'title_page' => array(
                    'caption' => 'Page title',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_description' => array(
                    'caption' => 'Meta description',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_keywords' => array(
                    'caption' => 'Meta keywords',
                    'value' => '',
                    'type' => 'string',
                )
			),
			),
		//------------------------------Разделы-------------------------------------------------------

		//------------------------------Товары-------------------------------------------------------
		'items' => array (

			'db_name'=>$module_name.'_items',
			'dialog' => array ('width'=>640,'height'=>500),
			'onpage'=>20,
			//------если ордер не указан то сортировка будет вручную
//			'order_field' => '`title`',

			//---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
			'add_new_on_top' => false,

			'config' => array (

				'title' => array(
					'caption' => 'Заголовок',
					'value' => '',
					'type' => 'string',
					'in_list'=>1,
					'filter'=>1,
					//	'to_code' => true,
				),
					/*
				'code' => array(
					'type' => 'code',
					'caption' => 'Символьный код',
					'value' => '',
					),
					*/
				'img' => array(
					'caption' => 'Изображение 1',
					'value' => '',
					'type' => 'image',
					'thumbs' => array(
						0 => array(300, 300, 0),
						1 => array(200, 200, 0),
                        2 => array(1024, 1024, 0),
					),
					'in_list' => 1,
				),
                'mark' => array(
                    'caption' => 'Применен водный знак',
                    'value' => 1,
                    'type' => 'boolean',
                ),
                'photo' => array(
                    'caption' => 'Изображение 2',
                    'value' => '',
                    'type' => 'image',
                    'thumbs' => array(
                    	0 => array(300, 300, 0),
						2 => array(1024, 1024, 0),
					),
                ),
                'mark_photo' => array(
                    'caption' => 'Применен водный знак',
                    'value' => 1,
                    'type' => 'boolean',
                ),
				'type' => array(
					'caption' => 'Серия',
					'value' => '',
					'type' => 'select',
					'values' => array(
						0=>'',
						1=>'эконом',
						2=>'элит',
						3=>'премиум'
					),
					'in_list'=>1,
					'filter'=>1,
				),
				'width' => array(
					'caption' => 'Ширина',
					'value' => '',
					'type' => 'string',
				),
				'deep' => array(
					'caption' => 'Глубина',
					'value' => '',
					'type' => 'string',
				),
				'height' => array(
					'caption' => 'Высота',
					'value' => '',
					'type' => 'string',
				),

//				'shema1' => array(
//					'caption' => 'Схема',
//					'value' => '',
//					'type' => 'image',
//					'thumbs' => array( 0=>array( 300, 0 ) ),
//				),

				/*
				'shema2' => array(
					'caption' => 'Схема 2',
					'value' => '',
					'type' => 'image',
					'thumbs' => array( 0=>array( 140, 180 ) ),
				),
				*/

				'text' => array(
					'caption' => 'Текст',
					'value' => '',
					'height'=>260,
					'type' => 'wysiwyg',
				),
				'fulltext' => array(
					'caption' => 'Подробное описание',
					'value' => '',
					'height'=>260,
					'type' => 'wysiwyg',
				),
                'h1' => array(
                    'caption' => 'H1',
                    'value' => '',
                    'type' => 'string',
                ),
                'title_page' => array(
                    'caption' => 'Page title',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_description' => array(
                    'caption' => 'Meta description',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_keywords' => array(
                    'caption' => 'Meta keywords',
                    'value' => '',
                    'type' => 'string',
                ),
				'active' => array(
					'caption' => 'Опубликовано',
					'value' => 1,
					'type' => 'boolean',
					'in_list'=>1,
					'filter'=>1,
				),
			),
		),
		//------------------------------Товары-------------------------------------------------------


		),

	);
?>
