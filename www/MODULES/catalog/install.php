<?php
$config_module = $_SERVER['DOCUMENT_ROOT'].'/MODULES/'.$module['module_name'].'/config.php';
if (is_file($config_module))
	{
	
	$DB->moduleType = 'catalog';
	
	include($config_module);

	if (isset($CONFIG['tables']['articles']))
		$DB->createModuleTable($CONFIG['tables']['articles']);
	
	if (isset($CONFIG['tables']['items']))
		$DB->createModuleTable($CONFIG['tables']['items']);

	unset($CONFIG);
	unset($module_name);
	}

?>