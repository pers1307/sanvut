
<?php	
global $auth_isROOT;
$mod_name = $CONFIG['module_name'];
$d_width = (int)$CONFIG['tables']['items']['dialog']['width'];
$d_height = (int)$CONFIG['tables']['items']['dialog']['height'];
$table_config = $CONFIG['tables']['items'];
$order=isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : '`order`';

?>
<div align="center" style="margin-top: 10px;">

	<?if($auth_isROOT || auth_Access($mod_name,'add')){?>

		<a style="float: left" onclick="displayMessage('/<?=ROOT_PLACE;?>/<?=$mod_name;?>/add_item/0/<?=(int)$parent;?>/<?=(int)$page;?>', <?=$d_width;?>, <?=$d_height;?>)" href="#" class="button">Добавить</a>

	<?}?>
	

</div>
<div style="clear:both;height:10px;"></div>
<?
echo $Forms->writeModuleTableStart();
echo $Forms->writeModuleTableHeader($CONFIG['tables']['items'], true);


foreach ($items as $item){
	/**
	* Подготовка конфига для ячейки
	*/
	$item['sub_config']=array(
		'path2item'=>'/'.ROOT_PLACE.'/'.$mod_name.'/%%ACTION%%/'.(int)$parent.'/'.(int)$page.'/'.(int)$item['id'],
		'output_id'=>$output_id
		);

	$ed_click="displayMessage('/".ROOT_PLACE."/".$mod_name."/add_item/0/".(int)$parent."/".(int)$page."/".(int)$item['id']."/', ".(int)$d_width.", ".(int)$d_height.")";

	$del_click="if (confirm('Удалить запись?')) doLoad('','/".ROOT_PLACE."/".$mod_name."/delete_item/0/".(int)$parent."/".(int)$page."/".(int)$item['id']."/','".$output_id."')";					

	$move_up = 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/swap_item/up/'.$item['id'].'/'.$page.'/'.$parent.'/\', \'items\');';
	$move_down = 'doLoad(\'\', \'/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/swap_item/down/'.$item['id'].'/'.$page.'/'.$parent.'/\', \'items\');';
	$btn_move=$order=='`order`' && ($auth_isROOT || auth_Access($CONFIG['module_name'], 'move'))?'
		<a href="#" onclick="'.$move_up.'"><img src="/DESIGN/ADMIN/images/up_page.gif" width="8" height="14" border="0" alt="Перенести вверх" title="Перенести вверх"/></a>
		<a href="#" onclick="'.$move_down.'"><img src="/DESIGN/ADMIN/images/down_page.gif" width="8" height="14" border="0" alt="Перенести вниз" title="Перенести вниз"/></a>
	':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	
	$act_buttons='<div class="act_buts">';

	if ($auth_isROOT || auth_Access($CONFIG['module_name'], 'move')){
		$act_buttons.=$btn_move;
	}
	
	
	$ed_act_dbclick='';
	if ($auth_isROOT || auth_Access($CONFIG['module_name'], 'edit')){
		$act_buttons.=writeSmallEditButton($ed_click);
		$ed_act_dbclick='ondblclick="'.$ed_click.'"';
	}
	
	if ($auth_isROOT || auth_Access($CONFIG['module_name'], 'del'))
		$act_buttons.=writeSmallDeleteButton($del_click);

	$act_buttons.='</div>';
	
	/**
	* Доп. параметры ячейки таблицы
	*/
	$addon_params_tag=$ed_act_dbclick.' onmouseover="this.style.backgroundColor=\'#FBFCE2\';" onmouseout="this.style.backgroundColor=\'white\'"';

	/**
	* Вывод ячейки таблицы
	*/
	echo $Forms->moduleTableCell($table_config, $item, $act_buttons, $addon_params_tag);
}

echo $Forms->writeModuleTableEnd($pager);
?>

<div align="center" style="margin-top: 10px;">

	<?if($auth_isROOT || auth_Access($mod_name,'add')){?>

		<a style="float: left" onclick="displayMessage('/<?=ROOT_PLACE;?>/<?=$mod_name;?>/add_item/<?=(int)$path_id;?>/<?=(int)$parent;?>/<?=(int)$page;?>', <?=$d_width;?>, <?=$d_height;?>)" href="#" class="button">Добавить</a>

	<?}?>
	

</div>

<script type="text/javascript">
	
	/**
	Выделям загруженный раздел
	**/

	var articlesPanel = $('#articles');

	articlesPanel.find('a').removeClass('active');
	articlesPanel.find('a#catalogArt' + <?=$parent?>).addClass('active');

</script>

<script type="text/javascript">
<? if( isset($apply) && !$apply ){ ?>
closeDialog();
<? } ?>
</script>